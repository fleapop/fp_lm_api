<?php

return [
    /**
     * Api Store 카카오 알림톡 설정
     */

    'key'       => env('APISTORE_KEY'),
    'client_id' => env('APISTORE_ID'),
];
