<?php

namespace App\ApiStore;

use App\Models\BlockSms;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

trait SMS
{
    public function sendSMS($phoneNumber, $message)
    {
      try {
        if ($this->checkBlockedPhoneNumber($phoneNumber)) {
          $url = 'https://api.apistore.co.kr/ppurio/2/message/sms/fleapopinc';
          $headers = [
            'Content-Type' => 'application/x-www-form-urlencoded; charset=UTF-8',
            'x-waple-authorization' => env('APISTORE_KEY'),
          ];

          $client = new Client();
          $response = $client->request('POST', $url, [
            'headers' => $headers,
            'form_params' => [
              'send_name' => '플리팝',
              'send_phone' => '07088120820',
              'dest_phone' => $phoneNumber,
              'msg_body' => $message,
            ],
          ]);

          $result = json_decode($response->getBody()->getContents());

          if ($result->result_code !== 200) {
            Log::channel('slack')->alert('[SMS 발송 오류] ' . $result->result_message .
              '(result code: ' . $result->result_code .
              ', cmid: ' . $result->cmid .
              ', phone:' . $phoneNumber .
              ', msg: ' . $message . ')');
          }

          return $result;
        }

        return false;
      } catch (\Exception $e) {
        Log::stack(['single', 'slack'])
          ->alert(
            "[SMS 발송 오류] "
          );
        return false;
      }
    }

    private function checkBlockedPhoneNumber($phone)
    {
        $blocked = BlockSms::whereNull('delete_date')->where('number', $phone)->first();

        if ($blocked) {
            Log::channel('slack')->alert('[SMS 발송 오류] 발송 차단된 번호 (' . $phone . ')');
            return false;
        } else {
            return true;
        }
    }
}
