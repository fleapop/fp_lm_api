<?php

namespace App\ApiStore;

use App\Models\AlimtalkSendLog;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

trait KakaoMessage
{
    private $clientId;
    private $client;
    private $baseUrl;
    private $header;

    /**
     * KakaoMessage constructor.
     */
    public function createClient()
    {
        $this->clientId = config('kakao.client_id');
        $this->client = new Client();
        $this->baseUrl = 'http://api.apistore.co.kr';
        $this->header = [
            'Content-Type'          => 'application/x-www-form-urlencoded; charset=UTF-8',
            'x-waple-authorization' => config('kakao.key'),
        ];
    }

    public function report($cmid)
    {
      try {
        $this->createClient();

        $response = $this->client->request('GET', $this->baseUrl . '/kko/1/report/' . $this->clientId, [
            'headers' => $this->header,
            'query'   => [
                'cmid' => $cmid,
            ],
        ]);

        return $response->getBody();
      } catch (\Exception $e) {
        Log::stack(['single', 'slack'])
          ->alert(
            "[카카오 알림톡 발송 오류] ".$e->getMessage()
          );
        return false;
      }
    }

    /**
     * 카카오 알림톡 메세지 전송
     *
     * @param $phone
     */
    public function send($phone, $message, $templateCode, $failedSubject, $failedMessage, $date = null)
    {
      try {
        $this->createClient();
        $response = $this->client->request('POST', $this->baseUrl . '/kko/1/msg/' . $this->clientId, [
          'headers' => $this->header,
          'form_params' => [
            'phone' => $phone,
            'callback' => '07088120820',
            'msg' => $message,
            'template_code' => $templateCode,
            'failed_type' => 'LMS',
            'failed_subject' => $failedSubject,
            'failed_msg' => $failedMessage,
            'reqdate' => $date,
          ],
        ]);

        $result = json_decode($response->getBody());

        $this->createSendLog($result, $phone, $message, $templateCode);

        if ($result->result_code !== '200') {
          Log::stack(['single', 'slack'])
            ->alert(
              "[카카오 알림톡 발송 오류] $result->result_message(result code:$result->result_code, cmid:$result->cmid,
                    client_id:$this->clientId, phone:$phone, msg:$message, template_code:$templateCode"
            );
        }

        return $result->result_code;
      } catch (\Exception $e) {
        Log::stack(['single', 'slack'])
          ->alert(
            "[카카오 알림톡 발송 오류] ".$e->getMessage()
          );
        return 500;
      }
    }

    /**
     * 등록된 Template 조회
     *
     * @param $template
     * @return mixed
     */
    public function getTemplate($template)
    {
      try {
        $this->createClient();

        $response = $this->client->request('GET', $this->baseUrl . '/kko/1/template/list/' . $this->clientId, [
          'headers' => $this->header,
          'query' => [
            'template_code' => $template,
          ],
        ]);

        $template = json_decode($response->getBody());

        return $template->templateList[0]->template_msg;
      } catch (\Exception $e) {
        Log::stack(['single', 'slack'])
          ->alert(
            "[카카오 알림톡 발송 오류] ".$e->getMessage()
          );
        return false;
      }
    }

    /**
     * 등록된 템플릿으로 카카오 알림톡 전송
     *
     * @param $phone
     * @param $templateCode
     * @return mixed
     */
    public function sendFromTemplate($phone, $templateCode, $failedSubject)
    {
        $template = $this->getTemplate($templateCode);

        return $this->send($phone, $template, $templateCode, $failedSubject, $template);
    }

    /**
     * 템플릿 변수 치환
     *
     * @param       $template
     * @param array $replace
     * @return string|string[]
     */
    public function replaceTemplate($template, array $replace)
    {
      try {
        foreach ($replace as $item) {
            $template = str_replace($item[0], removeEmoji($item[1]), $template);
        }

        return $template;
      } catch (\Exception $e) {
        Log::stack(['single', 'slack'])
        ->alert(
        "[카카오 알림톡 발송 오류] sendWithTemplateReplace ".$e->getMessage()
        );
      }
    }

    /**
     * 템플릿 변수 치환 메세지 전송
     *
     * @param       $phone
     * @param       $templateCode
     * @param       $failedSubject
     * @param array $replace
     * @return mixed
     */
    public function sendWithTemplateReplace($phone, $templateCode, $failedSubject, array $replace)
    {
      try {
        $template = $this->getTemplate($templateCode);
        $template = $this->replaceTemplate($template, $replace);

        return $this->send($phone, $template, $templateCode, $failedSubject, $templateCode);
      } catch (\Exception $e) {
        Log::stack(['single', 'slack'])
          ->alert(
            "[카카오 알림톡 발송 오류] sendWithTemplateReplace ".$e->getMessage()
          );
      }
    }

    /**
     * 카카오 알림톡 전송 로깅
     *
     * @param $result
     * @param $phone
     * @param $message
     * @param $templateCode
     */
    private function createSendLog($result, $phone, $message, $templateCode)
    {
        AlimtalkSendLog::create([
            'phone_number'   => $phone,
            'template_code'  => $templateCode,
            'message'        => $message,
            'result_code'    => $result->result_code,
            'result_message' => $result->result_message,
            'cmid'           => $result->cmid,
        ]);
    }
}
