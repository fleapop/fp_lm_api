<?php


//function encryptPassword($plainText)
//{
//  $encryptText = base64_encode(hash('sha256', $plainText, true));
//  return $encryptText;
//}

function makeToken($idx)
{
  return hash("sha256", $idx . "_fleapop");
}

function checkToken($token, $idx)
{
  $hash = hash("sha256", $idx . "_fleapop");
  if ($token == $hash) {
    return true;
  } else {
    return false;
  }
}

function array_encode(&$arr)
{
  foreach ($arr as $key => $val) {
    $str[] = "$key=$val";
  }
  $string = sizeof($str) > 0 ? implode("|", $str) : '';
  return $string;
}

function array_decode($str)
{
  $arr = array();
  $str_split = explode('|', $str);
  foreach($str_split as $val) {
    $kv = explode('=', $val);
    $arr[$kv[0]] = urldecode($kv[1]);
  }
  return $arr;
}


function insert_array($arr, $idx, $add) {
  $arr_front = array_slice($arr, 0, $idx); //처음부터 해당 인덱스까지 자름
  $arr_end = array_slice($arr, $idx); //해당인덱스 부터 마지막까지 자름
  $arr_front[] = $add;//새 값 추가
  return array_merge($arr_front, $arr_end);
}

function arrayToJson($arr) {
  $temp = json_decode("{}");
  if(isset($arr[0])) $temp = $arr[0];
  return $temp;
}

function getOrderMethodString($method)
{
  switch ($method) {
    case 'card' :
      return '카드';
    case 'vbank':
      return '가상계좌';
    case 'lmpay':
      return '러마페이';
    case 'trans':
      return '계좌이체';
    case 'PAYCO':
      return 'PAYCO';
    case 'samsung':
      return '삼성페이';
    case 'kakaopay' : 
      return '카카오페이';
    default : return '';
  }

}
