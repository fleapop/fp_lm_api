<?php


namespace App\Fleapop;


use Exception;
use Illuminate\Support\Facades\Storage;

trait Content
{
    public function replaceMixedContent($content)
    {
        $pattern = '/(http:\/\/)([a-z0-9\w]+\.*)+[a-z0-9]{2,4}([\/가-힣a-z0-9-%#?&=_ \w\s\(\)])+(\.[a-z0-9]{2,4}(\?[\/a-z0-9-%#?&=\w]+)*)*/';
        $allowExtension = ['jpg', 'jpeg', 'png', 'gif'];

        preg_match_all($pattern, $content, $matches);

        foreach ($matches[0] as $item) {
            $extension = pathinfo($item, PATHINFO_EXTENSION);

            if (in_array($extension, $allowExtension)) {
                try {
                    $file = file_get_contents($item);
                    $path = '/goods_contents/' . getUnique() . time() .  '.' . $extension;
                    Storage::disk('s3')->put($path, $file, 'public');
                    $s3Path = 'https://fps3bucket.s3.ap-northeast-2.amazonaws.com' . $path;
                    $content = str_replace($item, $s3Path, $content, $count);
                } catch (Exception $e) {
                    continue;
                }
            }
        }

        return $content;
    }

    public function replaceMissingHttps($content)
    {
        $content = preg_replace('/src=[\']\/\//', "src='https://", $content);
        $content = preg_replace('/src=[\"]\/\//', 'src="https://', $content);

        return $content;
    }
}
