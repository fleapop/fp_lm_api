<?php


namespace App\Fleapop;

use App\Lib\FPIamport;
use App\Models\LMPay;
use App\Models\OrderDetail;
use App\Models\PaymentCancel;
use App\Seller\Order;
use App\Models\ShippingPrice;
use Exception;
use Illuminate\Support\Facades\Log;

class Payment
{
    static public function cancel($orderDetail, $reason = null)
    {
        if (! in_array($orderDetail->state, ['상품준비', '배송준비', '취소접수'])) {
            throw new Exception('해당 주문건은 취소 불가 상태입니다.');
        }

        $order = $orderDetail->order;
        $leave_amount = [
            'total' => $order->total_price,
            'imp'   => $order->total_price - $order->lmpay_amount,
            'lmpay' => $order->lmpay_amount,
        ];

        $canceledAmount = self::calculateCancelAmountCancel($orderDetail, ['취소완료', '반품완료', '취소접수', '환불접수']);
        $leave_amount['total'] -= $canceledAmount;

        if ($leave_amount['total'] < $orderDetail->total_price) {
            throw new Exception('해당 주문건을 취소할 수 없습니다. (E1)');
        }

        $leave_amount['imp'] = $leave_amount['imp'] - $canceledAmount;

        if ($leave_amount['imp'] < 0) {
            $leave_amount['lmpay'] += $leave_amount['imp'];
            $leave_amount['imp'] = 0;
        }

        $return_amount = ['imp' => 0, 'lmpay' => 0];

        if ($leave_amount['imp'] >= $orderDetail->total_price) {
            $return_amount['imp'] = $orderDetail->total_price;

            // 배송비는 제일 마지막 남은 주문상품 취소 시 같이 취소.
            if ($orderDetail->isLastOrder()) {
                $return_amount['imp'] += ShippingPrice::getByOrderDetail($orderDetail)->sum('price');
            }

        } else {
            $return_amount['imp'] = $leave_amount['imp'];
            $return_amount['lmpay'] = abs($leave_amount['imp'] - $orderDetail->total_price);

            if ($orderDetail->isLastOrder()) {
                $return_amount['lmpay'] += ShippingPrice::getByOrderDetail($orderDetail)->sum('price');
            }
        }

        if ($return_amount['lmpay'] > 0) {
            $objCashLog = LMPay::cancel($orderDetail, $return_amount['lmpay']);

            if (! $objCashLog['success']) {
                throw new Exception($objCashLog['data']['error_msg']);
            }

            $cancelData = [
                'order_id'        => $order->id,
                'order_detail_id' => $orderDetail->id,
                'state'           => '취소완료',
                'amount'          => abs($return_amount['lmpay']),
                'method'          => 'lmpay',
                'imp_uid'         => 'lmpay',
                'merchant_id'     => 'lmpay',
                'bank'            => '',
                'bank_account'    => '',
                'bank_holder'     => '',
                'canceled_at'     => now(),
                'message'         => $reason,
            ];

            PaymentCancel::create($cancelData);
        }

        if ($return_amount['imp'] > 0) {
            self::requestPurchaseCancel($order, $orderDetail, $return_amount, $reason);
        }

        $orderDetail->sku->stock->addStock($orderDetail->quantity);
        $orderDetail->state = '취소완료';
        $orderDetail->save();
    }

    static public function refund($orderDetailId, $refundAmount, $quantity, $reason)
    {
        $detail = Order::find($orderDetailId);
        $detail->replicate();
        $refundAmount = (int)$refundAmount;
        $price = (($detail->total_price / $detail->quantity) * $quantity) - $refundAmount;
        $maxPrice = (($detail->total_price / $detail->quantity) * $quantity);
        $block_states = ['결제대기', '상품준비', '취소완료', '교환접수', '취소대기', '환불대기'];

        if (in_array($detail->state, $block_states)) {
            Log::info('해당 상태는 반품이 불가능한 상태입니다.');
            return response()->json("해당 상태는 반품이 불가능한 상태입니다.", 422);
        }

        if ($detail->quantity < $quantity) {
            Log::info('해당 수량은 반품처리가 불가능합니다.');
            return response()->json("해당 수량은 반품처리가 불가능합니다.", 422);
        }

        if ($refundAmount > $maxPrice) {
            Log::info('반품비는 상품 주문금액을 초과할 수 없습니다.');
            return response()->json("반품비는 상품 주문금액을 초과할 수 없습니다.", 422);
        }

        if ($detail->order->payment->state != "결제완료") {
            Log::info('해당상품은 결제완료 상태가 아닙니다.');
            return response()->json("해당상품은 결제완료 상태가 아닙니다.", 422);
        }

        $cancelCashLog = NULL; // 취소된 cash_log 객체
        $cancelObj_Imp = NULL; // 취소된 payment_cancel IMP 객체
        $cancelObj_Lmpay = NULL; // 취소된 payment_cancel Lmpay 객체
        $cancelObj = NULL; // 취소된 order detail 객체

        if (count($detail->paymentCancel) > 0) {
            Log::info('이미 취소처리된 거래건이 있습니다.');
            return response()->json('이미 취소처리된 거래건이 있습니다.', 422);
        }

        $order = $detail->order;
        $leave_amount = [
            'total' => $order->total_price,
            'imp'   => $order->total_price - $order->lmpay_amount,
            'lmpay' => $order->lmpay_amount,
        ];
        $canceled_amount = self::calculateCancelAmountCancel($detail, ['취소완료', '반품완료', '취소접수', '반품접수', '취소보류', '반품보류']);
        $leave_amount['total'] -= $canceled_amount;

        if ($leave_amount['total'] < $price) {
            Log::info('해당 반품금액을 환불해줄 수 없습니다.');
            return response()->json('해당 반품금액을 환불해줄 수 없습니다.', 422);
        }

        $leave_amount['imp'] = $leave_amount['imp'] - $canceled_amount;

        if ($leave_amount['imp'] < 0) {
            $leave_amount['lmpay'] += $leave_amount['imp'];
            $leave_amount['imp'] = 0;
        }

        $return_amount = ['imp' => 0, 'lmpay' => 0,];

        if ($leave_amount['imp'] >= $price) {
            $return_amount['imp'] = $price;

            if ($detail->isLastOrder()) {
                $return_amount['imp'] += ShippingPrice::getByOrderDetail($detail)->sum('price');
                $return_amount['lmpay'] = $leave_amount['lmpay'];
            }

        } else {
            $return_amount['imp'] = $leave_amount['imp'];
            $return_amount['lmpay'] = abs($leave_amount['imp'] - $price);

            if ($detail->isLastOrder()) {
                $return_amount['lmpay'] += ShippingPrice::getByOrderDetail($detail)->sum('price');
            }
        }

        $order->payment;

        if (($return_amount['imp'] + $return_amount['lmpay']) == 0) {
            PaymentCancel::create([
                'order_id'        => $order->id,
                'order_detail_id' => $detail->id,
                'state'           => '취소완료',
                'amount'          => 0,
                'method'          => 'lmpay',
                'imp_uid'         => 'lmpay',
                'merchant_id'     => 'lmpay',
                'bank'            => '',
                'bank_account'    => '',
                'bank_holder'     => '',
                'canceled_at'     => now(),
                'message'         => $reason,
            ]);
        }

        if ($return_amount['lmpay'] > 0) {
            $objCashLog = LMPay::cancel($detail, $return_amount['lmpay']);

            if (! $objCashLog['success']) {
                throw new Exception($objCashLog['data']['error_msg']);
            }

            PaymentCancel::create([
                'order_id'        => $order->id,
                'order_detail_id' => $detail->id,
                'state'           => '취소완료',
                'amount'          => abs($return_amount['lmpay']),
                'method'          => 'lmpay',
                'imp_uid'         => 'lmpay',
                'merchant_id'     => 'lmpay',
                'bank'            => '',
                'bank_account'    => '',
                'bank_holder'     => '',
                'canceled_at'     => now(),
                'message'         => $reason,
            ]);
        }

        if ($return_amount['imp'] > 0) {
            self::requestPurchaseCancel($order, $detail, $return_amount, $reason);
        }

        return response()->json('반품처리 완료', 200);
    }

    private static function requestPurchaseCancel($order, $detail, $return_amount, $reason)
    {
        $imp = new FPIamport();
        $payment = $order->payment;
        $cancelData = [
            'order_id'        => $order->id,
            'order_detail_id' => $detail->id,
            'state'           => '취소완료',
            'amount'          => abs($return_amount['imp']),
            'method'          => $payment->method,
            'imp_uid'         => $payment->imp_uid,
            'merchant_id'     => $payment->merchant_uid,
            'bank'            => $payment->bank,
            'bank_account'    => $payment->account,
            'bank_holder'     => $payment->holder,
            'message'         => $reason,
            'canceled_at'     => now(),
        ];

        if (in_array($payment->method, ['vbank', 'trans'])) {
            $bankData = [
                'bank'    => $payment->bank,
                'account' => $payment->account,
                'holder'  => $payment->holder,
            ];
            $imp_response = $imp->cancelPayment($cancelData['imp_uid'], $cancelData['amount'], $reason, $bankData);
        } else {
            $imp_response = $imp->cancelPayment($cancelData['imp_uid'], $cancelData['amount'], $reason);
        }

        if ($imp_response->getStatusCode() != 200) {
            throw new Exception($imp_response->original);
        }

        Log::info($imp_response->original);
        PaymentCancel::create($cancelData);
    }

    private static function calculateCancelAmountCancel($orderDetail, $state)
    {
        return OrderDetail::where([
            ['order_id', $orderDetail->order_id],
            ['id', '!=', $orderDetail->id],
        ])->whereIn('state', $state)->sum('total_price');
    }
}
