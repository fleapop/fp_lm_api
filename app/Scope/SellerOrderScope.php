<?php


namespace App\Scope;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class SellerOrderScope implements Scope
{
    /**
     * @inheritDoc
     */
    public function apply(Builder $builder, Model $model)
    {
        @session_start();

        if (!empty($_SESSION['seller'])) {
            $builder->where('seller_id', $_SESSION['seller']['seller_idx'])->whereHas('order', function ($query) {
                $query->whereNull('deleted_at');
            });
        }
    }
}
