<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ShippingPrice extends Model
{
    protected $connection = 'fp_payment_w';
    protected $guarded    = [];

    public function seller()
    {
        $this->belongsTo(Seller::class);
    }

    public function order()
    {
        $this->belongsTo(Order::class);
    }

    public static function persistExtraShippingPrice($orderId)
    {
        $deliveries = OrderDelivery::where('order_id', $orderId)
            ->whereHas('orderDetail', function (Builder $builder) {
                $builder->distinct('seller_id');
            })->with('orderDetail.seller')->get();

        foreach ($deliveries as $item) {
            $price = 0;

            if ($item->orderDetail->goods->use_extra_shipping_price) {
                if (mb_strpos($item->address_1, '제주') !== false) {
                    $price = $item->orderDetail->seller->extra_shipping_price;
                } else if (AddShippingCode::isContained($item->zip_code)) {
                    $price = $item->orderDetail->seller->shipping_price;
                }
            }

            $result = self::where([
                ['order_id', $item->order_id], ['seller_id', $item->orderDetail->seller_id], ['price', $price]]
            )->first();

            if($result == null) {
                self::create([
                    'order_id'  => $item->order_id,
                    'seller_id' => $item->orderDetail->seller_id,
                    'price'    => $price
                ]);
            }
        }
    }

    public function scopeGetByOrderDetail($query, $orderDetail) {
        return $query->where([
            ['order_id', $orderDetail->order_id],
            ['seller_id', $orderDetail->seller_id]
        ]);
    }
}
