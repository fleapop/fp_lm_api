<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsPromotion extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $primaryKey = 'idx';
    protected $guarded    = [];
    protected $appends    = ['fee_rate', 'fee_per_quantity', 'total_support_amount'];

    public function goods()
    {
        return $this->belongsTo(Goods::class);
    }

    public function details()
    {
        return $this->hasManyThrough(OrderDetail::class, Goods::class);
    }

    public function batchHistory()
    {
        return $this->hasOne(PromotionDiscountBatchHistory::class);
    }

    public function getSoldCount($revenueStart, $revenueEnd)
    {
        return OrderDetail::where('goods_id', $this->goods_id)
            ->whereBetween('confirmed_at', [$revenueStart, $revenueEnd])
            ->whereBetween('created_at', [$this->started_at, $this->ended_at])
            ->count();
    }

    public function getFeeRateAttribute()
    {
        $seller = $this->goods->seller_id;
        $conditions = [
            ['seller_id', $seller],
            ['started_at', '<=', $this->started_at],
            ['ended_at', '>=', $this->ended_at],
        ];

        $promotion = PromotionFee::where($conditions)->first();

        if ($promotion) {
            return $promotion->fee_rate;
        }

        $current = Revenue::where($conditions)->first();

        if ($current) {
            return $current->fee_rate;
        }

        $default = Seller::where('idx', $seller)->first();

        if ($default) {
            return $default->commission;
        }

        return 0;
    }

    public function getFeePerQuantityAttribute()
    {
        $fee = ($this->goods->price - $this->discount_amount) * ($this->fee_rate / 100);
        return $fee + floor($fee * 0.1);
    }

    public function getTotalSupportAmountAttribute()
    {
        return $this->lm_share_amount * $this->sold_count;
    }

    public function scopeSearch($query, $sellerId, $start, $end)
    {
        return $query->whereHas('goods.orderDetails', function (Builder $query) use ($sellerId, $start, $end) {
            $query->where('seller_id', $sellerId)
                ->whereBetween('order_detail.confirmed_at', [$start, $end])
                ->where('order_detail.state', '구매확정');
        });
    }
}
