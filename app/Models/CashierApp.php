<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashierApp extends Model
{
    use SoftDeletes;
    protected $connection = 'fleapop_w';
    protected $table      = 'cashier_app_tbl';
    protected $primaryKey = 'idx';

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';
    const DELETED_AT = 'delete_date';
}
