<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class OrderLog extends Model
{
    use SoftDeletes;
    protected $connection = 'fp_payment_w';
    protected $table = 'order_log';
}
