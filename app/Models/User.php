<?php

namespace App\Models;

use App\Scope\DeletedUserScope;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
  use Notifiable, SoftDeletes;

  protected $connection = 'fleapop_w';
  protected $table = 'user_list';
  protected $primaryKey = 'user_id';

  const CREATED_AT = 'insert_date';
  const UPDATED_AT = 'update_date';
  const DELETED_AT = 'delete_date';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'email', 'password','use_pay_type','grade', 'shipping_address_1', 'shipping_address_2',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  protected static function boot()
  {
    parent::boot();
    static::bootSoftDeletes();
  }

  public function orders()
  {
    return $this->hasMany(Order::class);
  }

  public static function findSocialAccount($account)
  {
    return self::whereIn('account_type', [2, 4])
      ->where('fb_account', $account)
      ->whereNull('delete_date')
      ->orderBy('user_id', 'desc')->get();
  }

  public function userAddresses() {
    return $this->hasMany(UserAddress::class);
  }

  static function searchIdByColumn($keyword, $column) {
    return self::where($column, 'LIKE', "%$keyword%")->pluck('user_id');
  }


}
