<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class LMPay extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $table      = 'cash_log';
    protected $primaryKey = 'idx';
    protected $guarded    = [];

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';
    const DELETED_AT = 'delete_date';

    public function user()
    {
        return $this->hasOne(User::class, 'user_id');
    }

    public function scopeCuTransaction($query, $action)
    {
        return $query->where([
            ['type', '오프라인'],
            ['action', $action],
        ])->whereNull('market_seller_idx');
    }

    static public function refundAmount($orderId)
    {
        return LMPay::where([
            ['order_info_idx', $orderId],
            ['action', '환불'],
        ])->sum('amount');
    }

    static public function refund($order, $amount)
    {
        self::create([
            'user_idx'       => $order->user->user_id,
            'order_info_idx' => $order->id,
            'type'           => '온라인',
            'action'         => '환불',
            'state'          => '결재됨',
            'amount'         => $amount,
        ]);
    }

    /**
     * 러마페이 구매 취소(환불) 로그 추가
     * @ param
     * required
     * obj : 취소할 객체 model
     * amount : 취소 금액
     */
    static public function cancel($obj, $amount)
    {
        $data = [
            'user_idx'       => '',
            'type'           => '',
            'order_info_idx' => '',
            'amount'         => $amount,
        ];
        switch ($obj->getTable()) {
            case 'order_detail' :
            {
                // Store 상품 환불
                $res = self::where([['type', '온라인'], ['order_info_idx', $obj->order->id], ['method', '마일리지'], ['action', '지불'], ['state', '결제됨']])->first();

                $exists = PaymentCancel::where('order_detail_id', $obj->id)->first();

                if (isset($exists)) {
                    return [
                        'success' => false,
                        'data'    => [
                            'error_msg' => '이미 취소된 거래건입니다.',
                        ],
                    ];
                }

                $data['type'] = '온라인';
                $data['user_idx'] = $obj->order->user_id;
                $data['order_info_idx'] = $obj->order->id;

                break;
            }
        }

        $res = self::create([
            'user_idx'       => $data['user_idx'],
            'order_info_idx' => $data['order_info_idx'],
            'type'           => "온라인",
            'action'         => '환불',
            'method'         => '마일리지',
            'state'          => '결제됨',
            'amount'         => $amount,
        ]);

        return [
            'success' => true,
            'data'    => [
                'cash_log_idx' => $res->idx,
            ],
        ];
    }

    public function leaveAmount()
    {
        $leaveAmount = $this->amount;
        $refundObjs = $this->where([['type', $this->type], ['cash_log_idx', $this->idx], ['method', '마일리지'], ['action', '환불'], ['state', '결제됨'], ['amount', '<', 0]])->whereNull('delete_date')->sum('amount');

        $leaveAmount -= abs($refundObjs);

        if ($leaveAmount > 0) {
            return $leaveAmount;
        } else {
            return 0;
        }
    }

    static public function usedAmount($userId)
    {
        $usedAmount = self::where([['user_idx', $userId], ['action', '지불'], ['state', '결제됨'], ['amount', '<', 0]])->whereNull('delete_date')->sum('amount');
        $canceledAmount = self::where([['user_idx', $userId], ['action', '환불'], ['state', '결제됨'], ['amount', '>', 0]])->whereNull('delete_date')->sum('amount');

        return $usedAmount + $canceledAmount;
    }

    public function getLog($isDetail = false)
    {
        $msg = '';

        if ($this->action == '환불') {
            if ($this->type == '오프라인') {

                if (! isset($this->market_seller_idx) && ! isset($this->order_info_idx)) {
                    // 러마페이 환불
                    $msg .= "러마페이 환불";
                    if ($isDetail) {
                        $msg .= " - {$this->cash_log_idx}";
                    }
                } else {
                    // 상품 환불
                    if (isset($this->market_seller_idx) && isset($this->order_info_idx)) {
                        $orderGoods = OrderGoodsTbl::where('order_info_idx', $this->order_info_idx)->first();
                        $msg .= "[{$orderGoods->marketSeller->market->title} {$orderGoods->marketSeller->seller->name}";
                        $goods = $orderGoods->goods()->withTrashed()->first();
                        $msg .= " {$goods->title}]";
                    } else if (! isset($this->market_seller_idx) && isset($this->order_info_idx)) {
                        //CU 상품 환불
                        $msg .= "CU상품 구매취소";
                    }
                }
            } else {
                if (! isset($this->market_seller_idx) && ! isset($this->order_info_idx)) {
                    // 러마페이 환불
                    $msg .= "러마페이 환불";
                    if ($isDetail) {
                        $msg .= " - {$this->cash_log_idx}, {$this->refund_imp_uid}";
                    }
                } else {
                    // 상품 환불
                    $order = Order::find($this->order_info_idx);
                    $msg .= "온라인 구매 환불 (주문번호 : {$order->order_number})";
                }
            }
        } else if ($this->action == '지불') {
            if ($this->type == '온라인' && isset($this->order_info_idx)) {
                // 온라인 구매
                $order = Order::find($this->order_info_idx);
                $msg = "온라인 구매 (주문번호 : {$order->order_number})";
            } else if ($this->type == '오프라인' && ! isset($this->market_seller_idx) && isset($this->order_info_idx)) {
                $msg = "CU 구매";
            } else {
                $orderGoods = OrderGoodsTbl::where('order_info_idx', $this->order_info_idx)
                    ->first();
                $orderGoodsCount = OrderGoodsTbl::where('order_info_idx', $this->order_info_idx)->count();
                $msg = "{$orderGoods->marketSeller->market->title} {$orderGoods->marketSeller->seller->name}";
                $goods = $orderGoods->goods()->withTrashed()->first();
                $msg .= "{$goods->title} 구매";
                if ($orderGoodsCount > 1) {
                    $orderGoodCount = $orderGoodsCount - 1;
                    $msg = "{$orderGoods->marketSeller->market->title} {$orderGoods->marketSeller->seller->name}";
                    $goods = $orderGoods->goods()->withTrashed()->first();
                    $msg .= "{$goods->title} 외 {$orderGoodsCount}개의 상품 구매";
                }
            }
        } else if ($this->action == '입금') {
            if ($this->type == '온라인') {
                $msg = "온라인 충전 ({$this->imp_uid})";
            }
        }

        $log = [
            'idx'         => $this->idx,
            'type'        => $this->type,
            'action'      => $this->action,
            'method'      => $this->method,
            'state'       => $this->state,
            'amount'      => number_format($this->amount) . "원",
            'msg'         => $msg,
            'insert_date' => $this->insert_date,
            'update_date' => $this->update_date,
        ];

        return $log;
    }
}
