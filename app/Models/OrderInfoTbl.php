<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderInfoTbl extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $table      = 'order_info_tbl';
    protected $primaryKey = 'idx';

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';
    const DELETED_AT = 'delete_date';

    public function marketSeller()
    {
        return $this->belongsTo(MarketSellerTbl::class, 'market_seller_idx');
    }

    public function orderGoods() {
        return $this->hasMany(OrderGoodsTbl::class, 'order_info_idx');
    }

    public function goods()
    {
        return $this->belongsTo(GoodsTbl::class, 'goods_idx');
    }
}
