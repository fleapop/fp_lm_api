<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionRevenue extends Model
{
    protected $connection = 'fp_payment_w';
    protected $guarded = [];

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id', 'idx');
    }

    public function revenue()
    {
        return $this->belongsTo(Revenue::class, 'nth');
    }
}
