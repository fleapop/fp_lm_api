<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cashier extends Model
{
    use SoftDeletes;

    protected $connection = 'fleapop_w';
    protected $table      = 'cashier_tbl';
    protected $primaryKey = 'idx';
    protected $guarded    = [];

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';
    const DELETED_AT = 'delete_date';

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_idx');
    }
}
