<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $guarded = [];
    protected $connection = 'fp_payment_w';

    public function imageable()
    {
        return $this->morphTo();
    }
}
