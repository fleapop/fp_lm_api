<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * CU > 러마페이 충전 로그
 *
 * Class CUCharge
 * @package App
 */
class CUCharge extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $table      = 'cu_charge_log';
    protected $primaryKey = 'idx';
    protected $guarded = [];

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';
    const DELETED_AT = 'delete_date';
}
