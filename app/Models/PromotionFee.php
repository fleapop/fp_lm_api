<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PromotionFee extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $primaryKey = 'idx';
    protected $guarded    = [];
}
