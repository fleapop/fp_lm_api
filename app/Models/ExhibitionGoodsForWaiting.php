<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ExhibitionGoodsForWaiting extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $table      = 'exhibition_goods_for_waiting';
    protected $primaryKey = 'idx';
    protected $guarded    = [];

    public function exhibition()
    {
        return $this->belongsTo(Exhibition::class);
    }

    public function goods()
    {
        return $this->belongsTo(Goods::class);
    }

    public function scopeGoodsBySellerAndExhibition($query, $exhibition, $seller)
    {
        return $query->with('goods')->where('exhibition_id', $exhibition)
            ->whereHas('goods', function ($query) use ($seller) {
                $query->where('seller_id', $seller);
            })
            ->orderBy('approval_state', 'DESC')
            ->orderBy('updated_at', 'DESC');
    }

    public function scopeMassUpdateWithColumnName($query, $column, $value, $goods)
    {
        return $query->whereIn('idx', $goods)->update([$column => $value]);
    }

    public function scopeGetCompletedGoods($query, $exhibition, $seller)
    {
        return $query->where([
            ['exhibition_id', $exhibition],
            ['approval_state', 1],
        ])->whereHas('goods', function ($query) use ($seller) {
            $query->where('seller_id', $seller);
        });
    }
}
