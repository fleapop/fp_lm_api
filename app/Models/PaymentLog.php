<?php

namespace App\Models;

use App\Lib\FPIamport;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PaymentLog extends Model
{
    use SoftDeletes;

    const CHR = 'CHR';
    const REF = 'REF';
    const PAY = 'PAY';
    const CAN = 'CAN';

    protected $connection = 'fp_payment_w';
    protected $table      = 'payment_logs';
    protected $fillable   = [
        'imp_uid',
        'action',
        'res_data',
        'res_time',
        'req_data',
        'req_time',
        'fail_message',
    ];

    static public function setLog($imp_uid, $action, $req = NULL, $fail_message = "")
    {


        $data = [
            'imp_uid'      => $imp_uid,
            'action'       => $action,
            'res_data'     => "",
            'res_time'     => date('Y-m-d H:i:s'),
            'req_time'     => date('Y-m-d H:i:s'),
            'req_data'     => "",
            'fail_message' => $fail_message,
        ];

        if ($imp_uid != "lmpay") {
            $imp = new FPIamport();
            $impData = $imp->getImpObject($imp_uid);
            $data["res_data"] = json_encode((array)$impData->data, JSON_UNESCAPED_UNICODE);
        }

        if (isset($req)) {
            $data['req_data'] = json_encode($req->all(), JSON_UNESCAPED_UNICODE);
        }


        self::create($data);
    }

    static public function getLog($imp_uid)
    {
        $obj = self::where('imp_uid', $imp_uid)->first();

        $obj->res_data = unserialize($obj->res_data);

        return $obj;
    }
}
