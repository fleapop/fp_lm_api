<?php

namespace App\Models;

use App\Seller\Order as SellerOrder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderCustomerService extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $guarded    = [];

    public function orderDetail()
    {
        return $this->belongsTo(OrderDetail::class, 'detail_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withTrashed();
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id');
    }

    public function scopeSearch($query, $request)
    {
        @session_start();

        switch ($request->order_state) {
            case '교환완료':
                $conditions = [
                    ['state', '답변완료'],
                    ['type', 'LIKE', '교환%'],
                ];
                $orderDetailState = ['배송완료', '구매확정'];
                break;

            case '교환접수':
                $conditions = [
                    ['state', '답변대기'],
                    ['type', 'LIKE', '교환%'],
                ];
                $orderDetailState = ['교환접수'];
                break;

            case '반품접수':
                $conditions = [
                    ['state', '답변대기'],
                    ['type', 'LIKE', '반품%'],
                ];
                $orderDetailState = ['반품접수'];
                break;

            case '반품완료':
                $conditions = [
                    ['state', '답변완료'],
                    ['type', 'LIKE', '반품'],
                ];
                $orderDetailState = ['반품완료'];
                break;
        }

        $query->where($conditions)
            ->whereHas('orderDetail', function (Builder $builder) use ($orderDetailState) {
                $builder->where('seller_id', $_SESSION['seller']['seller_idx'])
                    ->whereIn('state', $orderDetailState);
            });

        switch ($request->field) {
            case 'shipping_name':
                $users = User::where('shipping_name', 'LIKE', "%$request->keyword%")->pluck('user_id');
                $query->whereHas('order', function (Builder $builder) use ($users) {
                    $builder->whereIn('user_id', $users);
                });
                break;

            case 'delivery_name':
                $query->whereHas('orderDetail.delivery', function (Builder $builder) use ($request) {
                    $builder->where('name', 'LIKE', "%$request->keyword%");
                });
                break;

            case 'order_number':
                $query->whereHas('order', function (Builder $builder) use ($request) {
                    $builder->where('order_number', 'LIKE', "%$request->keyword%");
                });
                break;

            case 'order_goods_number':
                $query->whereHas('orderDetail', function (Builder $builder) use ($request) {
                    $builder->where('order_goods_number', 'LIKE', "%$request->keyword%");
                });
                break;

            case 'goods_title':
                $keywords = "%$request->keyword%";
                $query->whereHas('orderDetail', function (Builder $builder) use ($keywords) {
                    $builder->where('goods_title', 'like', $keywords)
                        ->orWhereHas('goods', function (Builder $builder) use ($keywords) {
                            $builder->where('title', 'like', $keywords);
                        });
                });
                break;

            case 'phone':
                $users = User::where('shipping_phone', 'LIKE', "%$request->keyword%")->pluck('user_id');
                $query->whereHas('order', function (Builder $builder) use ($users) {
                    $builder->whereIn('user_id', $users);
                })->whereHas('orderDetail.delivery', function (Builder $builder) use ($request) {
                    $builder->where('phone', 'LIKE', "%$request->keyword%");
                });
                break;
        }

        if ($request->has('start_date', 'end_date')) {
            $period = [$request->start_date . ' 00:00:00', $request->end_date . ' 23:59:59'];
            if (in_array($request->order_state, ['교환접수', '반품접수'])) {
                $query->whereBetween('created_at', $period);
            } else if (in_array($request->order_state, ['교환완료', '반품완료'])) {
                $query->whereBetween('answered_at', $period);
            }
        }

        return $query;
    }

    public function scopeFilterByType($query, $user, $type)
    {
        return $query->where([['user_id', $user], ['type', 'LIKE', "%$type%"]])->whereHas('order', function (Builder $builder) {
            $builder->whereNull('deleted_at');
        });
    }

    static public function answered($answer, $ids)
    {
        self::whereIn('id', $ids)->update([
            'answer'      => $answer,
            'answered_at' => now(),
            'state'       => '답변완료',
        ]);
    }

    static public function updateStatus($ids, $state)
    {
        $query = OrderCustomerService::whereIn('id', $ids);
        $query->update(['type' => $state]);
        SellerOrder::whereIn('id', $query->pluck('detail_id'))->update(['state' => '배송완료']);
    }
}
