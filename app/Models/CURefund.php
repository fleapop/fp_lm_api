<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CURefund extends Model
{
    use SoftDeletes;
    protected $connection = 'fp_payment_w';
    protected $table      = 'cu_refund_log';
    protected $primaryKey = 'idx';
    protected $guarded = [];

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';
    const DELETED_AT = 'delete_date';

    public function getTRANNO($userIdx = 0) {
        $res = $this->create([
            "user_idx"=>$userIdx
        ]);

        $tranNo = $res->idx;
        while(strlen($tranNo) < 9) {
            $tranNo = '0'.$tranNo;
        }

        $tranNo = '1'.$tranNo;
        $res->TRANNO = $tranNo;
        $res->save();

        return $tranNo;
    }

    static public function setData($data, $TRANNO = 0) {
        if($TRANNO != 0) {
            $res = CURefund::where('TRANNO',$TRANNO)->update($data);
        }
    }
}
