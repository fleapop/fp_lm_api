<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $table      = 'cart';
    protected $fillable   = ['user_id', 'non_user_id', 'goods_id', 'sku_id', 'quantity'];

    public function goods()
    {
        return $this->belongsTo(Goods::class, 'goods_id');
    }

    public function option()
    {
        return $this->belongsTo(Option::class, 'option');
    }

    public function sku() {
      return $this->belongsTo(Sku::class, 'sku_id');
    }

    public static function removeAlreadyOrderedItems(Order $order)
    {
        self::where('user_id', $order->user_id)
            ->whereIn('sku_id', $order->detail->pluck('sku_id'))
            ->delete();
    }
}
