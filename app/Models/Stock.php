<?php

namespace App\Models;

use App\Events\StockSoldOut;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Stock extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $guarded    = [];

    /**
     * Goods Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goods()
    {
        return $this->belongsTo(Goods::class);
    }

    /**
     * Sku Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sku()
    {
        return $this->belongsTo(Sku::class,'sku_id', 'id');
    }

    /**
     * 재고 추가
     *
     * @param $quantity
     * @return mixed
     */
    public function addStock($quantity)
    {
        $this->stock += $quantity;
        $this->save();

        return $this->stock;
    }

    /**
     * 재고 차감
     *
     * @param int $quantity
     * @return int|mixed
     */
    public function subStock($quantity = 1)
    {
        $this->stock -= (int)$quantity;
        $this->save();

        if ($this->stock <= 0) {
            $this->update(['stock' => 0]);     // 재고 수량 마이너스 방지.
            event(new StockSoldOut($this->goods->seller, $this->sku));
        }

        $this->checkStock($this);

        return $this->stock;
    }

    /**
     * 해당 상품의 모든 옵션이 품절인지 확인 후 해당 상품 품절 처리.
     *
     * @param $stock
     */
    public function checkStock($stock)
    {
        $stockTotal = Stock::where('goods_id', $stock->goods_id)->sum('stock');

        if ($stockTotal <= 0) {
            Goods::find($stock->goods_id)->update(['stock_state' => 'soldout']);
        }
    }
}
