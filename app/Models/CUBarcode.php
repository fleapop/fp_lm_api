<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CUBarcode extends Model
{
    //
    use SoftDeletes;
    protected $connection = 'fp_payment_w';
    protected $table = 'cu_barcode_tbl';
    protected $guarded = [];
    protected $primaryKey = 'idx';

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';
    const DELETED_AT = 'delete_date';

}
