<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MarketSellerTbl extends Model
{
    use SoftDeletes;
    protected $connection = 'fleapop_w';
    protected $table      = 'market_seller_tbl';
    protected $primaryKey = 'idx';

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';
    const DELETED_AT = 'delete_date';

    public function sellers() {
        return $this->belongsToMany(Seller::class, 'seller_idx');
    }

    public function seller() {
        return $this->belongsTo(Seller::class, 'seller_idx');
    }

    public function markets() {
        return $this->belongsToMany(Market::class, 'market_idx');
    }

    public function market() {
        return $this->belongsTo(Market::class, 'market_idx');
    }

}
