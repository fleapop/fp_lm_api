<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $fillable   = ['user_id','phone','name', 'user_type', 'order_number', 'lmpay_amount', 'total_price', 'state', 'device'];
    protected $appends    = ['order_detail_count'];

    public function payment()
    {
        return $this->hasOne(OrderPayment::class, 'order_id');
    }

    public function delivery()
    {
        return $this->hasMany(OrderDelivery::class, 'order_id');
    }

    public function detail()
    {
        return $this->hasMany(OrderDetail::class, 'order_id');
    }

    public function lmpay()
    {
        return LMPay::where([["type", "=", "온라인"], ["order_info_idx", "=", $this->id], ["state", "=", "결제됨"]])->first();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'user_id')->withTrashed();
    }

    public function paymentCancel()
    {
        return $this->hasMany(PaymentCancel::class, 'order_id');
    }

    public function shippingPrices()
    {
        return $this->hasMany(ShippingPrice::class);
    }

    public function orderCustomerServices()
    {
        return $this->hasMany(OrderCustomerService::class);
    }

    public function getOrderDetailCountAttribute()
    {
        return $this->detail()->count();
    }

    public function getTotalShippingPriceAttribute()
    {
        return $this->shippingPrices->sum('price');
    }

    public function getItemName()
    {
        $itemName = $this->detail[0]->goods->title;
        $goodsCount = $this->detail->count();
        if ($goodsCount > 1) {

            $itemName .= ' 외 ' . ($goodsCount - 1) . '건';
        }

        return $itemName;
    }

    public function scopeSearch($query, $request)
    {
        $results = DB::table('fp_pay.orders')
            ->join('fp_pay.payment', 'fp_pay.orders.id', '=', 'fp_pay.payment.order_id')
            ->leftJoin('fp_db.user_list', 'fp_pay.orders.user_id', '=', 'fp_db.user_list.user_id')
            ->whereNull('fp_pay.payment.deleted_at')->whereNull('fp_pay.orders.deleted_at');
        if ($request->filled(['field', 'search'])) {

            if ($request->field === 'name') {
                $results->where(function ($sql) use ($request) {
                    $sql->where('fp_pay.orders.name', 'LIKE', "%$request->search%")
                        ->orWhere('fp_db.user_list.shipping_name', 'LIKE', "%$request->search%");
                });
            } else if ($request->field === 'phone') {
                $results->where(function ($sql) use ($request) {
                    $sql->where('fp_pay.orders.phone', 'LIKE', "%$request->search%")
                        ->orWhere('fp_db.user_list.shipping_phone', 'LIKE', "%$request->search%");
                });
            } else {
                $results->where($request->field, 'LIKE', "%$request->search%");
            }
        }

        return $results;
    }
}
