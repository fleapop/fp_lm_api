<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDelivery extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $table      = 'delivery';
    protected $guarded    = [];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function orderDetail()
    {
        return $this->belongsTo(OrderDetail::class, 'detail_id');
    }

    public function sellerOrderDetail()
    {
        return $this->belongsTo(Seller\Order::class, 'detail_id');
    }
}
