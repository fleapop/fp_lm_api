<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionDiscountBatchHistory extends Model
{
    protected $connection = 'fp_payment_r';

    public function promotion()
    {
        return $this->belongsTo(Promotion::class);
    }
}
