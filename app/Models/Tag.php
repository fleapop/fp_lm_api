<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $connection = 'fp_payment_w';
    protected $guarded = [];

    public function goods()
    {
        return $this->morphedByMany(Goods::class, 'taggable')->withTimestamps();
    }
}
