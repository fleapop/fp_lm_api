<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Alliv\Iamport\Iamport;

class PaymentCancel extends Model
{
    protected $connection = 'fp_payment_w';
    protected $guarded = [];

    public function order() {
        return $this->hasOne(Order::class, 'order_id');
    }

    public function detail() {
        return $this->hasOne(OrderDetail::class, 'id', 'order_detail_id');
    }

    static public function checkRemainingCancellationAmount($orderId, $cancelAmount)
    {
        return $cancelAmount <= self::remainingCancelAmount($orderId);
    }

    static public function checkRemainingLMPayCancellationAmount($orderId, $cancelAmount)
    {

        return $cancelAmount <= self::remainingLMPayCancelAmount($orderId);
    }

    static public function remainingLMPayCancelAmount($orderId) {
        $order = Order::find($orderId);
        $lmpay = LMPay::where([["type","온라인"],["order_info_idx",$orderId],["action","환불"],["state","결제됨"]])->sum("amount");

        return $order->lmpay_amount - $lmpay;
    }

    static public function remainingImpCancelAmount($orderId) {
        $order = Order::find($orderId);
        return $order->total_price - self::totalCancelAmount($orderId) - $order->lmpay_amount;
    }

    static public function remainingCancelAmount($orderId)
    {
        return Order::find($orderId)->total_price - self::totalCancelAmount($orderId);
    }

    static private function totalCancelAmount ($orderId)
    {
        return self::whereOrderId($orderId)->sum('amount');
    }


}
