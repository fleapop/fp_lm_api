<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BlockSms extends Model
{
    protected $connection = 'fleapop_r';
    protected $table = 'block_sms_tbl';
    protected $primaryKey = 'idx';

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';
}
