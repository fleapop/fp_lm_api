<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Revenue extends Model
{
    protected $connection = 'fp_payment_w';
    protected $dates      = ['settlement_at'];
    protected $guarded    = [];

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id', 'idx');
    }

    public function reflectInSettlement(PromotionRevenue $promotion)
    {
        $this->update([
            'final_settlement_amount' => $this->getSettlementAmountByDate($promotion->started_at, $promotion->ended_at, $promotion->fee_rate),
        ]);
    }

    /**
     * 셀러 기본 수수료 조회
     *
     * @param $sellerId
     * @return int
     */
    public function getSellerFeeRate($sellerId)
    {
        $seller = Seller::where('idx', $sellerId)->first();

        return ($seller->commission) ?: 0;
    }

    public function scopeSearch($query, Request $request)
    {
        $startDate = $request->start . ' 00:00:00';
        $endDate = ((isset($request->end)) ? $request->end : now()->format('Y-m-d')) . ' 23:59:59';

        $excludeSeller = Cashier::where('account', 'LIKE', '%_old')->pluck('seller_idx');

        $results = OrderDetail::revenueSettlement($startDate, $endDate, $request->keyword, $request->field)
            ->whereNotIn('seller_id', $excludeSeller)
            ->orderBy('nth', 'desc')
            ->orderBy('fp_db.seller_tbl.name', 'asc')
            ->get();

        $results->map(function ($item) {
            if ($item->nth) {
                $settlement = $this->where([['nth', $item->nth], ['seller_id', $item->seller_id]])->first();

                $item->revenue_id = (isset($settlement)) ? $settlement->id : null;
                $item->date_range = $this->createDateRange($item);
                $item->fee_rate = (isset($settlement)) ? $settlement->fee_rate : $this->getSellerFeeRate($item->seller_id);
                $item->total_refund = OrderDetail::refundByNth($item->nth, $item->seller_id)->sum('refund_price');
                $item->default_fee_rate = $this->getSellerFeeRate($item->seller_id);
                $item->goods_promotion = $settlement ? $settlement->goods_promotion : $this->getTotalLMShareAmount($item->seller_id, $item->date_range);

                if ($settlement) {
                    $item->id = $settlement->id;
                }

                if (isset($settlement) && $settlement->settlement_at) {
                    $item->settlement_at = $settlement->settlement_at->format('Y-m-d');
                    $item->fee = $settlement->fee;
                    $item->vat = $settlement->vat;
                    $item->goods_promotion = $settlement->goods_promotion;
                    $item->final_settlement_amount = $settlement->final_settlement_amount;
                } else {
                    $item->fee = $this->getFee($item);
                    $item->vat = floor($item->fee * 0.1);
                    $item->final_settlement_amount = $this->getSettlement($item);
                    $item->settlement_at = '';
                }
            }
        });

        return $results;
    }

    private function getTotalLMShareAmount($seller, $date)
    {
        $start = substr($date, 0, 10) . ' 00:00:00';
        $end = substr($date, 13, 10) . ' 23:59:59';
        $goodsPromotions = GoodsPromotion::search($seller, $start, $end)->get();
        $total = 0;

        foreach ($goodsPromotions as $goodsPromotion) {
            $total += $goodsPromotion->lm_share_amount * $goodsPromotion->getSoldCount($start, $end);
        }

        return $total;
    }

    private function getSettlement($item)
    {
        return $item->total - ($item->fee + $item->vat) + $item->total_refund + $item->goods_promotion;
    }

    private function getFee($item)
    {
        $promotionTotal = 0;
        $goodsPromotionTotal = 0;
        $exclusiveTotal = 0;
        $confirmedRange = $this->parseNthStringToDateRange($item->nth);
        $seller = $item->seller->idx;

        // 상품 프로모션 적용 주문금액
        $goodsPromotions = GoodsPromotion::where([
            ['started_at', '<=', $confirmedRange['start']],
            ['ended_at', '>=', $confirmedRange['end']],
        ])->get();

        foreach ($goodsPromotions as $goodsPromotion) {
            $goodsPromotionTotal += OrderDetail::getConfirmedOrderBySeller($confirmedRange['start'], $confirmedRange['end'], $seller)
                ->whereBetween('created_at', [$goodsPromotion->started_at, $goodsPromotion->ended_at])
                ->where('goods_id', $goodsPromotion->goods_id)->sum('total_price');
        }

        // 프로모션 수수료 적용 주문금액
        $promotionFees = PromotionFee::where('seller_id', $item->seller_id)->get();

        foreach ($promotionFees as $promotion) {
            $sum = OrderDetail::getConfirmedOrderBySeller($confirmedRange['start'], $confirmedRange['end'], $seller)
                ->whereBetween('created_at', [$promotion->started_at, $promotion->ended_at])
                ->sum('total_price');
            $exclusiveTotal += $sum;
            $fee = floor($sum * ($promotion->fee_rate / 100));
            $promotionTotal += $fee;
        }

        // 수수료 계산
        return floor(($item->total - $goodsPromotionTotal - $exclusiveTotal) * ($item->fee_rate / 100)) + $promotionTotal;
    }

    private function parseNthStringToDateRange($nth)
    {
        $array = explode('-', $nth);

        $start = $array[0] . '-' . $array[1];
        ($array[2] == 1) ? $start .= '-01 00:00:00' : $start .= '-16 00:00:00';
        $end = ($array[2] == 1) ? $array[0] . '-' . $array[1] . '-15' : Carbon::parse($nth)->endOfMonth()->format('Y-m-d');
        $end .= ' 23:59:59';

        return ['start' => $start, 'end' => $end];
    }

    private function getSettlementAmountByDate($start, $end, $promotionFeeRate)
    {
        $promotionTotal = OrderDetail::promotionRevenueSettlement(
            $this->started_at, $this->ended_at, $start, $end, $this->seller_id
        )->sum('total_price');

        $total = OrderDetail::whereBetween('confirmed_at', [$this->started_at, $this->ended_at])
            ->whereHas('order.payment', function (Builder $query) use ($start, $end) {
                $query->where('payment.state', '결졔완료')
                    ->whereNotBetween('payment.updated_at', [$start . ' 00:00:00', $end . ' 23:59:59']);
            })->sum('total_price');

        $finalTotal = $this->calculateFeeRateAndVat($total, $this->fee_rate);
        $finalPromotionTotal = $this->calculateFeeRateAndVat($promotionTotal, $promotionFeeRate);
        $refund = OrderDetail::getRefundTotalByDate($this->seller_id, $start, $end);

        return $finalTotal + $finalPromotionTotal + $refund;
    }

    private function calculateFeeRateAndVat($total, $feeRate)
    {
        $fee = floor($total * ($feeRate / 100));
        $vat = floor($fee * 0.1);
        return $total - ($fee + $vat);
    }

    private function createDateRange($item)
    {
        $array = explode('-', $item->nth);

        if ($array[2] == 1) {
            $dateRange = $array[0] . '-' . $array[1] . '-01 ~ ' . $array[0] . '-' . $array[1] . '-15';
        } else {
            $dateRange = $array[0] . '-' . $array[1] . '-16 ~ ' . Carbon::parse($item->nth)->endOfMonth()->format('Y-m-d');
        }

        return $dateRange;
    }
}
