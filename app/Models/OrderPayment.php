<?php

namespace App\Models;

use Alliv\Iamport\Iamport;
use DB;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderPayment extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $table      = 'payment';
    protected $fillable   = [
        'order_id',
        'action',
        'state',
        'amount',
        'lmpay',
        'imp_uid',
        'merchant_uid',
        'apply_num',
        'sender',
        'vbank_num',
        'vbank_name',
        'vbank_date',
        'receipt_url',
        'error_msg',
        'bank',
        'holder',
        'account',
        'method'
    ];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function getMethodString()
    {
        switch ($this->method) {
            case 'card' :
                return '카드';
            case 'vbank':
                return '가상계좌';
            case 'lmpay':
                return '러마페이';
            case 'trans':
                return '계좌이체';
            case 'PAYCO':
                return 'PAYCO';
            case 'samsung':
                return '삼성페이';
            default : return '';
        }
    }

    static public function paid($data)
    {
        return OrderPayment::create($data);
    }

    static public function cancel($orderDetail, $amount, $reason)
    {
        if ($orderDetail->order->payment->method === 'lmpay') {
            try {
                DB::transaction(function () use ($orderDetail) {
                    LMPay::refund($orderDetail->order_id, $orderDetail->order->lmpay_amount);
                    $orderDetail->update(['state' => '취소완료']);
                });
            } catch (Exception $e) {
                return $e->getMessage();
            }
        } else {
            // 결재 취소 잔액 확인
            if (PaymentCancel::checkRemainingCancellationAmount($orderDetail->order_id, $amount)) {

                // 아임포트로 $cancelAmount 만큼 부분취소
                self::iamportRequestPaymentCancel($orderDetail, $amount, $reason);

            } else {
                // 결제 취소 부족 금액
                $remainingCancelAmount = PaymentCancel::remainingCancelAmount($orderDetail->order_id);

                // 러마페이 결재 금액 확인
                $remainLmPayAmount = $orderDetail->order->lmpay_amount - LMPay::refundAmount($orderDetail->order_id);

                // 러마페이 차감 금액 계산
                $cancelLmPayAmount = $amount - $remainLmPayAmount;

                // 러마페이 결재 잔액이 차감금액 만큼 있는지 확인
                if ($remainLmPayAmount > $cancelLmPayAmount) {
                    // 아임포트 $remainingCancelAmount 만큼 부분취소
                    self::iamportRequestPaymentCancel($orderDetail, $remainingCancelAmount, $reason);

                    // 러마페이 환불처리
                    LMPay::refund($orderDetail->order, $cancelLmPayAmount);
                } else {
                    // 결제취소 잔액 부족, 러마페이 잔액 부족
                    throw new Exception('not enough cancel remain amount and lmpay');
                }
            }
        }
    }

    static private function iamportRequestPaymentCancel($orderDetail, $amount, $reason)
    {
        $iamport = new Iamport([
            'apiKey'    => config('iamport.apiKey'),
            'apiSecret' => config('iamport.apiSecret'),
        ]);

        $result = $iamport->cancelPayment($orderDetail->order->payment->imp_uid, $amount, $reason);

        if ($result->success) {
            $orderDetail->paymentCancel()->create([
                'order_id' => $orderDetail->order_id,
                'state'    => '취소완료',
                'method'   => $orderDetail->order->payment->method,
                'amount'   => $amount,
                'imp_uid'  => $orderDetail->order->payment->imp_uid,
            ]);
        } else {
            throw new Exception($result->error['code'] . ': ' . $result->error['message']);
        }
    }
}
