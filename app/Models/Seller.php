<?php

namespace App\Models;

use App\Offline\Market;
use App\Offline\SellerMarket;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

class Seller extends Model
{
    use SoftDeletes;

    protected $connection = 'fleapop_w';
    protected $table      = 'seller_tbl';
    protected $primaryKey = 'idx';
    protected $guarded    = [];

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';
    const DELETED_AT = 'delete_date';

    /**
     * Goods Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function goods()
    {
        return $this->hasMany(Goods::class);
    }

    public function orderDetail()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function salesCalculatedDetails()
    {
        return $this->hasMany(SalesCalculateDetail::class);
    }

    public function customerServices()
    {
        return $this->hasMany(CustomerService::class);
    }

    public function cashiers()
    {
        return $this->hasMany(Cashier::class);
    }

    public function promotionFee()
    {
        return $this->hasMany(PromotionFee::class, 'seller_id');
    }

    /**
     * SellerCategory Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function categoryCode()
    {
        return $this->hasOne(SellerCategoryCode::class, 'category_code');
    }

    /**
     * Market Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function markets()
    {
        return $this->belongsToMany(Market::class, 'market_seller_tbl', 'seller_idx', 'market_idx')
            ->withPivot('idx');
    }

    public function orderCustomerServices()
    {
        return $this->hasMany(OrderCustomerService::class);
    }

    public function getCategoryCode($category)
    {
        switch ($category) {
            case 'all' :
                $code = 0;
                break;
            case 'design' :
                $code =  101;
                break;
            case 'shopmall' :
                $code =  102;
                break;
            case 'brand' :
                $code =  103;
                break;
            case 'beauty' :
                $code =  201;
                break;
            case 'stuff' :
                $code =  202;
                break;
            case 'acc' :
                $code =  301;
                break;
        }

        return $code;
    }

    /**
     * 판매자 정보 필수값이 누락되었는지 확인
     *
     * @return bool
     */
    public function checkRequiredSalesInformation()
    {
        return ! ($this->co_name == '' || $this->company_reg_num == '' || $this->co_img1 == '' || $this->manager == '' ||
            $this->manager_hp == '' || $this->manager_mail == '' || $this->cs_tel == '' || $this->co_zipcode == '' ||
            $this->co_address1 == '' || $this->co_address2 == '' || $this->bank_name == '' ||
            $this->account_holder == '' || $this->bank_account == '' || $this->default_courier == '' ||
            $this->default_return_address == '');
    }

    public function getExtraShippingPrice($zipCode)
    {
        if (AddShippingCode::isContained($zipCode)) {
            $addShippingCode = AddShippingCode::where('zip_code', $zipCode)->first();

            if (mb_strpos($addShippingCode->address, '제주') !== false) {
                return $this->extra_shipping_price;
            } else {
                return $this->shipping_price;
            }
        }

        return 0;
    }

    public function getPromotionFeeInProgressAttribute()
    {
        $promotionFee = $this->promotionFee()->where([
            ['started_at', '<=', now()],
            ['ended_at', '>=', now()],
        ])->first();

        return ($promotionFee) ? $promotionFee->fee_rate : '';
    }

    public function scopeGetCanReceiveMessage($query)
    {
        return $query->where([['display_state', 'show'], ['allow_message_receive', 1]]);
    }
}
