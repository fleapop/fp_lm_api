<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use James\Sortable\SortableTrait;

class ExhibitionGoods extends Model
{
    use SoftDeletes;
    use SortableTrait;

    protected $connection = 'fp_payment_w';
    protected $table      = 'exhibition_goods';
    protected $primaryKey = 'id';
    protected $guarded    = [];

    public $sortable = [
        'sort_field'         => 'view_order',
        'sort_when_creating' => true,
    ];

    public function exhibition()
    {
        return $this->belongsTo(Exhibition::class, 'exhibition_id');
    }

    public function goods()
    {
        return $this->belongsTo(Goods::class, 'goods_id');
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id');
    }

    public function promotion()
    {
        return $this->hasOne(Promotion::class, 'id', 'promotion_id');
    }
}
