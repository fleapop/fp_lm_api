<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $connection = 'fleapop_w';
    protected $table      = 'like_tbl';
    protected $primaryKey = 'idx';
    protected $guarded = [];

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';
    const DELETED_AT = 'delete_date';

    public function getLikeData($params) {

        return [
            "is_like_state"=>$this->getLikeState($params),
            "like_count"=>$this->getLikeCount($params)
        ];
    }

    public function toggleLike($params) {

        $data = [
            "table_name"=>$params["table_name"],
            "table_idx"=>$params["table_idx"],
            "user_idx"=>$params["user_idx"]
        ];




        if($this->getLikeState($params)) {
//            $this->connection = "fleapop_w";

            $this->where([
                ["table_name",$data["table_name"]],
                ["table_idx",$data["table_idx"]],
                ["user_idx",$data["user_idx"]]
            ])->delete();

            return $this->getLikeData($params);
        } else {
//            $this->connection = "fleapop_w";

            self::create($data);

            return $this->getLikeData($params);
        }
    }

    public function getLikeState($params) {
//        $this->connection = "fleapop_r";
        $data = [
            "table_name"=>$params["table_name"],
            "table_idx"=>$params["table_idx"],
            "user_idx"=>$params["user_idx"]
        ];

        $exists = $this->where([
            ["table_name",$data["table_name"]],
            ["table_idx",$data["table_idx"]],
            ["user_idx",$data["user_idx"]]
        ])->count();

        if($exists > 0) {
            return true;
        } else {
            return false;
        }

    }

    public function getLikeCount($params) {
//        $this->connection = "fleapop_r";
        $res = $this->where([["table_name",$params["table_name"]],["table_idx",$params["table_idx"]]])->count();

        return $res;
    }
}
