<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppTopMenu extends Model
{
    protected $connection = 'fp_payment_w';
    protected $primaryKey = 'idx';
    protected $guarded    = [];
    protected $table      = 'app_top_menu';
}
