<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use James\Sortable\SortableTrait;

class Goods extends Model
{
    use SoftDeletes;
    use SortableTrait;

    protected $connection = 'fp_payment_w';
    protected $appends    = ['promotion_price','promotion'];
    protected $guarded    = [];
    protected $casts      = [
        'detail'                   => 'array',
        'use_option'               => 'boolean',
        'use_extra_shipping_price' => 'boolean',
    ];

    public $sortable = [
        'sort_field'         => 'order',
        'sort_when_creating' => true,
    ];


    /**
     * Seller Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id');
    }

    /**
     * Option Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function options()
    {
        return $this->hasMany(Option::class);
    }

    public function promotions()
    {
        return $this->hasMany(Promotion::class);
    }

    /**
     * Tag Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    /**
     * Image Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this->MorphMany(Image::class, 'imageable')->orderBy('order');
    }

    /**
     * CustomerServices Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany(CustomerService::class);
    }

    /**
     * Sku Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function skus()
    {
        return $this->hasMany(Sku::class);
    }

    /**
     * Stock Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stocks()
    {
        return $this->hasMany(Stock::class);
    }

    /**
     * OrderDetail Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class);
    }

    /**
     * Cart Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function carts()
    {
        return $this->hasMany(Cart::class);
    }

    /**
     * GoodsPromotion Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function goodsPromotions()
    {
        return $this->hasMany(GoodsPromotion::class);
    }

    public function exhibitionGoodsForWaiting()
    {
        return $this->hasMany(ExhibitionGoodsForWaiting::class);
    }

    public function scheduledPromotion()
    {
        return $this->promotions()->where('start_date', '>', now())
            ->orderBy('start_date', 'asc');
    }

    public function onGoingPromotion()
    {
        return $this->promotions()->where('start_date', '<=', now())
            ->where('end_date', '>=', now());
    }

    public function entirePeriodPromotion()
    {
        return $this->promotions()->where('use_date', 0)->latest();
    }

    /**
     * 결제취소 이력이 없는 주문 건 조회
     *
     * @param $goodsId
     * @param $state
     * @return mixed
     */
    public static function getNotCanceledPayment($goodsId, $state)
    {
        return OrderDetail::whereGoodsId($goodsId)
            ->doesntHave('paymentCancel')
            ->whereHas('order.payment', function (Builder $query) use ($state) {
                $query->whereState($state);
            });
    }

    /**
     * 상품 state 검사
     *
     * @return bool
     */
    public function getState()
    {
        if (array_unique($this->skus()->pluck('state')->toArray()) === ['enable']) {
            $stocks = array_unique($this->stocks()->pluck('stock')->toArray());
            if ($stocks === [0]) {
                if (max($stocks) <= 0) {
                    return 'soldout';
                } else {
                    return 'sale';
                }
            } else {
                return 'sale';
            }
        } else if (array_unique($this->skus()->pluck('state')->toArray()) === ['disable']) {
            return 'soldout';
        } else {
            $stocks = array_unique($this->stocks()->pluck('stock')->toArray());
            if ($stocks === [0]) {
                return 'soldout';
            } else {
                if (max($stocks) <= 0) {
                    return 'soldout';
                } else {
                    return 'sale';
                }
            }
        }
    }

    public function totalPromotionAmounts()
    {
        $result = $this->price;

        if ($this->use_discount) {
            $result -= $this->promotions->sum('calculated_amount');
        }

        return $result;
    }

    public function getPromotion($goods_id = 0)
    {
        return Promotion::where('goods_id', '=', $this->id)
            ->whereRaw('(use_date = 0 or (start_date <= now() and end_date >= now())) and deleted_at is null')
            ->orderBy('id', 'DESC')->limit(1)->get();
    }

    public function getPromotionAttribute($goods_id = 0)
    {
        return Promotion::where('goods_id', '=', $this->id)->orderBy('id', 'DESC')->limit(1)->get();
    }

    public function optionStock()
    {
        return Sku::where('goods_id', '=', $this->id)->get();
    }

    public function optionStockOnce()
    {
        return Sku::where('goods_id', '=', $this->id)->first()->id;
    }

    public function getOptionBySku($sku_id, $get_string = false)
    {
        $res_sku = Sku::find($sku_id);

        if ($res_sku === null) {
            $res_sku = Sku::where('sku_id', $sku_id)->first();
        }

        $option_idxs = [];


        foreach ($res_sku->options as $item) {
            array_push($option_idxs, $item->id);
        }

        if ($get_string) {
            $res_option = Option::whereIn('id', $option_idxs)->whereNull('deleted_at')->get();
            $option = '';
            foreach ($res_option as $item) {
                $option .= $item->value . ' ';
            }
            if ($res_sku->price !== 0) {
                $option_price = number_format($res_sku->price);
                $option .= "(옵션가 : {$option_price}원)";
            }
            return $option;
        } else {
            return Option::whereIn('id', $option_idxs)->whereNull('deleted_at')->get();
        }

    }

    public function setState($state)
    {
        $this->state = $state;
        $this->save();
    }

    /**
     * 온라인 상품 필터 query
     *
     * @param $sellerIdx
     * @param $request
     * @return mixed
     */
    public function scopeFilter($query, $sellerId, $request)
    {
        $query->whereSellerId($sellerId);

        if ($request->field == 'title') {
            $query->where('title', 'like', '%' . $request->keyword . '%');
        }

        if ($request->field == 'goodsNumber') {
            $query->where('goods_code', 'like', '%' . $request->keyword . '%');
        }

        if ($request->field == 'tag' && $request->keyword) {
            $keyword = $request->keyword;

            $query->with('tags')->whereHas('tags.goods', function ($query) use ($keyword) {
                $query->where('name', 'like', "%{$keyword}%");
            });
        }

        if ($request->main_category) {
            $query->whereMainCategory($request->main_category);
        }

        if ($request->sub_category) {
            $query->whereSubCategory($request->sub_category);
        }

        if ($request->display_state) {
            $query->where('display_state', '=', $request->display_state);
        }

        if ($request->stock_state) {
            $query->where('stock_state', '=', $request->stock_state);
        }

        if (! (empty($request->start_date) && empty($request->end_date))) {
            $query->whereBetween('created_at', [$request->start_date . ' 00:00:00', $request->end_date . ' 23:59:59']);
        }

        switch ($request->state) {
            case 'sale':
                $query->whereState('enable');
                break;
            case 'soldout':
                $query->whereHas('stock', function (Builder $query) {
                    $query->whereStock(0);
                });
                break;
            case 'hide':
                $query->whereState('disable');
                break;
        }

        return $query;
    }

    /**
     * 상품조회, 주문, 판매, 장바구니 숫자 추가
     *
     * @param $query
     * @return mixed
     */
    public function scopeWithAggregateCount($query)
    {
        return $query->withCount([
            'carts', 'orderDetails', 'orderDetails as sold_count' => function ($query) {
                $query->where('state', '구매확정');
            },
        ]);
    }


    public function getPromotionPrice($quantity = 1)
    {
        if ($this->use_discount) {
            $res_promotion = $this->getPromotion($this->id);
            if (isset($res_promotion[0])) {
                // 할인 적용
                $res_promotion = $res_promotion[0];
                $is_promotion = true;

                if ($res_promotion->use_date) {
                    // 기간 할인 적용
                    $now_date = date("Y-m-d H:i:s");
                    $str_now_date = strtotime($now_date);
                    $str_start_date = strtotime($res_promotion->start_date);
                    $str_end_date = strtotime($res_promotion->end_date);

                    if ($str_start_date <= $str_now_date && $str_end_date >= $str_now_date) {
                        $is_promotion = true;
                    } else {
                        $is_promotion = false;
                    }
                }

                if ($is_promotion) {
                    // 금액 할인
                    return $res_promotion->amount * $quantity;
                } else {
                    // 할인 미적용
                    return 0;
                }
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    /**
     * 현재진행중인 핫딜 조회
     *
     * @param $query
     * @return mixed
     */
    public function scopeGetHotDealInProgress($query)
    {
        return $query->whereHas('goodsPromotions', function ($q) {
            $q->where([
                ['started_at', '<=', now()],
                ['ended_at', '>=', now()],
            ]);
        });
    }

    public function getPromotionPriceAttribute()
    {
        $quantity = 1;
        if ($this->use_discount) {
            $res_promotion = $this->getPromotion($this->id);

            if (isset($res_promotion[0])) {
                // 할인 적용
                $res_promotion = $res_promotion[0];

                $is_promotion = true;
                if ($res_promotion->use_date) {
                    // 기간 할인 적용
                    $now_date = date("Y-m-d H:i:s");
                    $str_now_date = strtotime($now_date);
                    $str_start_date = strtotime($res_promotion->start_date);
                    $str_end_date = strtotime($res_promotion->end_date);

                    if ($str_start_date <= $str_now_date && $str_end_date >= $str_now_date) {
                        $is_promotion = true;
                    } else {
                        $is_promotion = false;
                    }
                }

                if ($is_promotion) {
                    if ($res_promotion->type == "amount") {
                        // 금액 할인

                        $price = ($this->price * $quantity) - ($res_promotion->amount * $quantity);
                        return $price;
                    } else {
                        // 퍼센트 할인
                        $price = (((100 - $res_promotion->amount) * 0.01) * ($this->price * $quantity)) * 0.01 * 100;
                        return round($price);
                    }
                } else {
                    // 할인 미적용
                    return $this->price;
                }

            } else {
                return $this->price;
            }

        } else {
            return $this->price;

        }
    }

    public function getUseDiscountAttribute($value)
    {
        if ($value) {
            $promotion = $this->getPromotion($this->id);

            if (isset($promotion[0])) {
                $promotion = $promotion[0];
                $submit_promotion = true;

                if ($promotion->use_date) {
                    $now = Carbon::now();
                    $start_date = Carbon::createFromFormat("Y-m-d H:i:s", $promotion->start_date);
                    $end_date = Carbon::createFromFormat("Y-m-d H:i:s", $promotion->end_date);
                    $submit_promotion = ($start_date <= $now && $end_date >= $now);
                }

                return $submit_promotion;
            }
        }

        return false;
    }

    public function isLike($user)
    {
        $result = Like::where([
            ['table_name', 'goods'],
            ['table_idx', $this->id],
            ['user_idx', $user],
        ])->first();

        return ! ! $result;
    }

    public function createGoodsCode()
    {
        $goodsCode = '000000000';
        $goodsCode = substr($goodsCode, 0, -strlen($this->id));
        $this->update(['goods_code' => $goodsCode . $this->id]);
    }

    public function scopeUpdateDisplayState($query, array $ids, $state)
    {
        $query->whereIn('id', $ids)->update([
            'display_state' => $state,
        ]);
    }

    public function scopeUpdateStockState($query, array $ids, $state)
    {
        $query->whereIn('id', $ids)->update([
            'stock_state' => $state,
        ]);
    }

    public function isHotDealInProgress()
    {
        return ! ! $this->goodsPromotions->where('started_at', '<=', now())->where('ended_at', '>=', now())->count();
    }

    public function isExhibitionInProgress()
    {
        return ! ! ExhibitionSeller::where('seller_id', $this->seller_id)->whereHas('exhibition', function ($query) {
            $query->where('end_date', '>', now())->whereHas('exhibitionGoods', function ($query) {
                $query->where('goods_id', $this->id);
            });
        })->count();
    }

    public function isNoOrderHistory()
    {
        return $this->orderDetails->count() === 0;
    }
}
