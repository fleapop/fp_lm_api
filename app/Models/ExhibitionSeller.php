<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExhibitionSeller extends Model
{
    protected $connection = 'fp_payment_w';
    protected $table      = 'exhibition_seller';
    protected $primaryKey = 'idx';
    protected $guarded    = [];

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id', 'idx');
    }

    public function exhibition()
    {
        return $this->belongsTo(Exhibition::class);
    }

    public function scopeUpdateApprovalStateToWaiting($query, $exhibition, $seller)
    {
        $query->where([
            ['exhibition_id', $exhibition],
            ['seller_id', $seller],
        ])->update(['approval_state' => 3]);
    }
}
