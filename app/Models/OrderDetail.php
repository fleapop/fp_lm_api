<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OrderDetail extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $table      = 'order_detail';
    protected $guarded    = [];

    public function goods()
    {
        return $this->belongsTo(Goods::class, 'goods_id')->withTrashed();
    }

    public function delivery()
    {
        return $this->hasOne(OrderDelivery::class, 'detail_id');
    }

    public function option()
    {
        return $this->hasManyThrough(Option::class, Sku::class, 'sku_id', 'id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function paymentCancel()
    {
        return $this->hasMany(PaymentCancel::class);
    }

    public function sku()
    {
        return $this->hasOne(Sku::class, 'id', 'sku_id');
    }

    public function payment()
    {
        return $this->hasManyThrough(Order::class, OrderPayment::class, 'order_id', 'id');
    }

  public function payment_one()
  {
    return $this->hasOne(OrderPayment::class, 'order_id', 'order_id');
  }

    public function seller()
    {
        return $this->belongsTo(Seller::class, 'seller_id', 'idx');
    }

    public function orderCustomerServices()
    {
        return $this->hasMany(OrderCustomerService::class, 'detail_id');
    }

    public function isLastOrder()
    {
        $count = self::where([
                ['order_id', $this->order_id],
                ['seller_id', $this->seller_id],
            ])->whereNotIn('state', ['취소완료', '반품완료'])->count();

        return $count == 1;
    }

    public function scopeRefundByNth($query, $nth, $seller)
    {
        $array = explode('-', $nth);

        $dateRange = [
            $array[0] . '-' . $array[1] . (($array[2] == 1) ? '-01' : '-16') . ' 00:00:00',
            $array[0] . '-' . $array[1] . (($array[2] == 1) ? '-15' : '-31') . ' 23:59:59',
        ];

        return $query->where([['state', '반품완료'], ['seller_id', $seller]])
            ->whereBetween('updated_at', $dateRange);
    }

    public function scopeRevenueSettlement($query, $start, $end, $keyword, $field)
    {
        $query->whereBetween('confirmed_at', [$start, $end])
            ->join('fp_db.seller_tbl', 'seller_id', '=', 'seller_tbl.idx')
            ->groupBy('seller_id', 'nth')
            ->where('state', '구매확정')
            ->select(
                DB::raw('sum(total_price) total'),
                DB::raw('CONCAT(date_format(confirmed_at, "%Y-%m"), (CASE WHEN date_format(confirmed_at, "%d") <= 15 THEN "-1" ELSE "-2" END)) nth'),
                'seller_id', 'fp_db.seller_tbl.*')
            ->with('seller');
        if (isset($keyword)) {
            if ($field == '브랜드') {
                $sellerIds = Seller::where('name', 'LIKE', "%$keyword%")->get()->pluck('idx');
            } else if ($field == '아이디') {
                $sellerIds = Cashier::where('account', 'LIKE', "%$keyword%")->get()->pluck('seller_idx');
            }

            $query->whereIn('seller_id', $sellerIds);
        }
    }

    public function isDuplicateExchange()
    {
      return $this->isDuplicateByType('교환접수');
    }

    public function isDuplicateCancel()
    {
      return $this->isDuplicateByType('구매취소');
    }

    private function isDuplicateByType($type)
    {
      return $this->state === $type
        || $this->orderCustomerServices()
          ->where('state', '=', '답변대기')
          ->where('type', '=', $type)->count() > 0;
    }

    static public function promotionRevenueSettlement($start, $end, $paymentStart, $paymentEnd, $seller)
    {
        return self::whereBetween('confirmed_at', [$start, $end])
            ->where('seller_id', $seller)
            ->whereHas('order', function (Builder $query) use ($paymentStart, $paymentEnd) {
                $query->whereBetween('order.created_at', [$paymentStart . ' 00:00:00', $paymentEnd . ' 23:59:59']);
            });
    }

    static public function filter($sellerId, $request)
    {
        $query = self::where('seller_id', $sellerId);

        switch ($request->state) {
            case '상품준비':
                $query->where('state', '결제완료')
                    ->whereHas('delivery', function (Builder $query) {
                        $query->where('state', '상품준비');
                    });
                break;
            case '교환환불':
                $query->whereIn('state', ['교환접수', '환불접수'])
                    ->whereHas('delivery', function (Builder $query) {
                        $query->where('state', '!=', '상품준비');
                    });
                break;
            default:
                $query->where('state', $request->state)
                    ->whereHas('delivery', function (Builder $query) use ($request) {
                        $query->where('state', $request->state);
                    });
        }

        $query->whereHas('goods', function (Builder $query) {
            $query->whereNull('deleted_at');
        });

        $query = self::filterByFields($query, $request);

        if (! (empty($request->start_date) && empty($request->end_date))) {
            $query->whereHas('order', function (Builder $query) use ($request) {
                $query->whereBetween(
                    'created_at',
                    [$request->start_date . ' 00:00:00', $request->end_date . ' 23:59:59']);
            });
        }

        if (! (empty($request->start_price) && empty($request->end_price))) {
            $query->where([
                ['total_price', '>=', $request->start_price],
                ['total_price', '<=', $request->end_price],
            ]);
        }

        return $query->with(['order', 'goods', 'sku', 'order.user', 'delivery', 'order.payment', 'orderCustomerServices']);
    }

    static public function filterWithoutState($sellerId, $request)
    {
        $query = self::whereSellerId($sellerId);

        $query = self::filterByFields($query, $request);

        $query->whereHas('goods', function (Builder $query) {
            $query->whereNull('deleted_at');
        });

        if ($request->start_date && $request->end_date) {
            switch ($request->date_type) {
                case 'ordered':
                    $query->whereHas('order', function (Builder $query) use ($request) {
                        $query->whereBetween('created_at', [
                            $request->start_date . ' 00:00:00',
                            $request->end_date . ' 23:59:59',
                        ]);
                    });
                    break;
                case 'ready':
                    $query->whereState('결제완료')->whereBetween('updated_at', [
                        $request->start_date . ' 00:00:00',
                        $request->end_date . ' 23:59:59',
                    ]);
                    break;
                case 'send':
                    $query->whereState('배송중')->whereBetween('updated_at', [
                        $request->start_date . ' 00:00:00',
                        $request->end_date . ' 23:59:59',
                    ]);
                    break;
                case 'complete':
                    $query->whereHas('delivery', function (Builder $query) use ($request) {
                        $query->whereState('배송완료')->whereBetween('updated_at', [
                            $request->start_date . ' 00:00:00',
                            $request->end_date . ' 23:59:59',
                        ]);
                    });
                    break;
            }
        }

        if ($request->order_state) {
            $orderState = explode(',', $request->order_state);
            $query->whereIn('state', $orderState);
        }

        return $query->with(['order', 'goods', 'sku', 'order.user', 'delivery']);
    }

    static private function filterByFields($query, $request)
    {
        switch ($request->search_field) {
            case 'shipping_name':
                $users = User::where('shipping_name', 'LIKE', '%' . $request->keyword . '%')->get()->pluck('user_id');
                $query->whereHas('order', function (Builder $query) use ($request, $users) {
                    $query->whereIn('user_id', $users);
                });
                break;
            case 'delivery_name':
                $query->whereHas('delivery', function (Builder $query) use ($request) {
                    $query->where('name', 'LIKE', '%' . $request->keyword . '%');
                });
                break;
            case 'delivery_phone':
                $query->whereHas('delivery', function (Builder $query) use ($request) {
                    $query->where('phone', 'LIKE', '%' . $request->keyword . '%');
                });
                break;
            case 'order_goods_number':
                $query->where('order_goods_number', 'LIKE', '%' . $request->keyword . '%');
                break;
            case 'order_number':
                $query->whereHas('order', function (Builder $query) use ($request) {
                    $query->where('order_number', 'LIKE', '%' . $request->keyword . '%');
                });
                break;
            case 'goods_title':
                $query->whereHas('goods', function (Builder $query) use ($request) {
                    $query->where('title', 'LIKE', '%' . $request->keyword . '%');
                });
                break;
        }

        return $query;
    }

    static public function salesOrderCount($seller, $startedAt, $endedAt)
    {
        return self::getOrderDetailBySalesCalculateDate($seller, $startedAt, $endedAt)->count();
    }

    static private function getOrderDetailBySalesCalculateDate($seller, $startedAt, $endedAt, $keyword = '')
    {
        return self::getTotalOrderDetail($startedAt, $endedAt, $keyword)->where('seller_id', $seller);
    }

    static public function getTotalOrderDetail($startedAt, $endedAt, $keyword = '')
    {
        return self::whereIn('state', ['배송중', '배송완료'])->where('deleted_at', null)
            ->whereHas('order.payment', function (Builder $query) use ($startedAt, $endedAt) {
                if ($startedAt && $endedAt) {
                    $query->whereBetween('updated_at', [
                        $startedAt . ' 00:00:00',
                        $endedAt . ' 23:59:59',
                    ])->where('state', '결제완료');
                } else {
                    $query->where('state', '결제완료');
                }
            })->whereHas('goods', function (Builder $query) use ($keyword) {
                $query->whereNull('deleted_at')
                    ->where('title', 'LIKE', '%' . $keyword . '%');
            });
    }

    public static function getTotalByDate($seller, $start, $end)
    {
        return self::whereBetween('confirmed_at', [$start . ' 00:00:00', $end . ' 23:59:59'])
            ->where('seller_id', $seller)->sum('total_price');
    }

    public static function getRefundTotalByDate($seller, $start, $end)
    {
        return self::where([['state', '반품완료'], ['seller_id', $seller]])
            ->whereBetween('updated_at', [$start . ' 00:00:00', $end . ' 23:59:59'])
            ->sum('refund_price');
    }

    public function scopeGetConfirmedOrderBySeller($query, $start, $end, $seller)
    {
        return $query->whereBetween('confirmed_at', [$start, $end])
            ->where([['seller_id', $seller], ['state', '구매확정']]);
    }

    public function getLmShareAmount()
    {
        $result = GoodsPromotion::where([
            ['goods_id', $this->goods_id],
            ['started_at', '>=', $this->created_at],
            ['ended_at', '<=', $this->created_at],
        ])->first();

        return ($result) ? $result->lm_share_amount : 0;
    }

    public function getSellerFeeRate()
    {
        // 프로모션 수수료가 있는지 확인
        $result = PromotionFee::where([
            ['seller_id', $this->seller->idx],
            ['started_at', '<=', $this->order->payment->created_at->format('Y-m-d H:i:s')],
            ['ended_at', '>=', $this->order->payment->created_at->format('Y-m-d H:i:s')],
        ])->first();

        if ($result) {
            return $result->fee_rate;
        }

        // 기본 수수료 리턴
        return $this->seller->commission ?: 0;
    }

}
