<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddShippingCode extends Model
{
    protected $connection = 'fp_payment_w';

    public static function isContained($zipCode) {
        return (self::where('zip_code', $zipCode)->count() > 0);
    }
}
