<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Promotion extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $casts      = ['use_date' => 'boolean', 'start_date' => 'datetime', 'end_date' => 'datetime'];
    protected $appends    = ['calculated_amount'];
    protected $fillable   = ['id', 'goods_id', 'type', 'amount', 'use_date', 'start_date', 'end_date',];

    /**
     * Goods Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goods()
    {
        return $this->belongsTo(Goods::class);
    }

    public function getCalculatedAmountAttribute()
    {
        return $this->calculatedAmount();
    }

    public function calculatedAmount()
    {
        if ($this->isWithInPeriod()) {
            if ($this->type === 'percent') {
                $result = round($this->goods->price * ($this->amount / 100));
            } else if ($this->type === 'amount') {
                $result = $this->amount;
            }
        } else {
            $result = 0;
        }

        return $result;
    }

    private function isWithInPeriod()
    {
        if ($this->use_date) {
            return now()->between($this->start_date, $this->end_date);
        } else {
            return true;
        }
    }

    public function isStartedAlready()
    {
        return $this->start_date <= now();
    }
}
