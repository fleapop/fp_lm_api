<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Option extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $guarded = [];

    /**
     * Sku Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function skus() {
        return $this->belongsToMany(Sku::class);
    }

    /**
     * Goods Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goods() {
        return $this->belongsTo(Goods::class);
    }

    public function setState($state) {
        $this->state = $state;
        $this->save();
    }
}
