<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Market extends Model
{
    protected $connection = 'fleapop_w';
    protected $table      = 'market_tbl';
    protected $primaryKey = 'idx';

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';
    const DELETED_AT = 'delete_date';
}
