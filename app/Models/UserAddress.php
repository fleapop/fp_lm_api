<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserAddress extends Model
{
    //
    use SoftDeletes;
    protected $connection = 'fp_payment_w';
    protected $table = 'user_address';
    protected $fillable = ['user_id','address_type','name','phone','shipping_name','zip_code','address_1','address_2','msg','is_default'];
    public    $timestamps = true;

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }


    /**
    * 주소 등록
    * @ param
    * required
    * user_id : 유저 id
    * address_type : 주소이름
    * name : 수취인 이름
    * phone : 수취인 연락처
    * zip_code : 수취인 우편번호
    * address_1 : 주소1
    * address_2 : 주소2
    * msg : 배송메시지
    * is_default : 기본배송지로 등록 여부 [true, false]
    * address_id : 기본 배송지 id 신규배송지면 0
    *
    */
    static public function setAddress($data) {
        $isExists = UserAddress::where([["user_id", $data["user_id"]], ["address_type", "=", $data['address_type']]])->first();

        $addressId = 0;
        if(!isset($isExists)) {
            $addressId = UserAddress::create([
                'user_id'=>$data['user_id'],
                'address_type'=>$data['address_type'],
                'name'=>$data['name'],
                'phone'=>$data['phone'],
                'zip_code'=>$data['zip_code'],
                'address_1'=>$data['address_1'],
                'address_2'=>$data['address_2'],
                'msg'=>$data['msg'] ?? ""
            ])->id;

        } else {
            $addressId = $isExists->id;
        }


        if($data['is_default'] === true) {

            UserAddress::where("user_id", $data['user_id'])->update(['is_default' => 'false']);

            $address = UserAddress::find($addressId);
            $address->update(["is_default"=>'true']);
            $address->save();
        } else {

        }

        return $addressId;
    }


}
