<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlimtalkSendLog extends Model
{
    protected $connection = 'fp_payment_w';
    protected $guarded = [];
}
