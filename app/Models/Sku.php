<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Sku extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $appends    = [
        'option_values', 'promotion_price',
    ];
    protected $guarded    = [];

    /**
     * Goods Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goods()
    {
        return $this->belongsTo(Goods::class)->withTrashed();
    }

    /**
     * Options Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function options()
    {
        return $this->belongsToMany(Option::class)
            ->whereNull('option_sku.deleted_at')
            ->withTimestamps();
    }

    /**
     * Stock Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function stock()
    {
        return $this->hasOne(Stock::class, 'sku_id', 'id');
    }

    /**
     * Cart Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function carts()
    {
        return $this->hasMany(Cart::class, 'sku_id', 'sku_id');
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::class, 'sku_id', 'id');
    }

    /**
     * 옵션값 Accessor
     *
     * @return \Illuminate\Support\Collection
     */
    public function getOptionValuesAttribute()
    {
        return $this->options()->pluck('value', 'title');
    }

    /**
     * 할인가, 옵션별 추가금액 적용된 가격 Accessor
     *
     * @return mixed
     */
    public function getPromotionPriceAttribute()
    {
        if ($this->goods) {
            return $this->goods->promotion_price + $this->price;
        }
    }

    public function option()
    {
        return Option::find($this->option_id);
    }

    public function getOptionBySku()
    {
        return $this->options;
    }

    public function enabled()
    {
        return $this->update(['state' => 'enable']);
    }

    public function disabled()
    {
        return $this->update(['state' => 'disable']);
    }

    public function getBuyPrice($goods, $quantity = 1)
    {
        $item_goods = $goods;
        $price = $item_goods->price * $quantity;


        if (! $item_goods->use_discount) {
            $price += ($this->price * $quantity);
            return $price;
        } else {
            $promotion = $item_goods->getPromotion($item_goods->id);

            if (isset($promotion[0])) {
                $promotion = $promotion[0];

                $submit_promotion = true;
                if ($promotion->use_date) {
                    $now = Carbon::now();

                    $start_date = Carbon::createFromFormat("Y-m-d H:i:s", $promotion->start_date);
                    $end_date = Carbon::createFromFormat("Y-m-d H:i:s", $promotion->end_date);

                    if($start_date <= $now && $end_date >= $now) {
                      $submit_promotion = true;
                    } else {
                      $submit_promotion = false;
                    }

                }

                if ($submit_promotion) {
                    // FIX SW
                    if ($promotion->type == "percent") {
                        $price = (((100 - $promotion->amount) * 0.01) * ($price)) * 0.01 * 100;
                        $price += $this->price * $quantity;

                        return round($price);
                    } else {

                        $price = $price - ($promotion->amount * $quantity);
                        $price += $this->price * $quantity;

                        return $price;
                    }
                }
            } else {
                $price += ($this->price * $quantity);
                return $price;
            }
        }
    }
}
