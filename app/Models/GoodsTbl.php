<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class GoodsTbl extends Model
{
    use SoftDeletes;
    protected $connection = 'fp_payment_w';
    protected $table      = 'goods_tbl';
    protected $primaryKey = 'idx';

    const CREATED_AT = 'insert_date';
    const UPDATED_AT = 'update_date';
    const DELETED_AT = 'delete_date';
    protected $appends = ['goodsImage', 'optionValue'];

    private $imgPath = "https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/";



    public function getGoodsImageAttribute() {


        if(!isset($this->image) && isset($this->ref_idx)) {
            $ref = GoodsTbl::find($this->ref_idx);
            $image = $ref->image;
        } else {
            $image = $this->image;
        }

        if(is_numeric($image)) {
            return $this->imgPath."thumb/".$image.".png";
        } else {
            return $this->imgPath.$image;

        }
    }

    public function getOptionValueAttribute() {
        $optionValue = "";

        if(isset($this->option_value1)) {
            $optionValue .= $this->option_value1;
        }

        if(isset($this->option_value2)) {
            $optionValue .= " ".$this->option_value2;
        }

        return $optionValue;
    }


}
