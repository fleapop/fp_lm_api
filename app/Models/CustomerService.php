<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * 상품 문의 관련 Model Class
 *
 * @package App
 */
class CustomerService extends Model
{
    use SoftDeletes;

    protected $guarded = [];
    protected $connection = 'fp_payment_w';
    protected $casts      = ['is_secret' => 'boolean'];

    /**
     * User Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Gods Model Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goods()
    {
        return $this->belongsTo(Goods::class)->withTrashed();
    }
}
