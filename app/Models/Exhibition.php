<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use James\Sortable\SortableTrait;

class Exhibition extends Model
{
    use SoftDeletes;
    use SortableTrait;

    protected $connection = 'fp_payment_w';
    protected $table      = 'exhibition';
    protected $primaryKey = 'id';
    protected $guarded    = [];

    public $sortable = [
        'sort_field'         => 'view_order',
        'sort_when_creating' => true,
    ];

    public function exhibitionGoods()
    {
        return $this->hasMany(ExhibitionGoods::class, 'exhibition_id');
    }

    public function appTopMenu()
    {
        return $this->hasOne(AppTopMenu::class);
    }

    public function exhibitionSellers()
    {
        return $this->hasMany(ExhibitionSeller::class);
    }

    public function addSeller($sellers)
    {
        foreach ($sellers as $seller) {
            ExhibitionSeller::updateOrCreate([
                'exhibition_id' => $this->id,
                'seller_id'     => $seller,
            ]);
        }
    }

    public function addAllSeller()
    {
        $sellers = Seller::where([
            ['idx', '>=', 325],     // 7D1B 행사 이후 셀러만 조회
            ['idx', '!=', 336],     // 셀러앱 테스트용 제외
        ])->pluck('idx')->toArray();

        $this->addSeller($sellers);
    }

    public function scopeNotClosed($query, $search)
    {
        return $query->where([
            ['end_date', '>', now()],
            ['title', 'LIKE', "%$search%"],
        ]);
    }

    public function scopeParticipatingSeller($query)
    {
        return ExhibitionSeller::where('exhibition_id', $this->id)
            ->with('seller', 'seller.cashiers')
            ->orderBy('approval_state', 'DESC')
            ->orderBy('updated_at', 'DESC');
    }

    public function scopeApplicableBySeller($query, $seller)
    {
        return $query->where('start_date', '>', now())
            ->whereHas('exhibitionSellers', function ($query) use ($seller) {
                $query->where('seller_id', $seller);
            });
    }

    public function scopeAppliedBySeller($query, $seller)
    {
        return $query->whereHas('exhibitionSellers', function ($query) use ($seller) {
            $query->where([['seller_id', $seller], ['approval_state', '!=', 0]]);
        });
    }
}
