<?php


namespace App\Http\dao;


use Illuminate\Support\Facades\Log;

class CCommunityDAO extends CBaseDAO
{
  public function communityReviewFirstCheck ($orderDetailId) {
    $sql = "select count(*) as cnt
            from fp_db.cm_timeline ct 
            inner join fp_db.postscript_tbl pt on ct.idx = pt.service_idx and ct.deleted_at is null and ct.claim_deleted_at is null
            inner join fp_pay.order_detail od on pt.order_detail_id = od.id 
            where od.goods_id in (select goods_id from fp_pay.order_detail od where id = {$orderDetailId}) ";

    $res = $this->selectQuery($sql, "db");
    return $res[0]->cnt;
  }
}