<?php

namespace App\Http\dao;

use DB;
use Facade\Ignition\Support\Packagist\Package;

class CBaseDAO
{
    private $conn;
    public $conn_w;
    private $pconn;
    private $pconn_w;

    public function __construct()
    {
        $this->conn = DB::connection(DB_DATABASE_R);
        $this->conn_w = DB::connection(DB_DATABASE_W);
        $this->pconn = DB::connection(DB_PAYMENT_R);
        $this->pconn_w = DB::connection(DB_PAYMENT_W);
    }

    function dbConnect($connect) {
        $res = null;
        if ($connect == null) {
            $res = $this->conn;
        } else {
            switch ($connect) {
                case "db":
                    $res = $this->conn;
                    break;
                case "pay":
                    $res = $this->pconn;
                    break;
                case "db_w":
                    $res = $this->conn_w;
                    break;
                case "pay_w":
                    $res = $this->pconn_w;
                    break;
            }
        }
        return $res;
    }

    function getLimit($page, $perPage)
    {
        $limit_srt = '';

        if ($page && $perPage) {
            $from = ($page - 1) * $perPage;
            $limit_srt = " LIMIT $from, $perPage";
        }

        return $limit_srt;
    }

    public function selectQuery($query, $connect = null)
    {
        $res = $this->dbConnect($connect)->select($query);

        return $res;
    }

    public function insertQuery($table, $hash, $connect = null, $ignore = false) {
        if (!$connect) $connect = $this->conn_w;
        if ($table == NULL || !is_array($hash)) return false;
        $fields = $values = array();

        foreach ($hash as $field => $value) {
            $nq = false;
            if (substr($field, 0, 4) == '_NQ_') {
                $fields[] = substr($field, 4);
                $nq = true;
            } else {
                $fields[] = $field;
            }

            $values[] = $nq ? $value : "'" . addslashes($value) . "'";
        }

        $fields = implode(", ", $fields);
        $values = implode(", ", $values);

        if($ignore) {
          $query = "INSERT IGNORE INTO $table ($fields) VALUES ($values)";
        } else {
          $query = "INSERT INTO $table ($fields) VALUES ($values)";
        }
        $this->dbConnect($connect)->insert($query);
        $result = $this->dbConnect($connect)->getPdo()->lastInsertId();


        return $result;
    }

    public function insertMultiQuery($table, $hash, $connect = null)
    {
        if (!$connect) $connect = $this->conn_w;
        if (!$hash[0]) return false;
        $fo = $hash[0];
        foreach ($fo as $field => $val) $fields[] = $field;
        foreach ($hash as $obj) {
            $value = [];
            foreach ($fields as $field) {
                $value[] = $obj[$field];
            }
            $values[] = $value;
        }

        $fieldStr = implode(", ", $fields);
        $queryStr = "INSERT INTO $table ($fieldStr) VALUES ";
        foreach ($values as $key => $value) {
            if ($key != 0) $queryStr .= ",";
            $queryStr .= "('" . implode("','", $value) . "')";
        }

        $res = $this->dbConnect($connect)->insert($queryStr);
        return $res;
    }


    public function deleteQuery($table, $where, $connect = null)
    {
        if (!$where) return false;
        if (!$connect) $connect = $this->conn_w;

        $where = " WHERE $where";
        $res = $this->dbConnect($connect)->delete("DELETE FROM $table $where");
        return $res;
    }

    public function updateQuery($table, $hash, $where, $connect = null, $isUpdateNull=false)
    {
        if ($where == NULL || $table == NULL || !is_array($hash)) return false;
        if (!$connect) $connect = $this->conn_w;
        $where = " WHERE $where";
        $fields_values = array();
        foreach ($hash as $field => $value) {
            $nq = false;
            if (substr($field, 0, 4) == '_NQ_') {
                $field = substr($field, 4);
                $nq = true;
            }

            if (is_array($value)) {
                $nq = true;
                $value = $value[0];
            }

            $updateNull = false;
            if ($value == "null" && $isUpdateNull){
                $updateNull = true;
            }
            if($updateNull) {
                $value_q = $value;
            } else {
                $value_q = $nq ? $value : "'" . addslashes($value) . "'";
            }
            $fields_values[] = "$field = $value_q";
        }
        $fields_values = implode(", ", $fields_values);

        $res = $this->dbConnect($connect)->update("UPDATE $table SET $fields_values$where");

        return $res;
    }

    public function returnRow($val) {
      if(count($val) <= 0) {
        return \GuzzleHttp\json_decode("{}");
      } else {
        return $val[0];
      }
    }

}
