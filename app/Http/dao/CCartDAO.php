<?php


namespace App\Http\dao;


class CCartDAO extends CBaseDAO
{
  public function getCartCount($user_id, $non_user_id) {
    $where = "where c.deleted_at is null";
    $isUser = false;

    if ($user_id != 0) {
      $where .= " and c.user_id = $user_id ";
      $isUser = true;
    }
//    dd($user_id);
    if ($user_id || $non_user_id) {
      if ($isUser == false) {
        if ($non_user_id != 0 || strlen($non_user_id) > 1) {
          $where .= " and c.non_user_id = '$non_user_id' ";
        }
      }


      $sql = "select count(c.id) as cnt
            from fp_pay.cart c
            inner join fp_pay.goods g on g.id = c.goods_id and g.deleted_at is null and g.display_state = 'show'
            inner join fp_pay.skus s  on c.sku_id = s.id and s.deleted_at is null and s.state = 'enable'
            $where";

      $res = $this->selectQuery($sql, "pay")[0]->cnt;
      return $res;
    } else {
      return 0;
    }


  }


  public function updateCart($cart_id, $quantity)
  {
    $pt = array(
      "quantity" => $quantity
    );
    $this->updateQuery("cart", $pt,"id = '{$cart_id}'", "pay_w");

    return true;
  }

  public function delCart($cart_ids)
  {
    $pt = array(
      "_NQ_deleted_at" => 'now()'
    );
    $this->updateQuery("cart", $pt,"id in ({$cart_ids})", "pay_w");
//    $this->deleteQuery("cart", "id in ({$cart_ids})", "pay_w");
    return true;
  }

  public function getCartWhere($where) {
    $res = $this->selectQuery("select * from cart ".$where ,"pay");
    return $res;
  }
}