<?php


namespace App\Http\dao;


use App\Helper\CExhibitionHelper;

class CExhibitionDAO extends CBaseDAO
{
  public function getExhibitionToday()
  {
    $sql = "select g.id , g.title, g.price
                    , IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0) AS promotion_price
                    ,g.use_discount ,g.imgs ,g.stock_state ,g.view , st.name as seller_name, st.idx as seller_id
                    , gp.ended_at as hotdeal_end
                    , g.like_count
                from fp_pay.goods g
                inner join fp_pay.goods_promotions gp
                    on g.id = gp.goods_id
                    and gp.started_at <= now()
                    and gp.ended_at >= now()
                    and date_format(gp.ended_at, '%Y-%m-%d') = date_format(now(), '%Y-%m-%d')
                    and gp.display_state = 'show'
                left join fp_db.seller_tbl st on g.seller_id = st.idx
                where g.display_state = 'show' and g.deleted_at is null and g.stock_state = 'sale' and st.display_state = 'show' and st.delete_date is null
                order by gp.display_order, gp.ended_at desc
                ";
    $res = $this->selectQuery($sql ,"pay");
    return $res;
  }

  public function getExhibitionTab()
  {
    $erSel = "";
    $erJoin = "";
    $exhibitionRep = new CExhibitionHelper();
    $erRes = $exhibitionRep->getExhibitionRepMenu()['result'];
    if ($erRes) {
      $erSel = " , '' as exhibition_name";
//      $erSel = " , atm_tbl.exhibition_name";
//      $erJoin = "left join (select e.list_tag_name  as exhibition_name,eg.goods_id from exhibition e
//                          inner join exhibition_goods eg on e.id = eg.exhibition_id and eg.deleted_at is null
//                          where e.deleted_at is null and e.list_tag_name is not null and (e.start_date <= now() and e.end_date >= now()) and e.type = 'exhibition'
//                          ) as atm_tbl on g.id = atm_tbl.goods_id";
    }
    $sql1 = "select g.id , g.title, g.price
                    , IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0) AS promotion_price
                    ,g.use_discount ,g.imgs ,g.stock_state ,g.view , st.name as seller_name, st.idx as seller_id
                    , gp.ended_at as hotdeal_end
                    , g.like_count
                    , IF(gp.idx is null, 0, 1) as is_hotdeal
                from fp_pay.goods g
                inner join fp_pay.goods_promotions gp
                    on g.id = gp.goods_id
                    and gp.started_at <= now()
                    and gp.ended_at >= now()
                    and date_format(gp.ended_at, '%Y-%m-%d') = date_format(now(), '%Y-%m-%d')
                    and gp.display_state = 'show'
                left join fp_db.seller_tbl st on g.seller_id = st.idx
                where g.display_state = 'show' and g.deleted_at is null and g.stock_state = 'sale' and st.display_state = 'show' and st.delete_date is null
                order by gp.display_order, gp.ended_at desc
                ";

    $sql2 = "select *
                from fp_pay.exhibition e
                where e.is_hidden = 'true'
                and e.deleted_at is null
                and (start_date <= now() and end_date >= now())
                order by view_order DESC, id desc
                limit 3
                ";

    $sql3 = "select g.id , g.title, g.price
                    , IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0) AS promotion_price
                    ,g.use_discount ,g.imgs ,g.stock_state ,g.view , st.name as seller_name, st.idx as seller_id
                    , gp.ended_at as hotdeal_end
                    , g.like_count
                    {$erSel}
                from fp_pay.goods g
                inner join fp_pay.goods_promotions gp
                    on g.id = gp.goods_id
                    and gp.started_at <= now()
                    and gp.ended_at >= now()
                    and gp.display_state = 'show'
                left join fp_db.seller_tbl st on g.seller_id = st.idx
                {$erJoin}
                where g.display_state = 'show' and g.deleted_at is null and g.stock_state = 'sale' and st.display_state = 'show' and st.delete_date is null
                order by gp.ended_at asc  ";

    $Res1 = $this->selectQuery($sql1 ,"pay");
    $Res2 = $this->selectQuery($sql2 ,"pay");
    $Res3 = $this->selectQuery($sql3 ,"pay");

    $result = [
      'today'      => $Res1,
      'exhibition' => $Res2,
      'hotdeal'    => $Res3,
    ];

    return $result;
  }

  public function getExhibitionGoodsList($id,$page, $perPage, $sort = "like") {
    $res_exhibition = null;
//    if ($sort == "price") {
//      $order = " order by promotion_price";
//    } else if ($sort == "like" || $sort == "popular") {
//      $order = " order by g.like_count desc ";
//    } else {
//      $order = " order by `view` desc ";
//    }

    if ($sort == "price") {
      $order = " order by master.promotion_price";
    } else if ($sort == "like" || $sort == "popular") {
      $order = " order by master.first_order asc, master.score desc, master.score2 desc ";
    } else {
      $order = " order by master.id desc ";
    }

    $limit = $this->getLimit($page, $perPage);

    $erSel = "";
    $erJoin = "";
    $erMasterSel = "";
    $tiemWhere = 72;
    $tiemSubWhere = " AND order_detail.created_at >= DATE_ADD(NOW(), INTERVAL -72 hour)";
    $tiemSecWhere = " AND (order_detail.created_at < DATE_ADD(NOW(), INTERVAL -72 hour) or order_detail.created_at is null)";
    $exhibitionRep = new CExhibitionHelper();
    $erRes = $exhibitionRep->getExhibitionRepMenu($id)['result'];
    $sellerException = "";
    if ($id == 102) $sellerException = "and g.seller_id not in (450,448,449)";
    if ($erRes) {
      $tiemWhere = 2;
      $tiemSubWhere = " AND order_detail.created_at < DATE_ADD(NOW(), INTERVAL -2 hour) and order_detail.created_at >= DATE_ADD(NOW(), INTERVAL -336 hour)";
      $tiemSecWhere = " AND (order_detail.created_at < DATE_ADD(NOW(), INTERVAL -336 hour) or order_detail.created_at is null) ";
      $erSel = ", atm_tbl.exhibition_name";
      $erMasterSel = ", exhibition_name";
      $erJoin = "left join (select sub.list_tag_name  as exhibition_name,eg.goods_id from exhibition_goods eg
  inner join (
select e.id, e.list_tag_name
from app_top_menu atm
inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true' and e.id = {$id} and e.type = 'exhibition'
where atm.display = 'show' and e.is_hidden = 'true' and e.deleted_at is null and (e.start_date <= now() and e.end_date >= now())
order by e.id desc limit 1
) as sub on eg.exhibition_id = sub.id and eg.deleted_at is null group by eg.goods_id) as atm_tbl on g.id = atm_tbl.goods_id";
    }

    $sql = "select first_order, id, seller_id,  seller_name, title, price, use_discount, imgs, stock_state, `view`, promotion_price, seller_idx, is_new, is_hotdeal, hotdeal_end, like_count, sum(score) as score, sum(score2) as score2 {$erMasterSel}
                from (
                    select 1 as first_order, g.id, g.seller_id, st.name as seller_name , g.title ,g.price ,g.use_discount ,g.imgs , g.stock_state , g.`view`
                    , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                    , st.idx as seller_idx
                    , IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                    , IF(sp.idx is null, 0, 1) as is_hotdeal
                    , sp.ended_at as hotdeal_end
                    , g.like_count
                    , IFNULL((IFNULL(SUM(order_detail.quantity),0) * 0.70 + IFNULL(SUM(order_detail.total_price),0) * 0.15 + IFNULL(SUM(g.view),0) * 0.15),0) AS score
                    , 0 AS score2
                    {$erSel}
                    from fp_pay.goods g
                    inner join exhibition_goods eg on eg.exhibition_id = {$id} and eg.goods_id = g.id
                    left join fp_db.seller_tbl st on st.idx = g.seller_id
                    left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                    LEFT JOIN order_detail ON order_detail.goods_id = g.id and state not in ('결제대기', '취소완료', '반품완료')
                    {$erJoin}
                    where eg.exhibition_id = {$id} and g.display_state = 'show' and g.deleted_at is null
                    and g.deleted_at is null and eg.deleted_at is null
                    and st.display_state = 'show' and st.delete_date is null
                    AND order_detail.created_at >= DATE_ADD(NOW(), INTERVAL -{$tiemWhere} hour)
                    {$sellerException}
                    group by g.id
                    union all
                    select 2 as first_order, g.id, g.seller_id, st.name as seller_name , g.title ,g.price ,g.use_discount ,g.imgs , g.stock_state , g.`view`
                    , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                    , st.idx as seller_idx
                    , IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                    , IF(sp.idx is null, 0, 1) as is_hotdeal
                    , sp.ended_at as hotdeal_end
                    , g.like_count
                    , 0 as score
                    , IFNULL((IFNULL(SUM(order_detail.quantity),0) * 0.70 + IFNULL(SUM(order_detail.total_price),0) * 0.15 + IFNULL(SUM(g.view),0) * 0.15),0) AS score2
                    {$erSel}
                    from fp_pay.goods g
                    inner join exhibition_goods eg on eg.exhibition_id = {$id} and eg.goods_id = g.id
                    left join fp_db.seller_tbl st on st.idx = g.seller_id
                    left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                    LEFT JOIN order_detail ON order_detail.goods_id = g.id and state not in ('결제대기', '취소완료', '반품완료')
                    {$erJoin}
                    where eg.exhibition_id = {$id} and g.display_state = 'show' and g.deleted_at is null
                    and g.deleted_at is null and eg.deleted_at is null
                    and st.display_state = 'show' and st.delete_date is null
                    {$tiemSubWhere}
                    {$sellerException}
                    group by g.id
                    union all
                    select 2 as first_order, g.id, g.seller_id, st.name as seller_name , g.title ,g.price ,g.use_discount ,g.imgs , g.stock_state , g.`view`
                    , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                    , st.idx as seller_idx
                    , IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                    , IF(sp.idx is null, 0, 1) as is_hotdeal
                    , sp.ended_at as hotdeal_end
                    , g.like_count
                    , 0 as score
                    , IFNULL((IFNULL(SUM(order_detail.quantity),0) * 0.70 + IFNULL(SUM(order_detail.total_price),0) * 0.15 + IFNULL(SUM(g.view),0) * 0.15),0) AS score2
                    {$erSel}
                    from fp_pay.goods g
                    inner join exhibition_goods eg on eg.exhibition_id = {$id} and eg.goods_id = g.id
                    left join fp_db.seller_tbl st on st.idx = g.seller_id
                    left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                    LEFT JOIN order_detail ON order_detail.goods_id = g.id and state not in ('결제대기', '취소완료', '반품완료')
                    {$erJoin}
                    where eg.exhibition_id = {$id} and g.display_state = 'show' and g.deleted_at is null
                    and g.deleted_at is null and eg.deleted_at is null
                    and st.display_state = 'show' and st.delete_date is null
                    {$tiemSecWhere}
                    {$sellerException}
                    group by g.id
                ) as master
                group by master.id
                {$order}
                {$limit}";
    $Res = $this->selectQuery($sql ,"pay");

    $sqlCnt = "select master.id
                    from (
                    select 1 as first_order, g.id, g.seller_id, st.name as seller_name , g.title ,g.price ,g.use_discount ,g.imgs , g.stock_state , g.`view`
                    , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                    , st.idx as seller_idx
                    , IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                    , IF(sp.idx is null, 0, 1) as is_hotdeal
                    , sp.ended_at as hotdeal_end
                    , g.like_count
                    , IFNULL((IFNULL(SUM(order_detail.quantity),0) * 0.70 + IFNULL(SUM(order_detail.total_price),0) * 0.15 + IFNULL(SUM(g.view),0) * 0.15),0) AS score
                    , 0 AS score2
                    {$erSel}
                    from fp_pay.goods g
                    inner join exhibition_goods eg on eg.exhibition_id = {$id} and eg.goods_id = g.id
                    left join fp_db.seller_tbl st on st.idx = g.seller_id
                    left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                    LEFT JOIN order_detail ON order_detail.goods_id = g.id and state not in ('결제대기', '취소완료', '반품완료')
                    {$erJoin}
                    where eg.exhibition_id = {$id} and g.display_state = 'show' and g.deleted_at is null
                    and g.deleted_at is null and eg.deleted_at is null
                    and st.display_state = 'show' and st.delete_date is null
                    AND order_detail.created_at >= DATE_ADD(NOW(), INTERVAL -{$tiemWhere} hour)
                    {$sellerException}
                    group by g.id
                    union all
                    select 2 as first_order, g.id, g.seller_id, st.name as seller_name , g.title ,g.price ,g.use_discount ,g.imgs , g.stock_state , g.`view`
                    , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                    , st.idx as seller_idx
                    , IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                    , IF(sp.idx is null, 0, 1) as is_hotdeal
                    , sp.ended_at as hotdeal_end
                    , g.like_count
                    , 0 as score
                    , IFNULL((IFNULL(SUM(order_detail.quantity),0) * 0.70 + IFNULL(SUM(order_detail.total_price),0) * 0.15 + IFNULL(SUM(g.view),0) * 0.15),0) AS score2
                    {$erSel}
                    from fp_pay.goods g
                    inner join exhibition_goods eg on eg.exhibition_id = {$id} and eg.goods_id = g.id
                    left join fp_db.seller_tbl st on st.idx = g.seller_id
                    left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                    LEFT JOIN order_detail ON order_detail.goods_id = g.id and state not in ('결제대기', '취소완료', '반품완료')
                    {$erJoin}
                    where eg.exhibition_id = {$id} and g.display_state = 'show' and g.deleted_at is null
                    and g.deleted_at is null and eg.deleted_at is null
                    and st.display_state = 'show' and st.delete_date is null
                    {$tiemSubWhere}
                    {$sellerException}
                    group by g.id
                    union all
                    select 2 as first_order, g.id, g.seller_id, st.name as seller_name , g.title ,g.price ,g.use_discount ,g.imgs , g.stock_state , g.`view`
                    , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                    , st.idx as seller_idx
                    , IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                    , IF(sp.idx is null, 0, 1) as is_hotdeal
                    , sp.ended_at as hotdeal_end
                    , g.like_count
                    , 0 as score
                    , IFNULL((IFNULL(SUM(order_detail.quantity),0) * 0.70 + IFNULL(SUM(order_detail.total_price),0) * 0.15 + IFNULL(SUM(g.view),0) * 0.15),0) AS score2
                    {$erSel}
                    from fp_pay.goods g
                    inner join exhibition_goods eg on eg.exhibition_id = {$id} and eg.goods_id = g.id
                    left join fp_db.seller_tbl st on st.idx = g.seller_id
                    left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                    LEFT JOIN order_detail ON order_detail.goods_id = g.id and state not in ('결제대기', '취소완료', '반품완료')
                    {$erJoin}
                    where eg.exhibition_id = {$id} and g.display_state = 'show' and g.deleted_at is null
                    and g.deleted_at is null and eg.deleted_at is null
                    and st.display_state = 'show' and st.delete_date is null
                    {$tiemSecWhere}
                    {$sellerException}
                    group by g.id
                ) as master
                group by master.id
                   ";
    $ResCnt = count($this->selectQuery($sqlCnt ,"pay"));

    $resTemp = $this->getExhibition($id);
    $exhibition["data"] = isset($resTemp[0]) ? $resTemp[0] : json_decode("{}");

    $exhibition["goods_count"] = $ResCnt;
    $data = [
      "list" => $Res,
      "cnt"      => $ResCnt,
      "profile" =>  $exhibition
    ];

    return $data;
  }

  public function getExhibition($id) {
    $sql = "select * from exhibition where id = {$id}";
    $res = $this->selectQuery($sql, "pay");
    return $res;
  }
}