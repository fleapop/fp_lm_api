<?php


namespace App\Http\dao;

use App\Helper\CExhibitionHelper;
use DB;


class CStoreDAO extends CBaseDAO
{

    public function getStoreList ($page, $perPage) {
        $limit = $this->getLimit($page, $perPage);
        $res = array();
        $erSel = "";
        $erJoin = "";
        $exhibitionRep = new CExhibitionHelper();
        $erRes = $exhibitionRep->getExhibitionRepMenu()['result'];
        if ($erRes) {
          $erSel = " , (select sub.list_tag_name  as exhibition_name from exhibition_goods eg
  inner join (
select e.id, e.list_tag_name
from app_top_menu atm
inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true'
where atm.display = 'show' and e.is_hidden = 'true' and e.deleted_at is null and (e.start_date <= now() and e.end_date >= now())
) as sub on eg.exhibition_id = sub.id and eg.deleted_at is null where eg.goods_id = master.goods_idx order by sub.id desc limit 1 ) as exhibition_name";
          $erJoin = "left join (select sub.list_tag_name  as exhibition_name,eg.goods_id from exhibition_goods eg
  inner join (
select e.id, e.list_tag_name
from app_top_menu atm
inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true' and e.type = 'exhibition'
where atm.display = 'show' and e.is_hidden = 'true' and e.deleted_at is null and (e.start_date <= now() and e.end_date >= now())
order by e.id asc limit 1
) as sub on eg.exhibition_id = sub.id and eg.deleted_at is null) as atm_tbl on master.goods_idx = atm_tbl.goods_id";
        }

      $sql = "select DISTINCT goods_idx as id, title,price
                    , if(use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = master.goods_idx and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), price) AS promotion_price
                , use_discount ,imgs ,stock_state ,view_count as `view` , st.name as seller_name, st.idx as seller_id
                ,is_new, is_hotdeal, master.like_count, hotdeal_end {$erSel}
                from (
                    SELECT 2 as rank_order, 1 as orders, GD.id as goods_idx, title ,cast(`view` as unsigned) as view_count ,GD.seller_id, GD.price ,GD.use_discount,imgs ,stock_state
                    ,IF(TIMESTAMPDIFF(hour,GD.created_at, now()) <= 48, 1, 0) as is_new
                    , IF(sp.idx is null, 0, 1) as is_hotdeal
                    , sp.ended_at as hotdeal_end
                    , GD.like_count
                    , IF(sp.idx is null, 0, GD.price - sp.discount_amount) as hotdeal_price
                    , count(lt.idx) as latest_like_count
                    FROM goods GD
                    left join goods_sort_orders GSR on GD.id = GSR.goods_id
                    left join goods_promotions sp on sp.goods_id = GD.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                    left join fp_db.like_tbl lt on lt.table_name = 'goods' and GD.id = lt.table_idx and TIMESTAMPDIFF(hour,lt.insert_date, now()) <= 48
                    where GSR.goods_id is null
                    and GD.display_state = 'show'
                    and GD.deleted_at is null
                    and GD.stock_state = 'sale'
                    and GD.seller_id != 431
                    group by GD.id
                    union all
                    SELECT 1 as rank_order, SGR.`order` as orders, SGR.goods_id as goods_idx, GDS.title as title ,cast(GDS.`view` as unsigned) as view_count ,GDS.seller_id, GDS.price ,GDS.use_discount,imgs ,stock_state
                    , IF(TIMESTAMPDIFF(hour,GDS.created_at, now()) <= 48, 1, 0) as is_new
                    , IF(sp.idx is null, 0, 1) as is_hotdeal
                    , sp.ended_at as hotdeal_end
                    , GDS.like_count
                    , IF(sp.idx is null, 0, GDS.price - sp.discount_amount) as hotdeal_price
                    , count(lt.idx) as latest_like_count
                    FROM goods_sort_orders SGR
                    inner join goods GDS on SGR.goods_id = GDS.id  and GDS.display_state = 'show' and GDS.deleted_at is null and GDS.stock_state = 'sale'
                    left join fp_db.seller_tbl st on GDS.seller_id = st.idx
                    left join goods_promotions sp on sp.goods_id = GDS.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                    left join fp_db.like_tbl lt on lt.table_name = 'goods' and GDS.id = lt.table_idx and TIMESTAMPDIFF(hour,lt.insert_date, now()) <= 48
                    where st.display_state = 'show' and st.delete_date is null and GDS.seller_id != 431
                    group by SGR.goods_id
                    order by rank_order asc, orders asc, latest_like_count desc, view_count desc
                ) as master
                left join fp_db.seller_tbl as st on master.seller_id = st.idx
                left join fp_pay.promotions as p on master.goods_idx = p.goods_id and p.id = (select id from fp_pay.promotions pp where master.goods_idx = pp.id order by pp.id desc)
                
                where st.display_state = 'show' and st.delete_date is null
                ";
        $list = $this->selectQuery($sql.$limit ,"pay");

        $cntSql = "select count(*) as cnt from (
                    SELECT 2 as rank_order, 1 as orders, GD.id as goods_idx
                    FROM goods GD
                    left join goods_sort_orders GSR on GD.id = GSR.goods_id
                    left join fp_db.seller_tbl st on GD.seller_id = st.idx
                    where GSR.goods_id is null
                    and GD.display_state = 'show'
                    and GD.deleted_at is null
                    and GD.stock_state = 'sale'
                    and GD.seller_id != 431
                    and st.display_state = 'show' and st.delete_date is null
                    group by GD.id
                    union all
                    SELECT 1 as rank_order, SGR.`order` as orders, SGR.goods_id as goods_idx
                    FROM goods_sort_orders SGR
                    inner join goods GDS on SGR.goods_id = GDS.id  and GDS.display_state = 'show' and GDS.deleted_at is null and GDS.stock_state = 'sale'
                    left join fp_db.seller_tbl st on GDS.seller_id = st.idx
                    where st.display_state = 'show' and st.delete_date is null and GDS.seller_id != 431
                    group by SGR.goods_id
                 ) as master";
        $cnt = $this->selectQuery($cntSql ,"pay")[0]->cnt;

        $res = array(
          "list" => $list,
          "cnt" => $cnt
        );
        return $res;
    }

    public function getGoodsDetail ($goodsId, $userIdx) {

        $erSel = "";
        $erJoin = "";
        $exhibitionRep = new CExhibitionHelper();
        $erRes = $exhibitionRep->getExhibitionRepMenu()['result'];
        if ($erRes) {
          $erSel = " , atm_tbl.exhibition_name";
          $erJoin = "left join (select sub.list_tag_name  as exhibition_name,eg.goods_id from exhibition_goods eg
                                  inner join (
                                select e.id, e.list_tag_name
                                from app_top_menu atm
                                inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true' and e.type = 'exhibition'
                                where atm.display = 'show' and e.is_hidden = 'true' and e.deleted_at is null and (e.start_date <= now() and e.end_date >= now())
                                order by e.id desc limit 1
                                ) as sub on eg.exhibition_id = sub.id and eg.deleted_at is null) as atm_tbl on g.id = atm_tbl.goods_id";
        }
        $res = array();
      $sql = "select g.id as goods_id, g.seller_id, st.name as seller_name , g.title ,g.price ,g.use_discount ,g.imgs , g.stock_state , g.`view`, g.content
                , g.like_count, case when lt.idx is null then 0 else 1 end as is_like_state, g.detail, g.main_category, g.sub_category, g.use_option
                ,st.idx as seller_idx ,st.co_name as seller_co_name,st.name as seller_name,st.company_reg_num as seller_company_reg_num,st.comm_reg_num as seller_comm_reg_num
                ,st.cs_tel as seller_cs_tel ,st.manager_mail as seller_manager_mail ,st.co_address1 as seller_co_address1 ,st.co_address2 as seller_co_address2
                ,st.default_return_address as seller_default_return_address
                ,st.introduce as seller_introduce , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image
                , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                , (select p.`type` from fp_pay.promotions p where p.goods_id = g.id order by p.id desc limit 1) as promotion_type
                , IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                , IF(gp.idx is null, 0, 1) as is_hotdeal
                , gp.ended_at as hotdeal_end
                , g.ship_price
                , if(lt2.idx is null, 0, 1) as is_seller_like
                , if(st.delivery_notice_applied_at is null, null ,st.delivery_notice_image) as delivery_notice_image 
                {$erSel}
                from fp_pay.goods g
                inner join fp_db.seller_tbl st
                    on g.seller_id = st.idx
                    and st.display_state = 'show'
                    and st.delete_date is null
                    and g.display_state = 'show'
                    and g.deleted_at is null
                left join fp_pay.goods_promotions gp on gp.goods_id = g.id and (gp.started_at <= now() and gp.ended_at >= now()) and gp.display_state = 'show'
                left join fp_db.like_tbl lt on lt.table_idx = g.id and lt.table_name = 'goods' and lt.user_idx = '$userIdx'
                left join fp_db.like_tbl lt2 on lt2.table_idx = st.idx and lt2.table_name = 'seller_tbl' and lt2.user_idx = '$userIdx'
                {$erJoin}
                where g.display_state = 'show' and g.deleted_at is null
                and g.id = {$goodsId}";

        $res = $this->selectQuery($sql ,"pay");
        if(isset($res[0])) {
          $res[0]->review_cnt = 0;
          $reviewCntSql = "select count(ct.idx) as cnt
                 from cm_timeline ct 
                 inner join postscript_tbl pt on ct.idx = pt.service_idx 
                 inner join fp_pay.order_detail od on pt.order_detail_id = od.id and od.goods_id = {$res[0]->goods_id}
                 where ct.service_type = 'postscript' and ct.deleted_at is null";
          $reviewCntRes = $this->selectQuery($reviewCntSql, "db");
          if(isset($reviewCntRes[0])) {
            $res[0]->review_cnt = $reviewCntRes[0]->cnt;
          }
          $res[0]->delivery_diff = 0;
          $deliveryCntSql = "select ifnull(sum(if(master.diff_date <= 3 ,1, 0)),0) as diff_3
                 , ifnull(sum(if(master.diff_date > 3 and master.diff_date <= 4 ,1, 0)),0) as diff_4
                 , ifnull(sum(if(master.diff_date > 4 and master.diff_date <= 6 ,1, 0)),0) as diff_5
                 , ifnull(sum(if(master.diff_date >= 7 ,1, 0)),0) as diff_7
                 from (
	                 select DATEDIFF(d.delivered_at ,p.updated_at) as diff_date
	                 from order_detail od 
	                 inner join delivery d on od.id = d.detail_id 
	                 inner join payment p on od.order_id = p.order_id and p.state = '결제완료'
	                 where od.goods_id = {$res[0]->goods_id} and d.state = '배송완료' and d.delivered_at is not null
                 ) as master";
          $deliveryCntRes = $this->selectQuery($deliveryCntSql, "pay");
          if(isset($deliveryCntRes[0])) {
            $res[0]->delivery_diff = $deliveryCntRes[0];
            $tempDeliveryCnt = 0;
            if($tempDeliveryCnt < (int)$deliveryCntRes[0]->diff_3) {
              $tempDeliveryCnt = (int)$deliveryCntRes[0]->diff_3;
              $res[0]->delivery_diff->top_diff = "diff_3";
            }  if($tempDeliveryCnt < (int)$deliveryCntRes[0]->diff_4) {
              $tempDeliveryCnt = (int)$deliveryCntRes[0]->diff_4;
              $res[0]->delivery_diff->top_diff = "diff_4";
            }  if($tempDeliveryCnt < (int)$deliveryCntRes[0]->diff_5) {
              $tempDeliveryCnt = (int)$deliveryCntRes[0]->diff_5;
              $res[0]->delivery_diff->top_diff = "diff_5";
            }  if($tempDeliveryCnt < (int)$deliveryCntRes[0]->diff_7) {
              $res[0]->delivery_diff->top_diff = "diff_7";
            }
          }
          $optionSql = "select title from `options` o inner join option_sku os on o.id = os.option_id inner join skus s on os.sku_id = s.id
                where s.goods_id = {$res[0]->goods_id} and s.state = 'enable' and s.deleted_at is null and o.value is not null and o.value != ''
                and o.deleted_at is null and os.deleted_at is null
                group by title
                order by o.id asc";
          $res_option = $this->selectQuery($optionSql, "pay");
          $res[0]->option_titles = $res_option;


          $imageSql = "select `path` from fp_pay.images where imageable_id = {$res[0]->goods_id} and imageable_type  = 'App\\\Goods' order by `order`";
          $imageRes = $this->selectQuery($imageSql, "pay");
          $res[0]->images = $imageRes;
          $mainImg = array(
            "path" => $res[0]->imgs
          );
          array_unshift($res[0]->images, $mainImg);
        }
        return $res;
    }

    public function getNewList ($page, $perPage, $user_id) {
      $limit = $this->getLimit($page, $perPage);

      $erSel = "";
      $erJoin = "";
      $exhibitionRep = new CExhibitionHelper();
      $erRes = $exhibitionRep->getExhibitionRepMenu()['result'];
      if ($erRes) {
        $erSel = " , atm_tbl.exhibition_name";
        $erJoin = "left join (select sub.list_tag_name  as exhibition_name,eg.goods_id from exhibition_goods eg
  inner join (
select e.id, e.list_tag_name
from app_top_menu atm
inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true' and e.type = 'exhibition'
where atm.display = 'show' and e.is_hidden = 'true' and e.deleted_at is null and (e.start_date <= now() and e.end_date >= now())
order by e.id desc limit 1
) as sub on eg.exhibition_id = sub.id and eg.deleted_at is null) as atm_tbl on g.id = atm_tbl.goods_id";
      }

      $bannersql = "select sum(cnt) as cnt, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',profile_img) as profile_image
                        from (
                            select count(g.id) as cnt, st.profile_img
                            from fp_db.seller_tbl st
                            inner join fp_db.like_tbl lt on lt.table_idx = st.idx and lt.table_name = 'seller_tbl' and lt.user_idx = '$user_id'
                            left join fp_pay.goods g on g.seller_id = st.idx and g.display_state = 'show' and g.deleted_at is null
                            and TIMESTAMPDIFF(hour,g.created_at, now()) <= 48
                            left join fp_db.goods_view gv on g.id = gv.goods_id and gv.user_id = '$user_id'
                            where st.display_state = 'show' and st.delete_date is null and g.id is not null and gv.idx is null
                            GROUP by st.idx
                            order by RAND()
                        ) as master";

      $bannerRes = $this->selectQuery($bannersql ,"db");

      $sql = "select g.id, g.title ,g.price
                , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                , g.use_discount , g.imgs , g.stock_state , g.`view` ,st.name as seller_name, st.idx as seller_id
                , IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                , IF(sp.idx is null, 0, 1) as is_hotdeal
                , sp.ended_at as hotdeal_end
                , g.like_count
                {$erSel}
                from fp_pay.goods g
                left join fp_db.seller_tbl st on g.seller_id = st.idx
                left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                {$erJoin}
                where g.display_state = 'show' and g.deleted_at is null
                and st.display_state = 'show' and st.delete_date is null
                and g.seller_id != 431
                order by g.created_at desc
                  {$limit}";
      $goodsRes = $this->selectQuery($sql ,"pay");

      $sqlCnt = "select count(g.id) as cnt
                from fp_pay.goods g
                left join fp_db.seller_tbl as st on g.seller_id = st.idx
                where g.display_state = 'show' and g.deleted_at is null
                and st.display_state = 'show' and st.delete_date is null
                and g.seller_id != 431
                order by g.created_at desc
                  ";
      $goodsResCnt = $this->selectQuery($sqlCnt ,"pay")[0]->cnt;

      $res = array(
        "bannerRes" => $bannerRes,
        "goodsRes" => $goodsRes,
        "goodsResCnt" => $goodsResCnt
      );
      return $res;
    }

    public function getBestList ($page, $perPage, $cateWhere) {
      $limit = $this->getLimit($page, $perPage);

      $erSel = "";
      $erJoin = "";
      $exhibitionRep = new CExhibitionHelper();
      $erRes = $exhibitionRep->getExhibitionRepMenu()['result'];
      $tiemWhere = 72;
      if ($erRes) {
        $tiemWhere = 24;
        $erSel = " , (select sub.list_tag_name  as exhibition_name from exhibition_goods eg
  inner join (
select e.id, e.list_tag_name
from app_top_menu atm
inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true'
where atm.display = 'show' and e.is_hidden = 'true' and e.deleted_at is null and (e.start_date <= now() and e.end_date >= now())
) as sub on eg.exhibition_id = sub.id and eg.deleted_at is null where eg.goods_id = master.id order by sub.id desc limit 1 ) as exhibition_name";
        $erJoin = "left join (select sub.list_tag_name  as exhibition_name,eg.goods_id from exhibition_goods eg
  inner join (
select e.id, e.list_tag_name
from app_top_menu atm
inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true' and e.type = 'exhibition'
where atm.display = 'show' and e.is_hidden = 'true' and e.deleted_at is null and (e.start_date <= now() and e.end_date >= now())
order by e.id desc limit 1
) as sub on eg.exhibition_id = sub.id and eg.deleted_at is null) as atm_tbl on master.id = atm_tbl.goods_id";
      }
      $sql = "select master.*, st.name as seller_name
                , if(use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = master.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), price) AS promotion_price
                {$erSel}
                from (
                    select goods_id as id, title, first_order, sum(score) as score, order_date ,seller_id, price ,use_discount,imgs ,stock_state,`view`, is_new, is_hotdeal, like_count, hotdeal_end, hotdeal_price
                    from (
                        SELECT 1 as first_order, goods.id as goods_id, goods.title, IFNULL((IFNULL(SUM(order_detail.quantity),0) * 0.70 + IFNULL(SUM(order_detail.total_price),0) * 0.15 + IFNULL(SUM(goods.view),0) * 0.15),0) AS score, 0 as score2, DATE_FORMAT(order_detail.created_at, '%Y-%m-%d') as order_date
                        ,goods.seller_id, goods.price ,goods.use_discount,imgs ,goods.stock_state, `view`
                        ,IF(TIMESTAMPDIFF(hour,goods.created_at, now()) <= 48, 1, 0) as is_new
                        , IF(sp.idx is null, 0, 1) as is_hotdeal
                        , sp.ended_at as hotdeal_end
                        , goods.like_count
                        , IF(sp.idx is null, 0, goods.price - sp.discount_amount) as hotdeal_price
                        FROM goods
                        LEFT JOIN order_detail ON order_detail.goods_id = goods.id and state not in ('결제대기', '취소완료', '반품완료')
                        left join goods_promotions sp on sp.goods_id = goods.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                        WHERE goods.display_state = 'show'  and goods.deleted_at is null AND order_detail.created_at >= DATE_ADD(NOW(), INTERVAL -{$tiemWhere} hour)
                        and goods.seller_id != 431
                        $cateWhere
                        GROUP BY (goods.id)
                        union all
                        SELECT 2 as first_order,  goods.id as goods_id, goods.title ,0 as score , IFNULL((IFNULL(SUM(order_detail.quantity),0) * 0.70 + IFNULL(SUM(order_detail.total_price),0) * 0.15 + IFNULL(SUM(goods.view),0) * 0.15),0) AS score2 , DATE_FORMAT(order_detail.created_at, '%Y-%m-%d') as order_date
                        ,goods.seller_id, goods.price ,goods.use_discount,imgs ,goods.stock_state, `view`
                        ,IF(TIMESTAMPDIFF(hour,goods.created_at, now()) <= 48, 1, 0) as is_new
                        , IF(sp.idx is null, 0, 1) as is_hotdeal
                        , sp.ended_at as hotdeal_end
                        , goods.like_count
                        , IF(sp.idx is null, 0, goods.price - sp.discount_amount) as hotdeal_price
                        FROM goods
                        LEFT JOIN order_detail ON order_detail.goods_id = goods.id and state not in ('결제대기', '취소완료', '반품완료')
                        left join goods_promotions sp on sp.goods_id = goods.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                        WHERE goods.display_state = 'show' and goods.deleted_at is null AND order_detail.created_at >= '2020-07-05 :00:00' AND order_detail.created_at < DATE_ADD(NOW(), INTERVAL -{$tiemWhere} hour)
                        and goods.seller_id != 431
                        $cateWhere
                        GROUP BY (goods.id)
                    ) as temp
                    group by goods_id
                    ORDER BY first_order ASC, score DESC, score2 DESC
                    {$limit}
                ) as master
                left join fp_db.seller_tbl st on master.seller_id = st.idx
            
                where st.display_state = 'show' and st.delete_date is null
                limit 300";

      $goodsRes = $this->selectQuery($sql ,"pay");

      $sqlCount = "select goods_id, title, first_order, sum(score) as score, order_date
                from (SELECT 1 as first_order, goods.id as goods_id, goods.title
                        , IFNULL((IFNULL(SUM(order_detail.quantity),0) * 0.70 + IFNULL(SUM(order_detail.total_price),0) * 0.15 + IFNULL(SUM(goods.view),0) * 0.15),0) AS score, 0 as score2
                        , DATE_FORMAT(order_detail.created_at, '%Y-%m-%d') as order_date
                        FROM goods
                        LEFT JOIN order_detail ON order_detail.goods_id = goods.id and state not in ('결제대기', '취소완료', '반품완료')
                        WHERE goods.display_state = 'show' and goods.deleted_at is null
                        AND order_detail.created_at >= '2020-08-24 :00:00'
                        and goods.seller_id != 431
                         $cateWhere
                        AND order_detail.created_at >= DATE_ADD(NOW(), INTERVAL -72 hour)
                        GROUP BY (goods.id)
                    union all
                        SELECT 2 as first_order,  goods.id as goods_id, goods.title ,0 as score
                        , IFNULL((IFNULL(SUM(order_detail.quantity),0) * 0.70 + IFNULL(SUM(order_detail.total_price),0) * 0.15 + IFNULL(SUM(goods.view),0) * 0.15),0) AS score2
                        , DATE_FORMAT(order_detail.created_at, '%Y-%m-%d') as order_date
                        FROM goods
                        LEFT JOIN order_detail ON order_detail.goods_id = goods.id and state not in ('결제대기', '취소완료', '반품완료')
                        WHERE goods.display_state = 'show' and goods.deleted_at is null
                        AND order_detail.created_at >= '2020-07-05 :00:00' AND order_detail.created_at < DATE_ADD(NOW(), INTERVAL -72 hour) and goods.seller_id != 431
                        $cateWhere
                        GROUP BY (goods.id)) as temp group by goods_id ORDER BY first_order ASC, score DESC, score2 DESC";

      $goodsResCount = count($this->selectQuery($sqlCount ,"pay"));
//      $goodsResCount = 300;
      $res = array(
        "goodsRes" => $goodsRes,
        "goodsResCount" => $goodsResCount
      );

      return $res;
    }

    public function getCategoryList ($page, $perPage, $sort, $cate) {
    $limit = $this->getLimit($page, $perPage);
      $erSel = "";
      $erJoin = "";
      $exhibitionRep = new CExhibitionHelper();
      $erRes = $exhibitionRep->getExhibitionRepMenu()['result'];
      if ($erRes) {
        $erSel = " , atm_tbl.exhibition_name";
        $erJoin = "left join (select sub.list_tag_name  as exhibition_name,eg.goods_id from exhibition_goods eg
  inner join (
select e.id, e.list_tag_name
from app_top_menu atm
inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true' and e.type = 'exhibition'
where atm.display = 'show' and e.is_hidden = 'true' and e.deleted_at is null and (e.start_date <= now() and e.end_date >= now())
order by e.id desc limit 1
) as sub on eg.exhibition_id = sub.id and eg.deleted_at is null) as atm_tbl on g.id = atm_tbl.goods_id";
      }

    $sql = "";
    switch ($sort) {
      case 'price' :
      {
        $sql = "select g.id , g.title, g.price
                        , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                        ,g.use_discount ,g.imgs ,g.stock_state ,g.view , st.name as seller_name, st.idx as seller_id
                        ,IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                        , IF(sp.idx is null, 0, 1) as is_hotdeal
                        , sp.ended_at as hotdeal_end
                        , g.like_count
                        {$erSel}
                        from goods g
                        left join fp_db.seller_tbl st on g.seller_id = st.idx
                        left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                        {$erJoin}
                        where g.display_state = 'show' and g.sub_category  = '$cate' and g.deleted_at is null
                        and st.display_state = 'show' and st.delete_date is null
                        and g.seller_id != 431
                        order by promotion_price asc ,id ";
        break;
      }

      case 'popular' :
      {
        $sql = "select g.id , g.title, g.price
                        , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0),g.price) AS promotion_price
                        ,g.use_discount ,g.imgs ,g.stock_state ,g.view , st.name as seller_name, st.idx as seller_id
                        , IFNULL((SELECT (IFNULL(SUM(order_detail.quantity),0) * 0.7 + order_detail.total_price * 0.15) FROM order_detail WHERE order_detail.goods_id = g.id),0)  + g.view * 0.15 AS score
                        ,IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                        , IF(sp.idx is null, 0, 1) as is_hotdeal
                        , sp.ended_at as hotdeal_end
                        , g.like_count
                        {$erSel}
                        from goods g
                        left join fp_db.seller_tbl st on g.seller_id = st.idx
                        left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                        {$erJoin}
                        where g.display_state = 'show' and g.sub_category  = '$cate' and g.deleted_at is null
                        and st.display_state = 'show' and st.delete_date is null
                        and g.seller_id != 431
                        order by score desc ,id ";
        break;
      }

      case 'new' :
      {

        $sql = "select g.id , g.title, g.price
                        , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                        ,g.use_discount ,g.imgs ,g.stock_state ,g.view , st.name as seller_name, st.idx as seller_id
                        ,IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                        , IF(sp.idx is null, 0, 1) as is_hotdeal
                        , sp.ended_at as hotdeal_end
                        , g.like_count
                        {$erSel}
                        from goods g
                        left join fp_db.seller_tbl st on g.seller_id = st.idx
                        left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                        {$erJoin}
                        where g.display_state = 'show' and g.sub_category  = '$cate' and g.deleted_at is null
                        and st.display_state = 'show' and st.delete_date is null
                        and g.seller_id != 431
                        order by g.created_at desc ,id ";
        break;
      }
    }
//    dd($sql);
    $goodsRes = $this->selectQuery($sql.$limit ,"pay");
    $goodsResCount = count($this->selectQuery($sql ,"pay"));

    $res = array(
      "goodsRes" => $goodsRes,
      "goodsResCount" => $goodsResCount
    );

    return $res;
  }

    public function getBrandList ($page, $perPage, $userId = '') {
      $limit = $this->getLimit($page, $perPage);

      $sql = "select st.*, master.seller_id, (master.score + (count(lt.idx) * 0.25))  as score, if(lt2.idx is null, 0, 1) as is_like
                    from (
                        SELECT goods.seller_id, (ifnull(orders.score,0)+ sum(goods.view * 0.25)) as score
                        FROM goods goods
                        left join (SELECT ifnull((SUM(od.quantity) * 0.25 + SUM(od.total_price) * 0.25), 0) as score, seller_id  FROM order_detail AS od
                                    where created_at >= '2020-06-01 00:00:00'
                                    group by seller_id) as orders on goods.seller_id = orders.seller_id
                        WHERE created_at >= '2020-06-01 00:00:00'
                        GROUP BY goods.seller_id
                    ) as master
                    left join fp_db.like_tbl lt on lt.table_idx = master.seller_id and table_name = 'seller_tbl'
                    left join fp_db.like_tbl lt2 on lt2.table_idx = master.seller_id and lt2.table_name = 'seller_tbl' and lt2.user_idx = '{$userId}'
                    inner join fp_db.seller_tbl st on st.idx = master.seller_id and st.display_state = 'show' and st.delete_date is null
                    GROUP BY master.seller_id
                    ORDER BY score DESC
             ";

      $goodsRes = $this->selectQuery($sql.$limit ,"pay");
      $goodsResCount = count($this->selectQuery($sql ,"pay"));
      $res = array(
        "goodsRes" => $goodsRes,
        "goodsResCount" => $goodsResCount
      );
      return $res;
    }

  public function getMyLikeList ($page, $perPage, $user_id) {
    $limit = $this->getLimit($page, $perPage);

    $erSel = "";
    $erJoin = "";
    $exhibitionRep = new CExhibitionHelper();
    $erRes = $exhibitionRep->getExhibitionRepMenu()['result'];
    if ($erRes) {
      $erSel = ", atm_tbl.exhibition_name";
      $erJoin = "left join (select sub.list_tag_name  as exhibition_name,eg.goods_id from exhibition_goods eg
  inner join (
select e.id, e.list_tag_name
from app_top_menu atm
inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true' and e.type = 'exhibition'
where atm.display = 'show' and e.is_hidden = 'true' and e.deleted_at is null and (e.start_date <= now() and e.end_date >= now())
order by e.id desc limit 1
) as sub on eg.exhibition_id = sub.id and eg.deleted_at is null) as atm_tbl on g.id = atm_tbl.goods_id";
    }

    $sql = "select g.id, g.seller_id, st.name as seller_name , g.title ,g.price ,g.use_discount ,g.imgs , g.stock_state , g.`view`
                , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                , IFNULL((SELECT (IFNULL(SUM(order_detail.quantity),0) * 0.35 + order_detail.total_price * 0.35 + g.view * 0.3) FROM order_detail WHERE order_detail.goods_id = g.id),0) AS score
                , g.like_count
                , IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                , IF(sp.idx is null, 0, 1) as is_hotdeal
                , sp.ended_at as hotdeal_end
                {$erSel}
                from fp_pay.goods g
                inner join fp_db.like_tbl lt on g.id = lt.table_idx and lt.table_name = 'goods' and lt.user_idx = '$user_id'
                left join fp_db.seller_tbl st on g.seller_id = st.idx
                left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                {$erJoin}
                where g.display_state = 'show' and g.deleted_at is null and st.display_state = 'show' and st.delete_date is null
                and lt.user_idx = '$user_id'
                order by lt.idx desc
             ";

    $goodsRes = $this->selectQuery($sql.$limit ,"pay");
    $goodsResCount = count($this->selectQuery($sql ,"pay"));
    $res = array(
      "goodsRes" => $goodsRes,
      "goodsResCount" => $goodsResCount
    );
    return $res;
  }

  public function goodsSearch($user_id, $sort, $keyword, $page, $perPage) {
    $limit = $this->getLimit($page, $perPage);
    $sort = $sort;
    $order = "order by score desc, g.id desc";
    switch ($sort) {
      case 'price' :
      {
        $order = "order by promotion_price asc, g.id";
        break;
      }
      case 'popular' :
      {
        $order = "order by score desc, g.id";
        break;
      }
      case 'new' :
      {
        $order = "order by g.created_at desc ,g.id";
        break;
      }
    }

    $likejoin['sel'] = $user_id ? ", case when lt.idx is null then 0 else 1 end as is_like_state" : ", 0 as is_like_state";
    $likejoin['join'] = $user_id ? "left join fp_db.like_tbl lt on lt.table_idx = g.id = lt.table_name = 'goods' and lt.user_idx = '$user_id'" : "";

    $erSel = "";
    $erJoin = "";
    $exhibitionRep = new CExhibitionHelper();
    $erRes = $exhibitionRep->getExhibitionRepMenu()['result'];
    if ($erRes) {
      $erSel = " , atm_tbl.exhibition_name";
      $erJoin = "left join (select e.list_tag_name  as exhibition_name,eg.goods_id from exhibition e
                          inner join exhibition_goods eg on e.id = eg.exhibition_id and eg.deleted_at is null
                          where e.deleted_at is null and e.list_tag_name is not null and (e.start_date <= now() and e.end_date >= now()) and e.type = 'exhibition'
                          ) as atm_tbl on g.id = atm_tbl.goods_id";
    }

    $sql = "select g.id, g.title ,g.price
            , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
            , g.use_discount , g.imgs , g.stock_state , g.`view` ,st.name as seller_name, st.idx as seller_id
            ,IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
            ,IFNULL((IFNULL(SUM(od.quantity),0) * 0.70 + IFNULL(SUM(od.total_price),0) * 0.15 + IFNULL(SUM(g.view),0) * 0.15),0) AS score
            , g.like_count
            , IF(sp.idx is null, 0, 1) as is_hotdeal
            , sp.ended_at as hotdeal_end {$likejoin['sel']} {$erSel}
            from fp_pay.goods g
            left join fp_pay.order_detail od on od.goods_id =g.id
            left join fp_db.seller_tbl st on g.seller_id = st.idx
            left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
            {$likejoin['join']} {$erJoin}
            where g.display_state = 'show' and g.deleted_at is null
            and st.display_state = 'show' and st.delete_date is null
            and (
                exists (select * from tags inner join taggables on tags.id = taggables.tag_id where g.id = taggables.taggable_id and taggables.taggable_type = 'App\\\Goods' and name LIKE '%{$keyword}%')
                or g.title LIKE '%{$keyword}%'
                or g.seller_id IN (select idx from fp_db.seller_tbl WHERE name LIKE '%{$keyword}%' and display_state = 'show')
            )
            group by g.id
            {$order}";
    $sqlCnt = "select count('1') as cnt from (" . $sql . ") as master";

    $resCnt = $this->selectQuery($sqlCnt ,"pay")[0]->cnt;
    $result = $this->selectQuery($sql.$limit ,"pay");

    $res = array(
      "list" => $result,
      "cnt" =>  $resCnt
    );
    return $res;
  }

    public function setRoas ($goodsId, $userIdx) {

        $roasSql = "    SELECT A.roas_info_idx, A.service_id, A.channel_code
                                from fp_pay.roas_visit_info A
                                    inner join fp_pay.roas_info B on A.roas_info_idx = B.idx and B.service_type = 1
                                where user_id = {$userIdx} and depth = 1 and A.insert_date >= date_add(now(), interval -1 DAY)
                                union
                                SELECT A.roas_info_idx, A.goods_id as service_id, channel_code
                                from fp_pay.roas_basket_goods A
                                    inner join fp_pay.roas_visit_info B on A.idx = B.roas_info_idx and goods_id = {$goodsId}
                                where user_id = {$userIdx} and depth = 1 and insert_date >= date_add(now(), interval -1 DAY)
                                union
                                select A.roas_info_idx, A.service_id, A.channel_code from fp_pay.roas_visit_info A
                                    inner join fp_pay.exhibition B on A.service_id = B.id and insert_date between B.start_date and B.end_date
                                    inner join fp_pay.exhibition_goods C on B.id = C.exhibition_id and C.goods_id = {$goodsId}
                                where user_id = {$userIdx} and depth = 1
                                order by roas_info_idx desc limit 1";

        $roasRes = $this->selectQuery($roasSql);

        if (count($roasRes) > 0) {
            $join_data = [
                "roas_info_idx"     => $roasRes[0]->roas_info_idx,
                "channel_code"      => $roasRes[0]->channel_code,
                "depth"             => 2,
                "user_id"           => $userIdx,
                "service_id"        => $goodsId
            ];
            $this->insertQuery("fp_pay.roas_visit_info", $join_data, "db_w");
        }
        $res = array(
            "result" => 'OK'
        );
        return $res;
    }
}
