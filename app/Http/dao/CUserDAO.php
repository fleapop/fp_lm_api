<?php


namespace App\Http\dao;

use DB;

class CUserDAO extends CBaseDAO
{
    public function getUserCheck ($type, $account, $pwd = null) {
      $where = $pwd ? " and password = '{$pwd}'" : "";
      if ($type == 1) {
        $sql = "select * from fp_db.user_list
                    where account_type = {$type} and email_account = '{$account}' {$where}
                    and delete_date is null
                    order by user_id desc
                    ";
      } else {
        $sql = "select * from fp_db.user_list
                    where account_type = {$type} and fb_account = '{$account}' {$where}
                    and delete_date is null
                    order by user_id desc
                    ";
      }
      $res = $this->selectQuery($sql, "db");
      return $res;
    }

    public function getUserInfo ($idx) {
      $sql = "select ul.*
                     , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img_full
                     , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name_full
                     , uei.introduce, uei.is_shopping_open, uei.user_height, uei.user_size, uei.user_foot_size, uei.user_skin, uei.is_body_open
                     , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name_full
                     from user_list ul left join user_extra_info uei on ul.user_id = uei.user_id where ul.user_id = '{$idx}'";
      $res = $this->selectQuery($sql, "db");
      return $res;
    }

  public function getFindPassword ($account, $phone) {

    $sql = "select * from fp_db.user_list
              where account_type = 1 and email_account = '{$account}' and shipping_phone = '{$phone}'
              and delete_date is null
              order by user_id desc
              limit 1
              ";
    $res = $this->selectQuery($sql, "db");

    return $this->returnRow($res);
  }

  public function getAccountFindPassword ($newPassword, $user_id) {

    $sql = "select * from fp_db.user_list
              where account_type = 1 and password = '{$newPassword}' and user_id = '{$user_id}'
              and delete_date is null
              order by user_id desc
              limit 1
              ";
    $res = $this->selectQuery($sql, "db");

    return $this->returnRow($res);
  }
}
