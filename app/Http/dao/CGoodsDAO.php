<?php


namespace App\Http\dao;


class CGoodsDAO extends CBaseDAO
{
  public function getGoods($goods_id)
  {
    $sqlGoods = "select * from goods where id = {$goods_id}";
    $sqlSku = "select * from skus s where goods_id = {$goods_id}";

    $resGoods = $this->selectQuery($sqlGoods ,"pay");
    $resSku = $this->selectQuery($sqlSku ,"pay");

    $res = array(
      "goods" =>  $resGoods,
      "skus" =>  $resSku
    );
    return $res;
  }
}