<?php


namespace App\Http\dao;


use App\Helper\CExhibitionHelper;
use Illuminate\Support\Facades\Log;

class CSellerDAO extends CBaseDAO
{
  public function getGoodsList($user_id, $seller_id,$sort, $page, $perPage, $category, $lm_order, $exhibition_id = null) {
    $limit = $this->getLimit($page, $perPage);
    $erSel = "";
    $erJoin = "";
    $exhibitionRep = new CExhibitionHelper();
    $erRes = $exhibitionRep->getExhibitionRepMenu($exhibition_id)['result'];

    $cate = null;
    switch ($category) {
      case 'outer' :
      {
        $cate = '아우터';
        break;
      }
      case 'top' :
      {
        $cate = '상의';
        break;
      }
      case 'bottom' :
      {
        $cate = '하의';
        break;
      }
      case 'one' :
      {
        $cate = '원피스';
        break;
      }
      case 'acc' :
      {
        $cate = '악세사리';
        break;
      }
      case 'stuff' :
      {
        $cate = '잡화';
        break;
      }
      case 'beaut' :
      {
        $cate = '뷰티';
        break;
      }
      case 'digital' :
      {
        $cate = '디지털';
        break;
      }
    }
    $cateWhere = "";
    $isExhibitionId = isset($exhibition_id) ? "and e.id = {$exhibition_id}" : "";
    if($cate) $cateWhere = "and g.sub_category = '{$cate}'";
    $order = "";
//    if ($lm_order && $lm_order == "Y") $order = "sort asc,";
    if ($erRes) {
      $erSel = " , atm_tbl.exhibition_name,  if( atm_tbl.exhibition_name is null ,2,1) as sort";
      $erJoin = "left join (select sub.list_tag_name  as exhibition_name,eg.goods_id from exhibition_goods eg
  inner join (
select e.id, e.list_tag_name
from app_top_menu atm
inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true' {$isExhibitionId} and e.type = 'exhibition'
where atm.display = 'show' and e.is_hidden = 'true' and e.deleted_at is null and (e.start_date <= now() and e.end_date >= now())
order by e.id desc limit 1
) as sub on eg.exhibition_id = sub.id and eg.deleted_at is null) as atm_tbl on g.id = atm_tbl.goods_id";
      if (isset($lm_order) && $lm_order == 1) $order = "sort asc,";
    }
    $isNewJoin = "";
    $isNewOrder ="";
    $isNewSelect ="";
    if($user_id) {
      $isLikeCheck = "select * from fp_db.like_tbl lt where user_idx = {$user_id} and table_name = 'seller_tbl' and lt.table_idx = {$seller_id}";
//      $likeChkRes = count($this->conn->select($isLikeCheck));
      $likeChkRes = count($this->selectQuery($isLikeCheck ,"db"));
      if($likeChkRes > 0) {
        $isNewJoin = "left join fp_db.goods_view gv on gv.goods_id = g.id and gv.user_id = '$user_id'";
        $isNewOrder = " is_new_sort desc, ";
        $isNewSelect =", if(IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) + if(gv.idx is null, 1, 0) <2,0,1) as is_new_sort";
      }
    }
    switch($sort) {
      case 'new' :
        $sql = "select g.id, g.seller_id, st.name as seller_name , g.title ,g.price ,g.use_discount ,g.imgs , g.stock_state , g.`view`
                        , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                        , IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                        , IF(sp.idx is null, 0, 1) as is_hotdeal
                        , sp.ended_at as hotdeal_end
                        , g.like_count
                        {$erSel}
                        from goods g
                        left join fp_db.seller_tbl st on st.idx = $seller_id and g.seller_id = st.idx
                        left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                        {$erJoin}
                        where g.seller_id = {$seller_id}
                        and g.display_state = 'show'
                         and g.deleted_at is null
                         {$cateWhere}
                         group by g.id
                        order by {$order} g.created_at desc , g.id desc
                        ";
        break;
      case 'price' :
        $sql = "select g.id, g.seller_id, st.name as seller_name , g.title ,g.price ,g.use_discount ,g.imgs , g.stock_state , g.`view`
                        , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                        , IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                        , IF(sp.idx is null, 0, 1) as is_hotdeal
                        , sp.ended_at as hotdeal_end
                        , g.like_count
                        {$erSel}
                        from goods g
                        left join fp_db.seller_tbl st on st.idx = $seller_id and g.seller_id = st.idx
                        left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                        {$erJoin}
                        where g.seller_id = $seller_id
                        and g.display_state = 'show'
                         and g.deleted_at is null
                         {$cateWhere}
                         group by g.id
                        order by {$order} promotion_price
                        ";
        break;
      default:
      case 'popular' :
        $sql = "select g.id, g.seller_id, st.name as seller_name , g.title ,g.price ,g.use_discount ,g.imgs , g.stock_state , g.`view`
                        , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                        , IFNULL((SELECT (IFNULL(SUM(order_detail.quantity),0) * 0.7 + sum(order_detail.total_price) * 0.15) FROM order_detail WHERE order_detail.goods_id = g.id and order_detail.created_at >= DATE_ADD(NOW(),INTERVAL -1 MONTH ) ),0) + count(DISTINCT g.`view`) * 0.15 AS score
                        , IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                        {$isNewSelect}
                        , IF(sp.idx is null, 0, 1) as is_hotdeal
                        , sp.ended_at as hotdeal_end
                        , g.like_count
                        {$erSel}
                        from goods g
                        left join fp_db.seller_tbl st on st.idx = $seller_id and g.seller_id = st.idx
                        left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                        {$isNewJoin}
                        {$erJoin}
                        where g.seller_id = $seller_id
                        {$cateWhere}
                        and g.display_state = 'show'
                         and g.deleted_at is null
                         group by g.id
                        order by {$order} {$isNewOrder} score desc
                        ";
        break;
    }

    $res = $this->selectQuery($sql.$limit ,"pay");
    $resCnt = $this->selectQuery($sql ,"pay");

    $response = [
      "list" => $res,
      "cnt"         => count($resCnt),
    ];
    return $response;
  }

  public function getSellerRandomList ()
  {
    $sql = "select st.idx , st.name as seller_name, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image
        from fp_db.seller_tbl st
        where display_state = 'show' and delete_date is null
        and is_recommended_brand = 1
        order by rand()
                ";
    $res = $this->selectQuery($sql ,"pay");
    return $res;
  }
  public function getBrandInfo ($user_id)
  {
    $sql = "select st.idx , st.name as seller_name, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image
                from fp_db.seller_tbl st
                inner join fp_db.like_tbl lt on lt.table_idx =st.idx and lt.table_name = 'seller_tbl'
                where st.display_state = 'show' and st.delete_date is null
                and lt.table_name = 'seller_tbl'
                and lt.user_idx = '$user_id'
                ";

    $sqlList = "select @rownum:=@rownum+1 as rank_num, master.* from (
                    select  st.idx,st.name as seller_name,st.introduce
                    , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image
                    , case when lt.idx is null then 0 else 1 end as is_like_state
                    , st.like_count
                from fp_db.seller_tbl as st
                left join (
                SELECT goods.seller_id, (ifnull(orders.score,0) + sum(goods.view * 0.25) + sum(goods.like_count * 0.25)) as score
                        FROM fp_pay.goods goods
                        left join (SELECT ifnull((SUM(ifnull(od.quantity,0)) * 0.25 + SUM(ifnull(od.total_price,0)) * 0.25), 0) as score, seller_id  FROM fp_pay.order_detail AS od
                                    where created_at >= '2020-06-01 00:00:00'
                                    group by seller_id) as orders on goods.seller_id = orders.seller_id
                        WHERE created_at >= '2020-06-01 00:00:00'
                        GROUP BY goods.seller_id ORDER BY score DESC
                ) as sub on st.idx = sub.seller_id 
                left join (
                SELECT goods.seller_id, (ifnull(orders.score,0) + sum(goods.view * 0.25) + sum(goods.like_count * 0.25)) as score2
                        FROM fp_pay.goods goods
                        left join (SELECT ifnull((SUM(ifnull(od.quantity,0)) * 0.25 + SUM(ifnull(od.total_price,0)) * 0.25), 0) as score, seller_id  FROM fp_pay.order_detail AS od
                                    where DATEDIFF(now(), created_at)  <= 14
                                    group by seller_id) as orders on goods.seller_id = orders.seller_id
                        WHERE DATEDIFF(now(), created_at)  <= 14
                        GROUP BY goods.seller_id ORDER BY score DESC
                ) as sub2 on st.idx = sub2.seller_id 
                left join fp_db.like_tbl lt on st.idx =lt.table_idx and table_name = 'seller_tbl' and lt.user_idx = '$user_id'
                where st.display_state = 'show' and st.delete_date is null and sub.seller_id is not null
                order by score2 DESC, score DESC
                ) as master
                ,(SELECT @rownum := 0) r";
    $likeRes = $this->selectQuery($sql ,"pay");
    $ListRes = $this->selectQuery($sqlList ,"pay");
    $res = [
      "like_list" => $likeRes,
      "all_list"  => $ListRes,
    ];
    return $res;
  }

  public function getBrandLikeList ($user_id)
  {
    $sql = "select st.idx , st.name as seller_name, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image
                , CONVERT(if(ggv.id is null, 0,sum(if(ggv.gv_idx is null, 1 ,0))), UNSIGNED INTEGER) as goods_new_cnt, st.introduce, st.like_count, 1 as is_like_state
                from fp_db.seller_tbl st
                inner join fp_db.like_tbl lt on lt.table_idx =st.idx and lt.table_name = 'seller_tbl'
                left join (select g.*, gv.idx as gv_idx from fp_pay.goods g
			                left join fp_db.goods_view gv
			                	on g.id = gv.goods_id  and gv.user_id = '$user_id'
			                	where g.display_state = 'show' and g.deleted_at is null and TIMESTAMPDIFF(hour,g.created_at, now()) <= 48 group by g.id)
                	as ggv on st.idx = ggv.seller_id
                where st.display_state = 'show' and st.delete_date is null
                and lt.table_name = 'seller_tbl'
                and lt.user_idx = '$user_id'
                group by st.idx
                order by lt.idx desc
                ";

    $Res = $this->selectQuery($sql ,"pay");

    $result = [
      "cnt"  => count($Res),
      "list" => $Res,
    ];
    return $result;
  }

  public function getBrandProfile($seller_idx, $user_idx) {
    $sql = "select st.idx, st.name, st.introduce , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image 
               , count(lt.idx) as follow_cnt
               , if(lt2.idx is null , 0, 1) is_follow
               from seller_tbl st 
               left join like_tbl lt on st.idx = lt.table_idx and lt.table_name = 'seller_tbl'
               left join like_tbl lt2 on st.idx = lt2.table_idx and lt2.table_name = 'seller_tbl' and lt2.user_idx = '{$user_idx}'
               where st.display_state = 'show' and st.delete_date is null
               and st.idx = {$seller_idx}
               group by st.idx";
    $res = $this->selectQuery($sql, "db");
    return $res;
  }

  public function getBrandSnapList($seller_idx, $page, $perPage) {
    $limit = $this->getLimit($page, $perPage);
    $sql = "select ss.idx, i.path as profile_image  
               from seller_snap ss 
               inner join fp_pay.images i on ss.idx = i.imageable_id and i.imageable_type = 'App\\\SellerSnap'
               where ss.seller_idx = {$seller_idx}
               group by ss.idx
               order by ss.idx desc
               {$limit}";
    $res = $this->selectQuery($sql, "db");

    $cntSql = "select ss.idx, i.path as profile_image  
               from seller_snap ss 
               inner join fp_pay.images i on ss.idx = i.imageable_id and i.imageable_type = 'App\\\SellerSnap'
               where ss.seller_idx = {$seller_idx}
               group by ss.idx";
    $cnt = count($this->selectQuery($cntSql, "db"));
//dd($sql);
    $last_page = ceil((int)$cnt / $perPage);
    $response = [
      "current_page" => (int)$page,
      "data" => $res,
      "last_page" => $last_page,
      "total" => $cnt
    ];
    return $response;
  }

  public function getBrandSnapDetail($seller_idx, $snap_idx) {

    $sql = "select ss.*  
               from seller_snap ss 
               where ss.seller_idx = {$seller_idx} and ss.idx = {$snap_idx}
               group by ss.idx
               ";
    $res = $this->selectQuery($sql, "db");
    $sqlImgs = "
                select * ,`path` as full_path
               from fp_pay.images i
               where i.imageable_id = '{$snap_idx}' and i.imageable_type = 'App\\\SellerSnap'
               order by id asc";

    $imgs = $this->selectQuery($sqlImgs, "db");
    $data = array(
      "detail" => arrayToJson($res),
      "imgs" => $imgs
    );

    return $data;
  }


  public function fitList($category, $order, $user_idx, $limit, $seller_id) {
      $orderSql = "order by ct.created_at desc";
//      if ($order == "new") $orderSql = "order by ct.idx desc";
//      if ($order == "popular") $orderSql = "order by score desc";
//      if ($order == "reply") $orderSql = "order by ct.reply_count desc";

    $where = "and (ct.service_type = 'fit' or ct.service_type = 'postscript')";
    $categoryJoin = "";
    $categoryWhere = "";
//    if ($category =="all") {
      $categoryJoin = "left join fp_db.postscript_tbl pt on ct.idx = pt.service_idx and pt.deleted_at is null
			    left join fp_pay.order_detail od on pt.order_detail_id = od.id 
			    left join fp_db.cm_ld_fit clf on clf.timeline_idx = ct.idx ";
      $categoryWhere = " and (clf.seller_idx = $seller_id or od.seller_id = $seller_id)";
     if ($category =="postscript") {
      $where = "and ct.service_type = 'postscript'";
//      $categoryJoin = "inner join fp_db.postscript_tbl pt on ct.idx = pt.service_idx and pt.deleted_at is null
//      inner join fp_db.cm_ld_fit clf on clf.seller_idx = {$seller_id}";
    }




//    $categoryJoin = $categoryJoin."
//    left join fp_pay.order_detail od on pt.order_detail_id = od.id and od.seller_id = {$seller_id}
//    left join fp_db.cm_ld_fit clf on clf.seller_idx = {$seller_id}";

      $isLikeJoin = "left join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";

      $dao = new CBaseDAO();

      $sql = "select ct.* ,list_date_view(ct.created_at) as create_date ,count(cc.idx) as claim_cnt
                , (count(lt2.idx) * 0.7) + (count(ct2.idx) * 0.3) as score
                , uei.user_size , uei.user_height , uei.user_skin ,uei.user_foot_size, uei.is_body_open
                , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name
                , case when lt.idx is null then 0 else 1 end as is_like_state
                , case when ft2.idx is null then 0 else 1 end as is_follow
                from fp_db.cm_timeline ct 
                {$categoryJoin}
                inner join fp_db.user_list ul on ct.user_idx = ul.user_id
                left join fp_db.user_extra_info uei on ul.user_id = uei.user_id 
                left join fp_db.cm_claim cc on ct.service_type = cc.service_type and ct.idx = cc.service_idx and cc.deleted_at is null
                {$isLikeJoin}
                left join fp_db.follow_tbl ft2 on ft2.to_user_idx = ct.user_idx and ft2.from_user_idx = '{$user_idx}' 
                left join cm_imgs ci on ct.idx = ci.service_idx and ci.deleted_at is null
                left join fp_db.like_tbl lt2 on lt2.table_idx = ct.idx and lt2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 14
                left join fp_db.comment_tbl ct2 on ct2.table_idx = ct.idx and ct2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 14
                where ct.deleted_at is null  and ct.claim_deleted_at is null and ci.idx is not null
                {$where}
                {$categoryWhere}
                group by ct.idx 
                {$orderSql}
                {$limit}
                ";
//Log::info($sql);

      $res = $dao->selectQuery($sql, "db");
      $sqlCnt = "
                select ct.*
                from fp_db.cm_timeline ct 
                {$categoryJoin}
                {$isLikeJoin}  
                inner join fp_db.user_list ul on ct.user_idx = ul.user_id
                left join cm_imgs ci on ct.idx = ci.service_idx and ci.deleted_at is null
                where ct.deleted_at is null  and ct.claim_deleted_at is null and ci.idx is not null
                {$where}
                {$categoryWhere}
                group by ct.idx ";
      $resCnt = $dao->selectQuery($sqlCnt, "db");
      $data = array(
        "data" => $res,
        "cnt" => count($resCnt)
      );

      return $data;
    }

}