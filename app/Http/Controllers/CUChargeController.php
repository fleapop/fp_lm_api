<?php
namespace App\Http\Controllers;
require_once app_path()."/include_file/iamport.php";
include_once (app_path()."/include_file/refund_function.php");
require_once (app_path()."/include_file/func.php");

use DB;
use Storage;
use Image;
use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\PaymentController;
use App\CU\ChargeController;


class CUChargeController extends Controller
{
    // 2.4 충전 요청 응답

    public function viewChargeBody(Request $request) {
      $charge = new ChargeController();

      return $charge->viewChargeBody($request);
    }

    public function viewChargeResponseBody(Request $request) {
        $charge = new ChargeController();

        return $charge->viewChargeResponseBody($request);
    }

    public function makeChargeData($version, Request $request) {
        $charge = new ChargeController();
        return $charge->viewChargeBody($request);
    }


    public function apiRequestCharge($version, Request $request) {
        $charge = new ChargeController();
        return $charge->charge($request);
    }

    // 3.5 충전 취소 요청 응답
    public function apiRequestChargeCancel($version, Request $request) {
        $charge = new ChargeController();
        return $charge->cancel($request);
    }

    public function apiRequestCancel(Request $request) {
        $charge = new ChargeController();
        return $charge->requestCancel($request);
    }

    public function makeBarcode() {
        for($i=2889669; $i<100000000; $i++) {
            $barcode = "29003";
            $usercode = $i;
            while(strlen($usercode) < 8) {
                $usercode = "0".$usercode;

            }

            // echo $usercode;
            if(strlen($usercode) == 8) {
                $barcode = $barcode.$usercode;
                // echo "Barcode : ".$barcode."<br />";
                $res = $this->pconn->table("barcode_tbl")
                ->insertGetId([
                    "barcode"=>$barcode
                ]);


            }
        }
    }

    private function getUserIdx($barcode) {
        $res = $this->pconn->table("barcode_tbl")
        ->where("barcode","=",$barcode)
        ->whereNull("delete_date")
        ->whereNotNull("user_idx")
        ->get();

        if(isset($res[0])) {
            return $res[0]->user_idx;
        } else {
            return false;
        }
    }
}
