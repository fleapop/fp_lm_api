<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserAddress;
use App\Models\User;

class UserAddressController extends Controller
{
    //
    /*
    public function getAddresses()
    public function getEmptyUserAddress($user_id)
    public function getAddress($id)
    public function setAddress(Request $request)
    public function updateAddress(Request $request, $id)
    public function deleteAddress($id)
    */

    public function getAddresses($user_id) {
        return UserAddress::where('user_id', $user_id)->whereNull('deleted_at')->get();
    }

    public function getEmptyUserAddress($user_id) {
        $user = new User();
        $res_user = $user->where('user_id','=',$user_id)->select('shipping_name','shipping_phone','shipping_zip_code','shipping_address_1','shipping_address_2')->first();

        $addresses = [];

        if(isset($res_user)) {
            $data = [
                'id'=>0,
                'user_id'=>$user_id,
                'address_type'=>"신규배송지",
                'name'=>$res_user->shipping_name,
                'phone'=>$res_user->shipping_phone,
                'shipping_name'=>$res_user->shipping_name,
                'zip_code'=>$res_user->shipping_zip_code,
                'address_1'=>$res_user->shipping_address_1,
                'address_2'=>$res_user->shipping_address_2,
                'msg'=>'',
                'is_default'=>"true",
                'msg'=>'배송시 요청사항을 선택해 주세요.'
            ];
            array_push($addresses, $data);
            return $addresses;
        }

        return [];
    }

    public function getAddress( $id) {
        return $this->resultOk(UserAddress::find($id));
    }

    public function setAddress(Request $request) {
        try{
            $user = new UserAddress();
            $user->fill($request->all());
            $user->save();
            return response()->json('주소록 등록했습니다.', 200);
        }
        catch(\Exception $e){
           return response()->json('주소록 등록에 실패했습니다.', 422);
        }
    }

    public function updateAddress(Request $request, $id) {
        try{
            $user = UserAddress::find($id);
            
            $user->update([
                'user_id'=>$request->user_id,
                'address_type'=>$request->address_type,
                'name'=>$request->name,
                'phone'=>$request->phone,
                'shipping_name'=>$request->shipping_name,
                'zip_code'=>$request->zip_code,
                'address_1'=>$request->address_1,
                'address_2'=>$request->address_2,
                'msg'=>$request->msg,
                'is_default'=>$request->is_default
            ]);
            return response()->json('주소록을 변경했습니다.', 200);
        }
        catch(\Exception $e){
            // dd($e->getMessage());
           return response()->json('주소록 등록에 실패했습니다.', 422);
        }
    }

    public function deleteAddress($id) {
        $user = UserAddress::find($id);
        $user->delete();
        return response()->json('주소록을 삭제했습니다.', 200);
    }
}
