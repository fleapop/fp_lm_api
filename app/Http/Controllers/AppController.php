<?php

namespace App\Http\Controllers;

use App\Http\dao\CBaseDAO;
use App\Models\AndroidLog;
use App\Models\Goods;
use DB;
use DeviceDetector\DeviceDetector;
use Illuminate\Http\Request;
use Image;
use Session;
use Storage;

class AppController extends Controller
{
    /*
    AppController
    -public
    apiGetBanner() : 배너
    apiSetBanner() : 배너 변경
    apiSetAndroidLogger() : 안드로이드 앱 로그
    getFooter()
    -private
    */

//    private $conn;
//    private $conn_w;

    public function serverCheck()
    {
        echo $_SERVER['REMOTE_ADDR'];
    }

    function __construct()
    {
//        $this->conn = DB::connection(DATABASE);
//        $this->conn_w = DB::connection(DATABASE_W);
    }

    public function apiGetAppVersion(Request $request)
    {
        $response = $this->setResponse();

        if (! $request->filled(["device"])) {
            $response = $this->emptyResponse($response);
            return json_encode($response);
        }

        $res = $this->conn->table("version_tbl")
            ->where("idx", "=", 1)
            ->get();

        $version = "";
        $version_int = "";
        switch ($request->device) {
            case self::WEB :
            {
                $version = $res[0]->web;
                break;
            }
            case self::IOS :
            {
                $version = $res[0]->ios;
                $response["data"] = $version;

                break;
            }
            case self::ANDROID :
            {
                $version = $res[0]->android;
                $version_int = $res[0]->android_int;

                if (isset($request->version_int)) {
                    $response["data"]["string"] = $version;
                    $response["data"]["int"] = $version_int;
                } else {
                    $response["data"] = $version;
                }

                break;
            }
        }

        return json_encode($response);
    }

    public function apiGetBanner(Request $request)
    {
        $response = $this->setResponse();

//        $res = $this->conn->table("banner_tbl")
//            ->where("is_enable", "=", "Y")
//            ->whereNull("delete_date")
//            ->get();

        $sql = "select * from banner_tbl where is_enable = 'Y' and delete_date is null";
        $res = $this->conn->select($sql);

        foreach ($res as $item) {
            $item->banner = "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/banner/" . $item->banner;
        }

        $response["data"] = $res;

        return json_encode($response);
    }

    public function apiSetAndroidLogger(Request $request)
    {
        $userAgent = $request->header('User-Agent');

        $dd = new DeviceDetector($userAgent);

        AndroidLog::create([
            'user_agent' => $userAgent,
            'device'     => $dd->getBrand() . ' - ' . $dd->getDeviceName(),
            'os'         => $dd->getOs(),
            'log'        => $request->msg,
        ]);

        return $this->setResponse();
    }

    public function viewShare($type = NULL, $idx = NULL, $position = 0)
    {

        $data = [
            "host"     => "",
            "url_str"  => "",
            "type"     => "",
            "idx"      => "",
            "position" => "",
        ];

        if (! isset($type) && ! isset($idx)) {
            echo "잘못된 접근입니다.";
            return;
        }

        if (IS_TEST == true) {
            $data["host"] = "https://test.fleapop.co.kr";
        } else {
            $data["host"] = "https://fleapop.co.kr";
        }
      $dao = new CBaseDAO();
        switch ($type) {
            case "ldlife" :
            case "fit" :
            case "postscript" :
            case "answer" :
            case "ld_fit" :
            case "fllife" :
            case "community" :
            {

              $url = "/re/fleapop/{$type}/view/{$idx}";
              $sql = "select * 
                      from cm_timeline ct 
                      left join cm_imgs ci on ct.service_type = ci.service_type and ct.idx = ci.service_idx 
                      where ct.idx =  {$idx}";
              $res = $dao->selectQuery($sql, "db");
//              $res = $this->conn->table("user_content_tbl")
//                ->where("idx", "=", $idx)
//                ->get();
//              $data["meta_url"] = $data["host"] . $url;
              $data["title"] = "커뮤니티로 이동중입니다.";
              $data["meta_title"] = "러블리마켓 커뮤니티";
              $data["meta_description"] = $res[0]->contents;
              if(isset($res[0]->img_name)) {
                $data["meta_image"] = "https://s3.ap-northeast-2.amazonaws.com/fps3bucket" . $res[0]->img_path. $res[0]->img_name;
              }
              break;
            }
            case "magazine" :
            {

                $url = "/re/fleapop/magazine/view/{$idx}";
//                $res = $this->conn->table("user_content_tbl")
//                    ->where("idx", "=", $idx)
//                    ->get();
                $sql = "select * from user_content_tbl where idx = {$idx}";
                $res = $dao->selectQuery($sql, "db");
                $data["meta_url"] = $data["host"] . $url;
                $data["title"] = "매거진로 이동중입니다.";
                $data["meta_title"] = "러블리마켓 매거진 콘텐츠";
                $data["meta_description"] = $res[0]->content;
                $data["meta_image"] = S3_USER_CONTENT_URL . $res[0]->image;
                break;
            }
            case "ldfeed" :
            {
                $url = "/re/fleapop/view/all/" . $idx . "#0";
//                $res = $this->conn->table("user_content_tbl")
//                    ->where("idx", "=", $idx)
//                    ->get();
              $sql = "select * from user_content_tbl where idx = {$idx}";
              $res = $dao->selectQuery($sql, "db");
                $data["meta_url"] = $data["host"] . $url;
                $data["title"] = "러덕피드로 이동중입니다.";
                $data["meta_title"] = "러블리마켓 러덕피드 콘텐츠";
                $data["meta_description"] = $res[0]->content;
                $data["meta_image"] = S3_USER_CONTENT_URL . $res[0]->image;
                break;
            }
            case "market" :
            {
                $url = "/re/market/ing/data/" . $idx . "/info";
//                $res = $this->conn->table("open_market_news_tbl")
//                    ->where("market_idx", "=", $idx)
//                    ->get();
                $sql = "select * from open_market_news_tbl where market_idx = {$idx}";
                $res = $dao->selectQuery($sql, "db");
                $data["meta_url"] = $data["host"] . $url;
                $data["title"] = "마켓으로 이동중입니다.";
                $data["meta_title"] = $res[0]->turn . "회 러마 개최소식";
                $data["meta_description"] = $res[0]->sub_title;
                $data["meta_image"] = S3_OPEN_CONTENT_URL . $res[0]->poster_image;
                break;
            }
            case "lmchannel" :
            {
                $url = "/re/market/lm_channel/" . $idx;

//                $res = $this->conn->table("lmchannel_tbl")
//                    ->where("idx", "=", $idx)
//                    ->get();
                $sql = "select * from lmchannel_tbl where idx = {$idx}";
                $res = $dao->selectQuery($sql, "db");
                $data["title"] = "이동중입니다.";
                $data["meta_url"] = $data["host"] . $url;
                $data["meta_title"] = $res[0]->title;
                $data["meta_description"] = $res[0]->description;
                $data["meta_image"] = S3_LMCHANNER . $res[0]->cover_img;
                break;
            }
            case "ticket" :
            {
                $url = "/re/market/ticket/home";
                $data["meta_url"] = $data["host"] . $url;
                $data["title"] = "이동중입니다.";
                $data["meta_title"] = "러마 입장권 발급";
                $data["meta_description"] = "러마 입장권 발급 안내";
                $data["meta_image"] = "https://fleapop.co.kr/image/banner/ticket_meta_img.png";
                break;
            }
            case "event" :
            {
                $url = "/re/mypage/event/data/" . $idx;
//                $res = $this->conn->table("event_tbl")
//                    ->where("idx", "=", $idx)
//                    ->get();
                $sql = "select * from event_tbl where idx = {$idx}";
                $res = $dao->selectQuery($sql, "db");
                $data["title"] = "이동중입니다.";
                $data["meta_url"] = $data["host"] . $url;
                $data["meta_title"] = $res[0]->title;
                $data["meta_description"] = $res[0]->sub_title;
                $data["meta_image"] = "https://s3.ap-northeast-2.amazonaws.com/fps3bucket/event_contents/" . $res[0]->image;

                break;
            }
            case "goods" :
            {
                $url = "/re/store/goods/detail/" . $idx;

                $res = Goods::find($idx);

                $data["meta_url"] = $data["host"] . $url;
                $data["meta_title"] = $res->title;
                $data["meta_description"] = "";
                $data["meta_image"] = $res->imgs;


                break;
            }
            case "mypage" : {
                $url = "/re/mypage/home";
                $data["meta_url"] = $url;
                $data["meta_title"] = "";
                $data["meta_description"] = "";
                $data["meta_image"] = "";


                break;
            }
            case "order" : {
                $url = "/re/user/order/list/store";
                $data["meta_url"] = $url;
                $data["meta_title"] = "";
                $data["meta_description"] = "";
                $data["meta_image"] = "";
                break;
            }
            case "promotion" : {
              $url = "/store/promotion/".$idx;
              $data["meta_url"] = $url;
              $data["meta_title"] = "";
              $data["meta_description"] = "";
              $data["meta_image"] = "";
              break;
            }
        }

        $data["url_str"] = $url;
        $data["type"] = $type;
        $data["idx"] = $idx;
        $data["position"] = $position;
        // print_r($data);
//         dd($data);
        return view("remake.view_share", $data);

    }

    public function viewImgDown($table_name, $table_idx)
    {
        $res = $this->conn->table("user_content_tbl")
                   ->where("idx", "=", $table_idx)
                   ->get()[0];
        $url = "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/user_contents/" . $res->image;

        $data = [
            "image" => $url,
        ];
        return view("remake.image_capture", $data);
    }

    public function getFooter()
    {
        return $response['data'] = [
            'company_name'        => '(주)플리팝',
            'ceo_name'            => '김동화',
            'company_number'      => '888-88-00543',
            'company_sale_number' => '2019-서울영등포-0529호',
            'address'             => '서울특별시 강남구 학동로3길 9 2층 (주)플리팝',
            'email'               => 'fleamin@fleapop.co.kr',
            'info_name'           => '오준석',
            'company_phone'       => '070-8812-0820'
        ];
    }

    public function setBannerLog(Request $request)
    {

        $user_id = $this->userAuthCheck(['device' => $request->device, 'user_id' => $request->user_id, 'token' => $request->token]);

        $data = json_decode($request->data);
        $data->user_id = $request->token ? $user_id : $request->user_id;

        if ($request->device == self::IOS) $data->device_type = "NATIVE";
        else if ($request->device == self::ANDROID) $data->device_type = "NATIVE";
        else $data->device_type = "WEB";
        $res = null;
        if ($data->act_type == "VC") {
            $items = [
                "VIEW", "CLICK",
            ];
            foreach ($items as $item) {
                $fields = $values = [];
                foreach ($data as $field => $value) {
                    if ($field == "act_type") $value = $item;
                    $nq = false;
                    if (substr($field, 0, 4) == '_NQ_') {
                        $fields[] = substr($field, 4);
                        $nq = true;
                    } else {
                        $fields[] = $field;
                    }

                    $values[] = $nq ? $value : "'" . addslashes($value) . "'";

                }
                $fields = implode(", ", $fields);
                $values = implode(", ", $values);
                $sql = "INSERT INTO fp_db.banner_log ({$fields}) VALUES ({$values})";
                $res = $this->conn_w->insert($sql);
            }
        } else {
            $fields = $values = [];
            foreach ($data as $field => $value) {
                $nq = false;
                if (substr($field, 0, 4) == '_NQ_') {
                    $fields[] = substr($field, 4);
                    $nq = true;
                } else {
                    $fields[] = $field;
                }

                $values[] = $nq ? $value : "'" . addslashes($value) . "'";

            }
            $fields = implode(", ", $fields);
            $values = implode(", ", $values);
            $sql = "INSERT INTO fp_db.banner_log ({$fields}) VALUES ({$values})";
            $res = $this->conn_w->insert($sql);
        }

        return response()->json($res);

    }
}
