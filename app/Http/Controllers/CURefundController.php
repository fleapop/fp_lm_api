<?php
namespace App\Http\Controllers;
require_once app_path()."/include_file/iamport.php";
include_once (app_path()."/include_file/refund_function.php");
require_once (app_path()."/include_file/func.php");

use App\CU\RefundController;
use App\Models\CURefund;
use App\Models\LMPay;
use DB;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Image;
use Session;
use Storage;

class CURefundController extends Controller
{

  private $pconn;
    private $pconn_w;

  function __construct()
  {
      $this->conn = DB::connection(DB_DATABASE_R);
      $this->pconn = DB::connection(DB_PAYMENT_R);


  }

    public function apiRequestCheckRefund($version, Request $request) {

        $refund = new RefundController();
        return $refund->checkRefund($request);
    }

    public function apiRefundOnce(Request $request) {

      $client = new Client();

      $reqData = [
        'MBRNO'=>$request->MBRNO,
        'TXREQNO'=>$request->TXREQNO,
        'SNDUSRNM'=>$request->SNDUSRNM,
        'CHNCD'=>$request->CHNCD,
        'MCTCD'=>$request->MCTCD,
        'AMT'=>$request->AMT,
        'RCVBNKCD'=>$request->RCVBNKCD,
        'RCVACCNO'=>$request->RCVACCNO,
        'USERUNINO'=>$request->USERUNINO,
        'RCVDPSNM'=>$request->RCVDPSNM
      ];



      $response = $client->request('POST', 'http://112.175.103.79:4436/fleapop/v1/refund', [
          'form_params' => $reqData,
      ]);

      $result = json_decode($response->getBody());

      if($response->getStatusCode() != 200) {
          return false;
      }
    }

    public function apiCallRefundCheck(Request $request) {

        $sql = "SELECT * FROM cu_refund_log WHERE RESULTTP = 'S' AND RESULTCD = '0000' AND result_check = '결제대기' AND (update_date <= date_add(now(), interval -5 minute) OR update_date IS NULL)  ORDER BY idx ASC";
        $res = $this->pconn->select($sql);
        $refund = new RefundController();

        foreach($res as $item) {
            $refund->check($idx);
        }
    }

    public function apiCallRefundCheckOne(Request $request) {
      $lmpay = LMPay::find($request->input('cash_log_idx'));
      $refund = CURefund::where('TRANNO', $lmpay->appy_num)->first();

      $refundCon = new RefundController();

      $response = $refundCon->check($refund->idx);
      return $response;
    }

    public function apiCallRefundTest(Request $request) {
      $data = [
        "MBRNO"=> $this->getDecrypt_fp("FV2kfs0yAyTKuUdT0PcgOg=="),
        "TXREQNO"=> $this->getDecrypt_fp("uOr9XEPuMYSrb8HSO7L0ew=="),
        "SNDUSRNM"=> $this->getDecrypt_fp("6cGQ3TiWwMfoBMR5kz41qw=="),
        "CHNCD"=> $this->getDecrypt_fp("dXfoS6eWp3MCosD1wba49w=="),
        "MCTCD"=> $this->getDecrypt_fp("H6NTR66nCRalO8x59osQiA=="),
        "AMT"=> $this->getDecrypt_fp("+0YyxFJ21UE3nvZESmcpOg=="),
        "RCVBNKCD"=> $this->getDecrypt_fp("0y9jvlRSNJ2ZvUpKNxspLw=="),
        "RCVACCNO"=> $this->getDecrypt_fp("azpI7Kp8hBEohcvrTeRPDg=="),
        "USERUNINO"=> $this->getDecrypt_fp("5PoC\/jTxzlTySkWVzSwvlw=="),
        "RCVDPSNM"=> $this->getDecrypt_fp("GtUxM7b6Hgt6z9AWIW6CYw==")
      ];

      // $url_refund = "http://112.175.103.79:4436/fleapop/v1/refund";
      // $client = new Client();
      //
      // $response = $client->request('POST', $url_refund, [
      //     'form_params' => $data,
      // ]);
      //
      // $result = json_decode($response->getBody());
      // // dd($result);
      // if($response->getStatusCode() != 200) {
      //     return false;
      // }
    }

    private $pfkey    = "playtong2fleapop";
    private $fpkey    = "fleapop2playtong";
    private $is_enc   = true;

    public function getEncrypt_pf($msg)
    {
        if (! $this->is_enc) {
            return $msg;
        }

        $endata = @openssl_encrypt($msg, "aes-128-cbc", $this->pfkey, true, $this->pfkey);
        $endata = base64_encode($endata);
        return $endata;
    }

    public function getDecrypt_pf($msg)
    {
        if (! $this->is_enc) {
            return $msg;
        }

        $data = base64_decode($msg);
        $endata = @openssl_decrypt($data, "aes-128-cbc", $this->pfkey, true, $this->pfkey);
        return $endata;
    }

    public function getEncrypt_fp($msg)
    {
        if (! $this->is_enc) {
            return $msg;
        }

        $endata = @openssl_encrypt($msg, "aes-128-cbc", $this->fpkey, true, $this->fpkey);
        $endata = base64_encode($endata);
        return $endata;
    }

    public function getDecrypt_fp($msg)
    {
        if (! $this->is_enc) {
            return $msg;
        }

        $data = base64_decode($msg);
        $endata = @openssl_decrypt($data, "aes-128-cbc", $this->fpkey, true, $this->fpkey);
        return $endata;
    }
}
