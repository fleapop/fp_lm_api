<?php

namespace App\Http\Controllers;

use App\Helper\CExhibitionHelper;
use App\Helper\COrderHelper;
use App\Http\dao\CBaseDAO;
use App\Http\dao\CStoreDAO;
use App\Models\Option;
use App\Models\Sku;
use DB;
use http\Env\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Image;
use Illuminate\Database\Eloquent\Builder;
use Session;
use Storage;

class StoreController extends Controller
{
//    function __construct() {
//      parent::__construct();
//    }

    public function getStoreList () {
      $dao = new CStoreDAO();
      $page = $this->getParam("page", 1);
      $perPage = 40;
      $res = $dao->getStoreList($page, $perPage);

      $last_page = ceil((int)$res["cnt"] / $perPage);
      $response = [
        "current_page" => (int)$page,
        "data" => $res["list"],
        "last_page" => $last_page,
        "total" => $res["cnt"]
      ];

        return $this->resultOk($response);
    }

    public function getGoodsDetail () {
      $goodsId = $this->getParam("goodsId");
      $dao = new CStoreDAO();
      $res = $dao->getGoodsDetail($goodsId, $this->userIdx);

      if(isset($res[0])) {
        $res = $res[0];
        $res->content = htmlspecialchars_decode($res->content);
        $res->content = str_replace("&#39;", "'", $res->content);

        $optionSql = "select title from `options` o inner join option_sku os on o.id = os.option_id inner join skus s on os.sku_id = s.id
                where s.goods_id = {$goodsId} and s.state = 'enable' and s.deleted_at is null and o.value is not null and o.value != ''
                and o.deleted_at is null and os.deleted_at is null
                group by title
                order by o.id asc";
        $titles = $dao->selectQuery($optionSql ,"pay");
        $isSales = 0;
        $optionCnt = 0;
        if(isset($titles[0])) {
          $queryResultsSql = "select s2.id, s2.sku_id , os.option_id,o.title ,o.value , s.state ,s.price as amount, sum(s2.stock) as stock
                  from skus s
                  inner join option_sku os on s.id = os.sku_id 
                  inner join `options` o on os.option_id = o.id 
                  inner join stocks s2 on s.id = s2.sku_id 
                  where s.goods_id = $goodsId
                    and s.state = 'enable'
                    and o.title = '{$titles[0]->title}'
                    and s.deleted_at is null
                  group by o.id
                  having (count(if (s.state = 'enable', s.state, null)) > 0)";
          $results = $dao->selectQuery($queryResultsSql, "pay");


          $optionCnt = count($results);
          foreach ($results as $item) {
            if ((int)$item->stock <= 0) {
              $isSales += 1;
            }
          }
        }
        $viewCountData = array("_NQ_view" => "view + 1");
        $dao->updateQuery("goods",$viewCountData ,"id = $goodsId","pay_w");

        if (!empty($this->userIdx) && (int)$this->userIdx > 0) {
          $goodsViewData = array("goods_id" => $res->goods_id,
                          "seller_id" => $res->seller_id,
                          "user_id" => $this->userIdx);
          $dao->insertQuery("goods_view",$goodsViewData ,"db_w");
        }

        $res->isStockSale = $isSales != $optionCnt;
      } else {
        $res = null;
      }

      return $this->resultOk($res);
    }

    public function getNewList () {
      $page = $this->getParam("page", 1);

      $dao = new CStoreDAO();
      $perPage = 40;
      $res = $dao->getNewList($page, $perPage, $this->userIdx);

      $last_page = ceil((int)$res["goodsResCnt"] / $perPage);
      $response = [
        "current_page" => (int)$page,
        "data" => $res["goodsRes"],
        "last_page" => $last_page,
        "total" => $res["goodsResCnt"],
        "bannerRes" => $res["bannerRes"],
      ];

      if ($last_page > 13) {
        $response["last_page"] = 13;
      }

      if ((int)$page == 13) {
        $goodses = [];
        $count = 0;
        foreach ($this["goodsRes"] as $item) {
          if ($count < 20) {
            $goodses[] = $item;
          }

          $count++;
        }
        $response["data"] = $goodses;
      }

      if ($response["total"] > 500) {
        $response["total"] = 500;
      }
      return $this->resultOk($response);
    }

    public function getBestList () {
      $perPage = 40;
      $page = $this->getParam("page", 1);
      $cateCode = $this->getParam("cate");

      $cate = "all";
      $cateWhere = "";

      switch ($cateCode) {
        case 'outer' :
        {
          $cate = '아우터';
          break;
        }
        case 'top' :
        {
          $cate = '상의';
          break;
        }
        case 'bottom' :
        {
          $cate = '하의';
          break;
        }
        case 'one' :
        {
          $cate = '원피스';
          break;
        }
        case 'acc' :
        {
          $cate = '악세사리';
          break;
        }
        case 'stuff' :
        {
          $cate = '잡화';
          break;
        }
        case 'beaut' :
        {
          $cate = '뷰티';
          break;
        }
        case 'digital' :
        {
          $cate = '디지털';
          break;
        }
      }

      if ($cate !== "all") {
        $cateWhere = " and goods.sub_category  = '$cate'";
      }

      $dao = new CStoreDAO();
      $res = $dao->getBestList($page, $perPage, $cateWhere);

      $last_page = ceil((int)$res["goodsResCount"] / $perPage);
      $response = [
        "total" => $res["goodsResCount"],
        "data" => $res["goodsRes"],
        "current_page" => (int)$page,
        "last_page" => $last_page,
      ];

      if ($last_page > 8) {
        $response["last_page"] = 8;
      }

      if ($page == 8) {
        $goodses = [];
        $count = 0;
        foreach ($res["goodsRes"] as $item) {
          if ($count < 20) {
            $goodses[] = $item;
          }

          $count++;
        }
        $response["data"] = $goodses;
      }

      if ($response["total"] > 300) {
        $response["total"] = 300;
      }

      return $this->resultOk($response);
    }

    public function getBrandList () {

    $perPage = 40;
    $page = $this->getParam("page", 1);

    $user_idx = $this->userIdx;

    $dao = new CStoreDAO();
    $res = $dao->getBrandList($page, $perPage, $user_idx);

    $last_page = ceil((int)$res["goodsResCount"] / $perPage);
    $response = [
      "total" => $res["goodsResCount"],
      "data" => $res["goodsRes"],
      "current_page" => (int)$page,
      "last_page" => $last_page - 1,
    ];

    return $this->resultOk($response);
  }

    public function getCategoryList()
    {
      $response = [
        [
          'title' => '아우터',
          'name' => 'outer',
          'images' => [
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateOuter.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateOuter@2x.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateOuter@3x.png",
          ],
        ],
        [
          'title' => '상의',
          'name' => 'top',
          'images' => [
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateTop.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateTop@2x.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateTop@3x.png",
          ],
        ],
        [
          'title' => '하의',
          'name' => 'bottom',
          'images' => [
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBottom.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBottom@2x.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBottom@3x.png",
          ],
        ],
        [
          'title' => '원피스',
          'name' => 'one',
          'images' => [
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateOne.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateOne@2x.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateOne@3x.png",
          ],
        ],
        [
          'title' => '악세사리',
          'name' => 'acc',
          'images' => [
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateAcc.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateAcc@2x.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateAcc@3x.png",
          ],
        ],
        [
          'title' => '잡화',
          'name' => 'stuff',
          'images' => [
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBag.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBag@2x.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBag@3x.png",
          ],
        ],
        [
          'title' => '뷰티',
          'name' => 'beaut',
          'images' => [
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBeauty.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBeauty@2x.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBeauty@3x.png",
          ],
        ],
        [
          'title' => '디지털',
          'name' => 'digital',
          'images' => [
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateDigital.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateDigital@2x.png",
            "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateDigital@3x.png",
          ],
        ]
      ];

      return $this->resultOk($response);
    }

    public function getCategoryDetail()
    {
      $cate = $this->getParam("cate");
      $sort = $this->getParam("sort");
      $page = $this->getParam("page", 1);

      switch ($cate) {
        case 'outer' :
        {
          $cate = '아우터';
          break;
        }
        case 'top' :
        {
          $cate = '상의';
          break;
        }
        case 'bottom' :
        {
          $cate = '하의';
          break;
        }
        case 'one' :
        {
          $cate = '원피스';
          break;
        }
        case 'acc' :
        {
          $cate = '악세사리';
          break;
        }
        case 'stuff' :
        {
          $cate = '잡화';
          break;
        }
        case 'beaut' :
        {
          $cate = '뷰티';
          break;
        }
        case 'digital' :
        {
          $cate = '디지털';
          break;
        }
      }

      $dao = new CStoreDAO();
      $perPage = 40;
      $res = $dao->getCategoryList($page, $perPage, $sort, $cate);

      $goodsResCount = $res['goodsResCount'];
      $goodsRes = $res['goodsRes'];
      $last_page = ceil((int)$goodsResCount / $perPage);


      $response = [
        "total" => $goodsResCount,
        "data" => $goodsRes,
        "current_page" => (int)$page,
        "last_page" => $last_page - 1
      ];
      return $response;


    }

    public function newLike()
    {

      $page = $this->getParam("page", 1);
      $erSel = "";
      $erJoin = "";
      $exhibitionRep = new CExhibitionHelper();
      $erRes = $exhibitionRep->getExhibitionRepMenu()['result'];
      if ($erRes) {
        $erSel = " , atm_tbl.exhibition_name";
        $erJoin = "left join (select e.list_tag_name  as exhibition_name,eg.goods_id from fp_pay.exhibition e
                          inner join fp_pay.exhibition_goods eg on e.id = eg.exhibition_id and eg.deleted_at is null
                          where e.deleted_at is null and e.list_tag_name is not null and (e.start_date <= now() and e.end_date >= now())
                          ) as atm_tbl on g.id = atm_tbl.goods_id";
      }
      $user_id = $this->userIdx;
      $perPage = 6;
      $page = $page - 1;
      $dao = new CBaseDAO();
      $limit = $dao->getLimit($page, $perPage);

      $sql = "select group_concat(idx) as seller_ids
                  from (
                      select st.idx
                      from fp_db.seller_tbl st
                      inner join fp_db.like_tbl lt on lt.table_idx = st.idx and lt.table_name = 'seller_tbl' and lt.user_idx = $user_id
                      left join fp_pay.goods g on g.seller_id = st.idx and g.display_state = 'show' and g.deleted_at is null
                      and TIMESTAMPDIFF(hour,g.created_at, now()) <= 48
                      left join fp_db.goods_view gv on g.id = gv.goods_id and gv.user_id = '$user_id'
                      where st.display_state = 'show' and st.delete_date is null and g.id is not null and gv.idx is null
                      GROUP by st.idx
                      order by lt.insert_date desc
                      {$limit}
                  ) as m";
      $sellersRes = $dao->selectQuery($sql, "db")[0];
//      $sellersRes = $this->pconn->select($sql)[0];

      $cntSql = "select st.idx
                  from fp_db.seller_tbl st
                  inner join fp_db.like_tbl lt on lt.table_idx = st.idx and lt.table_name = 'seller_tbl' and lt.user_idx = $user_id
                  left join fp_pay.goods g on g.seller_id = st.idx and g.display_state = 'show' and g.deleted_at is null
                  and TIMESTAMPDIFF(hour,g.created_at, now()) <= 48
                  left join fp_db.goods_view gv on g.id = gv.goods_id and gv.user_id = '$user_id'
                  where st.display_state = 'show' and st.delete_date is null and g.id is not null and gv.idx is null
                  GROUP by st.idx";
      $cntRes = count($dao->selectQuery($cntSql, "db"));
      $last_page = ceil($cntRes / $perPage);

      $sellIdxs = $sellersRes->seller_ids;
      $sellers = explode(',', $sellIdxs);

      $goodslist = [];
      foreach ($sellers as $index => $item) {
        $goodslist[] = [];
      }

      if ($sellIdxs) {
        $goodsSql = "select g.id, g.title ,g.price
                          , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM fp_pay.promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                          , g.use_discount , g.imgs , g.stock_state , g.`view` ,st.name as seller_name, st.idx as seller_id
                          , IF(sp.idx is null, 0, 1) as is_hotdeal
                          , sp.ended_at as hotdeal_end
                          , g.like_count
                          , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image
                          {$erSel}
                      from fp_pay.goods g
                      left join fp_db.seller_tbl st on g.seller_id = st.idx
                      left join fp_pay.goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                      left join fp_db.goods_view gv on g.id = gv.goods_id and gv.user_id = '$user_id'
                      {$erJoin}
                      where g.display_state = 'show'
                      and g.seller_id in ({$sellIdxs})
                      and TIMESTAMPDIFF(hour,g.created_at, now()) <= 48
                       and g.deleted_at is null
                       and gv.idx is null
                       and st.display_state = 'show' and st.delete_date is null
                      order by g.created_at desc";
//        $goodsRes = $this->pconn->select($goodsSql);

        $goodsRes = $dao->selectQuery($goodsSql, "db");


        foreach ($goodsRes as $item) {
          $sellerIndex = array_search($item->seller_id, $sellers);
          if ($sellerIndex >= 0) {
            $goodslist[(int)$sellerIndex][] = $item;
          }
        }
      }
      $response = [
        "current_page" => (int)$page,
        "data" => $goodslist,
        "last_page" => $last_page,
        "total" => $cntRes,
      ];

      return $response;
    }

    public function getMyLikeList() {
      $page = $this->getParam("page", 1);
      $perPage = 40;
      $dao = new CStoreDAO();
      $res = $dao->getMyLikeList($page, $perPage, $this->userIdx);
      $last_page = ceil((int)$res["goodsResCount"] / $perPage);
      $response = [
        "current_page" => (int)$page,
        "data" => $res["goodsRes"],
        "last_page" => $last_page,
        "total" => $res["goodsResCount"],
      ];
      return $this->resultOk($response);
    }

    public function goodsSearch() {
      $page = $this->getParam("page", 1);
      $sort = $this->getParam("sort");
      $keyword = $this->getParam("keyword");
      $perPage = 40;
      $dao = new CStoreDAO();
      $res = $dao->goodsSearch($this->userIdx, $sort,$keyword,$page, $perPage);
      $last_page = ceil((int)$res["cnt"] / $perPage);

      $response = [
        "current_page" => (int)$page,
        "data" => $res["list"],
        "last_page" => $last_page,
        "total" => $res["cnt"],
      ];
      return $this->resultOk($response);
    }

  public function getByGoods($goodsId, Request $request)
  {
    $optionIds = explode(',', $request->optionId);
    $isSingle = $request->isSingle;

    $dao = new CBaseDAO();

    $optionSql = "select title from `options` o inner join option_sku os on o.id = os.option_id inner join skus s on os.sku_id = s.id
                where s.goods_id = {$goodsId} and s.state = 'enable' and s.deleted_at is null and o.value is not null and o.value != ''
                and o.deleted_at is null and os.deleted_at is null
                group by title
                order by o.id asc";
    $titles = $dao->selectQuery($optionSql ,"pay");
//        dd($titles[0]);
    $results = [];
//    dd($optionIds[0]."_".count($optionIds)."_".count($titles));
    if ($optionIds[0] == 0) {
      $queryResultsSql = "select s2.id, s2.sku_id , os.option_id,o.title ,o.value , s.state ,s.price as amount, sum(s2.stock) as stock
                    from skus s
                    inner join option_sku os on s.id = os.sku_id 
                    inner join `options` o on os.option_id = o.id 
                    inner join stocks s2 on s.id = s2.sku_id 
                    where s.goods_id = $goodsId
                      and s.state = 'enable'
                      and o.title = '{$titles[0]->title}'
                      and s.deleted_at is null
                    group by o.id
                    having (count(if (s.state = 'enable', s.state, null)) > 0)";
      $results = $dao->selectQuery($queryResultsSql ,"pay");

    } else if (count($optionIds) < count($titles)) {
      $option = Option::find($optionIds[0]);
      $skus = Sku::where([
        ['goods_id', $goodsId],
        ['state', 'enable'],
      ])->whereNull('deleted_at')
        ->whereHas('options', function (Builder $query) use ($option) {
          $query->where([['title', $option->title], ['value', $option->value]]);
        })->get();

      foreach ($skus as $sku) {
        $secondOption = $sku->options->where('title', $titles[1]->title)->first();
        $tempOptionName = $sku->price > 0 ? "(옵션가 : {$sku->price}원)" : "";
        $results[] = [
          'id'        => $sku->id,
          'sku_id'    => $sku->sku_id,
          'option_id' => $secondOption->id,
          'title'     => $secondOption->title,
          'value'     => $secondOption->value.$tempOptionName,
          'state'     => $sku->state,
          'amount'    => $sku->price,
          'stock'     => $sku->stock->stock,
        ];
      }
    } else {
      $option = Option::whereIn('id', $optionIds)->get();
      $sku = Sku::where([['goods_id', $goodsId], ['state', 'enable']])
        ->whereHas('options', function (Builder $query) use ($option) {
          $query->where([
            ['title', $option[0]->title], ['value', $option[0]->value],
          ]);
        });

      if (isset($option[1])) {
        $sku->whereHas('options', function (Builder $query) use ($option) {
          $query->where([
            ['title', $option[1]->title], ['value', $option[1]->value],
          ]);
        });
      }

      $sku = $sku->first();

      foreach ($option as $index => $item) {
        $results[] = [
          'id'        => $sku->id,
          'sku_id'    => $sku->sku_id,
          'option_id' => $option[$index]->id,
          'title'     => $option[$index]->title,
          'value'     => $option[$index]->value,
          'state'     => $sku->state,
          'amount'    => $sku->price,
          'stock'     => $sku->stock->stock,
        ];
      }
    }

    return $this->resultOk($results);
  }

  public function OrderSheet($meta)
  {
    Cookie::queue("redirect_url", url()->previous(), 60);

    $this->var["TAB_MENU"] = "STORE";
    $this->var["MAIN_MENU"] = "MYPAGE";
    $this->var["IS_BACK"] = true;
    $this->var['meta'] = $meta;

    $args = [
      'device' => 101,
      'meta' => $meta,
      'user_idx' => $this->userIdx
    ];
//    $request = new Request();
//    dd($this);

//    $request = $this->makeRequest($args);
    $order = new COrderHelper();
    $response = $order->getOrder($args);
    $this->var['data'] = $response;

    $isDefaultAddress = false;

    if (isset($response["data_order"]["shipping_address"])) {
      foreach ($response["data_order"]["shipping_address"] as $address) {
        if (isset($address->is_default)) {
          if($address->is_default === "true") {
            $isDefaultAddress = true;
          }
        }

      }

      if(!$isDefaultAddress) {
        if (isset($response["data_order"]["shipping_address"][0]->is_default)) {
          $response["data_order"]["shipping_address"][0]->is_default = true;
        }

      }
    }



    $this->var["hide_right_menu"] = true;
    $this->var["IAMPORT_ID"] = env('IAMPORT_ID', '');
    $this->var["m_redirect_url"] = env('APP_URL', '');

    return $this->resultOk($this->var);
  }

  public function setRoas()
  {
      $goodsId = $this->getParam("goodsId");
      $dao = new CStoreDAO();
      $dao->setRoas($goodsId, $this->userIdx);
  }
}
