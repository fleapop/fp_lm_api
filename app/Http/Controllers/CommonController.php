<?php


namespace App\Http\Controllers;


use App\Helper\CCommonHelper;
use App\Helper\CCommunityHelper;
use App\Helper\CExhibitionHelper;
use App\Helper\CLmPayHelper;
use App\Helper\CPushHelper;
use App\Http\dao\CBaseDAO;
use App\Http\dao\CUserDAO;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class CommonController extends Controller
{
  public function apiGetAppVersion()
  {
    $device = $this->getParam("device");
    $version_int = $this->getParam("version_int");
    if (!isset($device)) {
      return $this->resultFail("NO_REQUIRED");
    }

    $dao = new CBaseDAO();
    $res = $dao->selectQuery("select * from version_tbl where idx = 1");
//    $res = $this->conn->table("version_tbl")
//      ->where("idx", "=", 1)
//      ->get();

    $version = "";
    $version_int = "";
    $response = array();
    switch ($device) {
      case self::WEB :
      {
        $version = $res[0]->web;
        break;
      }
      case self::IOS :
      {
        $version = $res[0];
        $response["data"] = $version->ios;
        $response["ios_store_version"] = $version->ios_store_version;

        break;
      }
      case self::ANDROID :
      {
        $version = $res[0]->android;
        $version_int = $res[0]->android_int;

        if (isset($version_int)) {
          $response["data"]["string"] = $version;
          $response["data"]["int"] = $version_int;
        } else {
          $response["data"] = $version;
        }

        break;
      }
    }

    return $this->resultOk($response);
  }

  public function getServiceState($device = 101, $user_id = NULL, $token = NULL) {

    if (env('APP_ENV') == "production") {
      $stateFile = Storage::disk('s3')->get("/service_state/app_state.txt");
    } else {
      $stateFile = Storage::disk('s3')->get("/service_state/test_app_state.txt");
    }

    $stateData = json_decode($stateFile);
    $serverState = $stateData[0]->state;
    $trafficState = $stateData[1]->state;
    $storeState = $stateData[2]->state;

    $serverImage = 'https://fps3bucket.s3.ap-northeast-2.amazonaws.com/service_state/server/';
    $trafficImage = 'https://fps3bucket.s3.ap-northeast-2.amazonaws.com/service_state/traffic/';
    $storeImage = 'https://fps3bucket.s3.ap-northeast-2.amazonaws.com/service_state/store/';

    $serverMsg = $stateData[0]->msg;
    $trafficMsg = $stateData[1]->msg;
    $storeMsg = $stateData[2]->msg;

    switch ($device) {
      case self::WEB :
      {
        $serverImage = [
          'pc'     => $serverImage . $stateData[0]->img_pc,
          'mobile' => $serverImage . $stateData[0]->img_mobile,
        ];

        $trafficImage = [
          'pc'     => $trafficImage . $stateData[1]->img_pc,
          'mobile' => $trafficImage . $stateData[1]->img_pc,
        ];

        $storeImage = [
          'pc'     => $storeImage . $stateData[2]->img_pc,
          'mobile' => $storeImage . $stateData[2]->img_pc,
        ];

        break;
      }
      default :
      {
        $serverImage .= $stateData[0]->img_app;
        $trafficImage .= $stateData[1]->img_app;
        $storeImage .= $stateData[2]->img_app;
        break;
      }
    }

    $server = [
      'name'  => 'server',
      'state' => $serverState,
      'image' => $serverImage,
      'msg'   => $serverMsg,
    ];

    $traffic = [
      'name'  => 'traffic',
      'state' => $trafficState,
      'image' => $trafficImage,
      'msg'   => $trafficMsg,
    ];

    $store = [
      'name'  => 'store',
      'state' => $storeState,
      'image' => $storeImage,
      'msg'   => $storeMsg,
    ];
    @session_start();
    $authUserId = $this->userAuthCheck(['device' => $device, 'user_id' => $user_id, 'token' => $token]);
    if ($authUserId) {
      $user = User::find($authUserId);

      if (isset($user)) {
        if (strlen($user->grade) > 5) {
          $server['state'] = false;
          $traffic['state'] = false;
          $store['state'] = false;
        }
      }


    }

    $result = [$server, $traffic, $store];
    return response()->json($result, 200);
  }

  public function getCategory($category = null) {
    $commonHelper = new CCommonHelper();
    $data = $commonHelper->getCategory($category);
    return $this->resultOk($data);
  }

  public function addLmpay () {
    $user_idx = $this->userIdx;
    $method = $this->getParam("method");
    $amount = $this->getParam("amount");
    $action = $this->getParam("action");
    $order_info_idx = $this->getParam("order_info_idx");

    $lmPayHelper = new CLmPayHelper();
    $lmpayIdx = $lmPayHelper->addLmpay($user_idx, $method, $amount, $action, $order_info_idx);
    return $this->resultOk($lmpayIdx);
  }

  /**
   * 콕
   * table_name
   * table_idx
   * @return \Illuminate\Http\JsonResponse
   */
  public function setlike () {
    $table_name = $this->getParam("table_name");
    $table_idx = $this->getParam("table_idx");
    $user_id = $this->userIdx;

    if (!$user_id) {
      return $this->resultFail("NO_LOGIN", "로그인 후 이용가능합니다.");
    }

    if (!$table_name || !$table_idx) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $tableIdxs = [
      'goods'            => [
        'id'    => 'id',
        'table' => 'fp_pay.goods',
      ],
      'lmchannel_tbl'    => [
        'id'    => 'idx',
        'table' => 'fp_db.lmchannel_tbl',
      ],
      'reply_tbl'        => [
        'id'    => 'idx',
        'table' => 'fp_db.reply_tbl',
      ],
      'seller_tbl'       => [
        'id'    => 'idx',
        'table' => 'fp_db.seller_tbl',
      ],
      'user_content_tbl' => [
        'id'    => 'idx',
        'table' => 'fp_db.user_content_tbl',
      ],
      'cm_timeline' => [
        'id'    => 'idx',
        'table' => 'fp_db.cm_timeline',
      ],
    ];

    if (!isset($tableIdxs[$table_name])) {
      return $this->resultFail("NO_REQUIRED", "잘못된 정보가 입력됬습니다.");
    }

    $params = [
      "table_name" => $table_name,
      "table_idx"  => $table_idx,
      "user_idx"   => $user_id,
    ];
    $CommHelper = new CCommonHelper();
    $result = $CommHelper->toggleLike($params);



    $isSet = $result["is_like_state"] ? "like_count + 1" : "like_count - 1";
    $hash = array(
      "_NQ_like_count" => $isSet
    );
//    // dd($isSet);
    $dao = new CBaseDAO();
    $dao->updateQuery("{$tableIdxs[$params["table_name"]]['table']}", $hash, "{$tableIdxs[$params["table_name"]]['id']} = '{$params["table_idx"]}'", "db_w");

    if($table_name == "cm_timeline" && $result["is_like_state"]) {
      $pushHelper = new CPushHelper();
      $pushData = array(
        "table_name" => $table_name,
        "table_idx" => $table_idx,
        "is_img" => 0
      );
      $receveiver = $dao->selectQuery("select user_idx, service_type, category, contents from fp_db.cm_timeline where idx = {$table_idx}","db");

      if(isset($receveiver[0]) && $user_id != $receveiver[0]->user_idx) {
        $pushData['service_type'] = $receveiver[0]->service_type;
        $pushData['category'] = $receveiver[0]->category;

        $communitiHelper = new CCommunityHelper();
        if($communitiHelper->getTimelineImgType($table_idx)) {
          $pushData['is_img'] = 1;
        }

        $pushDataEncode = json_encode($pushData);
        $userDao = new CUserDAO();
        $sender = $userDao->getUserInfo($user_id);
        $communityTitle = array(
          "fllife" => "플리생활",
          "ldlife" => "러덕생활",
          "fit" => "러덕핏",
          "postscript" => "후기핏",
          "answer" => "답정러"
        );
        $pushCate = $communityTitle[$pushData['service_type']];
        $contents = mb_strlen($receveiver[0]->contents) > 20 ? mb_substr($receveiver[0]->contents,0,20)."..." : $receveiver[0]->contents;
        $pushHelper->sendPush($receveiver[0]->user_idx, '콕', "[{$pushCate}] {$contents} 게시물에 {$sender[0]->nic_name_full}님이 콕하셨습니다.", 'community', $pushDataEncode);
      }
    }
    $response = [
      "idx" => $table_idx,
      "cnt" => $result["like_count"],
      "is"  => $result["is_like_state"],
    ];

    return $this->resultOk($response);
  }

  public function getReplyList() {
    $table_name = $this->getParam("table_name");
    $table_idx = $this->getParam("table_idx");
    $page = $this->getParam("page", 1);
    $perPage = 20;


    $dao = new CUserDAO();
    $limit = $dao->getLimit($page, $perPage);
    if ($table_name == "user_content_tbl") {
      $sql = "select ct.*,'comment' as service_type, if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name 
, if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img
        ,list_date_view(ct.insert_date) as create_date 
			from fp_db.comment_tbl ct 
			inner join user_content_tbl uct on ct.table_idx = uct.idx
			left join fp_db.user_list ul on ct.from_user_idx = ul.user_id 
			where ct.table_name = '{$table_name}' 
			and ct.delete_date is null and ct.claim_deleted_at is null
			and (uct.idx = {$table_idx} or uct.parent_idx = {$table_idx})
			order by ct.idx desc
			{$limit}
			";
      $res = $dao->selectQuery($sql);
      $sqlCnt = "select count(ct.idx) as cnt from fp_db.comment_tbl ct inner join user_content_tbl uct on ct.table_idx = uct.idx where ct.table_name = '{$table_name}' and ct.delete_date is null and ct.claim_deleted_at is null and (uct.idx = {$table_idx} or uct.parent_idx = {$table_idx})";
      $resCnt = $dao->selectQuery($sqlCnt, "db")[0]->cnt;
      foreach ($res as $item) {
        if (isset($item->user_tags) && $item->user_tags != "") {
          $sqlTags = "select user_id, if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name from fp_db.user_list ul where user_id in ({$item->user_tags})";
          $item->user_tags = $dao->selectQuery($sqlTags, "db");
        } else {
          $item->user_tags = array();
        }
      }
    } else {
      $sql = "select ct.*,'comment' as service_type, if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name 
      , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img
        ,list_date_view(ct.insert_date) as create_date 
			from fp_db.comment_tbl ct 
			left join fp_db.user_list ul on ct.from_user_idx = ul.user_id 
			where ct.table_name = '{$table_name}' and ct.table_idx = {$table_idx}
			and ct.delete_date is null and ct.claim_deleted_at is null
			order by ct.idx desc
			{$limit}
			";
      $res = $dao->selectQuery($sql);
      $sqlCnt = "select count(idx) as cnt from fp_db.comment_tbl ct where ct.table_name = '{$table_name}' and ct.table_idx = {$table_idx} and ct.delete_date is null";
      $resCnt = $dao->selectQuery($sqlCnt, "db")[0]->cnt;
      foreach ($res as $item) {
        if (isset($item->user_tags) && $item->user_tags != "") {
          $sqlTags = "select user_id, if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name from fp_db.user_list ul where user_id in ({$item->user_tags})";
          $item->user_tags = $dao->selectQuery($sqlTags, "db");
        } else {
          $item->user_tags = array();
        }
      }
    }
    $last_page = ceil((int)$resCnt / $perPage);
    $response = [
      "current_page" => (int)$page,
      "data" => $res,
      "last_page" => $last_page,
      "total" => $resCnt
    ];
    $this->userInfo = $dao->getUserInfo($this->userIdx);
    $nonInfo = json_decode("{}");
    return $this->resultOk($response, null,isset($this->userInfo[0]) ? $this->userInfo[0] : $nonInfo);

  }

  public function setReplyEdit() {
    $table_name = $this->getParam("table_name");
    $table_idx = $this->getParam("table_idx");
    $user_tags = $this->getParam("user_tags");
    $comment = $this->getParam("comment");
    $comment_idx = $this->getParam("comment_idx");
    $user_idx = $this->userIdx;

//    $userTags = $this->getParam("user_tags");

    $data = array(
      "table_name" => $table_name,
      "table_idx" => $table_idx,
      "user_tags" => $user_tags,
      "comment" => $comment,
      "from_user_idx" => $user_idx
    );

    if (!isset($comment_idx)) {
      $dao = new CBaseDAO();
      $res = $dao->insertQuery("comment_tbl", $data, "db_w");
    } else {
      $dao = new CBaseDAO();
      $res = $dao->updateQuery("comment_tbl", $data, "idx = {$comment_idx}","db_w");
    }
    if ($table_name == "cm_timeline" && !isset($comment_idx)) {
      $ctData = array(
        "_NQ_reply_count" => "reply_count + 1"
      );
      $dao->updateQuery("cm_timeline", $ctData, "idx = {$table_idx}","db_w");

      $userDao = new CUserDAO();
      $sender = $userDao->getUserInfo($user_idx);

      $pushTempData = array(
        "table_name" => $table_name,
        "table_idx" => $table_idx,
        "user_tags" => $user_tags,
        "comment" => $comment,
        "from_user_idx" => $user_idx,
        "is_img" => 0
      );

      $pushHelper = new CPushHelper();
      $communitiHelper = new CCommunityHelper();
      if($communitiHelper->getTimelineImgType($table_idx)) {
        $pushTempData['is_img'] = 1;
      }
      $pushData = json_encode($pushTempData);
      $receveiver = $dao->selectQuery("select user_idx, contents, service_type from fp_db.cm_timeline where idx = {$table_idx}","db");
      $communityTitle = array(
        "ldlife" => "러덕생활",
        "fit" => "러덕핏",
        "postscript" => "후기핏",
        "answer" => "답정러",
        "fllife" => "플리생활"
      );
      $pushCate = $communityTitle[$receveiver[0]->service_type];

      $contents = mb_strlen($receveiver[0]->contents) > 20 ? mb_substr($receveiver[0]->contents,0,20)."..." : $receveiver[0]->contents;
      if(isset($receveiver[0]) && $user_idx != $receveiver[0]->user_idx) {
        $pushHelper->sendPush($receveiver[0]->user_idx, '댓글', "[{$pushCate}] {$contents} 게시물에 '{$sender[0]->nic_name_full}'님이 댓글을 입력하셨습니다.", 'community', $pushData);
      }

      if (isset($user_tags)) {
        $arr = explode(',', $user_tags);
        foreach ($arr as $value) {
          $pushTagData = json_encode(array(
            "table_name" => $table_name,
            "table_idx" => $table_idx,
            "is_img" => 0
          ));
          if($communitiHelper->getTimelineImgType($table_idx)) {
            $pushTagData = json_encode(array(
              "table_name" => $table_name,
              "table_idx" => $table_idx,
              "is_img" => 1
            ));
          }
          $pushHelper->sendPush($value, '태그', "[{$pushCate}] {$contents} 게시물의 댓글에 '{$sender[0]->nic_name_full}'님이 언급하셨습니다.", 'community', $pushTagData);
        }
      }

    }
    return $this->resultOk($res);
  }

  public function setReplyDel() {
    $table_name = $this->getParam("table_name");
    $table_idx = $this->getParam("table_idx");
    $comment_idx = $this->getParam("comment_idx");
    $user_idx = $this->userIdx;

    if (!$table_name || !$table_idx || !$comment_idx || !$user_idx) return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");

    $delData = array(
      "_NQ_delete_date" => "now()"
    );
    $dao = new CBaseDAO();
    $res = $dao->updateQuery("comment_tbl", $delData,"table_name = '{$table_name}' and table_idx = {$table_idx} and idx = {$comment_idx} and from_user_idx = {$user_idx}","db_w");
    if ($table_name == "cm_timeline") {
      $ctData = array(
        "_NQ_reply_count" => "reply_count-1"
      );
      $dao->updateQuery("cm_timeline", $ctData, "idx = {$table_idx}","db_w");
    }
    return $this->resultOk($res);
  }

  #region ##### 통합검색 #####
  public function getSearchTotal() {
    $user_idx = $this->userIdx;
    $keyword = $this->getParam("keyword");
    $page = $this->getParam("page", 1);
    $type = $this->getParam("type");
    //상품
    $store = $this->searchStore($user_idx,$keyword, "popular", $page);
    //러덕생활
    $ld = $this->searchCommunity($user_idx,"ldlife",$keyword, "new", "all", $page, true);
    //러덕핏
    $fit = $this->searchCommunity($user_idx, "fit", $keyword, "new", "all", $page, true);
    //답정러
    $answer = $this->searchCommunity($user_idx, "answer", $keyword, "new", "all", $page, true);
    //매거진
    $magazine = $this->searchMagazine("magazine", "all", $keyword, $page);
    // 러덕
    $loved = $this->searchLoveDuk($keyword, $page);

    $brand = $this->searchBrand($keyword, $page, $user_idx);


    $store["type"] = "store";
    $ld["type"] = "ldlife";
    $fit["type"] = "fit";
    $answer["type"] = "answer";
    $magazine["type"] = "magazine";
    $loved["type"] = "loveduk";
    $brand["type"] = "brand";
    if ($type == "store") {
      $data = array(
        0 =>  $store,
        1 =>  $ld,
        2 =>  $fit,
        3 =>  $answer,
        4 => $loved,
        5 => $brand,
        6 =>  $magazine
      );
    } else if ($type == "ldlife") {
      $data = array(
        0 =>  $ld,
        1 =>  $store,
        2 =>  $fit,
        3 =>  $answer,
        4 => $loved,
        5 => $brand,
        6 =>  $magazine
      );
    } else if ($type == "fit") {
      $data = array(
        0 => $fit,
        1 => $store,
        2 => $ld,
        3 => $answer,
        4 => $loved,
        5 => $brand,
        6 => $magazine
      );
    } else if ($type == "answer") {
      $data = array(
        0 =>  $answer,
        1 =>  $store,
        2 =>  $ld,
        3 =>  $fit,
        4 => $loved,
        5 => $brand,
        6 =>  $magazine
      );

    } else if ($type == "magazine") {
      $data = array(
        0 =>  $magazine,
        1 =>  $store,
        2 =>  $ld,
        3 =>  $fit,
        4 => $loved,
        5 => $brand,
        6 =>  $answer
      );
    } else {
      $data = array(
        0 => $store,
        1 => $ld,
        2 => $fit,
        3 => $answer,
        4 => $loved,
        5 => $brand,
        6 => $magazine
      );
    }

    return $this->resultOk($data,"",$brand);
  }

  public function getSearch($service_type) {
    $user_idx = $this->userIdx;
    $keyword = $this->getParam("keyword");
    $order = $this->getParam("order");
    $page = $this->getParam("page", 1);
    if($service_type == "store"){
      //상품
      $res = $this->searchStore($user_idx,$keyword, $order, $page, false);
    } else if($service_type == "ldlife"){
      //러덕생활
      $res = $this->searchCommunity($user_idx,"ldlife",$keyword, $order, "all", $page, false);
    } else if($service_type == "fit"){
      //러덕핏
      $res = $this->searchCommunity($user_idx, "fit", $keyword, $order, "all", $page, false);
    } else if($service_type == "answer"){
      //답정러
      $res = $this->searchCommunity($user_idx, "answer", $keyword, $order, "all", $page, false);
    } else if($service_type == "magazine"){
      //매거진
      $res = $this->searchMagazine("magazine", "all", $keyword, $page, false);
    } else if($service_type == "loveduk"){
      //매거진
      $res = $this->searchLoveDuk($keyword, $page, false);
    } else if($service_type == "brand"){
      //브랜드
      $res = $this->searchBrand($keyword, $page,$user_idx, false);
    }

    return $this->resultOk($res);
  }

  // 상품
  private function searchStore($user_id, $keyword,$sort, $page, $is_sample = true) {
    $tempPage = $page;

    $num_per_page = 40;
    if($is_sample) {
      $num_per_page = 6;
    }
    $page = $tempPage - 1;
    $offset = ($page * $num_per_page) < 0 ? 0 : ($page * $num_per_page);

    $order = "order by fulltext_score desc, score desc, g.id desc";
    switch ($sort) {
      case 'price' :
      {
        $order = "order by fulltext_score desc, promotion_price asc, g.id";
        break;
      }
      case 'popular' :
      {
        $order = "order by fulltext_score desc, score desc, g.id";
        break;
      }
      case 'new' :
      {
        $order = "order by fulltext_score desc, g.created_at desc ,g.id";
        break;
      }
    }

    $likejoin['sel'] = $user_id ? ", case when lt.idx is null then 0 else 1 end as is_like_state" : ", 0 as is_like_state";
    $likejoin['join'] = $user_id ? "left join fp_db.like_tbl lt on lt.table_idx = g.id and lt.table_name = 'goods' and lt.user_idx = '$user_id'" : "";

    $keyword = str_replace('|:|', '+', $keyword);

    $keywordFull = str_replace(" ","* ",$keyword)."*";

    $erSel = "";
    $erJoin = "";
    $exhibitionRep = new CExhibitionHelper();
    $erRes = $exhibitionRep->getExhibitionRepMenu()['result'];
    if ($erRes) {
      $erSel = " , atm_tbl.exhibition_name";
      $erJoin = "left join (select sub.list_tag_name  as exhibition_name,eg.goods_id from exhibition_goods eg
  inner join (
select e.id, e.list_tag_name
from app_top_menu atm
inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true' and e.type = 'exhibition'
where atm.display = 'show' and e.is_hidden = 'true' and e.deleted_at is null and (e.start_date <= now() and e.end_date >= now())
order by e.id desc limit 1
) as sub on eg.exhibition_id = sub.id and eg.deleted_at is null) as atm_tbl on g.id = atm_tbl.goods_id";
    }

    $sql = "select g.id, g.title ,g.price
            , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
            , g.use_discount , g.imgs , g.stock_state , g.`view` ,st.name as seller_name, st.idx as seller_id
            ,IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
            ,IFNULL((IFNULL(SUM(od.quantity),0) * 0.70 + IFNULL(SUM(od.total_price),0) * 0.15 + IFNULL(SUM(g.view),0) * 0.15),0) AS score
            , g.like_count
            , IF(sp.idx is null, 0, 1) as is_hotdeal
            , sp.ended_at as hotdeal_end {$likejoin['sel']}
            {$erSel}
            , if(ifnull(match(g.title) against('{$keywordFull}' in boolean mode),0) + (if(instr('{$keyword}', st.name) > 0, 10, 0) ) <= 0,5,0)
            + ifnull(match(g.title) against('{$keywordFull}' in boolean mode),0)
            + (if(instr('{$keyword}', st.name) > 0, 10, 0) )
             + (if(instr('{$keyword}', g.title) > 0, 5, 0) )
             + if('{$keyword}' = g.title, 100, 0)as fulltext_score
            from fp_pay.goods g
            left join fp_pay.order_detail od on od.goods_id =g.id
            left join fp_db.seller_tbl st on g.seller_id = st.idx
            left join goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
            {$likejoin['join']}
            {$erJoin}
            where g.display_state = 'show' and g.deleted_at is null
            and st.display_state = 'show' and st.delete_date is null
            and (
            EXISTS (select tags.name as tag_name, taggables.taggable_id ,match(tags.name) against('{$keywordFull}' in boolean mode) as tag_score
            			from fp_pay.tags
            			inner join fp_pay.taggables on tags.id = taggables.tag_id
            			where taggables.taggable_type = 'App\\\Goods'
            			and ((MATCH(tags.name) AGAINST('{$keywordFull}' in boolean mode) or tags.name like '%{$keyword}%') and g.id = taggables.taggable_id ))
                or MATCH(g.title) AGAINST('{$keywordFull}' in boolean mode)
                or st.name LIKE '%{$keyword}%' and st.display_state = 'show' and st.delete_date is null
                or g.title like '{$keyword}%'
                or g.title like '%{$keyword}'
            )
            group by g.id
            {$order}
            ";
    $sqlCnt = "select g.id
            from fp_pay.goods g
            left join fp_pay.order_detail od on od.goods_id =g.id
            left join fp_db.seller_tbl st on g.seller_id = st.idx
            where g.display_state = 'show' and g.deleted_at is null
            and st.display_state = 'show' and st.delete_date is null
            and (
            EXISTS (select tags.name as tag_name, taggables.taggable_id ,match(tags.name) against('{$keywordFull}' in boolean mode) as tag_score
            			from fp_pay.tags
            			inner join fp_pay.taggables on tags.id = taggables.tag_id
            			where taggables.taggable_type = 'App\\\Goods'
            			and ((MATCH(tags.name) AGAINST('{$keywordFull}' in boolean mode) or tags.name like '%{$keyword}%') and g.id = taggables.taggable_id ))
                or MATCH(g.title) AGAINST('{$keywordFull}' in boolean mode)
                or st.name LIKE '%{$keyword}%' and st.display_state = 'show' and st.delete_date is null
                or g.title like '{$keyword}%'
                or g.title like '%{$keyword}'
            )
            group by g.id";

    $dao = new CBaseDAO();

    $result = $dao->selectQuery($sql." limit $offset, $num_per_page", "pay");
    $resCnt = count($dao->selectQuery($sqlCnt, "pay"));
//        $result = array_splice($tempRes, $offset, $num_per_page);

    $last_page = ceil($resCnt / $num_per_page) + 1;

    foreach ($result as $item) {
      $seller = [
        'id'   => $item->seller_id,
        'name' => $item->seller_name,
      ];
      $item->seller = $seller;
    }


    $response = [
      "total"        => $resCnt,
      "data"         => $result,
      "current_page" => (int)$page,
      "last_page"    => $last_page - 1,
    ];
    return $response;
  }

  #region ##### 커뮤니티 #####
  private function searchCommunity ($user_idx,$service_type, $keyword, $order, $category, $page, $is_sample = true) {
//    phpinfo();
//    $page = $this->getParam("page", 1);
//    $category = $this->getParam("category");
//    $timeline_idx = $this->getParam("timeline_idx");
//    $order = $this->getParam("order");
//    $user_idx = $this->userIdx;

    $isLike = $this->getParam("islike", null);

    if(!$user_idx && $category == "follow") return $this->resultFail('NO_LOGIN', "로그인이 필요합니다.");

    #region 정렬
    $orderSql = "order by ct.idx desc";
    if ($order == "new") $orderSql = "order by ct.idx desc";
    if ($order == "popular") $orderSql = "order by score desc";
    if ($order == "reply") $orderSql = "order by ct.reply_count desc";
    #endregion

    $perPage = 40;

    $dao = new CBaseDAO();

    if ($service_type == "ldlife" || $service_type == "fllife") {
      if($is_sample) {
        $perPage = 3;
      }
      $limit = $dao->getLimit($page, $perPage);
      $resTemp = $this->ldFlLifeList($service_type, $category, null, $order, $user_idx, $limit, $isLike, $keyword);
    } else if ($service_type == "fit") {
      if($is_sample) {
        $perPage = 6;
      }
      $limit = $dao->getLimit($page, $perPage);
      $resTemp = $this->fitList($service_type, $category, null, $order, $user_idx, $limit, $isLike, $keyword);
    } else if ($service_type == "answer") {
      if($is_sample) {
        $perPage = 6;
      }
      $limit = $dao->getLimit($page, $perPage);
      $resTemp = $this->answerList($service_type, $category, null, $order, $user_idx, $limit, $isLike, $keyword);
    }
    $res = $resTemp["data"];

//    $replyResult = array();
    foreach ($res as $item) {
      switch ($service_type) {
        case "fit" :
        {
          // 러덕핏
          $sql = "select *
                from cm_ld_fit clf 
                where timeline_idx = {$item->idx}
                and deleted_at is null";
          $resFit = $dao->selectQuery($sql, "db");

          $item->ld_fit = $resFit;

          // 후기핏
          $sql = "select * 
                from postscript_tbl pt 
                where pt.service_idx = {$item->idx}
                and pt.deleted_at is null";
          $resPostscript = $dao->selectQuery($sql, "db");
          $item->postscript = null;
          if (isset($resPostscript[0])) {
            $item->postscript = $resPostscript;
            foreach ($resPostscript as $orderItem) {
              $orderSql = "select st.name , g2.title ,od.origin_price ,od.promotion_price ,od.total_price , GROUP_CONCAT(o2.value SEPARATOR ', ') as option_info
                          , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image 
                          , g2.id as goods_id, st.idx as seller_id
                          , if(count(lt.idx) > 0, 1, 0) as is_like
                          , case when g2.sub_category = '악세사리' then 'acc'
                            when g2.sub_category = '뷰티' then 'beauty'
                            else 'etc' end as option_type
                          , g2.imgs as goods_img
                          from order_detail od
                          inner join goods g2 on od.goods_id = g2.id 
                          inner join fp_db.seller_tbl st on g2.seller_id = st.idx 
                          inner join option_sku os on od.sku_id = os.sku_id 
                          inner join `options` o2 on os.option_id = o2.id 
                          left join fp_db.like_tbl lt on lt.table_name = 'goods' and lt.table_idx = g2.id and lt.user_idx = '{$user_idx}'
                          where od.id = {$orderItem->order_detail_id}";
              $orderRes = $dao->selectQuery($orderSql, "pay");
              $orderItem->order = isset($orderRes[0]) ? $orderRes[0] : null;
            }

          }
          break;
        }
      }
//      if ($category == "postscript") {
//        // 후기핏
//        $sql = "select *
//                from postscript_tbl pt
//                where pt.service_idx = {$item->idx}
//                and pt.deleted_at is null";
//        $resPostscript = $dao->selectQuery($sql, "db");
//        $item->postscript = null;
//        if (isset($resPostscript[0])) {
//          $item->postscript = $resPostscript;
//        }
//      }

      $item->files = $this->getCommunityFiles($item->service_type, $item->idx);
      $replySql = "select ct.idx,ct.table_name ,ct.table_idx ,ct.comment ,ct.parent_idx ,ct.to_user_idx ,ct.from_user_idx ,list_date_view(ct.insert_date) as create_date, ct.user_tags 
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as from_nic_name, ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/',ul.profile_img), '') as from_profile_img
                , if(ul2.nic_name is null or REPLACE(ul2.nic_name, ' ', '') = '', ul2.shipping_name, ul2.nic_name) as  to_nic_name, ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/',ul2.profile_img), '') as to_profile_img
                from comment_tbl ct 
                left join user_list ul on ct.from_user_idx = ul.user_id 
                left join user_list ul2 on ct.to_user_idx = ul2.user_id 
                where ct.table_name = 'cm_timeline' and ct.table_idx = {$item->idx} and ct.delete_date is null order by ct.idx desc";
      $resReply = $dao->selectQuery($replySql, "db");

      if (isset($resReply)) {
        foreach ($resReply as $item2) {
          if (isset($item2->user_tags) && $item2->user_tags != "") {
            $userTagSql = "select user_id, if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name from user_list ul where user_id in ({$item2->user_tags})";
            $resUserTags = $dao->selectQuery($userTagSql, "db");
            $item2->user_tags = $resUserTags;
          } else {
            $item2->user_tags = array();
          }
        }
        $item->replyList = $resReply;
        $item->replyCnt = count($resReply);
      } else {
        $item->replyList = null;
        $item->replyCnt = 0;
      }
//      $replyResult["list"] = $resReply;
//      $replyResult["cnt"] = count($resReply);
    }

//    $resDate = array(
//      "data" => $res,
//      "reply" => $replyResult
//    );
    $dao = new CUserDAO();
    $this->userInfo = $dao->getUserInfo($user_idx);
    $resData = isset($timeline_idx) && isset($res[0]) ? $res[0] : $res;
    $nonInfo = json_decode("{}");
    if (isset($timeline_idx)){
      $response = $resData;
    } else {
      $last_page = ceil((int)$resTemp["cnt"] / $perPage);
      $response = [
        "current_page" => (int)$page,
        "data" => $resData,
        "last_page" => $last_page,
        "total" => $resTemp["cnt"],
        "user_info" => isset($this->userInfo[0]) ? $this->userInfo[0] : $nonInfo
      ];
    }

    return $response;
  }

  private function ldFlLifeList($service_type, $category, $timeline_idx, $order, $user_idx, $limit, $isLike = null, $keyword) {
    $orderSql = "order by ct.idx desc";
    if ($order == "new") $orderSql = "order by ct.idx desc";
    if ($order == "popular") $orderSql = "order by score desc";
    if ($order == "reply") $orderSql = "order by ct.reply_count desc";

    $dao = new CBaseDAO();
    $where = ($category == "all" || !$category) ? "" : "and ct.category = '{$category}'";
    $detailWhere = $timeline_idx ? "and ct.idx = {$timeline_idx}" : "";

    $isLikeJoin = "left join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    if(isset($isLike)) {
      $isLikeJoin = "inner join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    }
    $isMypage =  "";
    $keywordFull = str_replace(" ","* ",$keyword)."*";
    $sql = "select ct.* ,list_date_view(ct.created_at) as create_date ,count(cc.idx) as claim_cnt
                , (count(lt2.idx) * 0.7) + (count(ct2.idx) * 0.3) as score
                , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name
                , case when lt.idx is null then 0 else 1 end as is_like_state
                , case when ft.idx is null then 0 else 1 end as is_follow
                ,ifnull(match(ct.contents) against('{$keywordFull}' in boolean mode),0) as fulltext_score
                from fp_db.cm_timeline ct 
                left join fp_db.user_list ul on ct.user_idx = user_id
                 {$isLikeJoin}
                left join fp_db.cm_claim cc on ct.service_type = cc.service_type and ct.idx = cc.service_idx and cc.deleted_at is null
                left join fp_db.follow_tbl ft on ft.to_user_idx = ct.user_idx and ft.from_user_idx = '{$user_idx}' 
                left join fp_db.like_tbl lt2 on lt2.table_idx = ct.idx and lt2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 14
                left join fp_db.comment_tbl ct2 on ct2.table_idx = ct.idx and ct2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 14
                where ct.deleted_at is null and ct.service_type = '{$service_type}' and ct.claim_deleted_at is null
                and (ct.title like '%{$keyword}%' or ct.contents like '%{$keyword}%'
                or MATCH(ct.contents) AGAINST('{$keywordFull}' in boolean mode))
                {$isMypage}
                {$where}
                {$detailWhere}
                group by ct.idx 
                {$orderSql}
                {$limit}
                ";
    $res = $dao->selectQuery($sql, "db");
    $sqlCnt = "select count(ct.idx) as cnt from fp_db.cm_timeline ct {$isLikeJoin} 
                where ct.deleted_at is null and ct.claim_deleted_at is null 
                and ct.service_type = '{$service_type}'
                and (ct.title like '%{$keyword}%' or ct.contents like '%{$keyword}%'
                or MATCH(ct.contents) AGAINST('{$keywordFull}' in boolean mode))
                 {$where}";
    $resCnt = $dao->selectQuery($sqlCnt, "db")[0]->cnt;
    $data = array(
      "data" => $res,
      "cnt" => $resCnt
    );


    return $data;
  }

  private function fitList($service_type, $category, $timeline_idx, $order, $user_idx, $limit, $isLike = null, $keyword) {
    $orderSql = "order by ct.idx desc";
    if ($order == "new") $orderSql = "order by ct.idx desc";
    if ($order == "popular") $orderSql = "order by score desc";
    if ($order == "reply") $orderSql = "order by ct.reply_count desc";

    $where = "and (ct.service_type = 'fit' or ct.service_type = 'postscript')";
    $followJon = "";
    if ($category =="postscript") {
      $where = "and ct.service_type = 'postscript'";
    } else if ($category =="follow") {
      if(!$user_idx) return $this->resultFail("NO_LOGIN");
      $followJon = "inner join fp_db.follow_tbl ft on ct.user_idx = ft.to_user_idx and ft.deleted_at is null and ft.state = 2 and ft.from_user_idx = {$user_idx}";
    }
    $isLikeJoin = "left join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    if(isset($isLike)) {
      $isLikeJoin = "inner join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    }
    $dao = new CBaseDAO();
    $isMypage =  "";
    $detailWhere = $timeline_idx ? "and ct.idx = {$timeline_idx}" : "";
    $keywordFull = str_replace(" ","* ",$keyword)."*";
    $sql = "select ct.* ,list_date_view(ct.created_at) as create_date ,count(cc.idx) as claim_cnt
                , (count(lt2.idx) * 0.7) + (count(ct2.idx) * 0.3) as score
                , uei.user_size , uei.user_height , uei.user_skin ,uei.user_foot_size
                , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name
                , case when lt.idx is null then 0 else 1 end as is_like_state
                , case when ft2.idx is null then 0 else 1 end as is_follow
                ,ifnull(match(ct.contents) against('{$keywordFull}' in boolean mode),0) as fulltext_score
                from fp_db.cm_timeline ct 
                inner join fp_db.user_list ul on ct.user_idx = ul.user_id
                left join fp_db.user_extra_info uei on ul.user_id = uei.user_id 
                {$followJon}
                left join fp_db.cm_claim cc on ct.service_type = cc.service_type and ct.idx = cc.service_idx and cc.deleted_at is null
                {$isLikeJoin}
                left join fp_db.follow_tbl ft2 on ft2.to_user_idx = ct.user_idx and ft2.from_user_idx = '{$user_idx}' 
                left join cm_imgs ci on ct.idx = ci.service_idx and ci.deleted_at is null
                left join fp_db.like_tbl lt2 on lt2.table_idx = ct.idx and lt2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 14
                left join fp_db.comment_tbl ct2 on ct2.table_idx = ct.idx and ct2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 14
                left join fp_db.postscript_tbl pt on ct.idx = pt.service_idx 
                left join fp_pay.order_detail od on pt.order_detail_id = od.id 
                left join fp_db.seller_tbl st on od.seller_id = st.idx 
                where ct.deleted_at is null  and ct.claim_deleted_at is null and ci.idx is not null
                and (ct.title like '%{$keyword}%' or ct.contents like '%{$keyword}%' or st.name like '%{$keyword}%'
                or MATCH(ct.contents) AGAINST('{$keywordFull}' in boolean mode))
                {$isMypage}
                {$where}
                {$detailWhere}
                group by ct.idx 
                {$orderSql}
                {$limit}
                ";

    $res = $dao->selectQuery($sql, "db");
    $sqlCnt = "select ct.* from fp_db.cm_timeline ct {$isLikeJoin} {$followJon} 
left join fp_db.postscript_tbl pt on ct.idx = pt.service_idx 
left join fp_pay.order_detail od on pt.order_detail_id = od.id 
left join fp_db.seller_tbl st on od.seller_id = st.idx 
left join cm_imgs ci on ct.idx = ci.service_idx and ci.deleted_at is null
where ct.deleted_at is null and ct.claim_deleted_at is null and ci.idx is not null
and (ct.title like '%{$keyword}%' or ct.contents like '%{$keyword}%' or st.name like '%{$keyword}%'
                or MATCH(ct.contents) AGAINST('{$keywordFull}' in boolean mode))
{$where}
group by ct.idx";

    $resCnt = count($dao->selectQuery($sqlCnt, "db"));
    $data = array(
      "data" => $res,
      "cnt" => $resCnt
    );

    if ($user_idx && $category =="follow") {
      $sqlBadge = "select ct.idx
                from fp_db.cm_timeline ct 
                inner join fp_db.user_list ul on ct.user_idx = ul.user_id
                left join fp_db.cm_timeline_read ctr on ct.idx = ctr.service_idx and ctr.user_idx = {$user_idx}
                {$followJon}
                where ct.deleted_at is null and ct.claim_deleted_at is null and ctr.idx is null
                {$where}
                {$detailWhere}
                group by ct.idx
                ";

      $resBadge = $dao->selectQuery($sqlBadge, "db");
      if(count($resBadge)) {
        $badgeData = array();
        foreach ($resBadge as $key => $item) {
          $badgeData[$key]["service_type"] = 'fit';
          $badgeData[$key]["service_idx"] = $item->idx;
          $badgeData[$key]["user_idx"] = $user_idx;
        }
        $dao->insertMultiQuery("cm_timeline_read", $badgeData, "db_w");
      }
    }
    return $data;
  }

  private function answerList($service_type, $category, $timeline_idx, $order, $user_idx, $limit, $isLike = null, $keyword) {
    $orderSql = "order by ct.idx desc";
    if ($order == "new") $orderSql = "order by fulltext_score desc, ct.idx desc";
    if ($order == "popular") $orderSql = "order by fulltext_score desc, score desc";
    if ($order == "reply") $orderSql = "order by fulltext_score desc, ct.reply_count desc";

    $isLikeJoin = "left join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    if(isset($isLike)) {
      $isLikeJoin = "inner join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    }

    $dao = new CBaseDAO();
    $where = ($category == "all" || !$category || $category == "" ) ? "" : "and ct.category = '{$category}'";
    $detailWhere = $timeline_idx ? "and ct.idx = {$timeline_idx}" : "";
    $isMypage =  "";
    $keywordFull = str_replace(" ","* ",$keyword)."*";
    $sql = "select ct.* ,list_date_view(ct.created_at) as create_date ,count(cc.idx) as claim_cnt
                , (count(lt2.idx) * 0.7) + (count(ct2.idx) * 0.3) as score
                , ads.img_index as select_img_index ,ads.img_idx as select_img_idx ,count(ads2.idx) as answer_total_cnt
                , ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/',ul.profile_img), '') as profile_img
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name
                , case when lt.idx is null then 0 else 1 end as is_like_state
                , case when ft.idx is null then 0 else 1 end as is_follow
                ,ifnull(match(ct.contents) against('{$keywordFull}' in boolean mode),0) as fulltext_score
                from fp_db.cm_timeline ct 
                left join fp_db.user_list ul on ct.user_idx = user_id
                left join fp_db.cm_claim cc on ct.service_type = cc.service_type and ct.idx = cc.service_idx and cc.deleted_at is null
                left outer join fp_db.answer_decided_selectd ads on ct.idx = ads.service_idx and ads.user_idx = {$user_idx}
                left outer join fp_db.answer_decided_selectd ads2 on ct.idx = ads2.service_idx
                {$isLikeJoin}
                left join fp_db.follow_tbl ft on ft.to_user_idx = ct.user_idx and ft.from_user_idx = '{$user_idx}' 
                left join fp_db.like_tbl lt2 on lt2.table_idx = ct.idx and lt2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 14
                left join fp_db.comment_tbl ct2 on ct2.table_idx = ct.idx and ct2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 14
                where ct.deleted_at is null and ct.service_type = '{$service_type}' and ct.claim_deleted_at is null
                and (ct.title like '%{$keyword}%' or ct.contents like '%{$keyword}%'
                or MATCH(ct.contents) AGAINST('{$keywordFull}' in boolean mode))
                $isMypage
                {$where}
                {$detailWhere}
                group by ct.idx 
                {$orderSql}
                {$limit}
                ";

    $res = $dao->selectQuery($sql, "db");
    $sqlCnt = "select count(ct.idx) as cnt from fp_db.cm_timeline ct {$isLikeJoin} 
where ct.deleted_at is null and ct.claim_deleted_at is null and ct.service_type = '{$service_type}' 
and (ct.title like '%{$keyword}%' or ct.contents like '%{$keyword}%'
                or MATCH(ct.contents) AGAINST('{$keywordFull}' in boolean mode))
{$where}";
    $resCnt = $dao->selectQuery($sqlCnt, "db")[0]->cnt;
    $data = array(
      "data" => $res,
      "cnt" => $resCnt
    );


    return $data;
  }

  private function getCommunityFiles ($service_type, $service_idx) {
    if ($service_type == "answer") {
      $sql = "select ci.*, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket',img_path,img_name) as full_path
                , ifnull(count(ads.idx), 0) as answer_cnt
                from cm_imgs ci 
                left join fp_db.answer_decided_selectd ads on ci.service_idx = ads.service_idx and ci.idx = ads.img_idx 
                where ci.service_idx = {$service_idx}
                and ci.service_type = '{$service_type}'
                and ci.deleted_at is null
                group by ci.idx ";
    } else {
      $sql = "select *, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket',img_path,img_name) as full_path
                from cm_imgs ci 
                where service_idx = {$service_idx}
                and service_type = '{$service_type}'
                and deleted_at is null";
    }
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "db");
    return $res;
  }
  #endregion

  // 매거진
  private function searchMagazine($type, $sort, $keyword, $page, $is_sample = true) {

    $where[] = "content_type != 'ticket_feed' AND content_type != 'repoter' AND parent_idx = 0 AND delete_date IS NULL";
    if(isset($type)) {
      array_push($where,"content_type = '{$type}'");
    }
    $order = [];
    $select = [];
    $select[] = "idx, view_order, parent_idx, content_order,content_type,user_idx,source,content, source_bg_color";
    $select[] = "CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_contents/', image) AS image";
    $select[] = "(SELECT COUNT(*) AS child_cnt FROM user_content_tbl AS uct2 WHERE uct2.parent_idx = uct.idx) AS child_cnt";
    if(isset($sort)) {
      if ($sort == 'all') {
        $order[] = "view_order DESC";
      } else if ($sort == 'popular') {
        $select[] = "(SELECT COUNT(*) FROM like_tbl AS lt use index(table_name_idx) WHERE (table_name = 'user_content_tbl' AND lt.table_idx IN (SELECT idx FROM user_content_tbl WHERE idx = uct.idx OR parent_idx = uct.idx))) AS like_cnt";
        $order[] = "like_cnt DESC";
      } else if ($sort == 'new') {
        $order[] = "view_order DESC";
      } else if ($sort == 'review') {
        $select[] = "tag_idxs IS NOT NULL";
        $order[] = "view_order DESC";
      } else {
        $order[] = "view_order DESC";
      }
    } else {
      $order[] = "view_order DESC";
    }

    if(isset($keyword)) {
      $where[] = "content LIKE '%{$keyword}%'";
      $response["data"]["keyword123"] = $keyword;
    }

    $where_str = "";
    $i = 0;
    foreach($where as $item) {
      if($i==0) {
        $where_str .= "{$item}";
      } else {
        $where_str .= " AND {$item}";
      }
      $i++;
    }

    $order_str = "";
    $i = 0;
    foreach($order as $item) {
      if($i==0) {
        $order_str .= "{$item}";
      } else {
        $order_str .= ", {$item}";
      }
      $i++;
    }

    $select_str = "";
    $i = 0;
    foreach($select as $item) {
      if($i==0) {
        $select_str .= "{$item}";
      } else {
        $select_str .= ", {$item}";
      }
      $i++;
    }
    $dao = new CBaseDAO();
    $perPage = 24;
    if($is_sample) {
      $perPage = 6;
    }
    $limit = $dao->getLimit($page, $perPage);

    $sql = "SELECT {$select_str} FROM user_content_tbl AS uct WHERE {$where_str} ORDER BY {$order_str} {$limit}";
    $cnt_sql = "SELECT COUNT(*) AS cnt FROM user_content_tbl WHERE {$where_str}";
    $res = $dao->selectQuery($sql, "db");
    $cnt_res = $dao->selectQuery($cnt_sql, "db");

    $last_page = ceil((int)$cnt_res[0]->cnt / $perPage);
    $response = [
      "current_page" => (int)$page,
      "data" => $res,
      "last_page" => $last_page,
      "total" => $cnt_res[0]->cnt,
      "keywords" => ["입장권","러마페이","42회 러블리마켓"]
    ];

    return $response;
  }

  // 러덕
  private function searchLoveDuk( $keyword, $page, $is_sample = true) {

    $dao = new CBaseDAO();
    $perPage = 20;
    if($is_sample) {
      $perPage = 3;
    }
    $limit = $dao->getLimit($page, $perPage);

    $sql = "select ul.*,uei.introduce  
             , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name_full
            from user_list ul left join user_extra_info uei on ul.user_id = uei.user_id 
            where if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) like '%{$keyword}%' and ul.delete_date is null {$limit}";

    $cnt_sql = "SELECT COUNT(*) AS cnt from user_list ul where if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) like '%{$keyword}%' and ul.delete_date is null ";
    $res = $dao->selectQuery($sql, "db");
    $cnt_res = $dao->selectQuery($cnt_sql, "db");

    $last_page = ceil((int)$cnt_res[0]->cnt / $perPage);

    $response = [
      "current_page" => (int)$page,
      "data" => $res,
      "last_page" => $last_page,
      "total" => $cnt_res[0]->cnt
    ];

    return $response;
  }

  // 브랜드 배너
  // 러덕
  private function searchBrand($keyword, $page,$user_idx, $is_sample= true) {

    $dao = new CBaseDAO();
    $perPage = 20;
    if($is_sample) {
      $perPage = 3;
    }
    $limit = $dao->getLimit($page, $perPage);

    $sql = "select st.idx,st.name as seller_name,st.introduce
                    , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image
                    , st.like_count 
                    , case when lt.idx is null then 0 else 1 end as is_like_state
                    from seller_tbl st
                    left join fp_db.like_tbl lt on st.idx =lt.table_idx and table_name = 'seller_tbl' and lt.user_idx = '$user_idx' 
                    where st.name like '%{$keyword}%' and st.delete_date is null and st.display_state = 'show'   {$limit}";
    $cnt_sql = "SELECT COUNT(*) AS cnt from seller_tbl st where st.name like '%{$keyword}%' and st.delete_date is null and st.display_state = 'show' ";
    $res = $dao->selectQuery($sql, "db");
    $cnt_res = $dao->selectQuery($cnt_sql, "db");
    $last_page = ceil((int)$cnt_res[0]->cnt / $perPage);
    $response = [
      "data" => $res,
      "current_page" => (int)$page,
      "last_page" => $last_page,
      "total" => $cnt_res[0]->cnt
    ];

    return $response;
  }

  #endregion

  public function setDeviceInfo()
  {
    $device = $this->getParam("device");
    $os_version = $this->getParam("os_version");
    $device_key = $this->getParam("device_key");
    $device_model = $this->getParam("device_model");

    $user_id = $this->userIdx;
    $res = false;
    if ($user_id > 0) {
      $deviceType = "";
      switch ($device) {
        case 102 :
          $deviceType = "IOS";
          break;
        case 103 :
          $deviceType = "ANDROID";
          break;
      }
      $deviceInfo = [
        "os_type"      => $deviceType,
        "os_version"   => $os_version ? $os_version : null,
        "device_key"   => $device_key ? $device_key : null,
        "device_model" => $device_model ? $device_model : null,
      ];

      $dao = new CBaseDAO();
      $res = $dao->updateQuery("user_list", $deviceInfo, "user_id = {$user_id}", "db_w");
//      $res = $this->conn_w->table("user_list")
//        ->where("user_id", "=", $user_id)
//        ->update($deviceInfo);
      $extraCheck = $dao->selectQuery("select * from user_extra_info where user_id = {$user_id}", "db");
      if (count($extraCheck) <= 0) {
        $dao->insertQuery("user_extra_info",array("user_id" => $user_id),"db_w");
      }
      $dao->updateQuery("user_list",array("_NQ_login_date" => "now()"), "user_id = {$user_id}","db_w");
    }
    return $res;
  }

  public function getBanner()
  {
    $bid = $this->getParam("bid");
    $bnPosition = $this->getParam("bnPosition");

    $bannerWhere = $bid > 0 ? " and sb.idx = {$bid}" : "";
    $sql = "select sb.idx, sb.title, sb.`type` ,sb.image ,sb.vertical_image ,sb.link_type ,sb.target_id , sb.target_service , sb.target_url
                from fp_pay.store_banners sb
                where sb.display_state = 'show'
                and (sb.started_at <= now() and sb.ended_at >= now())
                and banner_position = '{$bnPosition}'
                $bannerWhere
                order by display_order ";

    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "pay");

    $result = [
      'show'     => count($res) > 0,
      'location' => $bnPosition,
      'data'     => $res,
    ];
    return $result;
  }

  public function getFooter()
  {
    return $this->resultOk([
      'company_name'        => '(주)플리팝',
      'ceo_name'            => '김동화',
      'company_number'      => '888-88-00543',
      'company_sale_number' => '2019-서울영등포-0529호',
      'address'             => '서울특별시 강남구 학동로3길 9 2층 (주)플리팝',
      'email'               => 'fleamin@fleapop.co.kr',
      'info_name'           => '오준석',
      'company_phone'       => '070-8812-0820'
    ]);
  }

}