<?php


namespace App\Http\Controllers;

use App\Helper\CPushHelper;
use App\Helper\CUserHelper;
use App\Http\dao;
//use DB;
use App\Http\dao\CBaseDAO;
use App\Http\dao\CUserDAO;
use App\Lmpay\RefundController;
use Facade\Ignition\Support\Packagist\Package;
use App\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

//use App\Http\dao\CUserDAO;



class UserController extends Controller
{
  const FACEBOOK_ACCOUNT = 2;
  const EMAIL_ACCOUNT    = 1;

  const account_type_name = array(
    1 => "이메일",
    2 => "페이스북",
    3 => "",
    4 => "애플",
    5 => "카카오",
    6 => "네이버",
    7 => "구글"
  );


  public function nativeLogin() {
    $account_type = $this->getParam("account_type");
//    $account = $this->getParam("account");
//    $password = $this->getParam("password") ? encryptPassword($this->getParam("password")) : null;
    $device = $this->getParam("device");
    $user_idx = $this->getParam("user_id");
    $token = $this->getParam("token");

    if (!$account_type || !$user_idx || !$device || !$token) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $dao = new dao\CUserDAO();

    $userTemp = $dao->getUserInfo($user_idx);

    if (count($userTemp) > 0) {
      $user = $userTemp[0];
      $is_have_shipping_data = true;
      if (strlen($user->shipping_phone) < 5 || empty($user->shipping_name)) {
        $is_have_shipping_data = false;
      }
//      Helper\CUserHelper
      Helper\CUserHelper::setLoginCookie($user);

      $response = array();
      $response["data"] = [
        "account_type"          => $user->account_type,
        "user_id"               => $user->user_id,
        "nic_name"              => $user->nic_name,
        "token"                 => makeToken($user->user_id),
        "is_have_shipping_data" => $is_have_shipping_data,
        "info"                  => $user
      ];
      $extraCheck = $dao->selectQuery("select * from user_extra_info where user_id = {$user_idx}", "db");
      if (count($extraCheck) <= 0) {
        $dao->insertQuery("user_extra_info",array("user_id" => $user_idx),"db_w");
      }
      if ($device == 101) {
        $dao->updateQuery("user_list", array("_NQ_login_date" => "now()"), "user_id = {$user_idx}", "db_w");
      }
      return $this->resultOk($response);
    } else {
      return $this->resultFail("NO_USER", "입력하신 아이디 또는 비밀번호가 일치하지 않습니다.");
    }

  }
  public function Login() {
    $account_type = $this->getParam("account_type");
    $account = $this->getParam("account");
    $password = $this->getParam("password") ? encryptPassword($this->getParam("password")) : null;
    $device = $this->getParam("device");

    if (!$account_type || !$account || !$device || ($account_type == self::EMAIL_ACCOUNT && !$password)) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $dao = new dao\CUserDAO();

    $userTemp = $dao->getUserCheck($account_type, $account, $password);

    if (count($userTemp) > 0) {
      $user = $userTemp[0];
      $is_have_shipping_data = true;
      if (strlen($user->shipping_phone) < 5 || empty($user->shipping_name)) {
        $is_have_shipping_data = false;
      }
//      Helper\CUserHelper
      Helper\CUserHelper::setLoginCookie($user);

      $response = array();
      switch ($device) {
        case self::WEB :
        {
          $response["data"] = [
            "account_type"          => $user->account_type,
            "user_id"               => $user->user_id,
            "nic_name"              => $user->nic_name,
            "token"                 => makeToken($user->user_id),
            "is_have_shipping_data" => $is_have_shipping_data,
          ];
          break;
        }
        case self::IOS :
        {
          $response["data"] = [
            "account_type"          => $user->account_type,
            "user_id"               => $user->user_id,
            "nic_name"              => $user->nic_name,
            "token"                 => makeToken($user->user_id),
            "is_have_shipping_data" => $is_have_shipping_data,
          ];
          break;
        }
        case self::ANDROID :
        {
          $response["data"] = [
            "account_type"          => $user->account_type,
            "user_id"               => $user->user_id,
            "nic_name"              => $user->nic_name,
            "token"                 => makeToken($user->user_id),
            "is_have_shipping_data" => $is_have_shipping_data,
          ];
          break;
        }
      }
      $extraCheck = $dao->selectQuery("select * from user_extra_info where user_id = {$user->user_id}", "db");
      if (count($extraCheck) <= 0) {
        $dao->insertQuery("user_extra_info",array("user_id" => $user->user_id),"db_w");
      }
      if ($device == 101) {
        $dao->updateQuery("user_list", array("_NQ_login_date" => "now()"), "user_id = {$user->user_id}", "db_w");
      }
      return $this->resultOk($response);
    } else {
      return $this->resultFail("NO_USER", "입력하신 아이디 또는 비밀번호가 일치하지 않습니다.");
    }

  }

  public function logout()
  {
    unset($_COOKIE["gCV"]);
    setcookie('gCV', '', time() - 1);
  }

  public function checkAccount() {
    $account_type = $this->getParam("account_type");
    $account = $this->getParam("account");
    $password = $this->getParam("password") ? encryptPassword($this->getParam("password")) : null;

    $dao = new dao\CUserDAO();

    $userTemp = $dao->getUserCheck($account_type, $account, $password);

    if (count($userTemp) > 0) {
      return $this->resultFail("USER_OVERLAP", "중복된 계정이 존재합니다.");
    } else {
      return $this->resultOk();
    }
  }

  public function sendJoinSms() {
    $phoneNum = $this->getParam("phone");

    if (!$phoneNum) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $phone = preg_replace("/[^0-9]/", "", $phoneNum);
    if (preg_match("/^01[0-9]{8,9}$/", $phone)) {
      $secure = rand(1000, 9999);
      $text = "플리팝 가입 인증번호 입니다. [$secure]";

      $res = $this->sendSMS($phone, $text);

      if (@isset($res['error_msg'])) {
        if ($phone == '01055443350') {
          return $this->resultOk();
        } else {
          return $this->resultFail("ERROR_SMS", $res['error_msg']);
        }
      } else {
        $res = json_decode($res["result"], true);
        $time = strtotime(date('Y-m-d H:i:s') . ' +3 minute');
        $time = date('Y-m-d H:i:s', $time);
        // print_r($time);
        // echo "as : ".strtotime(date('Y-m-d H:i:s'), now());

        $pt = array(
          "number" => $secure,
          "phone"  => $phone,
          "time"   => $time,
          "code"   => $res['result_code'],
          "cmid"   => $res['cmid']
        );
        $dao = new dao\CUserDAO();
        $res = $dao->insertQuery("sms_auth_tbl", $pt, "db_w");
        return $this->resultOk();
      }
    } else {
      return $this->resultFail("NO_VALID", "유효하지 않은 번호입니다.");
    }
  }

  private function sendSMS($dest_phone, $text)
  {
    $response = ["success" => true];
    $data = [
      'send_name'  => '플리팝',
      'send_phone' => '07088120820',
      'dest_name'  => '플리팝고객',
      'dest_phone' => $dest_phone,
      'msg_body'   => $text,
    ];
    $data = http_build_query($data);

    $sql = "select * from block_sms_tbl where delete_date is null";
    $dao = new dao\CUserDAO();
    $block_numbers = $dao->selectQuery($sql, "db");

    $blocks = [];
    foreach ($block_numbers as $number) {
      array_push($blocks, $number->number);
    }

    if (in_array($dest_phone, $blocks)) {
      $response['success'] = false;
      $response['error_msg'] = "발송금지된 번호입니다.";
      return $response;
    }

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.apistore.co.kr/ppurio/1/message/sms/fleapopinc");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $headers = [
      "Content-Type: application/x-www-form-urlencoded; charset=UTF-8",
      "x-waple-authorization: Nzk2MS0xNTIyMTMxMTQxMjUyLTc2MzZjZDE0LWQ2YzctNDJlZS1iNmNkLTE0ZDZjN2QyZWU3Mg==",
    ];
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $response['result'] = curl_exec($ch);
    curl_close($ch);

    return $response;
  }

  public function checkJoinSMS()
  {
    $phone = $this->getParam("phone");
    $code = $this->getParam("code");

    if (!$phone || !$code) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $phone = preg_replace("/[^0-9]/", "", $phone);
    $dao = new CBaseDAO();
    $sql = "SELECT COUNT(*) as cnt FROM sms_auth_tbl WHERE number = '{$code}' AND phone = '{$phone}' AND time >= CURRENT_TIMESTAMP()";
    $res = $dao->selectQuery($sql, "db")[0];

    if ($res->cnt < 1) {
      return $this->resultFail("MISS_VALID", "올바른 인증번호가 아닙니다.");
    }

    return $this->resultOk();

  }

  public function Join()
  {
    $account_type = $this->getParam("account_type");
    $nic_name = $this->getParam("nic_name");
    $birth_year = $this->getParam("birth_year");
    $birth_month = $this->getParam("birth_month");
    $birth_day = $this->getParam("birth_day");
    $gender = $this->getParam("gender");
    $shipping_name = $this->getParam("shipping_name");
    $shipping_phone = $this->getParam("shipping_phone");
    $shipping_zip_code = $this->getParam("shipping_zip_code");
    $shipping_address_1 = $this->getParam("shipping_address_1");
    $shipping_address_2 = $this->getParam("shipping_address_2");

    $email_account = $this->getParam("email_account");
    $pwd = $this->getParam("pwd");
    $fb_account = $this->getParam("fb_account");

    $required = array(
      "account_type",  "shipping_name", "shipping_phone",
    );

//    $checkData = [$account_type, $nic_name, $birth_year, $birth_month, $birth_day, $gender, $shipping_name, $shipping_phone_header, $shipping_phone_body, $shipping_zip_code, $shipping_address_1, $shipping_address_2];
    $isReq = $this->isRequired($required, $this->getParameters());

    if (!$isReq) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }
    $phone = preg_replace("/[^0-9]/", "", $shipping_phone);
    $dao = new dao\CUserDAO();
    $sql = "SELECT * FROM user_list WHERE account_type = {$account_type} and shipping_phone = '{$phone}' and delete_date is null";
    $phoneCheck = $dao->selectQuery($sql, "db");
    if(count($phoneCheck) > 0) {
      $exData = array(
        'email_account' => $phoneCheck[0]->email_account
      );

      return $this->resultFail("ALREADY_EXIST", "이미 등록된 휴대폰 번호입니다.", $exData);
    }
    $join_data = [
      "account_type"       => $account_type,
      "email_account"      => $email_account,
      "fb_account"         => $fb_account,
      "password"           => "",
      "nic_name"           => $nic_name,
      "birth_day"          => $birth_year . "-" . $birth_month . "-" . $birth_day,
      "gender"             => $gender,
      "shipping_name"      => $shipping_name,
      "shipping_phone"     => $phone,
      "shipping_zip_code"  => $shipping_zip_code,
      "shipping_address_1" => $shipping_address_1,
      "shipping_address_2" => $shipping_address_2,
    ];

    if ($account_type == self::EMAIL_ACCOUNT) {
      // 이메일 회원가입

      if (!$email_account || !$pwd) {
        return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
      }

      $join_data["email_account"] = $email_account;
      $join_data["password"] = encryptPassword($pwd);

    } else {
      if (!$fb_account) {
        return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
      }
      $join_data["fb_account"] = $fb_account;
      $join_data["password"] = encryptPassword("fpfpfp123123!@");
    }

    $res = $dao->insertQuery("user_list", $join_data, "db_w");

    if (! $res) {
      return $this->resultFail("FAIL", "회원가입에 실패했습니다.");
    }

    return $this->resultOk();
  }

  public function getUserInfo() {
    $dao = new dao\CUserDAO();
    $res = $dao->getUserInfo($this->userIdx);

    if (isset($res[0])) {
      return $this->resultOk($res[0]);
    } else {
      return $this->resultFail("NO_USER", "회원정보가 없습니다.");
    }
  }

  public function getFindAccount(){
    $phone = $this->getParam("phone");
    $code = $this->getParam("code");

    if (!$phone || !$code) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $phone = preg_replace("/[^0-9]/", "", $phone);
    $dao = new CBaseDAO();
    $sql = "SELECT COUNT(*) as cnt FROM sms_auth_tbl WHERE number = '{$code}' AND phone = '{$phone}' AND time >= CURRENT_TIMESTAMP()";
    $res = $dao->selectQuery($sql, "db")[0];

    if ($res->cnt < 1) {
      return $this->resultFail("MISS_VALID", "올바른 인증번호가 아닙니다.");
    }

    $sql = "select account_type, email_account from fp_db.user_list ul where shipping_phone = '{$phone}' and delete_date is null order by user_id desc limit 1";
    $res = $dao->selectQuery($sql, "db");

    $sqlList = "select account_type, email_account from fp_db.user_list ul where shipping_phone = '{$phone}' and delete_date is null order by user_id desc ";
    $resList = $dao->selectQuery($sqlList, "db");
    $dataList = array();
    foreach ($resList as $key => $item) {
      $dataList[$key] = array(
        "account_type" => $item->account_type,
        "email_account" => $item->email_account,
        "message" => self::account_type_name[$item->account_type]
      );
    }

    if(isset($res[0])) {
      $data = array(
        "account_type" => $res[0]->account_type,
        "email_account" => $res[0]->email_account,
        "message" => self::account_type_name[$res[0]->account_type]." 계정으로 회원가입이 되어 있습니다.",
        "list" => $dataList
      );
      return $this->resultOk($data);
    } else {
      return $this->resultFail("NO_USER", "가입정보가 없습니다.");
    }
  }

  public function getFindPassword(){
    $phone = $this->getParam("phone");
    $email_account = $this->getParam("email_account");
    $code = $this->getParam("code");

    $required = array(
      "phone", "email_account"
    );
    if (!$this->isRequired($required, $this->getParameters())) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $phone = preg_replace("/[^0-9]/", "", $phone);
    $dao = new CBaseDAO();
    $sql = "SELECT COUNT(*) as cnt FROM sms_auth_tbl WHERE number = '{$code}' AND phone = '{$phone}' AND time >= CURRENT_TIMESTAMP()";
    $resAuth = $dao->selectQuery($sql, "db")[0];

    if ($resAuth->cnt < 1) {
      return $this->resultFail("MISS_VALID", "올바른 인증번호가 아닙니다.");
    }

    $dao = new dao\CUserDAO();
    $res = $dao->getFindPassword($email_account, $phone );
    if ($res) {
      if ($res->account_type == self::FACEBOOK_ACCOUNT) {
        return $this->resultFail("SNS_FACEBOOK", "페이스북 계정입니다.");
      } else {
        return $this->resultOk();
      }
    } else {
      return $this->resultFail("NO_USER", "해당 이메일 계정이 존재하지 않습니다.");
    }
  }

  public function setPasswordChange() {
    $newPassword = $this->getParam('password');
    $phone = $this->getParam("phone");
    $email_account = $this->getParam("email_account");

    $required = array(
      "phone", "email_account", "password"
    );
    if (!$this->isRequired($required, $this->getParameters())) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $dao = new dao\CUserDAO();
    $res = $dao->getFindPassword($email_account, $phone );

    $pt = array(
      "password" => encryptPassword($newPassword)
    );

    $res = $dao->updateQuery("user_list", $pt,"user_id = '{$res->user_id}'", "db_w");

    return $this->resultOk();
  }

  /**
   * 팔로우
   * to_user_idx
   * action Y/N
   * @return \Illuminate\Http\JsonResponse
   */
  public function setFollow() {
    $to_user_idx = $this->getParam("to_user_idx");
    $user_idx = $this->userIdx;
    if (!$user_idx) {
      return $this->resultFail("NO_LOGIN", "로그인 후 이용가능합니다.");
    }
    if (!$to_user_idx) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $data = array(
      "from_user_idx" => $user_idx,
      "to_user_idx" => $to_user_idx
    );
    $dao = new CBaseDAO();
    $nSql = "select * from fp_db.follow_tbl where from_user_idx = {$user_idx} and to_user_idx = {$to_user_idx}";
    $nRes = $dao->selectQuery($nSql, "db");
    if (count($nRes) <= 0) {
      $dao->insertQuery("fp_db.follow_tbl", $data, "db_w");

      $pushHelper = new CPushHelper();
      $pushData = json_encode(array(
        "to_user_idx" => $to_user_idx,
        "from_user_idx" => $user_idx
      ));
      $receveiver = $dao->selectQuery("select if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name from fp_db.user_list ul where user_id = {$user_idx}","db");

      $pushHelper->sendPush($to_user_idx, '팔로우', "[팔로우] {$receveiver[0]->nic_name} 님께서 팔로잉 하셨습니다.", 'community', $pushData);

    } else {
      $dao->deleteQuery("fp_db.follow_tbl", "from_user_idx = {$user_idx} and to_user_idx = {$to_user_idx}", "db_w");
    }
    $response = array(
      "follow" => !(count($nRes) > 0)
    );
    return $this->resultOk($response);
  }

  public function setUserProfileImg(Request $request)
  {
    $user_id = $this->userIdx;
    $is_del = $this->getParam("is_del");
    if(!$user_id) $this->resultFail("NO_LOGIN", "회원정보가 올바르지 않습니다.");
    $fileHelper = new Helper\CFileuploadHelper();
    $dao = new CBaseDAO();
    if ($is_del) {
      $dao = new CUserDAO();
      $userInfo = $dao->getUserInfo($user_id)[0];

      if (gettype(mb_strpos($userInfo->profile_img, "default")) == "boolean") {
        $fileHelper->fileDelete("/user_profile/" . $userInfo->profile_img);

        $delData = array(
          "profile_img" => "default_profile.png"
        );
        $dao->updateQuery("user_list", $delData, "user_id = {$user_id}", "db_w");
      }
    } else {
      $image = getUnique() . time() . '.png';
      $fileRes = $fileHelper->commonFileUpload($request, S3_PATH_USER_PROFILE . $image);

      if (!$fileRes["result"]) {
        return $this->resultFail("FAIL", "파일업로드에 실패했습니다.");
      }

      $data = array(
        "profile_img" => $image
      );
      $res = $dao->updateQuery("user_list", $data, "user_id = " . $user_id, "db_w");

      if (!$res) {
        return $this->resultFail("FAIL", "변경에 필요한 필수내용이 누락되었습니다");
      }
    }
    return $this->resultOk();
  }

  public function setUserInfo() {
    $user_id = $this->userIdx;
    if(!$user_id) return $this->resultFail("NO_LOGIN", "회원정보가 올바르지 않습니다.");


    // type: base, body
    $type = $this->getParam("type");
    $shipping_name = $this->getParam("shipping_name");
    $nic_name = $this->getParam("nic_name");
    $birth = $this->getParam("birth");
    $gender = $this->getParam("gender");
    $user_height = $this->getParam("user_height");
    $user_size = $this->getParam("user_size");
    $user_foot_size = $this->getParam("user_foot_size");
    $user_skin = $this->getParam("user_skin");
    $is_body_open = $this->getParam("is_body_open");
    $introduce = $this->getParam("introduce");

    $failedNickname = array(
      "플리팝", "플리팝오피셜", "러블리마켓", "러마", "러마오피셜", "러블리마켓오피셜", "fleapop", "fleapopOfficial", "lovelymarket", "lovelymarketOfficial"
    );
    if (in_array($nic_name, $failedNickname)) {
      return $this->resultFail("FAIL", "사용불가능한 닉네임입니다.");
    }

    $dao = new CBaseDAO();
//    dd($type ."___". "base");
    if($type == "base") {
//      dd($type);

      $data = array();
//      $data = array(
//        "shipping_name" => $shipping_name,
//        "nic_name" => $nic_name,
//        "birth_day" => $birth,
//        "gender" => $gender,
//      );
      if (isset($shipping_name)) $data["shipping_name"] = $shipping_name;
      if (isset($nic_name)) $data["nic_name"] = $nic_name;
      if (isset($birth)) $data["birth_day"] = $birth;
      if (isset($gender)) $data["gender"] = $gender;

      $dao->updateQuery("user_list", $data, "user_id = '{$user_id}'", "db_w");

//      $dataExtra = array(
//        "introduce" => $introduce
//      );
      if (isset($introduce)) {
        $dataExtra["introduce"] = $introduce;

        $dao->updateQuery("user_extra_info", $dataExtra, "user_id = '{$user_id}'", "db_w");
      }
    } else if($type == "body") {
//      $data = array(
//        "user_height" => $user_height,
//        "user_foot_size" => $user_foot_size,
//        "user_size" => $user_size,
//        "user_skin" => $user_skin,
//        "is_body_open" => $is_body_open
//      );
      $data = array();
      if ($user_height) $data["user_height"] = $user_height;
      if ($user_foot_size) $data["user_foot_size"] = $user_foot_size;
      if ($user_size) $data["user_size"] = $user_size;
      if ($user_skin) $data["user_skin"] = $user_skin;
      if (isset($is_body_open)) $data["is_body_open"] = $is_body_open;
      $dao->updateQuery("user_extra_info", $data, "user_id = '{$user_id}'", "db_w");
    }
    return $this->resultOk();
  }

  public function setAccountPasswordChange() {
    $newPassword = $this->getParam('pass');
    $n_pass = $this->getParam("n_pass");
    $user_id = $this->userIdx;

    if(!$user_id) $this->resultFail("NO_LOGIN", "회원정보가 올바르지 않습니다.");

    $required = array(
      "pass", "n_pass"
    );
    if (!$this->isRequired($required, $this->getParameters())) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $dao = new dao\CUserDAO();
    $res = $dao->getAccountFindPassword(encryptPassword($newPassword) ,$user_id);

    if(!isset($res->user_id)) return $this->resultFail("NO_USER", "현재 비밀번호가 잘못입력되었습니다.");
    $pt = array(
      "password" => encryptPassword($n_pass)
    );

    $dao->updateQuery("user_list", $pt,"user_id = '{$user_id}'", "db_w");

    return $this->resultOk();
  }

  public function setPhoneNumber() {
    $phone = $this->getParam("phone");
    $code = $this->getParam("code");

    $user_id = $this->userIdx;

    if(!$user_id) {
      return $this->resultFail("NO_LOGIN", "회원정보가 올바르지 않습니다.");
    }

    if (!$phone || !$code) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $phone = preg_replace("/[^0-9]/", "", $phone);
    $dao = new CBaseDAO();
    $sql = "SELECT * FROM sms_auth_tbl WHERE number = '{$code}' AND phone = '{$phone}' AND time >= CURRENT_TIMESTAMP()";
    $res = $dao->selectQuery($sql, "db");

    if (count($res) < 1) {
      return $this->resultFail("MISS_VALID", "올바른 인증번호가 아닙니다.");
    } else {
      $pt = array(
        "shipping_phone" => $phone
      );
      $dao->updateQuery("user_list", $pt,"user_id = '{$user_id}'", "db_w");
      return $this->resultOk();
    }
  }

  public function getUserWithdrawal() {
    $user_idx = $this->userIdx;
    if(!$user_idx) $this->resultFail("NO_LOGIN", "회원정보가 올바르지 않습니다.");
    $lmPaySql = "SELECT IFNULL(SUM(amount), 0) as amount FROM fp_pay.cash_log 
                  WHERE user_idx = {$user_idx} AND delete_date IS NULL 
                  AND (`state` = '결제됨' OR (`action` = '환불' AND `state` = '결제대기') OR (action = '환불' AND state = '기타')) 
                  AND `action` != '취소'
                  AND `method` not in ('EVENT', 'REVIEW', 'ITEMBUY')";
    $refund = new RefundController();
    $res = $refund->amount($user_idx);

    $sql = "select * from fp_db.lm_code_tbl lct where lct.type = 'USER_WITHDRAWAL' and lct.is_show = 'show' order by sort_order ";
    $dao = new CBaseDAO();
//    $res1 = $dao->selectQuery($lmPaySql, "db")[0];
    $res2 = $dao->selectQuery($sql, "db");
    $data = array(
      "lmpay" => $res['data']['trans'] + $res['data']['card'],
      "reason" => $res2
    );
    return $this->resultOk($data);
  }

  public function setUserWithdrawal() {
    $withdrawal_code = $this->getParam("withdrawal_code");
    $withdrawal_text = $this->getParam("withdrawal_text");
    $withdrawal_complain = $this->getParam("withdrawal_complain");
    $user_idx = $this->userIdx;
    if(!$user_idx) $this->resultFail("NO_LOGIN", "회원정보가 올바르지 않습니다.");

    $required = array(
      "withdrawal_code", "withdrawal_text"
    );

    if (!$this->isRequired($required, $this->getParameters())) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $dao = new CBaseDAO();
//    $userData = array(
//      "_NQ_delete_date" => "now()"
//    );
//    $dao->updateQuery("user_", $userData, "user_id = ". $user_idx, "db_w");
    $userExtraData = array(
      "withdrawal_code" => $withdrawal_code,
      "withdrawal_text" => $withdrawal_text,
      "withdrawal_complain" => $withdrawal_complain,
      "is_withdrawal" => 1,
      "_NQ_request_deleted_at" => "now()"
    );
    $dao->updateQuery("user_extra_info", $userExtraData, "user_id = ". $user_idx, "db_w");

    return $this->resultOk(null, "정상적으로 신청 되었습니다.");
  }

  public function getUserAddress() {
    $user_idx = $this->userIdx;
    $idx = $this->getParam("idx");
    if(!$user_idx) $this->resultFail("NO_LOGIN", "회원정보가 올바르지 않습니다.");
    $where = "";
    if($idx) $where = " and id = {$idx}";
    $sql = "select *
				from user_address ua where user_id = {$user_idx} and deleted_at is null
				{$where}
				order by is_default desc";
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "pay");
    return $this->resultOk($res);
  }

  public function setUserAddress() {
    $user_idx = $this->userIdx;
    if(!$user_idx) $this->resultFail("NO_LOGIN", "회원정보가 올바르지 않습니다.");
    $idx = $this->getParam("idx");
    $address_type = $this->getParam("address_type");
    $name = $this->getParam("name");
    $phone = $this->getParam("phone");
    $shipping_name = $this->getParam("shipping_name");
    $zip_code = $this->getParam("zip_code");
    $address_1 = $this->getParam("address_1");
    $address_2 = $this->getParam("address_2");
    $memo = $this->getParam("memo");
    $is_default = $this->getParam("is_default");

    $required = array(
      "address_type",  "shipping_name", "phone",
    );
    $isReq = $this->isRequired($required, $this->getParameters());
    if (!$isReq) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $dao = new CBaseDAO();
    $isUpdate = false;
    if(!$idx) {
      $res = $dao->selectQuery("select * from user_address where user_id = {$user_idx} and address_type = '{$address_type}' and deleted_at is null", "pay");
      if (count($res) > 0) {
        $idx = $res[0]->id;
        $isUpdate = true;
      }
    }
    if($is_default == "true") {
      $dataDetault = array(
        "is_default" => "false",
      );
      $dao->updateQuery("user_address", $dataDetault, "user_id = {$user_idx}","pay_w");
    }

    $data = array(
      "address_type" => $address_type,
      "name" => $name,
      "phone" => $phone,
      "shipping_name" => $shipping_name,
      "zip_code" => $zip_code,
      "address_1" => $address_1,
      "address_2" => $address_2,
      "msg" => $memo
    );
    if (!$isUpdate) {
      $data["is_default"] = $is_default;
    }


    if($idx) {
      $dao->updateQuery("user_address", $data, "id = {$idx} and user_id = {$user_idx}","pay_w");
    } else {
      $data["user_id"] = $user_idx;
      $dao->insertQuery("user_address", $data, "pay_w");
    }



    return $this->resultOk();
  }

  public function setUserAddressDel () {
    $user_idx = $this->userIdx;
    if(!$user_idx) $this->resultFail("NO_LOGIN", "회원정보가 올바르지 않습니다.");
    $idx = $this->getParam("idx");
    if (!$idx) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }
    $dao = new CBaseDAO();
    $data = array(
      "_NQ_deleted_at" => "now()",
    );
    $res = $dao->updateQuery("user_address", $data, "id = {$idx} and user_id = {$user_idx}","pay_w");
    $defaultCheck = $dao->selectQuery("select * from user_address where id = {$idx} and user_id = {$user_idx}", "pay");
    if (isset($defaultCheck[0]) && $defaultCheck[0]->is_default == "true") {
      $updateQuery = "update user_address set
                is_default = 'true'
                where user_id = {$user_idx} and deleted_at is null
                order by id desc
                limit 1";

      $dao->selectQuery($updateQuery, "pay_w");
    }
    return $this->resultOk($res);
  }

  public function getQuestion($type) {
    $user_idx = $this->userIdx;
    $page = $this->getParam("page", 1);
    $is_all = $this->getParam("is_all", 0);
    $goods_id = $this->getParam("goods_id");
    $isQnaFullPage = $this->getParam("isQnaFullPage");

    $perPage = 20;
    $dao = new CBaseDAO();


    if(isset($isQnaFullPage) && $isQnaFullPage == "true") {
      $perPage = $perPage * $page;
      $page = 1;
    }
    $limit = $dao->getLimit($page, $perPage);
    $isAllWhere1 = $is_all == 1? "" : " and ocs.user_id = {$user_idx}";
    $isAllWhere2 = $is_all == 1 ? "" : " and user_id = {$user_idx}";
    $goodsWhere = isset($goods_id) ? " and cs.goods_id = {$goods_id}" : "";

    $sqlOrder = "select ocs.id ,concat(ocs.order_id,'') as order_id ,concat(ocs.detail_id, '') as detail_id , ocs.`type` , ocs.title , ocs.cs
					, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/cs_contents/', ocs.images)as img
					,ocs.state ,ocs.answer 
					, DATE_FORMAT(ocs.created_at, '%y.%m.%d %H:%i' ) as created_at
					, DATE_FORMAT(ocs.answered_at , '%y.%m.%d %H:%i' ) as answered_at
					, if (ocs.answered_at is null , 0 , 1) as is_answer
										, g.title as goods_title, g.imgs as goods_img, st.name as seller_name, ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img), '') as seller_profile
					from order_customer_services ocs 
					inner join order_detail od on ocs.detail_id = od.id 
					inner join goods g on od.goods_id = g.id 
					inner join fp_db.seller_tbl st on od.seller_id = st.idx 
					where  ocs.deleted_at is null {$isAllWhere1}
					order by created_at desc 
					";

    $sqlGoods = "select cs.id, cs.seller_id ,cs.goods_id ,cs.category ,cs.question ,cs.answer ,cs.state ,DATE_FORMAT(cs.answered_at , '%y.%m.%d %H:%i' ) as answered_at ,cs.is_secret 
        , DATE_FORMAT(cs.created_at, '%y.%m.%d %H:%i' ) as created_at
				, if(cs.answer is null or cs.answer = '', 0, 1) as is_answer
				, g.title, g.imgs as img
				, g.title as goods_title, g.imgs as goods_img, st.name as seller_name, ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img), '') as seller_profile
				from customer_services cs 
				inner join goods g on cs.goods_id = g.id 
				inner join fp_db.seller_tbl st on cs.seller_id = st.idx 
				where cs.deleted_at is null {$isAllWhere2} {$goodsWhere}
				order by cs.id desc 
				";
    $sqlAll = "select * from (
					select 'order' as question_type ,ocs.id, '' as g_goods_seller_id , '' as goods_id , '' as category , '' as question , ocs.answer as answer 
					, DATE_FORMAT(ocs.answered_at , '%y.%m.%d %H:%i' ) as answered_at , '' as is_secret
					, DATE_FORMAT(ocs.created_at, '%y.%m.%d %H:%i' ) as created_at
					, if (ocs.answered_at is null , 0 , 1) as is_answer
					, ocs.order_id ,ocs.detail_id , ocs.`type` , ocs.title , ocs.cs
					, CONCAT('https://fps3bucket.s3.ap-northeast-2.amazonaws.com/cs_contents/', ocs.images)as img
					,ocs.state  
										, g.title as goods_title, g.imgs as goods_img, st.name as seller_name, ifnull(CONCAT('https://fps3bucket.s3.ap-northeast-2.amazonaws.com/seller_contents/',st.profile_img), '') as seller_profile
					from order_customer_services ocs 
					inner join order_detail od on ocs.detail_id = od.id 
					inner join goods g on od.goods_id = g.id 
					inner join fp_db.seller_tbl st on od.seller_id = st.idx 
					where  ocs.deleted_at is null {$isAllWhere1}
					union ALL 
					select 'goods' as question_type, cs.id, cs.seller_id ,cs.goods_id ,cs.category ,cs.question ,cs.answer , DATE_FORMAT(cs.answered_at , '%y.%m.%d %H:%i' ) as answered_at ,cs.is_secret 
					, DATE_FORMAT(cs.created_at, '%y.%m.%d %H:%i' ) as created_at
					, if(cs.answer is null or cs.answer = '', 0, 1) as is_answer
					, '' as order_od,'' as detail_id,'' as `type`,'' as title,'' as cs ,'' as img,cs.state as state
					, g.title as goods_title, g.imgs as goods_img, st.name as seller_name, ifnull(CONCAT('https://fps3bucket.s3.ap-northeast-2.amazonaws.com/seller_contents/',st.profile_img), '') as seller_profile 
					from customer_services cs 
					inner join goods g on cs.goods_id = g.id 
					inner join fp_db.seller_tbl st on cs.seller_id = st.idx 
					where cs.deleted_at is null {$isAllWhere2}
				) as master 
				order by created_at desc 
				";

    $res = array();
    $resCnt = 0;
    if($type == "order") {
      $res = $dao->selectQuery($sqlOrder.$limit, "pay");
      $resCnt = count($dao->selectQuery($sqlOrder, "pay"));
    } else if ($type == "goods") {
      $res = $dao->selectQuery($sqlGoods.$limit, "pay");
      $resCnt = count($dao->selectQuery($sqlGoods, "pay"));
    } else {
      $res = $dao->selectQuery($sqlAll.$limit, "pay");
      $resCnt = count($dao->selectQuery($sqlAll, "pay"));
    }
    $last_page = ceil((int)$resCnt / $perPage);
    $data = [
      "current_page" => (int)$page,
      "data" => $res,
      "last_page" => $last_page,
      "total" => $resCnt
    ];
    return $this->resultOk($data);

  }

  public function setUserSetting () {
    $user_idx = $this->userIdx;
    if(!$user_idx) return $this->resultFail("NO_LOGIN", "로그인 후 이용 가능합니다.");
//    $is_push_agree = $this->getParam("is_push_agree");
//    $is_ad_push_agree = $this->getParam("is_ad_push_agree");
//
    $data = $this->getParameters();

    $userHelper = new Helper\CUserHelper();
    $userHelper->setUserInfoSetting($data, $user_idx);

    return $this->resultOk();
  }

  public function getPushList() {
    $user_idx = $this->userIdx;
//    $user_idx = 10001455;
    if(!$user_idx) return $this->resultFail("NO_LOGIN", "로그인 후 이용 가능합니다.");
    $category = $this->getParam("category");
    $page = $this->getParam("page");
    $perPage = 40;
    $limit = "";
    $dao = new CBaseDAO();
    if(isset($page)) {
      $limit = $dao->getLimit($page, $perPage);
    }
    $where = "";
    if(!($category == "all" || !$category)) $where = " and pl.category = '{$category}'";
    $sql = "select pl.*
          from push_logs pl 
          where pl.user_id = {$user_idx}
          and pl.result not in ('fail', 'not_send')
            {$where}            
          order by pl.idx desc
          {$limit}";


    $res = $dao->selectQuery($sql, "pay");

    $sqlCnt = "select count(*) as cnt
          from push_logs pl 
          where pl.user_id = {$user_idx}
          and pl.result not in ('fail', 'not_send')
            {$where}            
          order by pl.idx desc";


    $cnt = $dao->selectQuery($sqlCnt, "pay")[0]->cnt;

    $readData = array(
      "result" => "read"
    );
    $dao->updateQuery("push_logs pl",$readData, " pl.user_id = {$user_idx} {$where}","pay_w");
    foreach ($res as $key => $val) {
     $jsonData = json_decode($val->data);
     $val->data = $jsonData;
    }

    $last_page = ceil((int)$cnt / $perPage);
    $response = array(
      "current_page" => (int)$page,
      "data" => $res,
      "last_page" => $last_page,
      "total" => $cnt
    );
    if(isset($page)) {
      return $this->resultOk($response);
    } else {
      return $this->resultOk($res);
    }

  }

  public function getPushCategoryBadge () {
    $user_idx = $this->userIdx;
    if(!$user_idx) return $this->resultFail("NO_LOGIN", "로그인 후 이용 가능합니다.");

    $sql = "select category, count(*) as cnt
            from push_logs pl 
            where user_id = {$user_idx}
            and `result` = 'success'
            group by category ";
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "pay");

    $category = array("store", "community", "event");
    $data = array(
      "store" => 0,
      "community" => 0,
      "event" => 0
    );
    foreach ($res as $item) {
      $data[$item->category] = $item->cnt;
    }

    return $this->resultOk($data);
  }

  public function lmpayPasswordUpdate () {
    $user_idx = $this->userIdx;
    $password = $this->getParam("password");

    $sql = "select * from user_list where user_id = {$user_idx}";
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "db");


    if (isset($res[0])) {
      if (!isset($res[0]->barcode)) {
        $userHelper = new CUserHelper();
        $userHelper->setUserBarcode($user_idx);
      }
    } else {
      return $this->resultFail();
    }
    $pw = password_hash($password, PASSWORD_DEFAULT);
    $data = array(
      "sec_password" => $pw
    );
    $dao->updateQuery("user_list", $data, "user_id = {$user_idx}","db_w");

    return $this->resultOk();

  }

  public function lmpayPasswordCheck () {
    $password = $this->getParam("password");
    $user_idx = $this->userIdx;
    $sql = "select * from user_list where user_id = {$user_idx}";
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "db");

//    $pw = password_hash($password, PASSWORD_DEFAULT);


    return $this->resultOk(password_verify($password, $res[0]->sec_password));
  }

  public function lmpayPasswordSetCheck () {
    $user_idx = $this->userIdx;
    $sql = "select * from user_list where user_id = {$user_idx}";
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "db");

    if (isset($res[0])) {
      return $this->resultOk(isset($res[0]->sec_password));
    } else {
      return $this->resultOk(false);
    }
  }

  public function setQuestion() {
    $user_idx = $this->userIdx;
    $seller_id = $this->getParam("seller_id");
    $goods_id = $this->getParam("goods_id");
    $category = $this->getParam("category");
    $question = $this->getParam("question");
    $is_secret = $this->getParam("is_secret");

    $cs_idx = $this->getParam("cs_idx");

    if(isset($cs_idx)) {
      $data = [
        "category" => $category,
        "question" => $question,
        "state" => "답변대기",
        "is_secret" => $is_secret,
        "_NQ_updated_at" => "now()"
      ];
      $dao = new CBaseDAO();
      $dao->updateQuery("customer_services", $data, "id = {$cs_idx}","pay_w");
    } else {
      $data = [
        "seller_id" => $seller_id,
        "user_id" => $user_idx,
        "goods_id" => $goods_id,
        "category" => $category,
        "question" => $question,
        "state" => "답변대기",
        "is_secret" => $is_secret,
        "_NQ_created_at" => "now()"
      ];
      $dao = new CBaseDAO();
      $dao->insertQuery("customer_services", $data, "pay_w");
    }
    return $this->resultOk();
  }

  public function getUserPushBadge () {
    $user_idx = $this->userIdx;
    $userHelper = new CUserHelper();
    $res = $userHelper->pushBacdge($user_idx);
    return $this->resultOk($res);
  }

  public function setUserBlock () {
    $user_idx = $this->userIdx;
    $t_user_idx = $this->getParam("t_user_idx");

    $data = array(
      "block_user_idx" => $t_user_idx,
      "user_idx"  => $user_idx
    );

    $dao = new CBaseDAO();
    $dao->insertQuery("user_block_tbl", $data, "db_w", true);

    return $this->resultOk();
  }
}