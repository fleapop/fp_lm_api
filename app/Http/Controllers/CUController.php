<?php

namespace App\Http\Controllers;
include_once(app_path() . "/include_file/func.php");

use App\CU\BuyController as CUBuyController;
use App\CU\CUController as CUNewController;
use DB;
use Illuminate\Http\Request;
use Image;
use Session;
use Storage;

class CUController extends Controller
{

    public function apiGetBalance(Request $request)
    {
        $cu = new CUNewController();
        return $cu->balance($request);
    }

    public function apiRequestPayment(Request $request)
    {
        $cu = new CUBuyController();
        return $cu->buy($request);
    }

    public function apiRequestPaymentCancel(Request $request)
    {
        $cu = new CUBuyController();
        return $cu->cancel($request);
    }

    public function apiRequestPaymentRequestCancel(Request $request)
    {
        $cu = new CUBuyController();
        return $cu->requestCancel($request);

    }
}
