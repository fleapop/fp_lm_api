<?php

namespace App\Http\Controllers;

use App\Helper\CLmPayHelper;
use App\Helper\CPushHelper;
use App\Http\dao\CBaseDAO;
use App\Http\dao\CUserDAO;
use App\Models\AddShippingCode;
use App\Models\Cart;
use App\Events\OrderCanceled;
use App\Events\OrderCancelRequest;
use App\Events\OrderCompleted;
use App\Events\OrderExchangeRequest;
use App\Events\OrderRefundRequest;
use App\Events\OrderVbankCompleted;
use App\Fleapop\Payment;
use App\Models\Goods;
use App\Lib\FPIamport;
use App\Models\LMPay;
use App\Lmpay\LMPayController;
use App\Models\Order;
use App\Models\OrderCustomerService;
use App\Models\OrderDelivery;
use App\Models\OrderDetail;
use App\Models\OrderPayment;
use App\Models\PaymentCancel;
use App\Models\PaymentLog;
use App\Models\ShippingPrice;
use App\Models\Sku;
use App\Models\User;
use App\Models\UserAddress;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Storage;
use Carbon\Carbon;
use DB;

class OrderController extends Controller
{
  private $key = "fleapop!@#abc123";

//  private $conn;
//  private $conn_w;
//  private $pconn;
//  private $pconn_w;

//  public function __construct()
//  {
//
//    $this->conn = DB::connection(DB_DATABASE_R);
//    $this->conn_w = DB::connection(DB_DATABASE_W);
//    $this->pconn = DB::connection(DB_PAYMENT_R);
//    $this->pconn_w = DB::connection(DB_PAYMENT_W);
//  }


  /**
   * 구매 가능 상태 검사 및 메타데이터 생성
   * @ param
   * required
   * sku_ids : 구매 검사할 sku_ids ex)1,2,3
   * quantities : 구매 검사할 수량들 ex)1,2,3
   * *sku_ids, quantities 1:1
   *
   */
  public function getOrderMetaData(Request $request)
  {
    if (! $request->filled(['sku_ids', 'quantities'])) {
      return $this->resultFail("FAIL",'필수 파라미터가 누락되었습니다.');
    }

    $ids = explode(',', $request->sku_ids);
    $quantities = explode(',', $request->quantities);

    if (count($ids) != count($quantities)) {
      return $this->resultFail("FAIL",'필수 파라미터가 누락되었습니다. 상품과 갯수가 일치하지 않습니다.');
    }

    $datas = [];

    for ($i = 0; $i < count($ids); $i++) {
      $sku = Sku::find($ids[$i]);

      $result_stock = $sku->stock->stock - $quantities[$i];

      if ($result_stock < 0 ||
        $sku->state == "disable" ||
        $sku->goods->display_state !== 'show' ||
        $sku->goods->stock_state == 'soldout'
      ) {
        return $this->resultFail("FAIL",'현재 품절이거나, 현재 주문 수량을 주문할 수 없습니다.');
      }

      $item = [
        'id'       => $ids[$i],
        'quantity' => $quantities[$i],
      ];

      $datas[] = $item;
    }

    $json = json_encode($datas);
    $en = $this->getEncrypt($json);
    $en64 = base64_encode($en);

//    return response()->json($en64, 200);
    return $this->resultOk($en64);
  }

  /**
   * 구매 메타데이터 복호화 및 주문서
   * @ param
   * required
   * meta : 구매 메타데이터 응답 값
   * user_id : 유저 id (비유저일시 0)
   * device : 접속 디바이스
   * optianal
   * token : 로그인시 발행된 토큰
   *
   */
  public function getOrder($request)
  {
//    $meta = $this->getParam("meta");
//    // dd($meta);
    @session_start();
//    $request = $this->request_sub;

    if (! $request["meta"]) {
      return $this->resultFail("FAIL",'필수 파라미터가 누락되었습니다.');
    }

    $meta = base64_decode($request["meta"]);
    $meta = $this->getDecrypt($meta);
    $meta = json_decode($meta);


    $dataOrder = [
      'user_id'      => 0,
      'user_type'    => 'non_user',
      'name'         => '',
      'phone'        => '',
      'lmpay'        => 0,
      'use_pay_type' => '',
      'addresses'    => [],
    ];

    // Set 주문자 정보
//    $authUserId = $this->userAuthCheckTemp([
//      'device'  => $request->input('device'),
//      'user_id' => $request->input('user_id'),
//      'token'   => $request->input('token'),
//    ]);
    $authUserId = $request["user_idx"];

    if (! $authUserId) {
      $dataOrder['user_type'] = 'non_user';
      $dataOrder['user_id'] = 0;
      $dataOrder['name'] = $meta->order_info->non_user_name;
      $dataOrder['phone'] = $meta->order_info->non_user_phone;
    } else {
      $dataOrder['user_type'] = 'user';
      $dataOrder['user_id'] = $authUserId;
      $userAddress = new UserAddressController();
      $addresses = $userAddress->getAddresses($authUserId);

      if (count($addresses) > 0) {
        $dataOrder['shipping_address'] = $addresses;
      } else {
        $address = $userAddress->getEmptyUserAddress($dataOrder['user_id']);
        $dataOrder['shipping_address'] = $address;
      }

      $user = User::find($authUserId);
      $dataOrder['name'] = $user->shipping_name;
      $dataOrder['phone'] = $user->shipping_phone;
      $defaultPay = $user->where('user_id', $authUserId)->select('use_pay_type')->get();

      if (isset($defaultPay[0])) {
        $dataOrder['use_pay_type'] = $defaultPay[0]->use_pay_type;
      }

      $lmpay = new LMPayController();
      $dataOrder['lmpay'] = $lmpay->getBalance($authUserId);
    }

    if (! $authUserId) {
      $items = json_decode($meta->meta);
    } else {
      $items = $meta;
    }

    $goods = new Goods();
    $dataPay = [
      'origin_price'    => 0,
      'promotion_price' => 0,
      'shipping_price'  => 0,
      'shipping_count'  => 0,
      'total_price'     => 0,
    ];

    $dataGoods = [];
    $colSellers = collect([]);

    foreach ($items as $item) {
      $itemSku = Sku::find($item->id);
      $itemGoods = $goods->find($itemSku->goods_id);
      $itemGoods->sku_id = $item->id;
      $itemGoods->seller;
      $itemGoods->promotion = $itemGoods->getPromotion($itemSku->goods_id);
      $itemGoods->option = $itemSku->options;
      $itemGoods->option_value = $itemGoods->getOptionBySku($itemSku->id, true);
      $itemGoods->quantity = $item->quantity;
      $itemGoods->promotion_price = $itemGoods->getPromotionPrice();

      if ($colSellers->has($itemGoods->seller_id)) {
        if ($itemGoods->ship_price > $colSellers->get($itemGoods->seller_id)) {
          $colSellers[$itemGoods->seller_id] = $itemGoods->ship_price;
        }
      } else {
        $colSellers->put($itemGoods->seller_id, $itemGoods->ship_price);
      }

      $originPrice = ((int)$itemGoods->price * (int)$item->quantity) + ((int)$itemSku->price * (int)$item->quantity);
      $dataPay['origin_price'] += $originPrice;

      $dataPay['promotion_price'] += $originPrice - $itemSku->getBuyPrice($itemGoods, $item->quantity);

      array_push($dataGoods, $itemGoods);
    }

    foreach ($colSellers as $shipItem) {
      $dataPay['shipping_price'] += $shipItem;
      $dataPay["shipping_count"]++;
    }

    $dataPay['total_price'] = $dataPay['origin_price'] - $dataPay['promotion_price'] + $dataPay['shipping_price'];
    $data = [
      'data_goods' => $dataGoods,
      'data_pay'   => $dataPay,
      'data_order' => $dataOrder,
    ];
    return $data;
  }

  public function setAppPGData(Request $request)
  {
    $data = [
      'method'   => $request->method,
      'amount'   => $request->amount,
      'name'     => $request->name,
      'phone'    => $request->phone,
      'address'  => $request->address,
      'zip_code' => $request->zip_code,
    ];

    $json = json_encode($data);
    $en = $this->getEncrypt($json);
    $en64 = base64_encode($en);
    $demeta = base64_decode($en64);
    $de = $this->getDecrypt($demeta);

    $data = [
      'en' => $en64,
      'de' => $de,
    ];

//    return response()->json($data, 200);
    return $this->resultOk($data);
  }

  /**
   * App viewAppPG 복호화
   * @ param GET
   * required
   * method : 결제 방식 ['lmpay','card','samsung','vbank','trans','PAYCO']
   * amount : 결제 금액
   * name : 주문자 이름
   * phone : 주문자 연락처
   * address : 주문자 주소 (상세주소1 상세주소2)
   * zip_code : 주문자 주소 우편번호
   */
  public function getAppPGData($meta = NULL, $device = 103)
  {

    if (! isset($meta)) {
      return [
        "success" => false,
        "msg"     => "필수 입력사항이 누락되었습니다.",
      ];
    }

    if ($device != self::IOS) {
      $meta = base64_decode($meta);
      $meta = $this->getDecrypt($meta);
      $meta = json_decode($meta);
    } else {
      $meta = $this->getDecrypt($meta);
      $meta = json_decode($meta);
    }

    if (! isset($meta->method) || ! isset($meta->amount) || ! isset($meta->name) || ! isset($meta->phone) || ! isset($meta->address) || ! isset($meta->zip_code)) {
      return [
        "success" => false,
        "msg"     => "필수 입력사항이 누락되었습니다. ",
      ];
    }

    return [
      "success" => true,
      "data"    => json_encode($meta),
    ];
  }

  /**
   * 도서산간지역 배송비 검색
   *
   * @param $zip_code
   * @return int
   */
  public function getPostAddPrice(Request $request, $zip_code)
  {
    $goods = Goods::whereIn('id', explode(',', $request->ids))
      ->groupBy('seller_id', 'use_extra_shipping_price')
      ->get();

    $shippingPrice = 0;

    foreach ($goods as $item) {

      if ($item->use_extra_shipping_price) {
        $result = AddShippingCode::where('zip_code', $zip_code)->first();
        if ($result) {
          if (mb_strpos($result->address, '제주') === 0) {
            $shippingPrice += $item->seller->extra_shipping_price;
          } else {
            $shippingPrice += $item->seller->shipping_price;
          }
        }
      }
    }

    return $shippingPrice;
  }

  /**
   * 구매 진행 (결제 진행)
   * @ param (json)
   * user_id : 유저ID
   * device : 디바이스
   * token : 토큰
   * user_type : 유저타입 ['user','non_user']
   * sku_ids : sku_ids ex) 935781,
   * quantities : 수량 ex) 1,
   * (sku_idx : quantities, 1:1)
   * order_name : 주문자 이름
   * order_phone : 주문자 연락처
   * address_name : 주소이름
   * shipping_name : 수취인 이름
   * shipping_phone : 수취인 연락처
   * shipping_zipcode : 우편번호
   * shipping_address_1 : 배송지 주소
   * shipping_address_2 : 배송지 상세주소
   * shipping_msg : 배송메시지
   * set_address_default : 기본주소로 선택할지 [true,false]
   * method : 결제방식 ['lmpay','card','samsung','vbank','trans','PAYCO']
   * (전금액 러마페이로 결제시 'lmapy')
   * total : 총 결제금액 default 0
   * shipping : 배송비 default 0
   * shipping_cnt: 배송하는 상품 갯수 default 0
   * add_shipping : 추가배송비 default 0
   * lmpay : 러마페이 사용금 default 0
   * set_default_method : 기본결제 방식 선택 여부 [true, false]
   * (전금액 러마페이로 결제시 false)
   * bank : 은행명 (method:'vbank,trans' 일때 필수)
   * account : 은행계좌 (method:'vbank,trans' 일때 필수)
   * holder : 예금주 (method:'vbank,trans' 일때 필수)
   * imp_uid : 결제후, (러마페이로 전금액 결제시 'lmpay')
   */
  public function setOrderSubmit(Request $request)
  {
    // 결제가 검증
    $args = [
      'sku_ids'      => $request->input('sku_ids'),
      'quantities'   => $request->input('quantities'),
      'lmpay'        => $request->input('lmpay'),
      'zip_code'     => $request->input('shipping_zipcode'),
      'user_id'      => $request->input('user_id'),
      'user_type'    => $request->input('user_type'),
      'add_shipping' => $request->input('add_shipping'),
    ];

    if (! isset($args['zip_code'])) {
      return $this->resultFail("NO_REQUIRED", "배송지가 누락되었습니다");
    }
    $response = $this->checkOrderPrice($args);

    $imp = new FPIamport();
    if (isset($request->bank) && strlen($request->bank) > 0) {
      $bankData = [
        'bank'    => $request->input('bank'),
        'holder'  => $request->input('holder'),
        'account' => $request->input('account'),
      ];

      $bankResponse = $imp->verificationBankAccount($bankData);

      if ($bankResponse->getStatusCode() != 200) {
//        Log::channel("payment_log")->info("계좌검증에 실패했습니다. : " . json_encode($request->all(), JSON_UNESCAPED_UNICODE));

        return $this->resultFail($bankResponse->getStatusCode(),json_decode($bankResponse->getContent()));
      }
    }

    if (! $response['success']) {
//      Log::info('결제 실패(' . $request->imp_uid . ': ' . $response['msg']);
//      Log::channel("payment_log")->info("결제실패 : " . json_encode($request->all(), JSON_UNESCAPED_UNICODE));
      return $this->resultFail("FAIL",$response['msg']);
    }

    if (isset($request->imp_uid)) {
      if ($request->imp_uid != 'lmpay') {
        // IMP 중복 검사
        $exists = OrderPayment::where('imp_uid', $request->imp_uid)->count();
        if ($exists > 0) {
//          Log::channel("payment_log")->info("이미 처리된 주문건입니다. : " . json_encode($request->all(), JSON_UNESCAPED_UNICODE));
          return $this->resultFail("FAIL",'이미 처리된 주문건입니다.');
        }
      }

      // 구매진행
      $order = NULL;
      $bankData = NULL;
      $payment = NULL;
      $lmpay = NULL;
      $details = [];
      $deliveries = [];

      try {
        $authUserId = $request->user_id;
        $orderData = $this->setDataOrder();
        $orderData['user_id'] = $request->input('user_id');
        $orderData['user_type'] = $request->input('user_type');
        $orderData['name'] = $request->input('order_name');
        $orderData['phone'] = $request->input('order_phone');
        $orderData['lmpay_amount'] = $request->input('lmpay');
        $orderData['total_price'] = $response['data']['total_price'] + $orderData['lmpay_amount'];
        $orderData['device'] = $request->input('device');
        $order = Order::create($orderData);
        $orderData['order_number'] = $this->getOrderNumber($order);
        $paymentData = $this->setDataPayment();
        if (isset($request->bank) && strlen($request->bank) > 0) {

          $bankData = [
            'bank'    => $request->input('bank'),
            'holder'  => $request->input('holder'),
            'account' => $request->input('account'),
          ];

          $bankResponse = $imp->verificationBankAccount($bankData);

          if ($bankResponse->getStatusCode() != 200) {
//            Log::channel("payment_log")->info("계좌검증 실패. : " . json_encode($request->all(), JSON_UNESCAPED_UNICODE));
//            Log::channel('slack')->info('[입금계좌 검증 실패] 주문번호:' . $order->id);
            throw new Exception($bankResponse->getContent());
          }

          $paymentData['bank'] = $bankData['bank'];
          $paymentData['holder'] = $bankData['holder'];
          $paymentData['account'] = $bankData['account'];
        }

        $paymentData['order_id'] = $order->id;
        $paymentData['method'] = $request->input('method');
        $paymentData['action'] = '입금';
        $paymentData['amount'] = $response['data']['total_price'];
        $paymentData['lmpay'] = $request->input('lmpay');

        if ($request->input('imp_uid') === 'lmpay') {
          $paymentData['imp_uid'] = 'lmpay';
          $paymentData['merchant_uid'] = 'lmpay';
          $paymentData['state'] = "결제완료";

          if ($request->input('lmpay') > 0) {

            $lmpayControl = new LMPayController();
            $lmpay = $lmpayControl->paid($authUserId, [
              'user_idx'       => $authUserId,
              'order_info_idx' => $order->id,
              'type'           => '온라인',
              'method'         => '마일리지',
              'action'         => '지불',
              'state'          => '결제됨',
              'amount'         => ($request->input('lmpay') * -1),
            ]);

            if (! isset($lmpay->idx)) {
//              Log::channel("payment_log")->info("러마페이 차감 실패 : " . json_encode($request->all(), JSON_UNESCAPED_UNICODE));
              throw new Exception('구매 취소 또는 문제가 발생했습니다.1');
            }
          }
        } else {
          $impData = $imp->getImpObject($request->input('imp_uid'));
//          // dd($impData);
//          Log::($impData);
//          Log::info($request->input('imp_uid'));
//          // dd($response);

          if ($impData->data->amount != $response['data']['total_price']) {

//            Log::channel("payment_log")->info("결제금액 불일치 : " . json_encode($request->all(), JSON_UNESCAPED_UNICODE));
//            Log::channel('slack')->info('[결제실패] 결제금액 불일치(주문번호: ' . $order->id . ')');
            throw new Exception('결제금액이 일치하지 않습니다.');
          }

          $paymentData['imp_uid'] = $impData->data->imp_uid;
          $paymentData['merchant_uid'] = $impData->data->merchant_uid;
          $paymentData['receipt_url'] = $impData->data->receipt_url;
//          // dd($impData->data->status);
          if ($impData->data->status == 'paid') {
            $paymentData['state'] = "결제완료";
          } else if ($impData->data->status == 'ready') {
            $paymentData["vbank_num"] = $impData->data->vbank_num;
            $paymentData["sender"] = $impData->data->vbank_holder;
            $paymentData["vbank_name"] = $impData->data->vbank_name;
            $paymentData["vbank_date"] = $impData->data->vbank_date;
          } else {

//            Log::channel("payment_log")->info("IMP Fail (" . $impData->data->fail_reason . ") : " . json_encode($request->all(), JSON_UNESCAPED_UNICODE));
            throw new Exception($impData->data->fail_reason);
          }

          if ($request->input('lmpay') > 0) {

            $lmpayControl = new LMPayController();
            $lmpay = $lmpayControl->paid($authUserId, [
              'user_idx'       => $authUserId,
              'order_info_idx' => $order->id,
              'type'           => '온라인',
              'method'         => '마일리지',
              'action'         => '지불',
              'state'          => '결제됨',
              'amount'         => ($request->input('lmpay') * -1),
            ]);

            if (! isset($lmpay->idx)) {
//              Log::channel("payment_log")->info("러파메이 차감 실패2 : " . json_encode($request->all(), JSON_UNESCAPED_UNICODE));
              throw new Exception('구매 취소 또는 문제가 발생했습니다.1');
            }
          }
        }

        if ($paymentData['method'] == 'payco') {
          $paymentData['method'] = 'PAYCO';
        }


        if ($paymentData['method'] == "lmpay" && $paymentData['amount'] > 0) {
//          Log::channel("payment_log")->info("메소드와 금액 불일치 : " . json_encode($request->all(), JSON_UNESCAPED_UNICODE));
          throw new Exception('구매 취소 또는 문제가 발생했습니다. 02.');
        }
//dd($paymentData);
        $payment = OrderPayment::create($paymentData);
        $i = 1;

        foreach ($response['datas'] as $item) {
          $detailData = [];
          $detailData['order_id'] = $order->id;
          $detailData['order_goods_number'] = $this->getOrderGoodsNumber($order->id, $i);

          if ($payment->state == '결제완료') {
            $detailData["state"] = "상품준비";
          } else {
            $detailData["state"] = "결제대기";
          }

          $detailData['seller_id'] = $item['seller_id'];
          $detailData['goods_id'] = $item['goods_id'];
          $detailData['sku_id'] = $item['sku_id'];
          $detailData['quantity'] = (int)$item['quantity'];
          $detailData['origin_price'] = $item['origin_price'];
          $detailData['option_price'] = $item['option_price'];
          $detailData['promotion_price'] = $item['promotion_price'];
          $detailData['delivery_price'] = $item['delivery_price'];
          $detailData['total_price'] = $item['total_price'];
          $detailData['bundle_shipping'] = $this->getBundleShippingOrder($item['seller_id'], $response['datas']);
          $detailData['goods_title'] = $item['goods_title'];
          $detailData["goods_promotion_id"] = $item["goods_promotion_id"];

          $i++;

          $detail = OrderDetail::create($detailData);

          $details[] = $detail;

          $leaveStock = Sku::find($detailData['sku_id'])->stock->stock - $detail->quantity;

          if ($leaveStock < 0) {
//            Log::channel("payment_log")->info("재고부족 : " . json_encode($request->all(), JSON_UNESCAPED_UNICODE));
            throw new Exception('주문가능한 재고가 없습니다.');
          } else {
            $goods = $detail->goods;

            if ($goods->stock_state == "soldout" || $goods->display_state == "hide") {
//              Log::channel("payment_log")->info("재고부족2 : " . json_encode($request->all(), JSON_UNESCAPED_UNICODE));
              throw new Exception('주문가능한 재고가 없습니다.');
            }

            $sku = Sku::find($detailData['sku_id']);
            $sku->stock->subStock($detail->quantity);

            if ($goods->display_state != "hide") {
              $goods->stock_state = $goods->getState();
              $goods->update();
            }
          }
        }

        $address = [
          'user_id'      => $authUserId,
          'address_type' => $request->input('address_name'),
          'name'         => $request->input('shipping_name'),
          'phone'        => $request->input('shipping_phone'),
          'zip_code'     => $request->input('shipping_zipcode'),
          'address_1'    => $request->input('shipping_address_1'),
          'address_2'    => $request->input('shipping_address_2'),
          'msg'          => $request->input('shipping_msg'),
          'is_default'   => $request->input('set_address_default'),
        ];

        UserAddress::setAddress($address);

        foreach ($details as $detail) {
          $deliveryData = $this->setDataDelivery();
          $deliveryData['order_id'] = $order->id;
          $deliveryData['detail_id'] = $detail->id;
          $deliveryData['name'] = $address['name'];
          $deliveryData['phone'] = $address['phone'];
          $deliveryData['zip_code'] = $address['zip_code'];
          $deliveryData['address_1'] = $address['address_1'];
          $deliveryData['address_2'] = $address['address_2'];

          if ($address['msg'] != "배송시 요청사항을 선택해 주세요.") {
            $deliveryData['msg'] = $address['msg'];
          }

          if ($payment->state == '결제완료') {
            $deliveryData["state"] = "상품준비";
          } else {
            $deliveryData["state"] = "결제대기";
          }

          $delivery = OrderDelivery::create($deliveryData);

          array_push($deliveries, $delivery);
        }

        Cart::removeAlreadyOrderedItems($order);
        ShippingPrice::persistExtraShippingPrice($order->id);

        if ($request->method === 'vbank') {
          event(new OrderVbankCompleted($order));
        } else {
          event(new OrderCompleted($order));
        }

        if ($request->set_default_method == "true") {
          $user = User::find($authUserId);
          $user->use_pay_type = $request->method;
          $user->save();
        }
        $user = User::find($request->input("user_id"));
//        dd($user);
        $userUpdateData = array();
        if($user->shipping_phone == "01000000000") {
          $userUpdateData["shipping_phone"] = $request->input("order_phone");
        }
        if($user->shipping_name == "AppleUser") {
          $userUpdateData["shipping_name"] = $request->input("order_name");
        }
        if($user->shipping_phone == "01000000000" || $user->shipping_name == "AppleUser") {
          $this->conn_w->table("user_list")
            ->where("user_id", "=", $request->input("user_id"))
            ->update($userUpdateData);
        }

//        Log::channel("payment_log")->info("결제성공 : " . json_encode($request->all(), JSON_UNESCAPED_UNICODE));
        PaymentLog::setLog($request->input('imp_uid'), 'PAY', $request, "");
//        return response()->json($order->id, 200);
        return $this->resultOk($order->id);

      } catch (Exception $e) {

//        Log::channel("payment_log")->info("Exception (" . $e->getMessage() . ") : " . json_encode($request->all(), JSON_UNESCAPED_UNICODE));


        if (isset($payment)) {
          $payment->delete();
        }

        if (isset($lmpay)) {
          $lmpay->delete();
        }

        if (isset($details)) {
          foreach ($details as $detail) {
            $detail->sku->stock->subStock($detail->quantity);
          }

          OrderDetail::where('order_id', $order->id)->delete();
        }

        if (isset($deliveries)) {
          OrderDelivery::where('order_id', $order->id)->delete();
        }

        if (isset($order)) {
          $order->delete();
        }

//        Log::error('결제 오류(' . $request->imp_uid . ') : ' . $e->getMessage());
        PaymentLog::setLog($request->input('imp_uid'), 'CAN', $request, $e->getMessage());
        if (isset($request->imp_uid) && $request->imp_uid != "lmpay") {

          $imp = new FPIamport();
          $data = $imp->getImpObject($request->input('imp_uid'));
          if (isset($bankData)) {
            $imp->cancelPayment($request->input('imp_uid'), $data->data->amount, "결제 실패", $bankData);

            return $this->resultFail("FAIL"," 결제실패로 결제를 취소하였습니다.");
          } else {
            $imp->cancelPayment($request->input('imp_uid'), $data->data->amount, "결제 실패");
            return $this->resultFail("FAIL"," 결제실패로 결제를 취소하였습니다.");
          }
        }

        return $this->resultFail("FAIL",$e->getMessage() . " - {$request->imp_uid}");
      }
    } else {
//      return $this->resultFail("FAIL",$response, 200);
      return $this->resultOk($response);
    }
  }

  /**
   * 주문번호 생성
   * @ param
   * required
   * user_type
   * user_id
   */
  private function getOrderNumber($order)
  {
    $order->order_number = $order->id;
    $order->update();
    return $order->id;
  }

  /**
   * 상품주문번호 생성
   * @ param
   * required
   * order_number
   * index
   */
  private function getOrderGoodsNumber($order_number, $index)
  {
    if($index < 10) {
      $index = "0".$index;
    }
    return $order_number . $index;
  }

  /**
   * 구매가 검증
   * @ param
   * required
   * sku_ids: sku id ex) 1,2,3,
   * quantities : 수량 ex) 1,1,1,
   * sku_ids : quantities 1:1
   * zip_code : 우편번호,
   * user_id : 유저 id
   * lmpay : 러마페이 사용금액
   * user_type : 유저타입 ['user','non_user']
   */
  private function checkOrderPrice($args)
  {
    if (! isset($args['sku_ids']) || ! isset($args['quantities']) || ! isset($args['zip_code']) || ! isset($args['lmpay']) || ! isset($args['user_type'])) {
      return [
        'success' => false,
        'msg'     => '필수 입력사항이 누락되었습니다.',
      ];
    }

    $skuIds = subStr($args['sku_ids'], 0, -1);
    $skuIds = explode(',', $skuIds);
    $quantities = subStr($args['quantities'], 0, -1);
    $quantities = explode(',', $quantities);

    foreach ($skuIds as $skuId) {
      if ($skuId != "") {
        $sku = Sku::find($skuId);

        if ($sku->state == "disable") {
          return [
            'success' => false,
            'msg' => '현재 판매가 중지된 상품이 포함되어 있습니다.',
          ];
        }

        if ($sku->goods->display_state == "hide") {
          return [
            'success' => false,
            'msg' => '현재 판매가 중지된 상품이 포함되어 있습니다.',
          ];
        }

        if ($sku->goods->stock_state == "soldout") {
          return [
            'success' => false,
            'msg' => '현재 품절된 상품이 포함되어 있습니다.',
          ];
        }
      }
    }

    if (count($skuIds) != count($quantities)) {
      return [
        'success' => false,
        'msg'     => '주문상품과, 수량이 일치하지 않습니다.',
      ];
    }

    $i = 0;
    $dataPay = [
      'origin_price'    => 0,
      'option_price'    => 0,
      'promotion_price' => 0,
      'shipping_price'  => 0,
      'total_price'     => 0,
    ];

    $dataDetails = [];

    foreach ($skuIds as $skuId) {
      $itemSku = Sku::find($skuId);
      $itemGoods = $itemSku->goods;
      $originPrice = ((int)$itemGoods->price * (int)$quantities[$i]);
      $dataPay['origin_price'] += $originPrice;
      $dataPay['promotion_price'] += $originPrice - $itemGoods->getPromotionPrice((int)$quantities[$i]);
      $dataPay['option_price'] += $itemSku->price * (int)$quantities[$i];
      $dataDetail['seller_id'] = $itemGoods->seller_id;
      $dataDetail['goods_id'] = $itemGoods->id;
      $dataDetail['goods_title'] = $itemGoods->title;
      $dataDetail['sku_id'] = $skuIds[$i];
      $dataDetail['quantity'] = $quantities[$i];
      $dataDetail['origin_price'] = ((int)$itemGoods->price * (int)$quantities[$i]);
      $dataDetail['delivery_price'] = 0;
      $dataDetail['option_price'] = $itemSku->price * (int)$quantities[$i];
      $dataDetail['promotion_price'] = $itemGoods->getPromotionPrice((int)$quantities[$i]);
      $dataDetail['total_price'] = $dataDetail['origin_price'] - $dataDetail['promotion_price'] + $dataDetail['option_price'];
      $dataPay['total_price'] += $dataDetail['total_price'];

      $nowDate = Carbon::now()->toDateString();
      $sql = "SELECT * FROM goods_promotions WHERE goods_id = {$itemGoods->id} AND DATE(started_at) <= '{$nowDate}' AND DATE(ended_at) >= '{$nowDate}'";
      $promotionRes = $this->pconn->select($sql);
      $goods_promotion_id = 0;
      if (isset($promotionRes[0])) {
        $goods_promotion_id = $promotionRes[0]->idx;
      }

      $dataDetail["goods_promotion_id"] = $goods_promotion_id;

      $dataDetails[] = $dataDetail;

      $i++;
    }

    $goodsIds = Sku::whereIn('id', $skuIds)->pluck('goods_id')->toArray();

    $goods = Goods::whereIn('id', $goodsIds)
      ->groupBy('seller_id', 'use_extra_shipping_price')
      ->get();

    foreach ($goods as $item) {
      if ($item->use_extra_shipping_price) {
        $result = AddShippingCode::where('zip_code', $args['zip_code'])->first();
        if ($result) {
          if (mb_strpos($result->address, '제주') === 0) {
            $dataPay['shipping_price'] += $item->seller->extra_shipping_price;
          } else {
            $dataPay['shipping_price'] += $item->seller->shipping_price;
          }
        }
      }
    }

    $dataPay['total_price'] += $dataPay['shipping_price'];

    if ($args['user_type'] == 'user') {
      $lmPay = new LMPayController();
      $balance = $lmPay->getBalance($args['user_id']);

      if ($balance < $args['lmpay']) {
        return [
          'success' => false,
          'msg'     => '사용가능한 러마페이금액을 초과했습니다.',
        ];
      }

      if ($dataPay['total_price'] < $args['lmpay']) {
        $dataPay['total_price'] = 0;
      } else {
        $dataPay['total_price'] -= $args['lmpay'];
      }
    } else {
      if ($args['lmpay'] > 0) {
        return [
          'success' => false,
          'msg'     => '사용가능한 러마페이금액을 초과했습니다.',
        ];
      }
    }

    return [
      'success' => true,
      'data'    => $dataPay,
      'datas'   => $dataDetails,
    ];
  }


  /**
   * 주문 문의 - 구매신청 (실행)
   * @ param
   * required
   * detail_id : 주문 id (order_detail - id)
   */
  public function cancel(Request $request)
  {
    if (! $request->filled(['detail_id', 'device'])) {
      return $this->resultFail("FAIL",'필수사항이 누락되었습니다.');
    }

    $detail_id = $request->input('detail_id');
    $dao = new CBaseDAO();
    $sql = "select state FROM order_detail od where deleted_at is null and id = {$detail_id}";
    $res = $dao->selectQuery($sql, "pay");
    if(isset($res[0])) {
      if($res[0]->state != "상품준비") {
        return $this->resultFail("FAIL",'배송준비중이거나, 배송중이여서 상품취소가 불가 합니다.');
      }
    }

    try {
      $detail = OrderDetail::find($request->input('detail_id'));
      Payment::cancel($detail);
      event(new OrderCanceled(
        $detail->order->user->shipping_phone,
        $detail->order->user->shipping_name,
        $detail->order->order_number,
        $detail->goods->title
      ));
//      return response()->json('주문취소 처리 되었습니다.', 200);
      return $this->resultOk(null, "주문취소 처리 되었습니다.");

    } catch (Exception $e) {
      return $this->resultFail("FAIL",$e->getMessage());
    }
  }

  /**
   * 구매 환불(반품)
   * @ param
   * required
   * detail_id : 취소할 order_detail id
   * quantity : 취소할 수량
   * refund_amount : 반송비
   */
  public function refund(Request $request)
  {
    if (! $request->filled('detail_id', 'refund_amount', 'quantity')) {
      return $this->resultFail("FAIL","필수값이 누락되었습니다.");
    }

    $refund_amount = $request->input('refund_amount');
    $quantity = $request->input('quantity');

    $detail = OrderDetail::find($request->detail_id);
    $cloneDetail = $detail->replicate();

    $price = (($detail->total_price / $detail->quantity) * $quantity) - $refund_amount;
    $maxPrice = (($detail->total_price / $detail->quantity) * $quantity);

    $block_states = ["결제대기", "상품준비", "취소완료", "교환접수", "취소대기", "환불대기"];
    if (in_array($detail->state, $block_states)) {
      return $this->resultFail("FAIL","해당 상태는 반품이 불가능한 상태입니다.");
    }

    if ($detail->quantity < $quantity) {
      return $this->resultFail("FAIL","해당 수량은 반품처리가 불가능합니다.");
    }

    if ($request->input('refund_amount') > $maxPrice) {
      return $this->resultFail("FAIL","반품비는 상품 주문금액을 초과할 수 없습니다.");
    }

    if ($detail->order->payment->state != "결제완료") {
      return $this->resultFail("FAIL","해당상품은 결제완료 상태가 아닙니다.");
    }

    $cancelCashLog = NULL; // 취소된 cash_log 객체
    $cancelObj_Imp = NULL; // 취소된 payment_cancel IMP 객체
    $cancelObj_Lmpay = NULL; // 취소된 payment_cancel Lmpay 객체
    $cancelObj = NULL; // 취소된 order detail 객체
    try {
      $isbank = false;
      $imp = new FPIamport();
      if (in_array($detail->order->payment->method, ["vbank", "trans"])) {
        $bank_data = [
          'bank'    => $detail->order->payment->bank,
          'holder'  => $detail->order->payment->holder,
          'account' => $detail->order->payment->account,
        ];

        $isbank = true;
      }

      if (count($detail->paymentCancel) > 0) {
        return $this->resultFail("FAIL","이미 취소처리된 거래건이 있습니다.");
      }

      $order = $detail->order;

      $leave_amount = [
        'total' => $order->total_price,
        'imp'   => $order->total_price - $order->lmpay_amount,
        'lmpay' => $order->lmpay_amount,
      ];

      $canceled_amount = 0;

      foreach ($order->detail as $ditem) {
        if (in_array($ditem->state, ["취소완료", "반품완료", "취소접수", "반품접수", "취소보류", "반품보류"])) {
          $canceled_amount += ($ditem->total_price - $ditem->refund_price);
        }
      }

      if ($leave_amount['total'] < $price) {
        return $this->resultFail("FAIL","해당 반품금액을 환불해줄 수 없습니다.");
      }

      $leave_amount['imp'] = $leave_amount['imp'] - $canceled_amount;
      if ($leave_amount['imp'] < 0) {
        $leave_amount['lmpay'] += $leave_amount['imp'];
        $leave_amount['imp'] = 0;
      }

      $return_amount = [
        'imp'   => 0,
        'lmpay' => 0,
      ];

      if ($leave_amount['imp'] >= $price) {
        $return_amount['imp'] = $price;
      } else {
        $return_amount['imp'] = $leave_amount['imp'];
        $return_amount['lmpay'] = abs($leave_amount['imp'] - $price);
      }

      $payment = $order->payment;

      if ($return_amount['imp'] > 0) {
        $payment = $order->payment;
        $cancelData = [
          "order_id"        => $order->id,
          "order_detail_id" => $detail->id,
          "state"           => "취소완료",
          "amount"          => abs($return_amount['imp']),
          "method"          => $payment->method,
          "imp_uid"         => $payment->imp_uid,
          "merchant_id"     => $payment->merchant_uid,
          "bank"            => $payment->bank,
          "bank_account"    => $payment->account,
          "bank_holder"     => $payment->holder,
        ];

        if (in_array($payment->method, ["vbank", "trans"])) {
          $bankData = [
            'bank'    => $payment->bank,
            'account' => $payment->account,
            'holder'  => $payment->holder,
          ];

          $imp_response = $imp->cancelPayment($cancelData['imp_uid'], $cancelData['amount'], '고객 구매취소 요청', $bankData);
        } else {
          $imp_response = $imp->cancelPayment($cancelData["imp_uid"], $cancelData['amount'], "고객 구매취소 요청");
        }

        PaymentLog::setLog($cancelData['imp_uid'], 'CAN');

        if ($imp_response->getStatusCode() != 200) {
          throw new Exception("결제 취소에 실패했습니다.");
        }

        $cancelObj_Imp = PaymentCancel::create($cancelData);
      }

      if ($return_amount['lmpay'] > 0) {
        $cancelCashLog = LMPay::cancel($detail, $return_amount['lmpay']);
        if (! $cancelCashLog['success']) {
          throw new Exception($cancelCashLog['data']['error_msg']);
        }
        $cancelData = [
          'order_id'        => $order->id,
          'order_detail_id' => $detail->id,
          'state'           => '취소완료',
          'amount'          => abs($return_amount['lmpay']),
          'method'          => 'lmpay',
          'imp_uid'         => 'lmpay',
          'merchant_id'     => 'lmpay',
          'bank'            => '',
          'bank_account'    => '',
          'bank_holder'     => '',
        ];

        $cancelObj_Lmpay = PaymentCancel::create($cancelData);
      }

      // TODO : 재고 살리기
      // $detail->sku->stock->addStock($detail->quantity);
      if ($detail->quantity == $quantity) {

        $detail->state = "반품완료";
        $detail->refund_price = $refund_amount;
        $detail->save();

        $cancelObj = $detail;

      } else {
        // TODO : 어떻게 처리할지..
        $cancelCloneObj = $detail->replicate();
        $orderGoodsNumber = $this->getOrderGoodsNumber($detail->order->order_number, count($detail->order->detail));

        $cancelCloneObj->order_goods_number = $orderGoodsNumber;

        $cancelCloneObj->quantity = $request->quantity;

        $cancelCloneObj->origin_price = ($detail->origin_price / $detail->quantity) * $quantity;
        $detail->origin_price = $detail->origin_price - $cancelCloneObj->origin_price;

        $cancelCloneObj->option_price = ($detail->option_price / $detail->quantity) * $quantity;
        $detail->option_price = $detail->option_price - $cancelCloneObj->option_price;

        $cancelCloneObj->promotion_price = ($detail->promotion_price / $detail->quantity) * $quantity;
        $detail->promotion_price = $detail->promotion_price - $cancelCloneObj->promotion_price;

        $cancelCloneObj->total_price = ($detail->total_price / $detail->quantity) * $quantity;
        $detail->total_price = $detail->total_price - $cancelCloneObj->total_price;

        $cancelCloneObj->delivery_price = 0;

        $cancelCloneObj->refund_price = $refund_amount;
        $cancelCloneObj->state = "반품완료";


        $cancelCloneObj->save();

        $detail->quantity = $detail->quantity - $request->quantity;
        $detail->save();


        if (isset($cancelObj_Imp)) {
          $cancelObj_Imp->order_detail_id = $cancelCloneObj->id;
          $cancelObj_Imp->save();
        }

        if (isset($cancelObj_Lmpay)) {
          $cancelObj_Lmpay->order_detail_id = $cancelCloneObj->id;
          $cancelObj_Lmpay->save();
        }

        $cancelObj = $cancelCloneObj;
      }
      return $this->resultOk();
    } catch (Exception $e) {
      if (isset($cancelCashLog)) {
        $lmpay = LMPay::find($cancelCashLog["data"]["cash_log_idx"]);
        $lmpay->state = "기타";
        $lmpay->error_msg = "반품 보류";
        $lmpay->save();
      }

      if (isset($cancelObj_Imp)) {

        $cancelObj_Imp->state = "반품보류";
        $cancelObj_Imp->save();
      }

      if (isset($cancelObj_Lmpay)) {
        $cancelObj_Lmpay->state = "반품보류";
        $cancelObj_Lmpay->save();
      }

      if (isset($cancelObj)) {
        $cancelObj->state = '반품보류';
        $cancelObj->save();
      }

      return $this->resultFail("FAIL",$e->getMessage() . " {$detail->order_goods_number}");
    }
  }

  /**
   * 구매 결과
   * @ param
   * required
   * order_id : order - id
   */
  public function getOrderPaymentResult($order_id)
  {
    $res_order = Order::find($order_id);
    if (isset($res_order->payment)) {
      $res_order->payment;
      if (isset($res_order->payment->vbank_date) || isset($res_order->payment_vbank_date)) {
        $res_order->payment->vbank_date = date("Y-m-d H:i:s", $res_order->payment->vbank_date);
      }

      $res_order->delivery;
      $res_order->detail;
      foreach ($res_order->detail as $item) {
        $item->goods;
        $item->goods->seller;
        $item->goods->option_value = $item->goods->getOptionBySku($item->sku_id, true);
      }

      return $res_order;
    } else {
      return false;
    }

  }

  /**
   * 구매 결과
   * @ param
   * required
   * order_id : order - id
   */
  public function getOrderPaymentResultRenewal($order_id)
  {
    $res_order = Order::find($order_id);
    if (isset($res_order->payment)) {
      $res_order->payment;
      if (isset($res_order->payment->vbank_date) || isset($res_order->payment_vbank_date)) {
        $res_order->payment->vbank_date = date("Y-m-d H:i:s", $res_order->payment->vbank_date);
      }

      $res_order->delivery;
      $res_order->detail;
      $res_order->userAddress = UserAddress::where([["user_id", $res_order->user_id], ["zip_code", "=", $res_order->delivery[0]->zip_code], ["address_1", "=", $res_order->delivery[0]->address_1], ["address_2", "=", $res_order->delivery[0]->address_2], ["shipping_name", "=", $res_order->delivery[0]->name]])->first();
      foreach ($res_order->detail as $item) {
        $item->goods;
        $item->goods->seller;
        $item->goods->option_value = $item->goods->getOptionBySku($item->sku_id, true);
      }

      return $this->resultOk($res_order);
    } else {
      return $this->resultFail();
    }

  }

  /**
   * 구매 결과 - Mobile
   * @ param
   * required
   * data : 주문서 접수 data
   * imp_uid : 결제정보 (GET)
   */
  public function viewOrderResultMobile($data)
  {

    if (isset($imp_uid) || isset($_GET['imp_uid'])) {
      $data = json_decode($data);

      $args = [
        "user_type"           => $data->user_type,
        "user_id"             => $data->user_id,
        "device"              => $data->device,
        "sku_ids"             => $data->sku_ids,
        "quantities"          => $data->quantities,
        "order_name"          => $data->order_name,
        "order_phone"         => $data->order_phone,
        "address_name"        => $data->address_name,
        "shipping_name"       => $data->shipping_name,
        "shipping_phone"      => $data->shipping_phone,
        "shipping_zipcode"    => $data->shipping_zipcode,
        "shipping_address_1"  => $data->shipping_address_1,
        "shipping_address_2"  => $data->shipping_address_2,
        "shipping_msg"        => $data->shipping_msg,
        "set_address_default" => $data->set_address_default,
        "method"              => $data->method,
        "total"               => $data->total,
        "shipping"            => $data->shipping,
        "shipping_cnt"        => $data->shipping_cnt,
        "add_shipping"        => $data->add_shipping,
        "lmpay"               => $data->lmpay,
        "set_default_method"  => $data->set_default_method,
        "bank"                => $data->bank,
        "holder"              => $data->holder,
        "account"             => $data->account,
        "imp_uid"             => $_GET['imp_uid'],
      ];

      $request = $this->makeRequest($args);
      $res = $this->setOrderSubmit($request);
      if ($res->getStatusCode() != 200) {
        $msg = json_decode($res->getContent());
//        return redirect('/re/store/order/error/' . $msg);
        return $this->resultFail("FAIL", $msg);
      } else {
//        return redirect('/re/store/order/result/' . $res->getContent());
        return $this->resultOk($res->getContent());
      }


    }
  }

  /**
   * 가상계좌 입금 처리
   * @ param
   * required
   * imp_uid : 결제 고유값
   * order_id : order_id
   */
  public function getOrderPaymentResultSubmit(Request $request)
  {
    $request->validate([
      'order_id' => 'required',
      'imp_uid'  => 'required',
    ]);
  }

  public function getOrderMetaDataNonUser(Request $request)
  {

    if (! $request->filled(['meta', 'name', 'phone'])) {
      return $this->resultFail("FAIL",'필수 파라미터가 누락되었습니다.');
    }
    $meta = base64_decode($request->meta);
    $meta = $this->getDecrypt($meta);
    $data = [
      "order_info" => ["non_user_name" => "", "non_user_phone" => ""],
      "meta"       => $meta,

    ];

    $data["order_info"]["non_user_name"] = $request->name;
    $data["order_info"]["non_user_phone"] = $request->phone;

    $json = json_encode($data);
    $en = $this->getEncrypt($json);
    $en64 = base64_encode($en);

//    return response()->json($en64, 200);
    return $this->resultOk($en64);
  }


  public function cancelOrderSubmit($order_id)
  {

    $order = Order::find($order_id);
    $order->state = "결제실패";
    $order->save();

    foreach ($order->detail as $detail) {
      $detail->state = "결제실패";
      $detail->save();
    }

    $payment = $order->payment;
    $payment->state = "결제실패";
    $payment->save();

    foreach ($order->delivery as $delivery) {
      $delivery->state = "결제실패";
      $delivery->save();
    }

    foreach ($order->lmpay() as $pay) {
      $pay->state = "기타";
      $pay->save();
    }

  }

  public function viewOrderComplete($order_id, $imp_uid = NULL)
  {
    if (! isset($order_id)) {
      return $this->resultFail("FAIL",'상품 결제에 실패했습니다.');
    }

    if (! isset($imp_uid) && ! isset($_GET['imp_uid'])) {
      return $this->resultFail("FAIL",'상품 결제에 실패했습니다.');
    } else {
      if (isset($imp_uid)) {
        $imp_uid = $imp_uid;
      } else {
        $imp_uid = $_GET['imp_uid'];
      }
    }

    $data = [
      'success'  => true,
      'order_id' => $order_id,
      'imp_uid'  => $imp_uid,
    ];

    $request = $this->makeRequest($data);
    $response = $this->getOrderPaymentResultSubmit($request);

    return json_encode($response);
  }

  public function getOrderResult($order_id = NULL, Request $request)
  {
    @session_start();
    if (! isset($order_id)) {
      return $this->resultFail("FAIL",'필수파라미터가 누락되었습니다.');
    }

    $user_id = 0;
    if ($request->is_user == 'true') {
      $response = $this->setResponse();
      $login_data = $this->loginCheck($response, $request);

      $is_user = false;
      if (gettype($login_data) != "array") {
        $user_id = $login_data;
      } else {
        return $this->resultFail("FAIL",'로그인이 필요한 항목입니다.');
      }
    }

    $order = new Order();
    $res_order = $order->find($order_id);


    if ($res_order->user_id == $user_id) {

      $res_order->payment;
      $res_order->delivery;

      foreach ($res_order->detail as $detail) {
        $detail->goods;
        $detail->goods->promotion = $detail->goods->getPromotion();
        $detail->goods->option = $detail->goods->getOptionBySku($detail->sku_id, true);
      }
    } else {
      return $this->resultFail("FAIL",'잘못된 접근입니다.');
    }

    return $res_order;
  }

  public function receiveVbankResult($imp_uid)
  {
    if (isset($imp_uid)) {
      $fpImp = new FPIamport();
      $impResponse = $fpImp->getImpObject($imp_uid);
      $payment = OrderPayment::where("imp_uid", $imp_uid)->first();

      if (isset($payment) && $impResponse->data->status == "paid") {
        if ($payment->state == "결제대기" && $payment->method == "vbank") {
          $order = $payment->order;
          $order->state = "결제완료";
          $order->save();

          foreach ($order->detail as $detail) {
            $detail->state = "상품준비";
            $detail->save();
          }

          foreach ($order->delivery as $delivery) {
            $delivery->state = "상품준비";
            $delivery->save();
          }

          $payment->state = "결제완료";
          $payment->save();

          return $payment;

        } else {
          return $this->resultFail("FAIL",'해당 주문건으로 결제대기 내역이 없습니다.');
        }
      } else {
        return $this->resultFail("FAIL",'해당 결제내역이 존재하지 않습니다.');
      }
    } else {
      return $this->resultFail("FAIL",'필수 입력사항이 누락되었습니다.');
    }
  }

  private function setDataOrder()
  {
    return [
      'user_id'      => 0,
      'user_type'    => 'non_user',
      'order_number' => '',
      'lmpay_amount' => 0,
      'total_price'  => 0,
      'state'        => '결제대기',
    ];
  }

  private function setDataOrderDetail()
  {
    return [
      'order_id'           => 0,
      'seller_id'          => 0,
      'order_goods_number' => '',
      'goods_id'           => '',
      'sku_id'             => '',
      'quantity'           => 1,
      'origin_price'       => 0,
      'option_price'       => 0,
      'promotion_price'    => 0,
      'delivery_price'     => 0,
      'total_price'        => 0,
      'state'              => '결제대기',
    ];
  }

  private function setDataPayment()
  {
    return [
      'order_id' => 0,
      'method'   => '',
      'action'   => '',
      'state'    => '결제대기',
      'amount'   => 0,
    ];
  }

  private function setDataOrderLog()
  {
    return [
      "order_id" => 0,
      "log"      => "",
    ];
  }

  private function setDataDelivery()
  {
    return [
      'order_id'  => 0,
      'detail_id' => 0,
      'name'      => '',
      'phone'     => '',
      'zip_code'  => '',
      'address_1' => '',
      'address_2' => '',
      'msg'       => '',
      'state'     => '결제대기',
    ];
  }

  private function getEncrypt($msg)
  {
    $endata = @openssl_encrypt($msg, "aes-128-cbc", $this->key, true, $this->key);
    $endata = base64_encode($endata);
    return $endata;
  }

  private function getDecrypt($msg)
  {
    $data = base64_decode($msg);
    $endata = @openssl_decrypt($data, "aes-128-cbc", $this->key, true, $this->key);
    return $endata;
  }


  /**
   * 주문 문의 - 주문문의
   * @ param
   * required
   * detail_id : 주문 id (order_detail - id)
   * type : 문의 유형 ['일반','배송 전 변경']
   * cs : 문의 내용
   * device : 디바이스 [101 - web, 102 - ios, 103 - ios]
   * user_id : (Optional) user_list - user_id
   * token : (Optional) 로그인시 발행된 토큰
   */
  public function setCSQuestion(Request $request)
  {
    $request->validate([
      'detail_id' => 'required',
      'device'    => 'required',
      'type'      => 'required',
      'cs'        => 'required',
    ]);

    $type = $request->input('type');
//    $authUserId = $this->userAuthCheckTemp(['device' => $request->input('device'), 'user_id' => $request->input('user_id'), 'token' => $request->input('token')]);
    $authUserId = $this->userIdx;
    if (! $authUserId) {
      return $this->resultFail("FAIL", "접근권한이 올바르지 않습니다");
    }

    $resDetail = OrderDetail::find($request->input('detail_id'));
    if ($authUserId != $resDetail->order->user_id) {
      return $this->resultFail("FAIL", "접근권한이 올바르지 않습니다");
    }


    if ($type == '배송 전 변경') {
      $exists = OrderCustomerService::where([["detail_id", $request->input('detail_id')], ['type', $type], ['state', '답변대기']])->first();
      if (isset($exists)) {
        return $this->resultFail("FAIL",'이미 문의를 등록한 주문건입니다.');
      }

      $resDetail->state = "답변대기";
      $resDetail->save();
    }

    if(mb_strpos($type, "일반") !== false) {
      $type = "일반";
    }

    $data = [
      'order_id'  => $resDetail->order_id,
      'detail_id' => $resDetail->id,
      'user_id'   => $authUserId,
      'type'      => $type,
      'title'     => '기타',
      'cs'        => $request->input('cs'),
      'state'     => "답변대기",
    ];

    OrderCustomerService::create($data);
    return $this->resultOk();
  }

  /**
   * 주문 문의 - 교환신청
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function setCSExchange(Request $request)
  {
    $request->validate([
      'detail_id' => 'required',
      'device'    => 'required',
      'title'     => 'required',
      'cs'        => 'required',
    ]);

    $authUserId = $this->userIdx;
    $resDetail = OrderDetail::find($request->input('detail_id'));

    if (! $authUserId) {
      return $this->resultFail("FAIL", "접근권한이 올바르지 않습니다");
    }

    if ($authUserId != $resDetail->order->user_id) {
      return $this->resultFail("FAIL", "접근권한이 올바르지 않습니다");
    }

    if ($resDetail->isDuplicateExchange()) {
      return $this->resultFail('FAIL', '이미 접수된 취소문의 건이 있습니다.');
    }

    $imgs = "";

    if ($request->hasFile('files')) {
      $files = $request->file('files');
      if(is_array($files)) {
        foreach ($files as $key => $file) {
          $image = $this->getUnique() . time() . $file->getClientOriginalName();
          Storage::disk('s3')->put(S3_PATH_CS_CONTENT . $image, file_get_contents($file), 'public');
          $imgs .= $image . ",";
        }
      }
    }

    $data = [
      'order_id'  => $resDetail->order_id,
      'detail_id' => $resDetail->id,
      'user_id'   => $authUserId,
      'type'      => '교환접수',
      'title'     => $request->input('title'),
      'cs'        => $request->input('cs'),
      'state'     => "답변대기",
      'images'    => $imgs,
    ];

    OrderCustomerService::create($data);
    $resDetail->state = "교환접수";
    $resDetail->save();

    event(new OrderExchangeRequest(
      $resDetail->order->user->shipping_phone,
      $resDetail->seller->name,
      $resDetail->order->user->shipping_name,
      $resDetail->order->order_number,
      $resDetail->goods->title,
      $request->cs
    ));

    return $this->resultOk();
  }

  function getUnique()
  {
    $res = preg_replace("/^0\.([0-9]+) ([0-9]+)$/", "$1-$2", microtime());
    $res = explode("-", $res);
    $res = strtoupper(dechex($res[0])) . strtoupper(dechex($res[1]));
    $res = str_pad($res, 15, '0', STR_PAD_LEFT);

    return $res;
  }

  /**
   * 주문/배송 > 배송중 > 취소 문의
   *
   * @param Request $request
   * @return \Illuminate\Http\JsonResponse
   */
  public function setCSCancel(Request $request)
  {
    $request->validate([
      'detail_id' => 'required',
      'device'    => 'required',
      'type'      => 'required',
      'cs'        => 'required',
    ]);

    $authUserId = $this->userIdx;
    $resDetail = OrderDetail::find($request->input('detail_id'));

    if (! $authUserId) {
      return $this->resultFail("FAIL", "접근권한이 올바르지 않습니다");
    }

    if ($authUserId != $resDetail->order->user_id) {
      return $this->resultFail("FAIL", "접근권한이 올바르지 않습니다");
    }

    if ($resDetail->isDuplicateCancel()) {
      return $this->resultFail('FAIL', '이미 접수된 취소문의 건이 있습니다.');
    }

    $data = [
      'order_id'  => $resDetail->order_id,
      'detail_id' => $resDetail->id,
      'user_id'   => $authUserId,
      'type'      => '구매취소',
      'title'     => $request->input('type'),
      'cs'        => $request->input('cs'),
      'state'     => "답변대기",
    ];

    OrderCustomerService::create($data);
    $resDetail->state = "취소접수";
    $resDetail->save();

    event(new OrderCancelRequest(
      $resDetail->order->user->shipping_phone,
      $resDetail->seller->name,
      $resDetail->order->user->shipping_name,
      $resDetail->order->order_number,
      $resDetail->goods->title,
      $request->cs
    ));

    return $this->resultOk();
  }

  /**
   * 주문 문의 - 반품신청
   * @ param
   * required
   * required
   * detail_id : 주문 id (order_detail - id)
   * title : 문의 유형 ['단숨변심','오배송','상품불량',기타]
   * cs : 문의 내용
   * device : 디바이스 [101 - web, 102 - ios, 103 - ios]
   * user_id : (Optional) user_list - user_id
   * token : (Optional) 로그인시 발행된 토큰
   * img1~4 :  (Optional) 첨부 이미지
   */
  public function setCSRefund(Request $request)
  {
    $request->validate([
      'detail_id' => 'required',
      'device'    => 'required',
      'title'     => 'required',
      'cs'        => 'required',
    ]);

    $authUserId = $this->userIdx;

//    // dd(json_decode($postscript_fit));
//    $authUserId = $this->userAuthCheckTemp(['device' => $request->input('device'), 'user_id' => $request->input('user_id'), 'token' => $request->input('token')]);
    if (!$authUserId) {
//      return response()->json("접근권한이 올바르지 않습니다.");
      return $this->resultFail("FAIL", "접근권한이 올바르지 않습니다");
    }

    $resDetail = OrderDetail::find($request->input('detail_id'));
    if ($authUserId != $resDetail->order->user_id) {
      return $this->resultFail("FAIL", "접근권한이 올바르지 않습니다");
    }

    $imgs = "";
//    for ($i = 1; $i <= 4; $i++) {
//      $requestImage = 'img' . $i;
//      if (isset($request->$requestImage)) {
//        $image = getUnique() . time() . '.' . $request->$requestImage->getClientOriginalExtension();
//        Storage::disk('s3')->put(S3_PATH_CS_CONTENT . $image, file_get_contents($request->$requestImage), 'public');
//        $imgs .= $image . ",";
//      }
//    }
    if ($request->hasFile('files')) {
      $files = $request->file('files');
      if(is_array($files)) {
        foreach ($files as $key => $file) {
          $image = $this->getUnique() . time() . $file->getClientOriginalName();
          Storage::disk('s3')->put(S3_PATH_CS_CONTENT . $image, file_get_contents($file), 'public');
          $imgs .= $image . ",";
        }
      }
    }

    $data = [
      'order_id'  => $resDetail->order_id,
      'detail_id' => $resDetail->id,
      'user_id'   => $authUserId,
      'type'      => '반품',
      'title'     => $request->input('title'),
      'cs'        => $request->input('cs'),
      'state'     => "답변대기",
      'images'    => $imgs,
    ];

    OrderCustomerService::create($data);
    $resDetail->state = "반품접수";
    $resDetail->save();


    event(new OrderRefundRequest(
      $resDetail->order->user->shipping_phone,
      $resDetail->seller->name,
      $resDetail->order->user->shipping_name,
      $resDetail->order->order_number,
      $resDetail->goods->title,
      $request->cs
    ));


//    return response()->json("", 200);
    return $this->resultOk();
  }

  private function getBundleShippingOrder($seller, $orderDetail)
  {
    $data = collect($orderDetail);

    return ($data->where('seller_id', $seller)->count() > 1) ? 1 : 0;
  }

  public function apiGetOrderDetail($detailId, $device, $userId = 0, $token = NULL)
  {
    $res_detail = OrderDetail::with(['goods', 'goods.seller', 'payment_one'])->find($detailId);
    $res_detail->goods->option_value = $res_detail->goods->getOptionBySku($res_detail->sku_id, true);
    $res_detail->delivery;
    if (isset($res_detail->payment_one->vbank_date)){
      $res_detail->payment_one->vbank_date = date("Y-m-d H:i:s", $res_detail->payment_one->vbank_date);
    }
//    return response()->json($res_detail, 200);
    return $this->resultOk($res_detail);
  }

  /**
   * 구매확정
   *
   * @param $detail_id
   */
  public function setOrderConfirm($detail_id)
  {
    $detail = OrderDetail::find($detail_id);
    $detail->state = '구매확정';
    $detail->confirmed_at = now();
    $detail->save();
    $detail->delivery->state = '구매확정';
    $detail->delivery->save();


    $order = (Order::find($detail->order_id));

    $lmPayHelper = new CLmPayHelper();
    $lmAmount = floor(($detail->total_price) * 0.015);
    $lmPayHelper->addLmpay($order->user_id, "ITEMBUY", $lmAmount, "입금", $detail->id);

    $orderDetailCount = (Order::find($detail->order_id))->detail->count();
    $orderDetailConfirmCount = OrderDetail::where([
      ['order_id', $detail->order_id],
      ['state', '구매확정'],
    ])->count();

    if ($orderDetailCount === $orderDetailConfirmCount) {
      $detail->order->update(['state' => '구매확정']);
    }
    $pushTagData = json_encode(array(
      "type" => '온라인',
      "order_id" => $detail->order_id
    ));
    $userDao = new CUserDAO();
    $userData = $userDao->getUserInfo($order->user_id)[0];
    $pushHelper = new CPushHelper();
    $pushHelper->sendPush($order->user_id, '구매확정', "{$userData->nic_name_full} 고객님 ‘{$detail->goods->title}’ 구매확정으로 러마페이 {$lmAmount}원이 적립되었습니다.", 'store', $pushTagData);
    return $this->resultOk();
  }

  /**
   * 주문 내역 삭제
   */
  public function setOrderDetailUnexposed($menu, $detail_id) {
    if ($menu === "lm") {
      $data = array(
        "_NQ_unexposed_at" => "now()"
      );
      $dao = new CBaseDAO();
      $dao->updateQuery("order_goods_tbl", $data, "idx = {$detail_id}", "pay_w");
    } else {
      $data = array(
        "_NQ_unexposed_at" => "now()"
      );
      $dao = new CBaseDAO();
      $dao->updateQuery("order_detail", $data, "id = {$detail_id}", "pay_w");
    }
    return $this->resultOk();
  }

  public function OrderResultMobileSubmit()
  {

//    $dao = new CBaseDAO();
    $data = $this->getParameters();
//    // dd($data);
//    // dd($data["user_type"]);

    if (isset($data["imp_uid"])) {
//      $data = json_decode($data);
      $args = array(
        "user_type"           => $data["user_type"],
        "user_id"             => $data["user_id"],
        "device"              => $data["device"],
        "sku_ids"             => $data["sku_ids"],
        "quantities"          => $data["quantities"],
        "order_name"          => $data["order_name"],
        "order_phone"         => $data["order_phone"],
        "address_name"        => $data["address_name"],
        "shipping_name"       => $data["shipping_name"],
        "shipping_phone"      => $data["shipping_phone"],
        "shipping_zipcode"    => $data["shipping_zipcode"],
        "shipping_address_1"  => $data["shipping_address_1"],
        "shipping_address_2"  => $data["shipping_address_2"],
        "shipping_msg"        => $data["shipping_msg"],
        "set_address_default" => false,
        "method"              => $data["method"],
        "total"               => $data["total"],
        "shipping"            => $data["shipping"],
        "shipping_cnt"        => $data["shipping_cnt"],
        "add_shipping"        => $data["add_shipping"],
        "lmpay"               => $data["lmpay"],
        "set_default_method"  => $data["set_default_method"],
        "bank"                => $data["bank"],
        "holder"              => $data["holder"],
        "account"             => $data["account"],
        "imp_uid"             => $data['imp_uid']
      );

//      // dd($args);
      $request = $this->makeRequest($args);
      $res = $this->setOrderSubmit($request);
//dd($res);
      if ($res->getStatusCode() != 200) {
        $msg = json_decode($res->getContent());
//        return redirect('/re/store/order/error/' . $msg);
        return $this->resultFail("FAIL", $msg);
      } else {
//        return redirect('/re/store/order/result/' . $res->getContent());
//        // dd($res->getContent());

        return $res->getContent();
      }
    }
  }

}
