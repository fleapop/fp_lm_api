<?php


namespace App\Http\Controllers;


use App\Helper\CCommunityHelper;
use App\Helper\CFileuploadHelper;
use App\Helper\CLmPayHelper;
use App\Helper\CPushHelper;
use App\Http\dao\CBaseDAO;
use App\Http\dao\CCommunityDAO;
use App\Http\dao\CUserDAO;
use Dotenv\Store\File\Reader;
use http\Client\Response;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Session;

class CommunityController extends Controller
{
  private $tUserId = 0;
  private $isLikeTemp = null;
  private $reviewColorArr = array(
    "C1" => "어두워요",
    "C2" => "화면과 같아요",
    "C3" => "밝아요"
  );
  private $reviewSizeArr = array(
    "S1" => "작아요",
    "S2" => "잘 맞아요",
    "S3" => "커요"
  );
  #region Community Insert&Update (러덕생활, 러덕핏, 플리생활, 후기핏, 답정러)
  // $service_type - 러덕생활 : ldlife,  플리생활 : fllife, 러덕핏 : fit, 답정러 : answer, 후기핏 : postscript
  public function setCommunityEdit($service_type, Request $request) {
    // 기본 필수 파라미터
    // 러덕생활 : ldlife,  플리생활 : fllife, 러덕핏 : fit, 답정러 - answer
//    $service_type = $this->getParam("service_type");
    $category = $this->getParam("category");
    $title = $this->getParam("title");
    $contents = $this->getParam("contents");

    $timeline_idx = $this->getParam("timeline_idx");
    $del_files = $this->getParam("del_files");

    // 러덕핏 정보
    $ld_fits = $this->getParam("ld_fits");

    // 후기핏 정보
    $postscript_fit_temp = $this->getParam("postscript_fit");
    $postscript_fit = isset($postscript_fit_temp) ? json_decode($postscript_fit_temp) : $postscript_fit_temp;

    $user_height = $this->getParam("user_height");
    $user_size = $this->getParam("user_size");
    $user_skin = $this->getParam("user_skin");
    $user_foot_size = $this->getParam("user_foot_size");
    $user_idx = $this->userIdx;
//    // dd(json_decode($postscript_fit));
    $dao = new CBaseDAO();
    if(!$user_idx) return $this->resultFail('NO_LOGIN', "로그인이 필요합니다.");

    $fileHelper = new CFileuploadHelper();
    // 이미지 삭제 & deleted_at Update
    if ($del_files && $timeline_idx) {
      $del_files_array = json_decode($del_files);
//      // dd($del_files);
      foreach ($del_files_array as $delItem) {
//        // dd($delItem);
        $fileHelper->fileDelete($delItem->path);
        $delData = array(
          "_NQ_deleted_at" => "now()"
        );
        $dao->updateQuery("cm_imgs", $delData, "idx = {$delItem->idx}", "db_w");

        if ($service_type == "answer") {
          $dao->deleteQuery("answer_decided_selectd", "img_idx = {$delItem->idx}", "db_w");
        }
      }
    }
    $fileRes = $fileHelper->multipleFileUpload($request, $service_type);

    if ($fileRes["result"]) {
      $img_type = isset($fileRes["img_type"]) ? $fileRes["img_type"] : null;
    } else {
      $img_type = null;
    }

    $timelineIdx = $this->setTimeline($service_type, $category,$title, $contents, $img_type, $user_idx, $timeline_idx);

//    // dd($fileRes["files"]);
    // 이미지 등록
    if ($fileRes["result"]) {
      foreach ($fileRes["files"] as $key => $item) {
        $data = [
          'service_type' => $service_type,
          'service_idx' => $timelineIdx,
          'img_path' => $item["path"],
          'img_name' => $item["name"]
        ];

        $imgRes = $dao->insertQuery("cm_imgs", $data, "db_w");
      }
    }
//    function setImgType($timeline_idx)
//    {
//      $fileHelper = new CFileuploadHelper();
//      $dao = new CBaseDAO();
      if ($timeline_idx) {
//      $imgCheckSql = "select concat(img_path,img_name) as path from cm_imgs where service_idx = {$timeline_idx}";
        $imgCheckSql = "select concat('https://fps3bucket.s3.ap-northeast-2.amazonaws.com',img_path,img_name) as path from cm_imgs where service_idx = {$timeline_idx} and deleted_at is null";
        $imgCheckRes = $dao->selectQuery($imgCheckSql, "db");
        $imgCount = count($imgCheckRes);
        $imgType = array(
          "width" => 0,
          "height" => 0
        );

        if ($imgCount > 0) {
          foreach ($imgCheckRes as $imgItem) {
            $imgSizeType = $fileHelper->imageSizeCheck($imgItem->path);
            if ($imgSizeType == 2) {
              $imgType["width"]++;
            } else if ($imgSizeType == 3) {
              $imgType["height"]++;
            }
          }
          if ($imgCount == 1) {
            $img_type = 1;
          } else if ($imgCount >= 3 && $imgType["height"] > 0) {
            $img_type = 5;
          } else if ($imgCount >= 3 && $imgType["height"] <= 0) {
            $img_type = 4;
          } else if ($imgCount == 2 && $imgType["height"] == 2) {
            $img_type = 3;
          } else if ($imgCount == 2 && $imgType["height"] < 2) {
            $img_type = 2;
          }
        }
        $hash = array(
          "img_type" => $img_type
        );
        $dao->updateQuery("cm_timeline", $hash, "idx = {$timeline_idx}", "db_w");
      }

//      return $img_type;
//    }


    switch ($service_type) {
      case "fit" : {
        $this->setLdFit($ld_fits, $timelineIdx);
       break;
      }
      case "postscript" :
      {
        if (!$timeline_idx) {
          if ($fileRes["result"]) {
//            if ($fileRes["files"]) $amount = 500;
            $cmDao = new CCommunityDAO();
            $first = $cmDao->communityReviewFirstCheck($postscript_fit->order_detail_id);
            if ((int)$first <= 0) {
              $amount = 2000;
            } else {
              $amount = 500;
            }
          } else {
            $amount = 200;
          }


          if(!$timeline_idx) {
            $lmPayHelper = new CLmPayHelper();
            $lmpayIdx = $lmPayHelper->addLmpay($user_idx, "REVIEW", $amount, "입금",$postscript_fit->order_detail_id);
          }

          $postscript_fit->lmpay_idx = $lmpayIdx;
          $postscript_fit->lmpay_amount = $amount;
          $id = $this->setPostscript($postscript_fit, $timelineIdx, $timeline_idx);
//          $data = [
//            'user_height' => $user_height,
//            'user_size'    => $user_size,
//            'user_skin'      => $user_skin,
//            'user_foot_size' => $user_foot_size
//          ];
          if(isset($user_height) || isset($user_size) || isset($user_skin) || isset($user_foot_size)) {
            if (isset($user_height)) $dataUser["user_height"] = $user_height;
            if (isset($user_size)) $dataUser["user_size"] = $user_size;
            if (isset($user_skin)) $dataUser["user_skin"] = $user_skin;
            if (isset($user_foot_size)) $dataUser["user_foot_size"] = $user_foot_size;
            $dao->updateQuery("user_extra_info", $dataUser, "user_id = '{$user_idx}' ", "db_w");
          }
          if(!$timeline_idx) {
            $pushTagData = json_encode(array(
              "service_type" => 'postscript',
              "idx" => $timelineIdx,
              "is_img" => 0
            ));

            $communitiHelper = new CCommunityHelper();
            if($communitiHelper->getTimelineImgType($timelineIdx)) {
              $pushTagData = json_encode(array(
                "service_type" => 'postscript',
                "idx" => $timelineIdx,
                "is_img" => 1
              ));
            }

            $userDao = new CUserDAO();
            $userData = $userDao->getUserInfo($user_idx)[0];
            $pushHelper = new CPushHelper();
            $pushHelper->sendPush($user_idx, '후기작성', "{$userData->nic_name_full} 고객님 러마페이 {$amount}원이 후기작성으로 적립되었습니다.", 'community', $pushTagData);
          }
        }
        break;
      }
      case "answer" : {

        $timelineIdx = $timeline_idx ? $timeline_idx : $timelineIdx;

        $sql = "update fp_db.answer_decided_selectd t1
                  inner join
                  (
                   select @a:=@a+1 as rowNum, idx 
                   from fp_db.answer_decided_selectd, (SELECT @a := 0) rn
                   where service_idx ='{$timelineIdx}'
                   order by idx asc
                  ) t2
                   on t1.idx= t2.idx
                set t1.img_index = t2.rowNum";
        $dao->conn_w->update($sql);

      }
    }
    if ($category == "postscript") {
      if ($timeline_idx) {

        $this->setPostscript($postscript_fit, $timelineIdx, $timeline_idx);
        $data = [
          'user_height' => $user_height,
          'user_size'    => $user_size,
          'user_skin'      => $user_skin
        ];
        $dao->updateQuery("user_extra_info", $data,"user_id = '{$user_idx}' ", "db_w");
      }
    }


    return $this->resultOk($timelineIdx,"등록 되었습니다.");
  }

  /**
   * CM_COMMUNITY Insert or Update
   * @param $service_type
   * @param $category
   * @param $contents
   * @param null $img_type
   * @param int $user_idx
   * @param null $timeline_idx
   * @return false
   */
  private function setTimeline ($service_type, $category = null, $title = null, $contents, $img_type = null, $user_idx = 0, $timeline_idx = null) {
    $dao = new CBaseDAO();

    if($timeline_idx) {
      $data = [
        'category' => $category,
        'title'    => $title,
        'contents'    => $contents,
        'user_idx'      => $user_idx,
        'img_type'    => $img_type,
      ];

      $resTemp = $dao->updateQuery("cm_timeline", $data,"service_type = '$service_type' and user_idx = '{$user_idx}' and idx = {$timeline_idx}", "db_w");
      $res = $timeline_idx;
    } else {
      $data = [
        'service_type'     => $service_type,
        'category' => $category,
        'title'    => $title,
        'contents'    => $contents,
        'user_idx'      => $user_idx,
        'img_type'    => $img_type,
      ];

      $res = $dao->insertQuery("cm_timeline",$data,"db_w");
    }
    return $res;
  }

  // 러덕핏
  private function setLdFit($ld_fits, $timeline_idx = null) {
    $dao = new CBaseDAO();
//    // dd($ld_fits);
//    // dd(json_decode($ld_fits));
//    json_decode($ld_fits);
//    // dd(json_last_error());
    $ld_fits_decode = json_decode($ld_fits);
//    // dd($ld_fits_decode->add);
    if (isset($ld_fits_decode->add)) {
      foreach ($ld_fits_decode->add as $ld_fit) {
        $data = [
          'timeline_idx' => $timeline_idx,
          'category' => $ld_fit->category,
          'seller_idx' => $ld_fit->seller_idx,
          'seller_name' => $ld_fit->seller_name,
          'goods_idx' => $ld_fit->goods_idx,
          'goods_name' => $ld_fit->goods_name,
        ];

        $res = $dao->insertQuery("cm_ld_fit", $data, "db_w");

      }
    }
    if (isset($ld_fits_decode->del)) {
      foreach ($ld_fits_decode->del as $ld_fit) {
        $data = [
          '_NQ_deleted_at' => "now()"
        ];

        $dao->updateQuery("cm_ld_fit", $data, "timeline_idx = '$timeline_idx' and idx = {$ld_fit}", "db_w");
      }
    }
  }

  // 후기핏
  private function setPostscript($answer_fit, $service_idx = null, $timeline_idx = null) {
    $dao = new CBaseDAO();
    if($timeline_idx) {
      $data = [
        'service_idx'    => $timeline_idx,
        'review_grade'      => $answer_fit->review_grade,
        'review_color'    => $answer_fit->review_color,
        'review_size' => $answer_fit->review_size,
        'order_detail_id'    => $answer_fit->order_detail_id
      ];
      $res = $dao->updateQuery("postscript_tbl", $data," service_idx = {$timeline_idx}", "db_w");

    } else {
      $data = [
        'service_idx'    => $service_idx,
        'review_grade'      => $answer_fit->review_grade,
        'review_color'    => $answer_fit->review_color,
        'review_size' => $answer_fit->review_size,
        'order_detail_id'    => $answer_fit->order_detail_id,
        'lmpay_idx'      => $answer_fit->lmpay_idx,
        'lmpay_amount'    => $answer_fit->lmpay_amount,
      ];
      $res = $dao->insertQuery("postscript_tbl",$data,"db_w");
    }
    return $res;
  }

  // 상품태그 검색 : 검색어 관련 리스트
  public function getTagSearch($keyword = "") {
    $dao = new CBaseDAO();
    $sql = "select case when INSTR(g.title, '{$keyword}') then g.title else st.name end as name from fp_pay.goods g inner join fp_db.seller_tbl st on g.seller_id = st.idx 
                        where g.display_state = 'show' and g.deleted_at is null and st.display_state = 'show' and st.delete_date is null
                        and (g.title like '%{$keyword}%' or st.name like '%{$keyword}%')
                        group by name";
    $res = $dao->selectQuery($sql, "pay");
    return $this->resultOk($res,"");
  }

  // 상품검색 검색 결과
  public function getTagSearchResult ($keyword = "") {
    $page = $this->getParam("page");
    $perPage = 30;
    $limit = "";

    $dao = new CBaseDAO();
    if(isset($page)) {
      $limit = $dao->getLimit($page, $perPage);
    }

    $goodsSql = "select g.id as goods_idx, g.title, g.sub_category ,st.idx as seller_idx, st.name, g.imgs  
                        from fp_pay.goods g inner join fp_db.seller_tbl st on g.seller_id = st.idx 
                        where g.display_state = 'show' and g.deleted_at is null and st.display_state = 'show' and st.delete_date is null
                        and (g.title like '%{$keyword}%' or st.name like '%{$keyword}%')
                        group by g.id
                        {$limit}";
    $goodsCntSql = "select g.id  
                        from fp_pay.goods g inner join fp_db.seller_tbl st on g.seller_id = st.idx 
                        where g.display_state = 'show' and g.deleted_at is null and st.display_state = 'show' and st.delete_date is null
                        and (g.title like '%{$keyword}%' or st.name like '%{$keyword}%')
                        group by g.id
                        ";
    $sellerSql = "select idx as seller_idx, name as seller_name, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image
                        from fp_db.seller_tbl st where delete_date is null and display_state = 'show' and name like '%{$keyword}%'";

    $goodsRes = $dao->selectQuery($goodsSql, "pay");
    $goodsCnt = count($dao->selectQuery($goodsCntSql, "pay"));
    $sellerRes = $dao->selectQuery($sellerSql, "pay");

    $last_page = ceil((int)$goodsCnt / $perPage);
    $response = array(
      "current_page" => (int)$page,
      "data" => $goodsRes,
      "last_page" => $last_page,
      "total" => $goodsCnt
    );
    $data = array(
      "goodsList" => isset($page) ? $response : $goodsRes,
      "goodsCnt" =>$goodsCnt,
      "sellerList" => $sellerRes,
      "sellerCnt" =>count($sellerRes)
    );
    return $this->resultOk($data);

  }

  // 게시글 삭제
  public function delTimeline($service_type) {
    $timeline_idx = $this->getParam("timeline_idx");
//    $service_type = $this->getParam("service_type");

    try {

      $dao = new CBaseDAO();
      $data = [
        '_NQ_deleted_at' => "now()"
      ];
      $dao->updateQuery("cm_timeline", $data," idx = {$timeline_idx}", "db_w");

      $sql ="select * from cm_imgs where service_idx = {$timeline_idx}";
      $files = $dao->selectQuery($sql);

      $fileHelper = new CFileuploadHelper();
      foreach ($files as $item) {
        $fileHelper->fileDelete($item->img_path.$item->img_name);
        $delData = array(
          "_NQ_deleted_at" => "now()"
        );
        $dao->updateQuery("cm_imgs", $delData, "idx = {$item->idx}", "db_w");
      }
      switch ($service_type) {
        case "fit" :
        {
          $delData = array(
            "_NQ_deleted_at" => "now()"
          );
          $dao->updateQuery("cm_ld_fit", $delData, "timeline_idx = {$timeline_idx}", "db_w");
          break;
        }
        case "postscript" :
        {
          $delData = array(
            "_NQ_deleted_at" => "now()"
          );
          $dao->updateQuery("postscript_tbl", $delData, "service_idx = {$timeline_idx}", "db_w");
          break;
        }
      }
      return $this->resultOk(null, "삭제 되었습니다.");
    } catch (\Exception $e) {
      return $this->resultFail("FAIL", "삭제되지 않아습니다.");
    }
  }
  #endregion

  #region Community List (러덕생활, 러덕핏, 플리생활, 후기핏, 답정러)

  // 타 서비스에서 커뮤니티 리스트 호출 시 사용
  public function getListRouter($service_type,$user_idx, $islike = null, $myUserId = null) {
    $this->userIdx = $myUserId;
    $this->tUserId = $user_idx;
//    dd($this->userIdx);
    if (isset($islike) && $islike) $this->isLikeTemp = true;
    return $this->getList($service_type);
  }

  public function getList ($service_type) {
//    phpinfo();
    $page = $this->getParam("page", 1);
    $category = $this->getParam("category");
    $timeline_idx = $this->getParam("timeline_idx");
    $order = $this->getParam("order");
    $user_idx = $this->userIdx;

    $isLike = $this->getParam("islike", null);

    if (isset($this->isLikeTemp) && $this->isLikeTemp) {
      $isLike = true;
    }
    if(!$user_idx && $category == "follow") return $this->resultFail('NO_LOGIN', "로그인이 필요합니다.");

    #region 정렬
    $orderSql = "order by ct.idx desc";
    if ($order == "new") $orderSql = "order by ct.idx desc";
    if ($order == "popular") $orderSql = "order by score desc";
    if ($order == "reply") $orderSql = "order by ct.reply_count desc";
    #endregion

    $perPage = 40;
    $dao = new CBaseDAO();
    $limit = $dao->getLimit($page, $perPage);
    if ($service_type == "ldlife" || $service_type == "fllife") {
      $resTemp = $this->ldFlLifeList($service_type, $category, $timeline_idx, $order, $user_idx, $limit, $isLike);
    } else if ($service_type == "fit") {
      $resTemp = $this->fitList($service_type, $category, $timeline_idx, $order, $user_idx, $limit, $isLike);
    } else if ($service_type == "answer") {
      $resTemp = $this->answerList($service_type, $category, $timeline_idx, $order, $user_idx, $limit, $isLike);
    }
    $res = $resTemp["data"];

//    $replyResult = array();
    foreach ($res as $item) {
      switch ($service_type) {
        case "fit" :
        {
          // 러덕핏
          $sql = "select *
                from cm_ld_fit clf 
                where timeline_idx = {$item->idx}
                and category != '' and seller_name != ''
                and category is not null and seller_name is not null
                and deleted_at is null";
          $resFit = $dao->selectQuery($sql, "db");

          $item->ld_fit = $resFit;

          // 후기핏
          $sql = "select * 
                from postscript_tbl pt 
                where pt.service_idx = {$item->idx}
                and pt.deleted_at is null";
          $resPostscript = $dao->selectQuery($sql, "db");
          $item->postscript = null;
          if (isset($resPostscript[0])) {
            $item->postscript = $resPostscript;
            foreach ($resPostscript as $orderItem) {
              $orderSql = "select st.name , g2.title ,g2.price as origin_price ,g2.use_discount 
, if(g2.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g2.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g2.price) AS total_price 
 , GROUP_CONCAT(o2.value SEPARATOR ', ') as option_info
                          , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image 
                          , g2.id as goods_id, st.idx as seller_id
                          , if(count(lt.idx) > 0, 1, 0) as is_like
                          , case when g2.sub_category = '악세사리' then 'acc'
                            when g2.sub_category = '뷰티' then 'beauty'
                            else 'etc' end as option_type
                          , g2.imgs as goods_img
                          from order_detail od
                          inner join goods g2 on od.goods_id = g2.id 
                          inner join fp_db.seller_tbl st on g2.seller_id = st.idx 
                          inner join option_sku os on od.sku_id = os.sku_id 
                          inner join `options` o2 on os.option_id = o2.id 
                          left join fp_pay.goods_promotions gp on gp.goods_id = g2.id and (gp.started_at <= now() and gp.ended_at >= now()) and gp.display_state = 'show'
                          left join fp_db.like_tbl lt on lt.table_name = 'goods' and lt.table_idx = g2.id and lt.user_idx = '{$user_idx}'
                          where od.id = {$orderItem->order_detail_id}";
              $orderRes = $dao->selectQuery($orderSql, "pay");
              $orderItem->review_color = strlen($orderItem->review_color) >= 3 ? $orderItem->review_color : $this->reviewColorArr[$orderItem->review_color];
              $orderItem->review_size = strlen($orderItem->review_size) >= 3 ? $orderItem->review_size : $this->reviewSizeArr[$orderItem->review_size];
              $orderItem->order = isset($orderRes[0]) ? $orderRes[0] : null;
            }

          }
          break;
        }
      }
//      if ($category == "postscript") {
//        // 후기핏
//        $sql = "select *
//                from postscript_tbl pt
//                where pt.service_idx = {$item->idx}
//                and pt.deleted_at is null";
//        $resPostscript = $dao->selectQuery($sql, "db");
//        $item->postscript = null;
//        if (isset($resPostscript[0])) {
//          $item->postscript = $resPostscript;
//        }
//      }

      $item->files = $this->getCommunityFiles($item->service_type, $item->idx);
      $replySql = "select ct.idx,ct.table_name ,ct.table_idx ,ct.comment ,ct.parent_idx ,ct.to_user_idx ,ct.from_user_idx ,list_date_view(ct.insert_date) as create_date, ct.user_tags 
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as from_nic_name
                , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as from_profile_img
                , if(ul2.profile_img = '' or ul2.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul2.profile_img,'default_profile.jpg','default_profile.png')), '')) as to_nic_name
                , ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/',ul2.profile_img), '') as to_profile_img
                from comment_tbl ct 
                left join user_list ul on ct.from_user_idx = ul.user_id 
                left join user_list ul2 on ct.to_user_idx = ul2.user_id 
                where ct.table_name = 'cm_timeline' and ct.table_idx = {$item->idx} and ct.delete_date is null order by ct.idx desc";
      $resReply = $dao->selectQuery($replySql, "db");

      if (isset($resReply)) {
        foreach ($resReply as $item2) {
          if (isset($item2->user_tags) && $item2->user_tags != "") {
            $userTagSql = "select user_id, ifnull(nic_name, shipping_name) as nic_name from user_list where user_id in ({$item2->user_tags})";
            $resUserTags = $dao->selectQuery($userTagSql, "db");
            $item2->user_tags = $resUserTags;
          } else {
            $item2->user_tags = array();
          }
        }
        $item->replyList = $resReply;
        $item->replyCnt = count($resReply);
      } else {
        $item->replyList = null;
        $item->replyCnt = 0;
      }
//      $replyResult["list"] = $resReply;
//      $replyResult["cnt"] = count($resReply);
    }

//    $resDate = array(
//      "data" => $res,
//      "reply" => $replyResult
//    );
    $dao = new CUserDAO();
    $this->userInfo = $dao->getUserInfo($user_idx);
    $nonInfo = json_decode("{}");
    if (isset($timeline_idx)) {
      if(isset($res[0])) {
        $communityHelper = new CCommunityHelper();
        $communityHelper->setTimelineRead($timeline_idx, $user_idx);
        $resData = $res[0];
      } else {
        $resData = $nonInfo;
      }
    } else {
      $resData = $res;
    }
//    $resData = isset($timeline_idx) && isset($res[0]) ? $res[0] : $res;

    if (isset($timeline_idx)){
      $response = $resData;
    } else {
      $last_page = ceil((int)$resTemp["cnt"] / $perPage);
      $response = [
        "current_page" => (int)$page,
        "data" => $resData,
        "last_page" => $last_page,
        "total" => $resTemp["cnt"]
      ];
    }
    $nonInfo = json_decode("{}");
    return $this->resultOk($response, null,isset($this->userInfo[0]) ? $this->userInfo[0] : $nonInfo);
  }

  private function ldFlLifeList($service_type, $category, $timeline_idx, $order, $user_idx, $limit, $isLike = null) {
    $orderSql = "order by ct.idx desc";
    if ($order == "new") $orderSql = "order by ct.idx desc";
    if ($order == "popular") $orderSql = "order by score desc, score_sub desc";
    if ($order == "reply") $orderSql = "order by ct.reply_count desc";

    if ($isLike) $orderSql = "order by lt.idx desc";

    $dao = new CBaseDAO();
    $where = ($category == "all" || !$category) ? "" : "and ct.category = '{$category}'";
    $detailWhere = $timeline_idx ? "and ct.idx = {$timeline_idx}" : "";

    $isLikeJoin = "left join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    if(isset($isLike)) {
      $isLikeJoin = "inner join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    }
    $isMypage = "";
    if(!isset($isLike)) {
      $isMypage = (int)$this->tUserId > 0 ? " and ct.user_idx = ".$this->tUserId : "";
    }

    $sql = "select ct.* ,list_date_view(ct.created_at) as create_date ,count(cc.idx) as claim_cnt
                , count(distinct lt2.idx) as score
                , count(distinct lt3.idx) as score_sub
                , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name
                , case when lt.idx is null then 0 else 1 end as is_like_state
                , case when ft.idx is null then 0 else 1 end as is_follow
                from fp_db.cm_timeline ct 
                left join fp_db.user_list ul on ct.user_idx = user_id
                 {$isLikeJoin}
                left join fp_db.cm_claim cc on ct.service_type = cc.service_type and ct.idx = cc.service_idx and cc.deleted_at is null
                left join fp_db.follow_tbl ft on ft.to_user_idx = ct.user_idx and ft.from_user_idx = '{$user_idx}' 
                left join fp_db.like_tbl lt2 on lt2.table_idx = ct.idx and lt2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 14
                left join fp_db.like_tbl lt3 on lt3.table_idx = ct.idx and lt3.table_name = 'cm_timeline' and DATEDIFF(now(), lt3.insert_date)  > 14
                where ct.deleted_at is null and ct.service_type = '{$service_type}' and ct.claim_deleted_at is null
                {$isMypage}
                {$where}
                {$detailWhere}
                group by ct.idx 
                {$orderSql}
                {$limit}
                ";
//dd($sql);
    $res = $dao->selectQuery($sql, "db");
    $sqlCnt = "select count(ct.idx) as cnt from fp_db.cm_timeline ct {$isLikeJoin} where ct.deleted_at is null and ct.claim_deleted_at is null and ct.service_type = '{$service_type}' {$isMypage} {$where}";
    $resCnt = $dao->selectQuery($sqlCnt, "db")[0]->cnt;
    $data = array(
      "data" => $res,
      "cnt" => $resCnt
    );


    return $data;
  }

  private function fitList($service_type, $category, $timeline_idx, $order, $user_idx, $limit, $isLike = null, $goods_id= null) {
    $orderSql = "order by ct.idx desc";
    if ($order == "new") $orderSql = "order by ct.idx desc";
    if ($order == "popular") $orderSql = "order by score_order asc, score desc";
    if ($order == "reply") $orderSql = "order by ct.reply_count desc";
    if ($order == "follow") $orderSql = "order by ct.idx desc";

    if ($isLike) $orderSql = "order by lt.idx desc";

    $where = "and (ct.service_type = 'fit' or ct.service_type = 'postscript')";
    $followJon = "";
    $followWhere = "";
    if ($category =="postscript") {
      $where = "and ct.service_type = 'postscript'";
    }
    if ($category =="follow" || $order == "follow") {
      if(!$user_idx) return $this->resultFail("NO_LOGIN");
      $followJon = "inner join fp_db.follow_tbl ft on ct.user_idx = ft.to_user_idx and ft.deleted_at is null and ft.state = 2 and ft.from_user_idx = {$user_idx}";
      $followWhere = " and ct.user_idx != $user_idx";
    }

    $isLikeJoin = "left join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    if(isset($isLike)) {
      $isLikeJoin = "inner join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    }
    $dao = new CBaseDAO();
//    $isMypage = (int)$this->tUserId > 0 ? " and ct.user_idx = ".$this->tUserId : "";
    $isMypage = "";
    if(!isset($isLike)) {
      $isMypage = (int)$this->tUserId > 0 ? " and ct.user_idx = ".$this->tUserId : "";
    }

    $detailWhere = $timeline_idx ? "and ct.idx = {$timeline_idx}" : "";

    $isPhoto = $timeline_idx ? "" : "and ci.idx is not null";
    $goodsJoin = "";
    $goodsWhere = "";
    if (isset($goods_id)) {
      $goodsJoin =" inner join postscript_tbl pt on ct.idx = pt.service_idx 
                      left join fp_pay.order_detail od on pt.order_detail_id = od.id ";
      $goodsWhere = " and od.goods_id = {$goods_id}";
    }
    $sql = "select ct.* ,list_date_view(ct.created_at) as create_date ,count(cc.idx) as claim_cnt
                , if(count(distinct lt2.idx) > 0, 1, 2) as score_order 
                , if(count(distinct lt2.idx) > 0,count(distinct lt2.idx),count(distinct lt3.idx)) as score
                , uei.user_size , uei.user_height , uei.user_skin ,uei.user_foot_size, uei.is_body_open
                , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name
                , case when lt.idx is null then 0 else 1 end as is_like_state
                , case when ft2.idx is null then 0 else 1 end as is_follow
                , if(cfb.idx is null, 0, 1) as is_best
                from fp_db.cm_timeline ct 
                inner join fp_db.user_list ul on ct.user_idx = ul.user_id
                {$goodsJoin}
                left join fp_db.user_extra_info uei on ul.user_id = uei.user_id 
                {$followJon}
                left join fp_db.cm_claim cc on ct.service_type = cc.service_type and ct.idx = cc.service_idx and cc.deleted_at is null
                {$isLikeJoin}
                left join fp_db.follow_tbl ft2 on ft2.to_user_idx = ct.user_idx and ft2.from_user_idx = '{$user_idx}' 
                left join cm_imgs ci on ct.idx = ci.service_idx and ci.deleted_at is null and ci.service_type in ('postscript', 'fit')
                left join fp_db.like_tbl lt2 on lt2.table_idx = ct.idx and lt2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 3
                left join fp_db.like_tbl lt3 on lt3.table_idx = ct.idx and lt3.table_name = 'cm_timeline' and DATEDIFF(now(), lt3.insert_date)
                left join fp_db.cm_fit_best cfb on cfb.timeline_idx = ct.idx
                where ct.deleted_at is null  and ct.claim_deleted_at is null {$isPhoto}
                {$followWhere}
                {$goodsWhere}
                {$isMypage}
                {$where}
                {$detailWhere}
                group by ct.idx 
                {$orderSql}
                {$limit}
                ";

    $res = $dao->selectQuery($sql, "db");
    $sqlCnt = "select ct.* 
                from fp_db.cm_timeline ct 
                inner join fp_db.user_list ul on ct.user_idx = ul.user_id
                {$goodsJoin}
                left join fp_db.user_extra_info uei on ul.user_id = uei.user_id 
                {$followJon}
                left join fp_db.cm_claim cc on ct.service_type = cc.service_type and ct.idx = cc.service_idx and cc.deleted_at is null
                {$isLikeJoin}
                left join fp_db.follow_tbl ft2 on ft2.to_user_idx = ct.user_idx and ft2.from_user_idx = '{$user_idx}' 
                left join cm_imgs ci on ct.idx = ci.service_idx and ci.deleted_at is null and ci.service_type in ('postscript', 'fit')
                left join fp_db.like_tbl lt2 on lt2.table_idx = ct.idx and lt2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 3
                left join fp_db.comment_tbl ct2 on ct2.table_idx = ct.idx and ct2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 3
                where ct.deleted_at is null  and ct.claim_deleted_at is null and ci.idx is not null
                {$goodsWhere}
                {$isMypage}
                {$where}
                {$detailWhere}
                group by ct.idx  ";
    $resCnt = count($dao->selectQuery($sqlCnt, "db"));
    $data = array(
      "data" => $res,
      "cnt" => $resCnt
    );

    if ($user_idx && ($category =="follow" || $order == "follow")) {
      $sqlBadge = "select ct.idx
                from fp_db.cm_timeline ct 
                inner join fp_db.user_list ul on ct.user_idx = ul.user_id
                left join fp_db.cm_timeline_read ctr on ct.idx = ctr.service_idx and ctr.user_idx = {$user_idx}
                {$followJon}
                where ct.deleted_at is null and ct.claim_deleted_at is null and ctr.idx is null
                {$where}
                {$detailWhere}
                group by ct.idx
                ";

      $resBadge = $dao->selectQuery($sqlBadge, "db");
      if(count($resBadge)) {
        $badgeData = array();
        foreach ($resBadge as $key => $item) {
          $badgeData[$key]["service_type"] = 'fit';
          $badgeData[$key]["service_idx"] = $item->idx;
          $badgeData[$key]["user_idx"] = $user_idx;
        }
        $dao->insertMultiQuery("cm_timeline_read", $badgeData, "db_w");
      }
    }
    return $data;
  }

  private function answerList($service_type, $category, $timeline_idx, $order, $user_idx, $limit, $isLike = null) {
    $orderSql = "order by ct.idx desc";
    if ($order == "new") $orderSql = "order by ct.idx desc";
    if ($order == "popular") $orderSql = "order by score desc";
    if ($order == "reply") $orderSql = "order by ct.reply_count desc";
    if ($isLike) $orderSql = "order by lt.idx desc";
    $isLikeJoin = "left join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    if(isset($isLike)) {
      $isLikeJoin = "inner join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    }

    $dao = new CBaseDAO();
    $where = ($category == "all" || !$category || $category == "" ) ? "" : "and ct.category = '{$category}'";
    $detailWhere = $timeline_idx ? "and ct.idx = {$timeline_idx}" : "";
//    $isMypage = (int)$this->tUserId > 0 ? " and ct.user_idx = ".$this->tUserId : "";
    $isMypage = "";
    if(!isset($isLike)) {
      $isMypage = (int)$this->tUserId > 0 ? " and ct.user_idx = ".$this->tUserId : "";
    }





    $sql = "select ct.* ,list_date_view(ct.created_at) as create_date ,count(DISTINCT cc.idx) as claim_cnt
                , count(distinct lt2.idx) as score
                , ads.img_index as select_img_index ,ads.img_idx as select_img_idx 
                ,count(DISTINCT ads2.idx) as answer_total_cnt
                , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name
                , case when lt.idx is null then 0 else 1 end as is_like_state
                , case when ft.idx is null then 0 else 1 end as is_follow
                from fp_db.cm_timeline ct 
                left join fp_db.user_list ul on ct.user_idx = user_id
                left join fp_db.cm_claim cc on ct.service_type = cc.service_type and ct.idx = cc.service_idx and cc.deleted_at is null
                left outer join fp_db.answer_decided_selectd ads on ct.idx = ads.service_idx and ads.user_idx = {$user_idx}
                left outer join fp_db.answer_decided_selectd ads2 on ct.idx = ads2.service_idx
                {$isLikeJoin}
                left join fp_db.follow_tbl ft on ft.to_user_idx = ct.user_idx and ft.from_user_idx = '{$user_idx}' 
                left join fp_db.like_tbl lt2 on lt2.table_idx = ct.idx and lt2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 14
                left join fp_db.comment_tbl ct2 on ct2.table_idx = ct.idx and ct2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 14
                where ct.deleted_at is null and ct.service_type = '{$service_type}' and ct.claim_deleted_at is null
                {$isMypage}
                {$where}
                {$detailWhere}
                group by ct.idx 
                {$orderSql}
                {$limit}
                ";

    $res = $dao->selectQuery($sql, "db");
    $sqlCnt = "select count(ct.idx) as cnt from fp_db.cm_timeline ct {$isLikeJoin} where ct.deleted_at is null and ct.claim_deleted_at is null and ct.service_type = '{$service_type}' {$isMypage} {$where}";
    $resCnt = $dao->selectQuery($sqlCnt, "db")[0]->cnt;
    $data = array(
      "data" => $res,
      "cnt" => $resCnt
    );


    return $data;
  }

  private function getCommunityFiles ($service_type, $service_idx) {
    if ($service_type == "answer") {
      $sql = "select ci.*, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket',img_path,img_name) as full_path
                , ifnull(count(ads.idx), 0) as answer_cnt
                from cm_imgs ci 
                left join fp_db.answer_decided_selectd ads on ci.service_idx = ads.service_idx and ci.idx = ads.img_idx 
                where ci.service_idx = {$service_idx}
                and ci.service_type = '{$service_type}'
                and ci.deleted_at is null
                group by ci.idx ";
    } else {
      $sql = "select *, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket',img_path,img_name) as full_path
                from cm_imgs ci 
                where service_idx = {$service_idx}
                and service_type = '{$service_type}'
                and deleted_at is null";
    }
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "db");
    return $res;
  }

  #endregion

  #region 신고
  // 신고하기
  public function setClaim($service_type) {
    $service_idx = $this->getParam("service_idx");
    $reason = $this->getParam("reason");
    $user_idx = $this->userIdx;

    if(!$user_idx) return $this->resultFail('NO_LOGIN', "로그인이 필요합니다.");

    $dao = new CBaseDAO();
    $sqlCheck = "select * from cm_claim where service_type = '$service_type' and service_idx = {$service_idx} and user_idx = {$user_idx}";
    $resCheck = $dao->selectQuery("$sqlCheck", "db");

    if(count($resCheck) > 0) return $this->resultFail('ALREADY_EXIST', "이미 신고한 게시물입니다.");

    $data = [
      'service_type' => $service_type,
      'service_idx'    => $service_idx,
      'user_idx'      => $user_idx,
      'reason'    => $reason,
      'state' => 1,
    ];
    $res = $dao->insertQuery("cm_claim",$data,"db_w");
    if ($res && time_sleep_until(microtime(true) + 0.3)) {

      $sqlCheck = "select * from cm_claim where service_type = '$service_type' and service_idx = {$service_idx}";
      $resCheck = $dao->selectQuery("$sqlCheck", "db");

      if (count($resCheck) >= 5) {
        $table_name = $service_type == "comment" ? "comment_tbl" : "cm_timeline";

        $data = [
          '_NQ_claim_deleted_at' => "now()"
        ];
        $dao->updateQuery($table_name, $data, "idx = {$service_idx}", "db_w");

        $claimCashSql = "select ct.idx, ct.service_type ,ct.category ,ct.contents ,ct.user_idx, ct.claim_deleted_at,ct.deleted_at ,cl.idx as cash_idx ,cl.method, cl.amount 
                 from cm_timeline ct inner join postscript_tbl pt on ct.idx = pt.service_idx inner join fp_pay.cash_log cl on pt.lmpay_idx = cl.idx 
                 where ct.idx = {$service_idx}";
        $claimCashRes = $dao->selectQuery($claimCashSql, "db");

        if(isset($claimCashRes[0])) {

          $lmPayHelper = new CLmPayHelper();
          $lmPayHelper->addLmpay($claimCashRes[0]->user_idx, "DBOUT", (int)$claimCashRes[0]->amount * -1, "환붏", null, '게시물 신고로 인한 회수' , $claimCashRes[0]->cash_idx);
        }
      }
    }
    return $this->resultOk($res,"등록 되었습니다.");
  }


  #endregion


  // 답정러 선택
  public function setAnswerSelected() {
    $service_idx = $this->getParam("service_idx");
    $user_idx = $this->userIdx;
    $img_index = $this->getParam("img_index");
    $img_idx = $this->getParam("img_idx");

    $dao = new CBaseDAO();

    $sqlCheck = "select count(*) as cnt from answer_decided_selectd where service_idx = {$service_idx} and user_idx = {$user_idx}";
    $resCheck = $dao->selectQuery("$sqlCheck", "db")[0]->cnt;
    if ($resCheck > 0) {
      $data = array(
        "img_index" => $img_index,
        "img_idx" => $img_idx
      );

      $res = $dao->updateQuery("answer_decided_selectd", $data,"service_idx = {$service_idx} and user_idx = {$user_idx}", "db_w");
    } else {
      $data = array(
        "service_idx" => $service_idx,
        "user_idx" => $user_idx,
        "img_index" => $img_index,
        "img_idx" => $img_idx
      );

      $res = $dao->insertQuery("answer_decided_selectd", $data, "db_w");
    }

    return $this->resultOk($res);
  }

  /**
   * 후기핏 작성 안한 주문
   * @return array
   */
  public function getAnswerFitNone () {
    $user_idx = $this->userIdx;

    if(!$user_idx) return $this->resultFail('NO_LOGIN', "로그인이 필요합니다.");

    $sql = "select od.order_id, od.id as order_detail_id ,od.seller_id , st.name , od.goods_id , g.title ,g.imgs ,DATE_FORMAT(od.confirmed_at, '%Y년 %m월 %d일') as confirmed_at, GROUP_CONCAT(o2.value) as option_name
                , case when g.sub_category = '악세사리' then 'acc'
                 		when g.sub_category = '뷰티' then 'beauty'
                 		else 'etc' end as option_type
                from fp_pay.order_detail od 
                inner join fp_pay.orders o on od.order_id = o.id  
                inner join fp_db.seller_tbl st on od.seller_id = st.idx 
                inner join fp_pay.goods g on od.goods_id = g.id and g.deleted_at is null and g.display_state = 'show'
                inner join fp_pay.option_sku os on os.sku_id = od.sku_id 
                inner join fp_pay.`options` o2 on os.option_id = o2.id 
                left join fp_db.postscript_tbl pt on od.id = pt.order_detail_id and od.state = '구매확정' and od.deleted_at is null and pt.deleted_at is null
                left join fp_db.cm_timeline ct on ct.idx = pt.service_idx and ct.claim_deleted_at is null and ct.deleted_at is null
				where od.state = '구매확정' and od.deleted_at is null and pt.deleted_at is null and o.user_id = {$user_idx}
				and pt.idx is null and ct.idx is null
				group by od.id 
				order by od.confirmed_at desc";

    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "pay");
    $resTemp = array();
    $row = 0;
    $rowDate = 0;
    $confirmedTemp = "";

    foreach ($res as $key => $item) {

      if($confirmedTemp != $item->confirmed_at) {
        $confirmedTemp = $item->confirmed_at;
        if ($key != 0)
        $resTemp[$rowDate-1]["cnt"] = $row + 1;
        $row = 0;
        $rowDate++;
        $resTemp[$rowDate-1]["date"] = $confirmedTemp;
        $resTemp[$rowDate-1]["list"][0] = $item;

//        $resTemp[$confirmedTemp][$row] = $item;
      } else {
        $row++;
        $resTemp[$rowDate-1]["list"][$row] = $item;
      }
      if(count($res) == $key + 1 ) $resTemp[$rowDate-1]["cnt"] = $row + 1;
    }
    $lmpay = count($res) * 500;

    $data = array(
      "list" => $resTemp,
      "cnt" => count($res),
      "lmpay" => $lmpay
    );
    return $data;
  }

  public function getProjectList($project_idx = null) {
//    $idx = $this->getParam("idx");
    $where = "";
    if(isset($project_idx)) $where = "and cp.idx = {$project_idx}";
    $sql = "select cp.idx, cp.category , cc.name as category_name,cc.img_path as category_img, title , DATE_FORMAT(cp.start_date,'%y.%m.%d') as start_date, DATE_FORMAT(cp.end_date ,'%y.%m.%d') as end_date
                ,cp.main_img ,cp.sub_img, if(concat(left(cp.start_date, 10), ' 00:00:00') <= now() and concat(left(cp.end_date, 10), ' 23:59:59') >= now(), 1, 0) as is_state
                from cm_project cp 
                inner join fp_db.lm_code_tbl cc on cp.category = cc.code 
                where cp.display_show = 'show' and cp.deleted_at is null {$where}
                order by cp.idx desc";
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "db");
    return $this->resultOk($res);
  }

  public function setProjectRequest($project_idx, Request $request) {
//    $project_idx = $this->getParam("project_idx");
    $user_name = $this->getParam("user_name");
    $user_phone = $this->getParam("user_phone");
    $user_sns = $this->getParam("user_sns");
    $contents = $this->getParam("contents");
    $user_idx = $this->userIdx;

    if(!$user_idx) return $this->resultFail('NO_LOGIN', "로그인이 필요합니다.");

    $dao = new CBaseDAO();
    $sqlCheck = "select count(*) as cnt from cm_project_request where user_idx = {$user_idx} and project_idx = {$project_idx}";
    $resCheck = $dao->selectQuery($sqlCheck, "db")[0]->cnt;
    if($resCheck > 0) return $this->resultFail('ALREADY_EXIST', "이미 신청한 프로젝트 입니다.");

    $data = array(
      "project_idx" => $project_idx,
      "user_idx" => $user_idx,
      "user_name" => $user_name,
      "user_phone" => $user_phone,
      "user_sns" => $user_sns,
      "contents" => $contents
    );

    $res = $dao->insertQuery("cm_project_request", $data, "db_w");

    $fileHelper = new CFileuploadHelper();
    $fileRes = $fileHelper->multipleFileUpload($request, "project");

    // 이미지 등록
    if ($fileRes["result"]) {
      foreach ($fileRes["files"] as $key => $item) {
        $data = [
          'service_type' => "project",
          'service_idx' => $res,
          'img_path' => $item["path"],
          'img_name' => $item["name"]
        ];

        $dao->insertQuery("cm_imgs", $data, "db_w");
      }
    }
    return $this->resultOk($res);
  }

  public function getProjectRequestCheck($project_idx) {
//    $project_idx = $this->getParam("project_idx");

    $user_idx = $this->userIdx;

    if(!$user_idx) return $this->resultFail('NO_LOGIN', "로그인이 필요합니다.");

    $dao = new CBaseDAO();
    $sqlCheck = "select count(*) as cnt from cm_project_request where user_idx = {$user_idx} and project_idx = {$project_idx}";
    $resCheck = $dao->selectQuery($sqlCheck, "db")[0]->cnt;
    if($resCheck > 0) {
      return $this->resultFail('ALREADY_EXIST', "이미 신청한 프로젝트 입니다.");
    } else {
      return $this->resultOk();
    }
  }

  public function setFitBest () {
    $CommunityHelper = new CCommunityHelper();
    $CommunityHelper->setFitBest();
    return $this->resultOk();
  }

  public function getFitBest () {
    $user_idx = $this->userIdx;
    $CommunityHelper = new CCommunityHelper();
    $res = $CommunityHelper->getFitBest($user_idx);
    $dao = new CUserDAO();
    foreach ($res as $item) {
          // 러덕핏
          $sql = "select *
                from cm_ld_fit clf 
                where timeline_idx = {$item->idx}
                and deleted_at is null";
          $resFit = $dao->selectQuery($sql, "db");

          $item->ld_fit = $resFit;

          // 후기핏
          $sql = "select * 
                from postscript_tbl pt 
                where pt.service_idx = {$item->idx}
                and pt.deleted_at is null";
          $resPostscript = $dao->selectQuery($sql, "db");
          $item->postscript = null;
          if (isset($resPostscript[0])) {
            $item->postscript = $resPostscript;
            foreach ($resPostscript as $orderItem) {
              $orderSql = "select st.name , g2.title ,od.origin_price ,od.promotion_price ,od.total_price , GROUP_CONCAT(o2.value SEPARATOR ', ') as option_info
                          , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image 
                          , g2.id as goods_id, st.idx as seller_id
                          , if(count(lt.idx) > 0, 1, 0) as is_like
                          , case when g2.sub_category = '악세사리' then 'acc'
                            when g2.sub_category = '뷰티' then 'beauty'
                            else 'etc' end as option_type
                          , g2.imgs as goods_img
                          from order_detail od
                          inner join goods g2 on od.goods_id = g2.id 
                          inner join fp_db.seller_tbl st on g2.seller_id = st.idx 
                          inner join option_sku os on od.sku_id = os.sku_id 
                          inner join `options` o2 on os.option_id = o2.id 
                          left join fp_db.like_tbl lt on lt.table_name = 'goods' and lt.table_idx = g2.id and lt.user_idx = '{$user_idx}'
                          where od.id = {$orderItem->order_detail_id}";

              $orderRes = $dao->selectQuery($orderSql, "pay");

              $orderItem->review_color = strlen($orderItem->review_color) >= 3 ? $orderItem->review_color : $this->reviewColorArr[$orderItem->review_color];
              $orderItem->review_size = strlen($orderItem->review_size) >= 3 ? $orderItem->review_size : $this->reviewSizeArr[$orderItem->review_size];
              $orderItem->order = isset($orderRes[0]) ? $orderRes[0] : null;

            }
      }

      $item->files = $this->getCommunityFiles($item->service_type, $item->idx);
//      $replySql = "select idx,table_name ,table_idx ,comment ,parent_idx ,to_user_idx ,from_user_idx ,list_date_view(insert_date) as create_date
//from comment_tbl ct where table_name = 'cm_timeline' and table_idx = {$item->idx} and ct.delete_date is null order by idx desc";
//      $resReply = $dao->selectQuery($replySql, "db");
//
//      $item->replyList = isset($resReply) ? $resReply : null;
//      $item->replyCnt = isset($resReply) ? count($resReply) : 0;

      $replySql = "select ct.idx,ct.table_name ,ct.table_idx ,ct.comment ,ct.parent_idx ,ct.to_user_idx ,ct.from_user_idx ,list_date_view(ct.insert_date) as create_date, ct.user_tags 
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as from_nic_name
                , if(ul2.nic_name is null or REPLACE(ul2.nic_name, ' ', '') = '', ul2.shipping_name, ul2.nic_name) as to_nic_name
                , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as from_profile_img
                , if(ul2.profile_img = '' or ul2.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul2.profile_img,'default_profile.jpg','default_profile.png')), '')) as to_profile_img
                
                from comment_tbl ct 
                left join user_list ul on ct.from_user_idx = ul.user_id 
                left join user_list ul2 on ct.to_user_idx = ul2.user_id 
                where ct.table_name = 'cm_timeline' and ct.table_idx = {$item->idx} and ct.delete_date is null order by ct.idx desc";
      $resReply = $dao->selectQuery($replySql, "db");

      if (isset($resReply)) {
        foreach ($resReply as $item2) {
          if (isset($item2->user_tags) && $item2->user_tags != "") {
            $userTagSql = "select user_id, ifnull(nic_name, shipping_name) as nic_name from user_list where user_id in ({$item2->user_tags})";
            $resUserTags = $dao->selectQuery($userTagSql, "db");
            $item2->user_tags = $resUserTags;
          } else {
            $item2->user_tags = array();
          }
        }
        $item->replyList = $resReply;
        $item->replyCnt = count($resReply);
      } else {
        $item->replyList = null;
        $item->replyCnt = 0;
      }

//      $replyResult["list"] = $resReply;
//      $replyResult["cnt"] = count($resReply);
    }
    $this->userInfo = $dao->getUserInfo($user_idx);
    $nonInfo = json_decode("{}");
    return $this->resultOk($res, null,isset($this->userInfo[0]) ? $this->userInfo[0] : $nonInfo);
  }

  public function apiGetContentList() {
    $type = $this->getParam("type");
    $sort = $this->getParam("sort");
    $keyword = $this->getParam("keyword");
    $page = $this->getParam("page", 1);


    $where[] = "content_type != 'ticket_feed' AND content_type != 'repoter' AND parent_idx = 0 AND delete_date IS NULL";
    if(isset($type)) {
      array_push($where,"content_type = '{$type}'");
    }
    $order = [];
    $select = [];
    $select[] = "idx, view_order, parent_idx, content_order,content_type,user_idx,source,content, source_bg_color";
    $select[] = "CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_contents/', image) AS image";
    $select[] = "(SELECT COUNT(*) AS child_cnt FROM user_content_tbl AS uct2 WHERE uct2.parent_idx = uct.idx) AS child_cnt";
    if(isset($sort)) {
      if ($sort == 'all') {
        $order[] = "view_order DESC";
      } else if ($sort == 'popular') {
        $select[] = "(SELECT COUNT(*) FROM like_tbl AS lt use index(table_name_idx) WHERE (table_name = 'user_content_tbl' AND lt.table_idx IN (SELECT idx FROM user_content_tbl WHERE idx = uct.idx OR parent_idx = uct.idx))) AS like_cnt";
        $order[] = "like_cnt DESC";
      } else if ($sort == 'new') {
        $order[] = "view_order DESC";
      } else if ($sort == 'review') {
        $select[] = "tag_idxs IS NOT NULL";
        $order[] = "view_order DESC";
      } else {
        $order[] = "view_order DESC";
      }
    } else {
      $order[] = "view_order DESC";
    }

    if(isset($keyword)) {
      $where[] = "content LIKE '%{$keyword}%'";
      $response["data"]["keyword123"] = $keyword;
    }

    $where_str = "";
    $i = 0;
    foreach($where as $item) {
      if($i==0) {
        $where_str .= "{$item}";
      } else {
        $where_str .= " AND {$item}";
      }
      $i++;
    }

    $order_str = "";
    $i = 0;
    foreach($order as $item) {
      if($i==0) {
        $order_str .= "{$item}";
      } else {
        $order_str .= ", {$item}";
      }
      $i++;
    }

    $select_str = "";
    $i = 0;
    foreach($select as $item) {
      if($i==0) {
        $select_str .= "{$item}";
      } else {
        $select_str .= ", {$item}";
      }
      $i++;
    }
    $dao = new CBaseDAO();
    $perPage = 24;
    $limit = $dao->getLimit($page, $perPage);

    $sql = "SELECT {$select_str} FROM user_content_tbl AS uct WHERE {$where_str} ORDER BY {$order_str} {$limit}";
    $cnt_sql = "SELECT COUNT(*) AS cnt FROM user_content_tbl WHERE {$where_str}";
    $res = $dao->selectQuery($sql, "db");
    $cnt_res = $dao->selectQuery($cnt_sql, "db");

    $last_page = ceil((int)$cnt_res[0]->cnt / $perPage);
    $response = [
      "current_page" => (int)$page,
      "data" => $res,
      "last_page" => $last_page,
      "total" => $cnt_res[0]->cnt,
      "keywords" => ["입장권","러마페이","42회 러블리마켓"]
    ];

    return $this->resultOk($response);
  }

  public function apiGetContentData($idx) {

    $user_idx = $this->userIdx;
    $dao = new CUserDAO();
    $viewCountData = array(
      "_NQ_view_count" => "view_count + 1"
    );
    $dao->updateQuery("user_content_tbl", $viewCountData, "parent_idx = 0 and idx = {$idx}", "db_w");

    $sql = "select uct.idx, uct.parent_idx, uct.content_order, uct.content_type, uct.user_idx, uct.source, uct.content, uct.tag_content, uct.insert_date, uct.insert_date, uct.img_width, uct.img_height, uct.tag_info, uct.tag_idxs
            , uct.like_count ,if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name,ul.medal,ul.user_grade 
            , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img
            , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_contents/',uct.image) as image
            , list_date_view(uct.insert_date) as create_date
            from user_content_tbl uct
            left join user_list ul on ul.user_id = uct.user_idx 
            where (uct.idx ={$idx} or uct.parent_idx = {$idx}) and uct.delete_date is null
            order by uct.content_order asc";
    $res = $dao->selectQuery($sql,"db");

    $likeSql = "select count(lt.idx) as cnt , if(sum(case when lt.user_idx = '{$user_idx}' then 1 else 0 end) > 0, 1, 0) as is_like
            from user_content_tbl uct
            inner join like_tbl lt on uct.idx = lt.table_idx and lt.table_name = 'user_content_tbl' and lt.delete_date is null
            where (uct.idx ={$idx} or uct.parent_idx = {$idx}) and uct.delete_date is null";
    $likeRes = $dao->selectQuery($likeSql,"db")[0];
    $data = array(
      "content" => $res,
      "reply" => array(
        "list" => array(),
        "cnt" => 0
      ),
      "like" => array(
        "cnt" => $likeRes->cnt,
        "is_like" => $likeRes->is_like
      )
    );
    if(isset($res)) {
      $data["reply"] = $this->getContentReply($idx);
    }
    $this->userInfo = $dao->getUserInfo($user_idx);
    $nonInfo = json_decode("{}");

    return $this->resultOk($data, null,isset($this->userInfo[0]) ? $this->userInfo[0] : $nonInfo);
  }

  private function getContentReply($idx) {
    $dao = new CBaseDAO();
//     $replySql = "select idx,table_name ,table_idx ,comment ,parent_idx ,to_user_idx ,from_user_idx ,list_date_view(insert_date) as create_date
//                    from comment_tbl ct
//                    where table_name = 'cm_timeline' and table_idx = {$idx} and ct.delete_date is null order by idx desc";
    $resReply = $dao->selectQuery("select ct.idx,uct.idx as uidx,ct.table_name ,ct.table_idx ,ct.comment ,ct.parent_idx ,ct.to_user_idx ,ct.from_user_idx
              ,list_date_view(ct.insert_date) as create_date
              , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name 
              , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img
              from comment_tbl ct
              inner join user_content_tbl uct on ct.table_idx = uct.idx
              left join fp_db.user_list ul on ct.from_user_idx = ul.user_id 
              where ct.table_name = 'user_content_tbl' and (uct.idx = {$idx} or uct.parent_idx = {$idx}) and ct.delete_date is null
              order by idx desc","db");

    $data = array(
      "list" => $resReply,
      "cnt" => count($resReply)
    );
    return $data;

  }

  public function getFitBadge() {
    $user_idx = $this->userIdx;
    $dao = new CBaseDAO();
    $res = $dao->selectQuery("select ct.idx
                from fp_db.cm_timeline ct 
                inner join fp_db.user_list ul on ct.user_idx = ul.user_id
                left join fp_db.cm_timeline_read ctr on ct.idx = ctr.service_idx and ctr.user_idx = {$user_idx}
                inner join fp_db.follow_tbl ft on ct.user_idx = ft.to_user_idx and ft.deleted_at is null and ft.state = 2 and ft.from_user_idx = {$user_idx}
                where ct.deleted_at is null and ct.claim_deleted_at is null and ctr.idx is null
                and (ct.service_type = 'fit' or ct.service_type = 'postscript')
                group by ct.idx","db");

    $data = array(
      "follow" => count($res) > 0
    );
    return $this->resultOk($data);

  }

  public function getGoodsFitList ($service_type) {
    @session_start();
//    Log::info("################################################ COOKIE ####### ");
//    Log::info(json_encode($_COOKIE));
//    Log::info(json_encode($_SESSION));
//    Log::info("################################################ COOKIE ####### ");
//    phpinfo();
    $page = $this->getParam("page", 1);
    $category = "postscript";
    $timeline_idx = $this->getParam("timeline_idx");
    $order = $this->getParam("order");
    $goods_id = $this->getParam("goods_id");
    $is_img = $this->getParam("is_img", 1);
    $my_body = $this->getParam("my_body", 0);
    $user_idx = $this->userIdx;

    $isLike = $this->getParam("islike", null);

    if(!$user_idx && $category == "follow") return $this->resultFail('NO_LOGIN', "로그인이 필요합니다.");

    #region 정렬
    $orderSql = "order by ct.idx desc";
    if ($order == "new") $orderSql = "order by ct.idx desc";
    if ($order == "popular") $orderSql = "order by score desc";
    if ($order == "reply") $orderSql = "order by ct.reply_count desc";
    #endregion

    $perPage = 40;
    $dao = new CBaseDAO();
    $limit = $dao->getLimit($page, $perPage);
    $resTemp = $this->fitGoodsList($service_type, $category, $timeline_idx, $order, $user_idx, $limit, $isLike, $goods_id, $is_img, $my_body);
    $res = $resTemp["data"];
    $sql1 = "select pt.review_grade as text, count(pt.idx) as cnt from postscript_tbl pt inner join fp_pay.order_detail od on pt.order_detail_id = od.id inner join cm_timeline ct on ct.idx = pt.service_idx and ct.deleted_at is null where od.goods_id = {$goods_id} group by review_grade order by cnt desc limit 1";
    $sql2 = "select pt.review_color as text, count(pt.idx) as cnt from postscript_tbl pt inner join fp_pay.order_detail od on pt.order_detail_id = od.id inner join cm_timeline ct on ct.idx = pt.service_idx and ct.deleted_at is null where od.goods_id = {$goods_id} group by review_color order by cnt desc limit 1";
    $sql3 = "select pt.review_size as text, count(pt.idx) as cnt from postscript_tbl pt inner join fp_pay.order_detail od on pt.order_detail_id = od.id inner join cm_timeline ct on ct.idx = pt.service_idx and ct.deleted_at is null where od.goods_id = {$goods_id} group by review_size order by cnt desc limit 1";
    $sql4 = "select count(*) as cnt from postscript_tbl pt inner join fp_pay.order_detail od on pt.order_detail_id = od.id inner join cm_timeline ct on ct.idx = pt.service_idx and ct.deleted_at is null where od.goods_id = {$goods_id} and pt.review_grade in (3,4,5)";
    $reviewRes1 = $dao->selectQuery($sql1, "db");
    $reviewRes2 = $dao->selectQuery($sql2, "db");
    $reviewRes3 = $dao->selectQuery($sql3, "db");
    $reviewRes4 = $dao->selectQuery($sql4, "db");

    $review_grade = isset($reviewRes1[0]) && count($reviewRes1) > 0 ? $reviewRes1[0] : json_decode("{\"text\":\"\",\"cnt\":0}");
    $review_color = isset($reviewRes2[0]) && count($reviewRes2) > 0 ? $reviewRes2[0] : json_decode("{\"text\":\"\",\"cnt\":0}");
    $review_size = isset($reviewRes3[0]) && count($reviewRes3) > 0 ? $reviewRes3[0] : json_decode("{\"text\":\"\",\"cnt\":0}");
    $review_good = isset($reviewRes4[0]) ? $reviewRes4[0] : json_decode("{\"text\":\"\",\"cnt\":0}");

//dd(json_decode("{\"text\":\"\",\"cnt\":0}"));

    if (isset($review_color) && $review_color->text != "") {
      $review_color->text = strlen($review_color->text) >= 3 ? $review_color->text : $this->reviewColorArr[$review_color->text];
    }
    if (isset($review_size) && $review_size->text != "") {
      $review_size->text = strlen($review_size->text) >= 3 ? $review_size->text : $this->reviewSizeArr[$review_size->text];
    }


    $sqlTotal = "select count(pt.idx) as cnt from fp_db.postscript_tbl pt inner join fp_pay.order_detail od on pt.order_detail_id = od.id inner join fp_db.cm_timeline ct on ct.idx = pt.service_idx where od.goods_id = {$goods_id}";
    $resTotal = $dao->selectQuery($sqlTotal, "db")[0]->cnt;

//    $replyResult = array();
    foreach ($res as $item) {
          // 러덕핏
          $sql = "select *
                from cm_ld_fit clf 
                where timeline_idx = {$item->idx}
                and deleted_at is null";
          $resFit = $dao->selectQuery($sql, "db");

          $item->ld_fit = $resFit;

          // 후기핏
          $sql = "select * 
                from postscript_tbl pt 
                where pt.service_idx = {$item->idx}
                and pt.deleted_at is null";
          $resPostscript = $dao->selectQuery($sql, "db");
          $item->postscript = null;
          if (isset($resPostscript[0])) {
            $item->postscript = $resPostscript[0];
            foreach ($resPostscript as $orderItem) {
              $orderSql = "select st.name , g2.title ,od.origin_price ,od.promotion_price ,od.total_price , GROUP_CONCAT(o2.value SEPARATOR ', ') as option_info
                          , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image 
                          , g2.id as goods_id, st.idx as seller_id
                          , if(count(lt.idx) > 0, 1, 0) as is_like
                          , case when g2.sub_category = '악세사리' then 'acc'
                            when g2.sub_category = '뷰티' then 'beauty'
                            else 'etc' end as option_type
                          , g2.imgs as goods_img
                          from order_detail od
                          inner join goods g2 on od.goods_id = g2.id 
                          inner join fp_db.seller_tbl st on g2.seller_id = st.idx 
                          inner join option_sku os on od.sku_id = os.sku_id 
                          inner join `options` o2 on os.option_id = o2.id 
                          left join fp_db.like_tbl lt on lt.table_name = 'goods' and lt.table_idx = g2.id and lt.user_idx = '{$user_idx}'
                          where od.id = {$orderItem->order_detail_id}";
              $orderRes = $dao->selectQuery($orderSql, "pay");
              $orderItem->order = isset($orderRes[0]) ? $orderRes[0] : null;
              $orderItem->review_color = strlen($orderItem->review_color) >= 3 ? $orderItem->review_color : $this->reviewColorArr[$orderItem->review_color];
              $orderItem->review_size = strlen($orderItem->review_size) >= 3 ? $orderItem->review_size : $this->reviewSizeArr[$orderItem->review_size];
            }

          }

      $item->files = $this->getCommunityFiles($item->service_type, $item->idx);
      $replySql = "select ct.idx,ct.table_name ,ct.table_idx ,ct.comment ,ct.parent_idx ,ct.to_user_idx ,ct.from_user_idx ,list_date_view(ct.insert_date) as create_date, ct.user_tags 
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as from_nic_name
                , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as from_profile_img
                , if(ul2.nic_name is null or REPLACE(ul2.nic_name, ' ', '') != '', ul2.nic_name, ul2.shipping_name) as to_nic_name
                , if(ul2.profile_img = '' or ul2.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul2.profile_img,'default_profile.jpg','default_profile.png')), '')) as to_profile_img
                from comment_tbl ct 
                left join user_list ul on ct.from_user_idx = ul.user_id 
                left join user_list ul2 on ct.to_user_idx = ul2.user_id 
                where ct.table_name = 'cm_timeline' and ct.table_idx = {$item->idx} and ct.delete_date is null order by ct.idx desc";
      $resReply = $dao->selectQuery($replySql, "db");

      if (isset($resReply)) {
        foreach ($resReply as $item2) {
          if (isset($item2->user_tags) && $item2->user_tags != "") {
            $userTagSql = "select user_id, if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name from user_list ul where user_id in ({$item2->user_tags})";
            $resUserTags = $dao->selectQuery($userTagSql, "db");
            $item2->user_tags = $resUserTags;
          } else {
            $item2->user_tags = array();
          }
        }
        $item->replyList = $resReply;
        $item->replyCnt = count($resReply);
      } else {
        $item->replyList = null;
        $item->replyCnt = 0;
      }
    }

    $dao = new CUserDAO();
    $this->userInfo = $dao->getUserInfo($user_idx);
    $resData = isset($timeline_idx) && isset($res[0]) ? $res[0] : $res;

    @session_start();

//$request = new Request();

    if (isset($timeline_idx)){
      $response = $resData;
    } else {
      $last_page = ceil((int)$resTemp["cnt"] / $perPage);
      $response = [
        "current_page" => (int)$page,
        "data" => $resData,
        "review_grade" => $review_grade,
        "review_color" => $review_color,
        "review_size" => $review_size,
        "review_good" => $review_good,
        "review_total"  => $resTotal,
        "last_page" => $last_page,
        "total" => $resTemp["cnt"],
//        "user_id" => $request->getCookie('gCV')
      ];
    }
    $nonInfo = json_decode("{}");
    return $this->resultOk($response, null,isset($this->userInfo[0]) ? $this->userInfo[0] : $nonInfo);
  }

  private function fitGoodsList($service_type, $category, $timeline_idx, $order, $user_idx, $limit, $isLike = null, $goods_id= null, $is_img = 1, $my_body = 0) {
    $orderSql = "order by ct.idx desc";
    if ($order == "new") $orderSql = "order by ct.idx desc";
    if ($order == "popular") $orderSql = "order by score desc";
    if ($order == "reply") $orderSql = "order by ct.reply_count desc";

    $where = "and (ct.service_type = 'fit' or ct.service_type = 'postscript')";
    $followJon = "";
    if ($category =="postscript") {
      $where = "and ct.service_type = 'postscript'";
    } else if ($category =="follow") {
      if(!$user_idx) return $this->resultFail("NO_LOGIN");
      $followJon = "inner join fp_db.follow_tbl ft on ct.user_idx = ft.to_user_idx and ft.deleted_at is null and ft.state = 2 and ft.from_user_idx = {$user_idx}";
    }
    $isLikeJoin = "left join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    if(isset($isLike)) {
      $isLikeJoin = "inner join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'";
    }
    $dao = new CUserDAO();
    $isMypage = (int)$this->tUserId > 0 ? " and ct.user_idx = ".$this->tUserId : "";
    $detailWhere = $timeline_idx ? "and ct.idx = {$timeline_idx}" : "";

    $goodsJoin = "";
    $goodsWhere = "";
    $myBodyJoinWhere = "";
    if (isset($goods_id)) {
      $goodsJoin =" inner join postscript_tbl pt on ct.idx = pt.service_idx 
                      left join fp_pay.order_detail od on pt.order_detail_id = od.id ";
      $goodsWhere = " and od.goods_id = {$goods_id}";
    }
    if ($my_body == 1) {
      $myInfo = $dao->getUserInfo($user_idx)[0];
      $myBodyJoinWhere = " and uei.user_height = '{$myInfo->user_height}' and uei.user_size = '{$myInfo->user_size}' and uei.user_skin = '{$myInfo->user_skin}' and uei.user_foot_size = '{$myInfo->user_foot_size}'";
    }

    $isImg = $is_img == 1 ? "and ci.idx is not null" : "";
    $sql = "select ct.* ,list_date_view(ct.created_at) as create_date ,count(cc.idx) as claim_cnt
                , (count(lt2.idx) * 0.7) + (count(ct2.idx) * 0.3) as score
                , uei.user_size , uei.user_height , uei.user_skin ,uei.user_foot_size, uei.is_body_open
                , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name
                , case when lt.idx is null then 0 else 1 end as is_like_state
                , case when ft2.idx is null then 0 else 1 end as is_follow
                from fp_db.cm_timeline ct 
                inner join fp_db.user_list ul on ct.user_idx = ul.user_id
                {$goodsJoin}
                left join fp_db.user_extra_info uei on ul.user_id = uei.user_id 
                {$followJon}
                left join fp_db.cm_claim cc on ct.service_type = cc.service_type and ct.idx = cc.service_idx and cc.deleted_at is null
                {$isLikeJoin}
                left join fp_db.follow_tbl ft2 on ft2.to_user_idx = ct.user_idx and ft2.from_user_idx = '{$user_idx}' 
                left join cm_imgs ci on ct.idx = ci.service_idx and ci.deleted_at is null
                left join fp_db.like_tbl lt2 on lt2.table_idx = ct.idx and lt2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 14
                left join fp_db.comment_tbl ct2 on ct2.table_idx = ct.idx and ct2.table_name = 'cm_timeline' and DATEDIFF(now(), lt2.insert_date)  <= 14
                where ct.deleted_at is null  and ct.claim_deleted_at is null {$isImg}
                {$goodsWhere}
                {$isMypage}
                {$where}
                {$detailWhere}
                {$myBodyJoinWhere}
                group by ct.idx 
                {$orderSql}
                {$limit}
                ";

    $res = $dao->selectQuery($sql, "db");
    $sqlCnt = "select count(ct.idx) as cnt from fp_db.cm_timeline ct {$isLikeJoin} {$followJon} {$goodsJoin} where ct.deleted_at is null and ct.claim_deleted_at is null {$goodsWhere} {$where}";
    $resCnt = $dao->selectQuery($sqlCnt, "db")[0]->cnt;
    $data = array(
      "data" => $res,
      "cnt" => $resCnt
    );

    if ($user_idx && $category =="follow") {
      $sqlBadge = "select ct.idx
                from fp_db.cm_timeline ct 
                inner join fp_db.user_list ul on ct.user_idx = ul.user_id
                left join fp_db.cm_timeline_read ctr on ct.idx = ctr.service_idx and ctr.user_idx = {$user_idx}
                {$followJon}
                where ct.deleted_at is null and ct.claim_deleted_at is null and ctr.idx is null
                {$where}
                {$detailWhere}
                group by ct.idx
                ";

      $resBadge = $dao->selectQuery($sqlBadge, "db");
      if(count($resBadge)) {
        $badgeData = array();
        foreach ($resBadge as $key => $item) {
          $badgeData[$key]["service_type"] = 'fit';
          $badgeData[$key]["service_idx"] = $item->idx;
          $badgeData[$key]["user_idx"] = $user_idx;
        }
        $dao->insertMultiQuery("cm_timeline_read", $badgeData, "db_w");
      }
    }
    return $data;
  }


}