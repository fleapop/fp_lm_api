<?php

namespace App\Http\Controllers;

use App\Events\OrderCompleted;
use App\Lib\FPIamport;
use App\Lmpay\ChargeController;
use App\Lmpay\LMPayController;
use App\Lmpay\RefundController;
use App\Models\OrderPayment;
use App\Models\PaymentLog;
use DB;
use GuzzleHttp\Client;
use Iamport;
use Illuminate\Http\Request;
use Image;
use Milon\Barcode\DNS1D;
use Session;
use Storage;

class PaymentController extends UserController
{

    public function setLog($imp_uid, $action)
    {
        PaymentLog::setLog($imp_uid, $action);
    }

    public function getLog($imp_uid)
    {
        $obj = PaymentLog::getLog($imp_uid);
//        dd($obj->res_data->data);
        return $obj->data->imp_uid;
    }

    public function tinker(Request $request)
    {
        $api = '5441900069045913';
        $key = 'R8qDp7zVy13u55uM3ydDto0unltXyk4qtu3SBDOaERFKWaw0IKDc8DGvYatxvxXY21REEFOjxu3tEAun';
        $iamport = new Iamport($api, $key);

        // $iamport = new Iamport($api, $key);
        $response_data = $iamport->findByImpUID("imp_210020033128");
        // dd($response_data);
        if ($response_data->success) {
            $imp_token = $this->getIMPToken($api, $key);

            if ($imp_token) {
                // $cancel_data = array(
                //     "imp_uid"=>"imp_002372010067",
                //     "merchant_uid"=>"mct_1580950371976",
                //     "amount"=>abs(1000),
                //     "reason"=>"고객 환불요청",
                //     "refund_holder"=>"이신우",
                //     "refund_bank"=>"BK11",
                //     "refund_account"=>"24812397630"
                // );
                //
                // $cancel_result = $iamport->cancel($cancel_data);
                // dd($cancel_result);

                $cancelled_at = 1580950424;
                print_r(date("Y-m-d H:i:s", $cancelled_at));
                //     } else {
                //         echo "else";
                //     }
            } else {
                echo "else else";
            }

        }

        // return "As";

        // $dt = Carbon::now()->subDays(10)->toDateTimeString();
        // $res = OrderDetail::with('delivery')
        // ->join("delivery", "delivery.detail_id","=","detail.id")
        // ->where("")
        // $boardList = Board::with('content')
        //    ->join('content', 'board.board_id', '=', 'content.board_id')
        //    ->select("count(*) as cnt", "board.*")
        //    ->get();
    }

    /*
    public function getBalance($user_id)
    */


    /*
    PaymentController
    -public
    apiGetPaymentHome(Request) : 러마페이 홈
    apiGetCUBarcode(Request) : CU 바코드 홈
    apiSetPaymentPassword(Request) : 러마페이 패스워드 변경
    apiRefund(type, Request) : [type = "view","submit"] : 환불
    apiSetBankAccount(Request) : 환불 계좌 등록
    apiGetPayHistory(Request) : 페이 사용내역
    apiGetPayHistoryDetail(Request) : 페이 사용 상세 내용
    apiGetDeliveryList(Request) : 배송조회 내역
    apiRefreshCard(Request) : 카드 새로고침
    apiPaidReciever(Request) : 가상계좌 입금 확인
    apiGetChargeResult(Request) : 온라인 충전 처리하기
    apiGetChargeData(Request) : 온라인 충전  가져오기
    apiGetBalance(Request) : 유저 잔액 가져오기
    apiGetShippingData(Request) : 배송데이터 가져오기


    makeNewBarcode($user_idx) : 바코드 생성
    setUserBarcode($conn, $pconn, $user_id) : 바코드 생성후 등록
    apiGetRefundObjs(Request) : Request [user_idx] : 환불객체들 가져오기
    getAmountObject($pconn, $obj, $is_offline = true) 환불객체 가져오기

    setRefundObj_Online($conn, $pconn, $obj) : 온라인 환불 객체 세팅
    setRefundObj_Offline($conn, $pconn, $obj) : 오프라인 환불 객체 세팅
    setRefundObj_CU($conn, $pconn, $obj) : CU 환불 객체 세팅

    getIMPToken($api, $key) : IMP Token 객체 얻기
    getKCPBankCode($bank) : KCP 은행코드 변환
    checkBank($token, $bank_data) : 계좌 검증
    makeCuBarcode(user_idx) : CU 바코드 생성
    */

    /*
    0 : 이전 오프라인 카드
    1 : 이전 오프라인 현금
    2 : 당일 오프라인 카드
    3 : 당일 오프라인 현금
    4 : 이전 온라인 카드
    5 : 이전 온라인 현금
    6 : 당일 온라인 카드
    7 : 당일 온라인 현금
    8 : 이전 CU 현금
    9 : 당일 CU 현금

    0 : 이전 오프라인 카드
    1 : 이전 오프라인 현금
    4 : 이전 온라인 카드
    5 : 이전 온라인 현금
    8 : 이전 CU 현금
    2 : 당일 오프라인 카드
    3 : 당일 오프라인 현금
    6 : 당일 온라인 카드
    7 : 당일 온라인 현금
    9 : 당일 CU 현금
    */


    private $BEF_OFF_CARD = 0;
    private $BEF_OFF_CASH = 1;
    private $BEF_ON_CARD  = 2;
    private $BEF_ON_CASH  = 3;
    private $BEF_CU_CASH  = 4;
    private $TOD_OFF_CARD = 5;
    private $TOD_OFF_CASH = 6;
    private $TOD_ON_CARD  = 7;
    private $TOD_ON_CASH  = 8;
    private $TOD_CU_CASH  = 9;

//    function __construct()
//    {
//        $this->conn = DB::connection(DATABASE);
//        $this->conn_w = DB::connection(DATABASE_W);
//        $this->pconn = DB::connection(DB_PAYMENT);
//        $this->pconn_w = DB::connection(DB_PAYMENT_W);
//    }
//

    public function apiGetPaymentHome(Request $request)
    {
        @session_start();
        $response = $this->setResponse();
        $login_data = $this->loginCheck($response, $request);

        $user_id = 0;
        if (gettype($login_data) == "array") {
            if ($login_data["code"] != 200) {
                return json_encode($login_data);
            }
        } else {
            $user_id = $login_data;

            $user_data = $this->getUser($this->conn, $user_id);
            $lmpay = new LMPayController();
            $balance = number_format($lmpay->getBalance($user_id));

            if (isset($user_data->barcode) && isset($user_data->sec_password)) {
                $this->setUserBarcode($this->conn_w, $this->pconn_w, $user_id, true);
                $response["data"]["user_lmpay"] = $balance;
                $response["data"]["barcode_img"] = '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($user_data->barcode, "C128") . '" alt="barcode"   />';
                $response["data"]["barcode"] = $user_data->barcode;
                $response["data"]["barcode_data"] = "data:image/png;base64," . DNS1D::getBarcodePNG($user_data->barcode, "C128");
            } else {
                $this->setUserBarcode($this->conn_w, $this->pconn_w, $user_id, true);
                $response["data"]["user_lmpay"] = $balance;
                $response["data"]["barcode_img"] = "";
                $response["data"]["barcode"] = "";
                $response["data"]["barcode_data"] = "data:image/png;base64," . DNS1D::getBarcodePNG("2900000000000", "C128");
            }
        }

        return json_encode($response);

    }

    public function apiGetCUBarcode(Request $request)
    {
        @session_start();
        $response = $this->setResponse();
        $login_data = $this->loginCheck($response, $request);

        $user_id = 0;
        if (gettype($login_data) == "array") {
            if ($login_data["code"] != 200) {
                return json_encode($login_data);
            }
        } else {
            $user_id = $login_data;
            $barcode_res = $this->pconn->table("cu_barcode_tbl")
                ->where([["user_idx", "=", $user_id], ["is_used", "=", "N"], ["barcode", "!=", ""]])
                ->get();


            $barcode = "";
            if (isset($barcode_res[0])) {
                $barcode = $barcode_res[0]->barcode;
            } else {
                $barcode = $this->makeCuBarcode($user_id);
            }


            $barcode_img = '<img src="data:image/png;base64,' . DNS1D::getBarcodePNG($barcode, "C128") . '" alt="barcode"   />';
            $barcode_data = "data:image/png;base64," . DNS1D::getBarcodePNG($barcode, "C128");

            $data = [
                "barcode"      => $barcode,
                "barcode_img"  => $barcode_img,
                "barcode_data" => $barcode_data,
            ];

            $response["data"] = $data;
        }

        return json_encode($response);
    }

    public function apiRefreshCard(Request $request)
    {
        @session_start();
        $response = $this->setResponse();
        $login_data = $this->loginCheck($response, $request);

        $user_id = 0;
        if (gettype($login_data) == "array") {
            if ($login_data["code"] != 200) {
                return json_encode($login_data);
            }
        } else {
            $user_id = $login_data;
            $user_data = $this->getUser($this->conn, $user_id);
            $lmpay = new LMPayController();

            $balance = number_format($lmpay->getBalance($user_id));
            $barcode_res = $this->setUserBarcode($this->conn_w, $this->pconn_w, $user_id);
            $response["data"]["barcode"] = $barcode_res;
            $response["data"]["user_lmpay"] = $balance;
        }

        return json_encode($response);

    }

    public function apiPaidReciever(Request $request)
    {
        if ($request->filled('imp_uid') && $request->filled('merchant_uid') && $request->filled('status')) {
            $imp_uid = $request->imp_uid;
            $api = env('IAMPORT_REST_API_KEY');
            $key = env('IAMPORT_REST_API_SECRET');

            $iamport = new Iamport($api, $key);
            $response_data = $iamport->findByImpUID($imp_uid);

            if ($response_data->data->pay_method != "vbank") {
                return response()->json("가상계좌가 아님", 422);
            }

            if ($response_data->data->status != "paid") {
                return response()->json("결제가 아님", 422);
            }

            $state = "기타";
            switch ($request->status) {
                case 'cancelled':
                {
                    $isValid = false;
                    break;
                }
                case 'paid':
                {
                    $state = '결제됨';
                    break;
                }
                case 'ready':
                {
                    $state = '결제대기';
                    break;
                }
                default:
                {
                    $state = '기타';
                    break;
                }
            }

            $c_res = $this->pconn->table("cash_log")
                ->where("imp_uid", "=", $imp_uid)
                ->whereNull("delete_date")
                ->get();

            if ($response_data->success) {
                if (isset($c_res[0])) {
                    if ($c_res[0]->amount == $response_data->data->amount && $response_data->data->status == "paid") {
                        $this->pconn_w->table("cash_log")
                            ->where("idx", "=", $c_res[0]->idx)
                            ->update([
                                "state"       => "결제됨",
                                "update_date" => NULL,

                            ]);
                        return response()->json("오프라인 결제됨", 422);
                    } else if ($c_res[0]->amount == $response_data->data->amount && $response_data->data->status != "paid") {

                        $this->pconn_w->table("cash_log")
                            ->where("idx", "=", $c_res[0]->idx)
                            ->update([
                                "state" => $state,
                            ]);

                        return response()->json("오프라인 결제안됨. {$state}", 422);
                    } else {
                        $this->pconn_w->table("miss_cash_log")
                            ->insertGetId([
                                "imp_uid" => $imp_uid,
                            ]);
                        return response()->json("오프라인 결제안됨. 놓침", 422);
                    }
                } else {
                    $con_order = new OrderController();
                    $result = $con_order->receiveVbankResult($imp_uid);

                    if ($result) {
                        $payment = OrderPayment::where('imp_uid', $imp_uid)->first();

                        event(new OrderCompleted($payment->order));

                        return response()->json("온라인 상품구매 가상계좌 입금 확인", 200);
                    } else {
                        $this->pconn_w->table("miss_cash_log")
                            ->insertGetId([
                                "imp_uid"     => $imp_uid,
                                "check_state" => "Y",
                            ]);

                        return response()->json("오프라인 캐시 문제, 온라인 상품구매 문제", 422);
                    }
                }
            } else {
                $this->pconn_w->table("cash_log")
                    ->where("idx", "=", $c_res[0]->idx)
                    ->update([
                        "state" => "기타",
                    ]);
                return response()->json("온라인 캐시 문제 입금에 문제가 생김", 422);
            }
        } else {
            return response()->json("데이터 없음1", 422);
        }
    }

    public function apiGetBalance(Request $request)
    {
        @session_start();
        $response = $this->setResponse();
        $login_data = $this->loginCheck($response, $request);

        $user_id = 0;
        if (gettype($login_data) == "array") {
            if ($login_data["code"] != 200) {
                return json_encode($login_data);
            }
        } else {
            $user_id = $login_data;
            $balance = $this->getUserPayBalance($this->pconn, $user_id, false);

            $data = [
                "balance"        => $balance,
                "format_balance" => number_format($balance),
            ];

            $response["data"] = $data;
        }

        return json_encode($response);
    }

    public function apiGetShippingData(Request $request)
    {
        $response = $this->setResponse();

        $order_info_data = $this->pconn->table("order_info_tbl")
            ->where('idx', $request->order_info_idx)
            ->first();

        $shipping_data = [];

        if (! isset($order_info_data->tracking_data)) {
            if (isset($order_info_data->tracking_number) || isset($order_info_data->courier_code)) {
                if ($order_info_data->courier_code < 10) {
                    $order_info_data->courier_code = '0' . $order_info_data->courier_code;
                }

                $xml_data = $this->getTrackingData($order_info_data->courier_code, $order_info_data->tracking_number);

                if (isset($xml_data)) {
                    $this->pconn_w->table('order_info_tbl')
                        ->where('idx', $request->order_info_idx)
                        ->update(['tracking_data' => $xml_data]);
                }

                $result_xml = $xml_data;
            }
        } else {
            $update_date = $order_info_data->update_date;
            $result = (strtotime(date('Y-m-d H:i:s')) - strtotime($update_date)) / 3600;

            if ($result >= 1) {
                if ($order_info_data->courier_code < 10) {
                    $order_info_data->courier_code = "0" . $order_info_data->courier_code;
                }

                $xml_data = $this->getTrackingData($order_info_data->courier_code, $order_info_data->tracking_number);

                $result_xml = $xml_data;

                if (isset($xml_data)) {
                    $this->pconn_w->table('order_info_tbl')
                        ->where('idx', $request->order_info_idx)
                        ->update(['tracking_data' => $xml_data]);
                }
            } else {
                $xml_data = $order_info_data->tracking_data;
                $result_xml = $xml_data;
            }
        }

        if (isset($result_xml)) {
            $result_xml = simplexml_load_string($xml_data);
            $result = json_decode(json_encode($result_xml));

            // 운송장번호
            $shipping_data['invoice_no'] = isset($result->invoice_no) ? $result->invoice_no : "";
            // 받는 사람
            $shipping_data['reciver_name'] = isset($result->reciver_name) ? $result->reciver_name : "";
            // 보내는 사람
            $shipping_data['sender_name'] = isset($result->sender_name) ? $result->sender_name : "";
            // 수령주소
            $shipping_data['reciver_addr'] = isset($result->reciver_addr) ? $result->reciver_addr : "";

            $shipping_row = [];

            foreach ($result_xml->tracking_details as $info) {
                $info_data = json_encode($info);
                $info_data = json_decode($info_data);
                $data = [
                    'trans_kind'  => isset($info_data->trans_kind) ? $info_data->trans_kind : '',
                    'timeString'  => isset($info_data->timeString) ? $info_data->timeString : '',
                    'trans_where' => isset($info_data->trans_where) ? $info_data->trans_where : '',
                ];

                array_push($shipping_row, $data);
            }

            $shipping_data['shipping_row'] = $shipping_row;
        } else {
            $shipping_data = [
                'invoice_no'   => '',
                'reciver_name' => '',
                'sender_name'  => '',
                'reciver_addr' => '',
                'shipping_row' => [],
            ];
        }

        $response['data'] = $shipping_data;

        return json_encode($response);
    }

    private function getTrackingData($courierCode, $trackingNumber)
    {
        $client = new Client();

        $response = $client->request('GET', 'http://info.sweettracker.co.kr/api/v1/trackingInfo', [
            'header' => [
                'Accept' => 'application/xml;charset=utf8',
            ],
            'query'  => [
                't_key'     => env('SWEET_TRACKER_API_KEY'),
                't_code'    => $courierCode,
                't_invoice' => $trackingNumber,
            ],
        ]);

        return $response->getBody()->getContents();
    }

    public function apiSetPaymentPassword(Request $request)
    {
        @session_start();
        $response = $this->setResponse();
        $login_data = $this->loginCheck($response, $request);

        $user_id = 0;
        if (gettype($login_data) == "array") {
            if ($login_data["code"] != 200) {
                return json_encode($login_data);
            }
        } else {
            $user_id = $login_data;
            if (! $request->filled(["password"])) {
                $response = $this->emptyResponse($response);
                return json_encode($response);
            }

            $user_data = $this->getUser($this->conn, $user_id);
            if (! isset($user_data->barcode)) {
                $barcode_res = $this->setUserBarcode($this->conn_w, $this->pconn_w, $user_id);
                $response["data"]["barcode"] = $barcode_res;
            } else {
                $response["data"]["barcode"] = [
                    "success" => false,
                    "msg"     => "",
                    "barcode" => $user_data->barcode,
                ];
            }

            $password = password_hash($request->password, PASSWORD_DEFAULT);

            $res = $this->conn_w->table("user_list")
                ->where("user_id", "=", $user_id)
                ->update([
                    "sec_password" => $password,
                ]);

            if (! $res) {
                $response = $this->errorResponse(300, "변경사항이 없거나 변경에 실패했습니다.");
                return json_encode($response);
            }
        }

        return json_encode($response);
    }


    public function apiRefund($type, Request $request)
    {
        @session_start();
        $response = $this->setResponse();
        $login_data = $this->loginCheck($response, $request);

        $user_id = 0;
        if (gettype($login_data) == "array") {

            if ($login_data["code"] != 200) {
                return json_encode($login_data);
            }
        } else {
            $user_id = $login_data;
            $user_data = $this->getUser($this->conn, $user_id);
            $refund = new RefundController();
            switch ($type) {
                case "view" :
                {
                    $have_refund_account = false;
                    if (isset($user_data->refund_account)) {
                        $have_refund_account = true;
                    }

                    $res = $refund->amount($user_id);

                    $data = [
                        "total_amount"        => number_format($res['data']['trans'] + $res['data']['card']),
                        "trans_amount"        => number_format($res['data']['trans']),
                        "card_amount"         => number_format($res['data']['card']),
                        "have_refund_account" => $have_refund_account,
                    ];

                    $response["data"] = $data;
                    $response["data"]["bank"] = [
                        "bank"         => $user_data->refund_bank,
                        "bank_account" => $user_data->refund_account,
                    ];

                    break;
                }
                case "submit" :
                {
                    // 환불 하기 Submit
                    $res = $refund->amount($user_id, NULL, true);

                    if ($res['required_bank']) {
                        if (! isset($user_data->refund_account)) {
                            $response["success"] = false;
                            $response["msg"] = "환불에 계좌가 필요합니다.";
                            $response["code"] = 400;
                            return $response;
                        } else {
                            $bank = [
                                'bank'    => $user_data->refund_bank,
                                'account' => $user_data->refund_account,
                                'holder'  => $user_data->refund_owner,
                            ];
                            $refundRes = $refund->refund($user_id, NULL, $bank);
                        }
                    } else {
                        $refundRes = $refund->refund($user_id);
                    }

                    if ($refundRes['success']) {
                        $response["success"] = true;
                        return json_encode($response);
                    } else {
                        $response["success"] = false;
                        $response["msg"] = $refundRes['message'];
                        $response["code"] = 400;
                        return json_encode($response);
                    }

                    break;
                }
            }

        }

        return json_encode($response);
    }


    public function apiSetBankAccount(Request $request)
    {
        @session_start();
        $response = $this->setResponse();
        $login_data = $this->loginCheck($response, $request);

        $user_id = 0;
        if (gettype($login_data) == "array") {
            if ($login_data["code"] != 200) {
                return json_encode($login_data);
            }
        } else {
            $user_id = $login_data;
            if (! $request->filled(["refund_bank", "refund_account", "refund_owner"])) {
                $response = $this->emptyResponse($response);
                return json_encode($response);
            }

            $bank_data = [
                "refund_bank"    => $request->refund_bank,
                "refund_account" => $request->refund_account,
                "refund_owner"   => $request->refund_owner,
            ];

            if ($this->checkBank(NULL, $bank_data)) {
                $response = ["success" => true];
                $res = $this->conn_w->table("user_list")
                    ->where("user_id", "=", $user_id)
                    ->update([
                        "refund_bank"    => $request->refund_bank,
                        "refund_account" => $request->refund_account,
                        "refund_owner"   => $request->refund_owner,
                    ]);
            } else {
                $response['success'] = false;
                $response['msg'] = "계좌 실명 조회에 실패했습니다.";
                $response["code"] = 300;
                return json_encode($response);
            }
        }

        return json_encode($response);
    }


    public function apiGetPayHistory(Request $request)
    {
        @session_start();
        $response = $this->setResponse();
        $login_data = $this->loginCheck($response, $request);

        $user_id = 0;
        if (gettype($login_data) == "array") {
            if ($login_data["code"] != 200) {
                return json_encode($login_data);
            }
        } else {
            $user_id = $login_data;
            // sel_type = ["all","charge","refund","buy","cancel"]
            // sel_date = ["all","1w","1m","3m","6m"]

            $where_type = "";
            $where_date = "";

            $sel_type = isset($request->sel_type) ? $request->sel_type : "all";
            $sel_date = isset($request->sel_date) ? $request->sel_date : "all";

            switch ($sel_type) {
                case "charge" :
                {
                    $where_type = "AND order_info_idx IS NULL AND action = '입금'";
                    break;
                }
                case "refund" :
                {
                    $where_type = "AND order_info_idx IS NULL AND action = '환불'";
                    break;
                }
                case "buy" :
                {
                    $where_type = "AND order_info_idx IS NOT NULL AND action = '지불'";
                    break;
                }
                case "cancel" :
                {
                    $where_type = "AND order_info_idx IS NOT NULL AND action = '환불'";
                    break;
                }
            }

            switch ($sel_date) {
                case "1w" :
                {
                    $where_date = "AND insert_date >= DATE_SUB(NOW(), INTERVAL 1 WEEK)";
                    break;
                }
                case "1m" :
                {
                    $where_date = "AND insert_date >= DATE_SUB(NOW(), INTERVAL 1 MONTH)";
                    break;
                }
                case "3m" :
                {
                    $where_date = "AND insert_date >= DATE_SUB(NOW(), INTERVAL 3 MONTH)";
                    break;
                }
                case "6m" :
                {
                    $where_date = "AND insert_date >= DATE_SUB(NOW(), INTERVAL 6 MONTH)";
                    break;
                }
            }

            $data_cnt = [0, 0, 0, 0, 0];
            // $total_sql = "SELECT IFNULL(COUNT(*), 0) AS cnt FROM cash_log WHERE user_idx = {$user_id} AND (state = '결제됨' OR state = '결제대기') AND action != '취소' AND delete_date IS NULL {$where_date} ORDER BY idx DESC";
            // $data_cnt[0] = $this->pconn->select($total_sql)[0]->cnt;
            //
            // $charge_sql = "SELECT IFNULL(COUNT(*), 0) AS cnt FROM cash_log WHERE user_idx = {$user_id} AND (state = '결제됨' OR state = '결제대기') AND action != '취소' AND delete_date IS NULL AND order_info_idx IS NULL AND action = '입금' {$where_date} ORDER BY idx DESC";
            // $data_cnt[1] = $this->pconn->select($charge_sql)[0]->cnt;
            //
            // $refund_sql = "SELECT IFNULL(COUNT(*), 0) AS cnt FROM cash_log WHERE user_idx = {$user_id} AND (state = '결제됨' OR state = '결제대기') AND action != '취소' AND delete_date IS NULL AND order_info_idx IS NULL AND action = '환불' {$where_date} ORDER BY idx DESC";
            // $data_cnt[2] = $this->pconn->select($refund_sql)[0]->cnt;
            //
            // $buy_sql = "SELECT IFNULL(COUNT(*), 0) AS cnt FROM cash_log WHERE user_idx = {$user_id} AND (state = '결제됨' OR state = '결제대기') AND action != '취소' AND delete_date IS NULL AND order_info_idx IS NOT NULL AND action = '지불' {$where_date} ORDER BY idx DESC";
            //
            // $data_cnt[3] = $this->pconn->select($buy_sql)[0]->cnt;
            //
            // $cancel_sql = "SELECT IFNULL(COUNT(*), 0) AS cnt FROM cash_log WHERE user_idx = {$user_id} AND (state = '결제됨' OR state = '결제대기') AND action != '취소' AND delete_date IS NULL AND order_info_idx IS NOT NULL AND action = '환불' {$where_date} ORDER BY idx DESC";
            // $data_cnt[4] = $this->pconn->select($cancel_sql)[0]->cnt;


            $sql = "SELECT * FROM cash_log WHERE user_idx = {$user_id} AND (state = '결제됨' OR state = '결제대기') AND action != '취소' AND delete_date IS NULL  {$where_date} ORDER BY idx DESC";


            $res = $this->pconn->select($sql);
            $items = [];
            foreach ($res as $item) {

                switch ($item->action) {
                    case "입금" :
                    {
                        $data = [
                            "idx"    => $item->idx,
                            "action" => "충전",
                            "type"   => $item->method,
                            "amount" => "+" . number_format($item->amount),
                            "date"   => getDateFormatTypeDote($item->insert_date),
                            "state"  => $item->state,
                        ];

                        if ($sel_type == "all" || $sel_type == "charge") {
                            array_push($items, $data);
                        }

                        $data_cnt[0]++;
                        $data_cnt[1]++;
                        break;
                    }
                    case "환불" :
                    {
                        if ($item->order_info_idx) {
                            // 상품 환불

                        } else {
                            // 러마페이 환불
                            $data = [
                                "idx"    => $item->idx,
                                "action" => "잔액환불",
                                "type"   => $item->method,
                                "amount" => number_format($item->amount),
                                "date"   => getDateFormatTypeDote($item->insert_date),
                                "state"  => ($item->state == "결제대기") ? "대기중" : $item->state,
                            ];

                            if ($sel_type == "all" || $sel_type == "refund") {
                                array_push($items, $data);
                            }
                            $data_cnt[0]++;
                            $data_cnt[2]++;
                        }


                        break;
                    }
                    case "지불" :
                    {
                        if (isset($item->market_seller_idx) && isset($item->order_info_idx)) {
                            // LM 상품 구매

                            $order_info_res = $this->pconn->table("order_info_tbl")
                                ->where("idx", "=", $item->order_info_idx)
                                ->get();

                            if (isset($order_info_res[0])) {

                                $market_seller_res = $this->conn->table("market_seller_tbl")
                                    ->where("idx", "=", $order_info_res[0]->market_seller_idx)
                                    ->get();

                                $seller_res = $this->conn->table("seller_tbl")
                                    ->where("idx", "=", $market_seller_res[0]->seller_idx)
                                    ->get();


                                $order_goods_sql = "SELECT gt.title, gt.image, ogt.state, (SELECT COUNT(*) FROM order_goods_tbl WHERE order_info_idx = {$item->order_info_idx}) AS cnt FROM goods_tbl AS gt, order_info_tbl AS oit INNER JOIN order_goods_tbl AS ogt ON ogt.order_info_idx = oit.idx WHERE oit.idx = {$item->order_info_idx} AND gt.idx = ogt.goods_idx";
                                // echo $order_goods_sql;

                                $order_goods_res = $this->pconn->select($order_goods_sql);

                                $title = $seller_res[0]->name . " [" . $order_goods_res[0]->state . "] ";

                                if ($order_goods_res[0]->cnt > 1) {
                                    $title .= "외 " . $order_goods_res[0]->cnt . "개";
                                }

                                $image = "";
                                if (is_numeric($order_goods_res[0]->image)) {
                                    $image = "thumb/" . $order_goods_res[0]->image . ".png";
                                    $image = $this->getImageURL(self::IMG_TYPE_GOODS_CONTENT, $image, 1000);
                                } else {
                                    $image = "thumb/" . $order_goods_res[0]->image;
                                    $image = $this->getImageURL(self::IMG_TYPE_GOODS_CONTENT, $image, 1000);
                                }

                                $data = [
                                    "idx"    => $item->idx,
                                    "action" => "구매",
                                    "title"  => $title,
                                    "image"  => $image,
                                    "date"   => getDateFormatTypeDote($item->insert_date),
                                    "amount" => number_format($item->amount),
                                ];

                                if ($order_goods_res[0]->state != "환불") {
                                    if ($sel_type == "all" || $sel_type == "buy") {
                                        array_push($items, $data);
                                    }
                                    $data_cnt[0]++;
                                    $data_cnt[3]++;
                                } else {
                                    if ($sel_type == "all" || $sel_type == "cancel") {
                                        array_push($items, $data);
                                    }
                                    $data_cnt[0]++;
                                    $data_cnt[4]++;
                                }

                            }


                        } else {
                            // CU 상품 구매
                        }

                        break;
                    }
                }
            }

            $response["data"]["count"] = $data_cnt;
            $response["data"]["items"] = $items;
        }


        return json_encode($response);
    }

    public function apiSetChargeSubmit(Request $request)
    {
        @session_start();
        $response = $this->setResponse();

        try {
            $login_data = $this->loginCheck($response, $request);
            $user_id = 0;
            if (gettype($login_data) == "array") {
                if ($login_data["code"] != 200) {
                    return json_encode($login_data);
                }
            } else {
                $user_id = $login_data;
            }

            if (! $request->filled(["imp_uid"])) {
                $response = $this->emptyResponse($response);
                return json_encode($response);
            }

            $charge = new ChargeController();
            $res = $charge->online($user_id, $request->input('imp_uid'));
            if ($res['success']) {
                $response["data"]["cash_log_idx"] = $res['data'];
            } else {
                $response = $this->errorResponse(300, "해당 거래건을 처리하는데 실패했습니다. E1");
            }

        } catch (\Illuminate\Database\QueryException $e) {
            $response = $this->errorResponse(300, "해당 거래건을 처리하는데 실패했습니다.ER3");

        } catch (PDOException $e) {
            $response = $this->errorResponse(300, "해당 거래건을 처리하는데 실패했습니다.ER3");
        }


        return json_encode($response);
    }

    public function apiSetAppChargeSubmit(Request $request)
    {
        @session_start();
        $response = $this->setResponse();

        try {

            if (! $this->checkToken($request->token, $request->user_id)) {


                $response = $this->setResponse(300, "올바른 접근이 아닙니다.");
                return json_encode($response);
            }

            $user_id = $request->user_id;

            if (! $request->filled(["imp_uid"])) {

                $response = $this->emptyResponse($response);
                return json_encode($response);
            }

            $charge = new ChargeController();
            $res = $charge->online($user_id, $request->input('imp_uid'));


            if ($res['success']) {
                $data = explode(",", $res['data']);
                $response["data"]["cash_log_idx"] = $data[0];
                $response["data"]["charge_success"] = $data[1];
            } else {
                if ($res['code'] == 423) {
                    return json_encode($response);
                }
                $response = $this->errorResponse(300, "해당 거래건을 처리하는데 실패했습니다. E1");
            }

        } catch (\Illuminate\Database\QueryException $e) {
            $response = $this->errorResponse(300, "해당 거래건을 처리하는데 실패했습니다.ER3");

        } catch (PDOException $e) {
            $response = $this->errorResponse(300, "해당 거래건을 처리하는데 실패했습니다.ER3");
        }


        return json_encode($response);
    }

    public function apiGetPayHistoryDetail(Request $request)
    {
        /*
        REQUEST
        type = ["buy"]
        idx = cash_log_idx
        */
        $response = $this->setResponse();
        $login_data = $this->loginCheck($response, $request);
        $user_id = 0;
        if (gettype($login_data) == "array") {
            if ($login_data["code"] != 200) {
                return json_encode($login_data);
            }
        } else {
            $user_id = $login_data;
            $cash_res = $this->pconn->table("cash_log")
                ->where("idx", "=", $request->idx)
                ->whereNull("delete_date")
                ->get();

            if (! $this->checkData($cash_res, true)) {

                $response = $this->errorResponse(300, "해당 정보가 존재하지 않습니다.");
                return json_encode($response);
            }

            switch ($request->type) {
                case "buy" :
                {
                    // 구입 상세 이력
                    $response = $this->apiGetPayHistoryDetail_Buy($cash_res[0], $response);
                    break;
                }
                case "charge" :
                {
                    // 충전 이력
                    $response = $this->apiGetPayHistoryDetail_Charge($cash_res[0], $response);
                    break;
                }
                case "refund" :
                {
                    // 환불 이력
                    break;
                }
            }
        }

        // return $response;
        return json_encode($response);
    }

    public function apiGetDeliveryList(Request $request)
    {
        @session_start();
        $response = $this->setResponse();
        $login_data = $this->loginCheck($response, $request);
        $user_id = 0;
        if (gettype($login_data) == "array") {
            if ($login_data["code"] != 200) {
                return json_encode($login_data);
            }
        } else {
            $user_id = $login_data;

            $where_type = "";
            $where_date = "";

            $sel_type = isset($request->sel_type) ? $request->sel_type : "all";
            $sel_date = isset($request->sel_date) ? $request->sel_date : "all";

            switch ($sel_type) {
                case "ready" :
                {
                    $where_type = "AND oit.delivery_state = '배송대기' AND ogt.state = '배송'";
                    break;
                }
                case "ing" :
                {
                    $where_type = "AND oit.delivery_state = '배송중' AND ogt.state = '배송'";
                    break;
                }
                case "complete" :
                {
                    $where_type = "AND oit.delivery_state = '배송완료' AND ogt.state = '배송'";
                    break;
                }
                case "cancel" :
                {
                    $where_type = "AND ogt.state = '환불'";
                    break;
                }
            }

            switch ($sel_date) {
                case "1w" :
                {
                    $where_date = "AND oit.insert_date >= DATE_SUB(NOW(), INTERVAL 1 WEEK)";
                    break;
                }
                case "1m" :
                {
                    $where_date = "AND oit.insert_date >= DATE_SUB(NOW(), INTERVAL 1 MONTH)";
                    break;
                }
                case "3m" :
                {
                    $where_date = "AND oit.insert_date >= DATE_SUB(NOW(), INTERVAL 3 MONTH)";
                    break;
                }
                case "6m" :
                {
                    $where_date = "AND oit.insert_date >= DATE_SUB(NOW(), INTERVAL 6 MONTH)";
                    break;
                }
            }


            $data_cnt = [];
            $total_sql = "SELECT IFNULL(COUNT(*), 0) AS cnt FROM order_goods_tbl AS ogt INNER JOIN order_info_tbl AS oit ON oit.idx = ogt.order_info_idx WHERE oit.user_idx = {$user_id} AND ogt.delete_date IS NULL AND ogt.state != '직접입력' AND ogt.state != '현장' {$where_date}";
            $total_res = $this->pconn->select($total_sql);
            $data_cnt[0] = $total_res[0]->cnt;

            $ready_sql = "SELECT IFNULL(COUNT(*), 0) AS cnt FROM order_goods_tbl AS ogt INNER JOIN order_info_tbl AS oit ON oit.idx = ogt.order_info_idx WHERE oit.user_idx = {$user_id} AND ogt.delete_date IS NULL AND ogt.state != '직접입력' AND ogt.state != '현장' AND oit.delivery_state = '배송대기' AND ogt.state = '배송' {$where_date}";
            $ready_res = $this->pconn->select($ready_sql);
            $data_cnt[1] = $ready_res[0]->cnt;

            $ing_sql = "SELECT IFNULL(COUNT(*), 0) AS cnt FROM order_goods_tbl AS ogt INNER JOIN order_info_tbl AS oit ON oit.idx = ogt.order_info_idx WHERE oit.user_idx = {$user_id} AND ogt.delete_date IS NULL AND ogt.state != '직접입력' AND ogt.state != '현장' AND oit.delivery_state = '배송중' AND ogt.state = '배송' {$where_date}";
            $ing_res = $this->pconn->select($ing_sql);
            $data_cnt[2] = $ing_res[0]->cnt;

            $complete_sql = "SELECT IFNULL(COUNT(*), 0) AS cnt FROM order_goods_tbl AS ogt INNER JOIN order_info_tbl AS oit ON oit.idx = ogt.order_info_idx WHERE oit.user_idx = {$user_id} AND ogt.delete_date IS NULL AND ogt.state != '직접입력' AND ogt.state != '현장' AND oit.delivery_state = '배송완료' AND ogt.state = '배송' {$where_date}";
            $complete_res = $this->pconn->select($complete_sql);
            $data_cnt[3] = $ing_res[0]->cnt;

            $cancel_sql = "SELECT IFNULL(COUNT(*), 0) AS cnt FROM order_goods_tbl AS ogt INNER JOIN order_info_tbl AS oit ON oit.idx = ogt.order_info_idx WHERE oit.user_idx = {$user_id} AND ogt.delete_date IS NULL AND ogt.state != '직접입력' AND ogt.state != '현장' AND ogt.state = '환불' {$where_date}";
            $cancel_res = $this->pconn->select($cancel_sql);
            $data_cnt[4] = $cancel_res[0]->cnt;

            $response["data"]["count"] = $data_cnt;

            $sql = "SELECT oit.order_number, oit.delivery_state, ogt.goods_idx, oit.idx as order_info_idx, ogt.state, oit.market_seller_idx, ogt.quantity, ogt.price, oit.insert_date, oit.tracking_number FROM order_goods_tbl AS ogt INNER JOIN order_info_tbl AS oit ON oit.idx = ogt.order_info_idx WHERE oit.user_idx = {$user_id} AND ogt.delete_date IS NULL AND ogt.state != '직접입력' AND ogt.state != '현장' {$where_type} {$where_date}";

            $goods_res = $this->pconn->select($sql);
            $goods_items = [];
            foreach ($goods_res as $goods_item) {

                $goods_item_res = $this->pconn->table("goods_tbl")
                                      ->where("idx", "=", $goods_item->goods_idx)
                                      ->get()[0];

                $seller_res = $this->conn->table("seller_tbl AS st")
                                  ->join("market_seller_tbl AS mst", "st.idx", "=", "mst.seller_idx")
                                  ->where("mst.idx", "=", $goods_item->market_seller_idx)
                                  ->get()[0];

                $market_res = $this->conn->table("market_tbl AS mt")
                                  ->join("market_seller_tbl AS mst", "mt.idx", "=", "mst.market_idx")
                                  ->where("mst.idx", "=", $goods_item->market_seller_idx)
                                  ->get()[0];

                $state = ($goods_item->state == "배송") ? $goods_item->delivery_state : "환불";
                $option = "";

                if ($goods_item_res->idx == 0) {
                    if (isset($goods_item_res->memo)) {
                        $option = $goods_item_res->memo;
                    }
                } else {
                    if (isset($goods_item_res->option_value1)) {
                        $option = $goods_item_res->option_value1;
                    }

                    if (isset($goods_item_res->option_value2)) {
                        $option .= $goods_item_res->option_value2;
                    }
                }

                $image = "";
                if (is_numeric($goods_item_res->image)) {
                    $image = "thumb/" . $goods_item_res->image . ".png";
                    $image = $this->getImageURL(self::IMG_TYPE_GOODS_CONTENT, $image, 1000);
                } else {
                    $image = "thumb/" . $goods_item_res->image;
                    $image = $this->getImageURL(self::IMG_TYPE_GOODS_CONTENT, $image, 1000);
                }

                $seller_res->cs_tel = str_replace(" ", "-", $seller_res->cs_tel);
                // $seller_res->cs_tel = str_replace("-","",$seller_res->cs_tel);

                $item = [
                    "order_info_idx"  => $goods_item->order_info_idx,
                    "order_number"    => $goods_item->order_number,
                    "delivery_state"  => $state,
                    "goods_image"     => $image,
                    "seller_name"     => $seller_res->name,
                    "goods_title"     => $goods_item_res->title,
                    "quantity"        => ($goods_item->quantity == 0) ? 1 : $goods_item->quantity,
                    "goods_option"    => $option,
                    "price"           => number_format($goods_item->price),
                    "cs_tel"          => "tel:" . $seller_res->cs_tel,
                    "date"            => getDateFormatTypeDote($goods_item->insert_date),
                    "cs_date"         => date("m월 d일", strtotime($market_res->cs_date)),
                    "tracking_number" => $goods_item->tracking_number,


                ];
                array_push($goods_items, $item);
            }

            $response["data"]["goods_items"] = $goods_items;
        }

        return json_encode($response);
    }

    public function apiGetPayHistoryDetail_Charge($cash_res, $response)
    {
        $data = [
            "type"           => $cash_res->type,
            "method"         => $cash_res->method,
            "state"          => $cash_res->state,
            "amount"         => number_format($cash_res->amount),
            "sender"         => $cash_res->sender,
            "vbank_num"      => $cash_res->vbank_num,
            "vbank_name"     => $cash_res->vbank_name,
            "vbank_date"     => $cash_res->vbank_date,
            "receipt_url"    => $cash_res->receipt_url,
            "result_msg"     => "",
            "result_sub_msg" => "",
            "appley_num"     => $cash_res->appy_num,
            "insert_date"    => $cash_res->insert_date,
            'user_idx'       => $cash_res->user_idx,
        ];

        if ($data["state"] == "기타") {
            $data["state"] = "결제실패";
        }

        $data["vbank_date"] = date("Y-m-d H:i:s", $data["vbank_date"]);
        switch ($data["state"]) {
            case "결제됨" :
            {
                $data["result_msg"] = "러마페이 충전이 완료 되었습니다.";
                break;
            }
            case "결제대기" :
            {
                $data["result_msg"] = "입금기한 내에 아래 계좌로 입금하시면 결제가 완료됩니다.";
                $data["result_sub_msg"] = "*입금자명과 실제 입금자가 같아야 합니다.";
                break;
            }
            default :
            {
                $data["result_msg"] = "충전에 실패 하였습니다. 다시 시도해주세요.";
                break;
            }
        }

        $response["data"] = $data;
        return $response;
    }


    private function apiGetPayHistoryDetail_Buy($cash_res, $response)
    {

        $order_type = ($cash_res->method == "마일리지") ? "러마페이" : $cash_res->method;


        $order_res = $this->pconn->table("order_info_tbl")
            ->where("idx", "=", $cash_res->order_info_idx)
            ->get();

        if (! $this->checkData($order_res, true)) {
            $response = $this->errorResponse(300, "해당 정보가 존재하지 않습니다. 1");
            return $response;
        }


        $goods_sql = "SELECT gt.image, ogt.state, ogt.insert_date, gt.price, gt.title FROM order_goods_tbl AS ogt INNER JOIN goods_tbl AS gt ON gt.idx = ogt.goods_idx WHERE gt.idx >= 0 AND ogt.order_info_idx = {$order_res[0]->idx}";
        $goods_res = $this->pconn->select($goods_sql);

        $goods_items = [];
        $seller_name = "";
        foreach ($goods_res as $goods_item) {

            $seller_sql = "SELECT st.name AS name FROM seller_tbl AS st, market_seller_tbl AS mst WHERE mst.idx = {$cash_res->market_seller_idx} AND st.idx = mst.seller_idx";

            $seller_res = $this->conn->select($seller_sql);
            $seller_name = $seller_res[0]->name;
            $image = "";
            if (is_numeric($goods_item->image)) {
                $image = "thumb/" . $goods_item->image . ".png";
                $image = $this->getImageURL(self::IMG_TYPE_GOODS_CONTENT, $image, 1000);
            } else {
                $image = "thumb/" . $goods_item->image;
                $image = $this->getImageURL(self::IMG_TYPE_GOODS_CONTENT, $image, 1000);
            }

            $data = [
                "seller_name" => $seller_res[0]->name,
                "goods_image" => $image,
                "goods_title" => $goods_item->title,
                "state"       => $goods_item->state,
                "order_date"  => getDateFormatTypeDote($goods_item->insert_date),
                "price"       => number_format($goods_item->price),
            ];

            array_push($goods_items, $data);
        }


        $order_data = [
            "normal_price"  => number_format($order_res[0]->normal_price),
            "sale_amount"   => number_format($order_res[0]->normal_price - $order_res[0]->sale_amount),
            "refund_amount" => number_format($order_res[0]->refund_amount),
            "result_amount" => number_format($order_res[0]->sale_amount - $order_res[0]->refund_amount),
            "order_date"    => $order_res[0]->insert_date,
            "order_type"    => $order_type,
            "seller_name"   => $seller_name,
            "goods_items"   => $goods_items,
        ];

        $response["data"] = $order_data;


        return $response;

    }


    public function getUserPayBalance($pconn, $user_id, $format = false)
    {
        $lmpay = new LMPayController();
        $balance = $lmpay->getBalance($user_id);
        if ($format) {
            return number_format($balance);
        } else {
            return $balance;
        }
        // $sql = "SELECT IFNULL(SUM(amount), 0) as cash FROM cash_log WHERE user_idx = {$user_id} AND delete_date IS NULL AND (`state` = '결제됨' OR (`action` = '환불' AND `state` = '결제대기') OR (action = '환불' AND state = '기타')) AND `action` != '취소'";
        //
        // $res = $pconn->select($sql);
        // if ($res) {
        //     if ($format) {
        //         return number_format($res[0]->cash);
        //     } else {
        //         return $res[0]->cash;
        //     }
        // } else {
        //     return 0;
        // }
    }

    public function setUserBarcode($conn, $pconn, $user_id, $isForced = false)
    {
        $response = [
            "success" => true,
            "msg"     => "",
            "barcode" => "",

        ];

        $user_data = $this->getUser($conn, $user_id);
        $is_change_barcode = true;
        if (! isset($user_data->update_date)) {
            $is_change_barcode = true;
        } else {
            $sql = "SELECT COUNT(*) AS cnt FROM user_list WHERE user_id = {$user_id} AND update_date < date_add(now(), interval -1 minute) order by user_id desc";
            $res = $conn->select($sql);
            if ($res[0]->cnt > 0) {
                $is_change_barcode = true;
            } else {
                $is_change_barcode = false;
                $response["barcode"] = $user_data->barcode;
            }
        }

        $barcode = "";

        if ($isForced) {
            $is_change_barcode = $isForced;
        }

        if ($is_change_barcode) {
            $barcode = $this->makeNewBarcode($user_data->user_id);
            $exists_barcode = true;

            while ($exists_barcode) {
                $exists_barcode_res = $pconn->table("barcode_tbl")
                    ->where("barcode", "=", $barcode)
                    ->get();

                if ($exists_barcode_res->count() > 0) {
                    $barcode = $this->makeNewBarcode($user_data->user_id, $barcode);
                    $exists_barcode = true;
                } else {
                    $exists_barcode = false;
                }
            }

            $barcode_res = $pconn->table("barcode_tbl")
                ->insertGetId([
                    "barcode"  => (string)$barcode,
                    "user_idx" => $user_data->user_id,
                ]);

            if ($barcode_res) {
                $conn->table("user_list")
                    ->where("user_id", "=", $user_data->user_id)
                    ->update([
                        "barcode"     => $barcode,
                        "barcode_idx" => $barcode_res,
                    ]);

            }

            $user_data = $this->getUser($conn, $user_id);
            $response["barcode"] = $user_data->barcode;

        } else {
            $response["success"] = false;
            $response["barcode"] = $user_data->barcode;
            $response["msg"] = "바코드는 1분단위로 변경하실수 있습니다.";
        }

        $response["barcode_img"] = "data:image/png;base64," . DNS1D::getBarcodePNG($user_data->barcode, "C128");
        $response["is_change"] = $is_change_barcode;

        return $response;
    }

    public function makeNewBarcode($user_idx, $isBarcode = "")
    {
        if (strlen($isBarcode) > 1) {

//            $minBarcode = substr($isBarcode,4);
//
//            $newBarcode = (int)($minBarcode) - 1;
//
//            $newBarcode = "2900".$newBarcode;
//            return $newBarcode;
            $query = "SELECT min(barcode+1) AS barcode FROM barcode_tbl WHERE (barcode+1) NOT IN (SELECT barcode FROM barcode_tbl)";
            $res = $this->pconn->select($query);
//            $minBarcode = substr($res[0]->barcode,4);
//            $newBarcode = (int)($minBarcode) - 1;
//            $newBarcode = "2900".$newBarcode;

//        dd($newBarcode);
            $newBarcode = $res[0]->barcode;
            return $newBarcode;
        } else {
            $query = "SELECT min(barcode+1) AS barcode FROM barcode_tbl WHERE (barcode+1) NOT IN (SELECT barcode FROM barcode_tbl)";
            $res = $this->pconn->select($query);
//            $minBarcode = substr($res[0]->barcode,4);
//            $newBarcode = (int)($minBarcode) - 1;
//            $newBarcode = "2900".$newBarcode;

//        dd($newBarcode);
            $newBarcode = $res[0]->barcode;
            return $newBarcode;
        }


    }


    public function apiGetRefundObjs(Request $request)
    {
        /*
        Request Data
        user_idx : 유저 고유번호
        */

        $response = [
            "success" => true,
            "msg"     => "",
        ];

        if (! $request->filled(['user_idx'])) {
            $response["success"] = false;
            $response["msg"] = "유저값이 없습니다.";
            return $response;
        }
        $user_idx = $request->user_idx;

        // $user_idx = 1;

        $res_user = $this->conn->table("user_list")
            ->where("user_id", "=", $user_idx)
            ->whereNull("delete_date")
            ->get();

        if (! isset($res_user[0])) {
            $response["success"] = false;
            $response["msg"] = "해당 유저가 존재하지 않습니다.";
            return $response;
        }

        $res_cash = $this->pconn->table("cash_log")
            ->where([["user_idx", "=", $user_idx], ["state", "=", "결제됨"]])
            ->whereNull("delete_date")
            ->get();


        $amount_types = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        $amount_objs = [[], [], [], [], [], [], [], [], [], []];

        $card_types = ["신용카드", "삼성페이", "페이코"];
        $cash_types = ["현금", "계좌이체", "가상계좌", "무통장"];
        $cu_types = ["CU현금"];

        $on_amount_objs = [];
        $off_amount_objs = [];

        // 온라인 충전, 환불금
        $on_charge_amount = 0;
        $on_refund_amount = 0;

        // 오프라인 충전, 환불금
        $off_charge_amount = 0;
        $off_refund_amount = 0;

        // CU 충전, 환불금
        $cu_charge_amount = 0;
        $cu_refund_amount = 0;

        $use_amount = 0;

        $now_date = date("Y-m-d", time());

        foreach ($res_cash as $item) {
            $data_date = "";
            if (isset($item_cash->update_date)) {
                $data_date = getDateFormatTypeOnlyDate($item->update_date);
            } else {
                $data_date = getDateFormatTypeOnlyDate($item->insert_date);
            }

            if ($item->action == "입금") {
                // 입금
                if ($now_date == $data_date) {
                    // 오늘 날짜
                    if (in_array($item->method, $card_types)) {
                        // 오늘 카드 타입 충전
                        if ($item->type == "온라인") {
                            // 오늘 카드 타입 온라인 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, false);
                            $obj = [
                                "idx"     => $amount_obj->idx,
                                "amount"  => $amount_obj->amount,
                                "type"    => "온라인",
                                "method"  => $amount_obj->method,
                                "imp_uid" => $amount_obj->imp_uid,
                            ];


                            // 0 이상인 온라인 환불 가능 목록으로 빼내기
                            if ($obj["amount"] > 0) {
                                array_push($amount_objs[$this->TOD_ON_CARD], $obj);
                                $amount_types[$this->TOD_ON_CARD] += $obj["amount"];
                                $on_charge_amount += $obj["amount"];
                                array_push($on_amount_objs, $obj);

                            }


                        } else {
                            // 오늘 카드 타입 오프라인 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, true);
                            $obj = [
                                "idx"    => $amount_obj->idx,
                                "amount" => $amount_obj->amount,
                                "type"   => "오프라인",
                            ];

                            // print_r($obj);
                            if ($obj["amount"] > 0) {
                                array_push($amount_objs[$this->TOD_OFF_CARD], $obj);
                                $amount_types[$this->TOD_OFF_CARD] += $obj["amount"];
                                $off_charge_amount += $obj["amount"];
                                array_push($off_amount_objs, $obj);

                            }
                        }
                    } else if (in_array($item->method, $cash_types)) {
                        // 오늘 현금 타입 충전
                        if ($item->type == "온라인") {
                            // 오늘 현금 타입 온라인 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, false);
                            $obj = [
                                "idx"     => $amount_obj->idx,
                                "amount"  => $amount_obj->amount,
                                "type"    => "온라인",
                                "method"  => $amount_obj->method,
                                "imp_uid" => $amount_obj->imp_uid,
                            ];

                            if ($obj["amount"] > 0) {
                                array_push($amount_objs[$this->TOD_ON_CASH], $obj);
                                $amount_types[$this->TOD_ON_CASH] += $obj["amount"];
                                $on_charge_amount += $obj["amount"];
                                array_push($on_amount_objs, $obj);

                            }
                        } else {
                            // 오늘 현금 타입 오프라인 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, true);
                            $obj = [
                                "idx"    => $amount_obj->idx,
                                "amount" => $amount_obj->amount,
                                "type"   => "오프라인",

                            ];
                            if ($obj["amount"] > 0) {
                                array_push($amount_objs[$this->TOD_OFF_CASH], $obj);
                                $amount_types[$this->TOD_OFF_CASH] += $obj["amount"];
                                $off_charge_amount += $obj["amount"];
                                array_push($off_amount_objs, $obj);


                            }
                        }
                    } else if (in_array($item->method, $cu_types)) {
                        // 오늘 CU 타입 충전
                        if ($item->method == "CU현금") {
                            // 오늘 CU현금 타입 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, true);
                            $obj = [
                                "idx"    => $amount_obj->idx,
                                "amount" => $amount_obj->amount,
                                "type"   => "오프라인",

                            ];

                            if ($obj["amount"] > 0) {
                                array_push($amount_objs[$this->TOD_CU_CASH], $obj);

                                $amount_types[$this->TOD_CU_CASH] += $obj["amount"];
                                $cu_charge_amount += $obj["amount"];
                                array_push($off_amount_objs, $obj);


                            }
                        }
                    }
                } else {
                    // 이전 날짜
                    if (in_array($item->method, $card_types)) {
                        // 이전 카드 타입 충전
                        if ($item->type == "온라인") {
                            // 이전 카드 타입 온라인 충전

                            $amount_obj = $this->getAmountObject($this->pconn, $item, false);

                            $obj = [
                                "idx"     => $amount_obj->idx,
                                "amount"  => $amount_obj->amount,
                                "type"    => "온라인",
                                "method"  => $amount_obj->method,
                                "imp_uid" => $amount_obj->imp_uid,
                            ];

                            if ($obj["amount"] > 0) {
                                array_push($amount_objs[$this->BEF_ON_CARD], $obj);
                                $amount_types[$this->BEF_ON_CARD] += $obj["amount"];
                                $on_charge_amount += $obj["amount"];

                                array_push($on_amount_objs, $obj);


                            }
                        } else {
                            // 이전 카드 타입 오프라인 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, true);
                            $obj = [
                                "idx"    => $amount_obj->idx,
                                "amount" => $amount_obj->amount,
                                "type"   => "오프라인",
                            ];
                            if ($obj["amount"] > 0) {
                                array_push($amount_objs[$this->BEF_OFF_CARD], $obj);

                                $amount_types[$this->BEF_OFF_CARD] += $obj["amount"];
                                $off_charge_amount += $obj["amount"];
                                array_push($off_amount_objs, $obj);


                            }
                        }
                    } else if (in_array($item->method, $cash_types)) {
                        // 이전 현금 타입 충전
                        if ($item->type == "온라인") {
                            // 이전 현금 타입 온라인 충전
                            // $amount_types[3] += $item->amount;
                            // $on_charge_amount += $item->amount;
                            $amount_obj = $this->getAmountObject($this->pconn, $item, false);
                            $obj = [
                                "idx"     => $amount_obj->idx,
                                "amount"  => $amount_obj->amount,
                                "type"    => "온라인",
                                "method"  => $amount_obj->method,
                                "imp_uid" => $amount_obj->imp_uid,
                            ];

                            if ($obj["amount"] > 0) {
                                array_push($amount_objs[$this->BEF_ON_CASH], $obj);
                                $amount_types[$this->BEF_ON_CASH] += $obj["amount"];
                                $on_charge_amount += $obj["amount"];

                                array_push($on_amount_objs, $obj);

                            }
                        } else {
                            // 이전 현금 타입 오프라인 충전 *

                            $amount_obj = $this->getAmountObject($this->pconn, $item, true);
                            $obj = [
                                "idx"    => $amount_obj->idx,
                                "amount" => $amount_obj->amount,
                                "type"   => "오프라인",
                            ];
                            // print_r($obj);
                            if ($obj["amount"] > 0) {
                                array_push($amount_objs[$this->BEF_OFF_CASH], $obj);
                                $amount_types[$this->BEF_OFF_CASH] += $obj["amount"];
                                $off_charge_amount += $obj["amount"];
                                array_push($off_amount_objs, $obj);


                            }
                        }
                    } else if (in_array($item->method, $cu_types)) {
                        // 이전 CU 타입 충전
                        if ($item->method == "CU현금") {
                            // 이전 CU카드 타입 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, true);
                            $obj = [
                                "idx"    => $amount_obj->idx,
                                "amount" => $amount_obj->amount,
                                "type"   => "오프라인",

                            ];

                            if ($obj["amount"] > 0) {
                                array_push($amount_objs[$this->BEF_CU_CASH], $obj);

                                $amount_types[$this->BEF_CU_CASH] += $obj["amount"];
                                $cu_charge_amount += $obj["amount"];
                                array_push($off_amount_objs, $obj);


                            }
                        }
                    }
                }
            } else if ($item->action == "환불") {
                // 환불
                if ($item->amount > 0 && isset($item->market_seller_idx)) {
                    // 상품 환불
                    $use_amount += $item->amount;
                } else if ($item->amount > 0 && ! isset($item->market_seller_idx) && isset($item->order_info_idx)) {
                    // CU 상품 환불
                    $use_amount += $item->amount;
                } else {
                    // 마일리지 환불
                    if ($item->type == "온라인" && isset($item->cash_log_idx) && ! isset($item->market_seller_idx)) {
                        $on_refund_amount += $item->amount;
                    } else if ($item->type == "오프라인" && isset($item->cash_log_idx) && ! isset($item->market_seller_idx) && ! in_array($item->method, $cu_types)) {
                        $off_refund_amount += $item->amount;
                    } else if ($item->type == "오프라인" && isset($item->cash_log_idx) && ! isset($item->market_seller_idx) && in_array($item->method, $cu_types)) {
                        $cu_refund_amount += $item->amount;
                    } else if ($item->amount <= 0) {
                        $use_amount += $item->amount;
                    }
                }
            } else {
                // 결제
                $use_amount += $item->amount;
            }
        }


        // print_r($amount_objs);
        // echo "use_amount : ".$use_amount;


        $result_objs = [[], [], [], [], [], [], [], [], [], []];
        if ($use_amount < 0) {
            $use_amount = $use_amount * -1;
        }

        for ($i = 0; $i < count($amount_objs); $i++) {
            foreach ($amount_objs[$i] as $obj) {

                $use_amount = $use_amount - $obj["amount"];

                if ($use_amount < 0) {
                    $obj["amount"] = $use_amount * -1;
                    $use_amount = 0;
                } else {
                    $obj["amount"] = 0;
                }


                array_push($result_objs[$i], $obj);
            }
        }

        // print_r($result_objs);
        $response["data"] = $result_objs;


        return $response;
    }


    private function getAmountObject($pconn, $obj, $is_offline = true)
    {

        if ($is_offline) {
            // 오프라인
            $res = $pconn->table("cash_log")
                ->where("cash_log_idx", "=", $obj->idx)
                ->whereNull("delete_date")
                ->whereNull("order_info_idx")
                ->get();

            $resultAmount = $obj->amount;

            foreach ($res as $item) {

                $waiting_refund_amount = 0;
                $resultAmount -= abs($item->amount);
            }

            $obj->amount = $resultAmount;

            return $obj;

        } else {
            // 온라인


            $api = IMPORT_API;
            $key = IMPORT_KEY;


            $iamport = new Iamport($api, $key);

            if (isset($obj->imp_uid)) {

                $result = $iamport->findByImpUID($obj->imp_uid);

                if ($result->success) {
                    $obj->amount = $result->data->amount - $result->data->cancel_amount;
                } else {
                    // echo "else";
                    $obj->amount = 0;
                }


                $resultAmount = $obj->amount;

                $res = $pconn->table("cash_log")
                    ->where("cash_log_idx", "=", $obj->idx)
                    ->whereNull("delete_date")
                    ->whereNull("order_info_idx")
                    ->get();
                // // print_r($res);
                $log_refund_amount = 0;
                foreach ($res as $item) {


                    $waiting_refund_amount = 0;
                    //
                    if ($item->state == "결제대기" && $item->action == "환불" && ! isset($market_seller_idx)) {
                        // $waiting_refund_amount = $item->amount;
                        $resultAmount += $item->amount;
                    }

                    if ($item->state == "결제됨" && $item->action == "환불" && ! isset($market_seller_idx)) {
                        $log_refund_amount += $item->amount;
                        // $resultAmount += $item->amount;

                    }

                    // echo "item->compulsion_refund : ".$item->compulsion_refund;
                    if ($item->compulsion_refund == "N") {
                        $result = $iamport->findByImpUID($item->refund_imp_uid);

                        if ($result->success) {
                            $item->amount = $result->data->amount - $result->data->cancel_amount;
                        } else {
                            // echo "else";
                            $item->amount = 0;
                        }

                        if (isset($result->data->cancel_amount) && $result->data->cancel_amount != $log_refund_amount) {
                            $resultAmount += $log_refund_amount;
                            // $resultAmount += $item->amount;
                        }
                    } else {
                        $resultAmount += $log_refund_amount;
                    }

                    // $resultAmount += $item->amount + $waiting_refund_amount;
                }


                $obj->amount = $resultAmount;

            } else {
                // echo "else";
                $obj->amount = 0;
            }


            // echo "obj amount : ".$obj->amount;

            return $obj;
        }

    }


    // 온라인 환불 오브젝트 세팅
    public function setRefundObj_Online($conn, $pconn, $obj)
    {
        $response = ["success" => true, "msg" => ""];
        $api = "";
        $key = "";
        // if(TEST_MODE == "true") {
        // $api = IMPORT_API_TEST;
        // $key = IMPORT_KEY_TEST;
        // } else {
        $api = IMPORT_API;
        $key = IMPORT_KEY;
        // }

        $iamport = new Iamport($api, $key);
        $response_data = $iamport->findByImpUID($obj["imp_uid"]);

        if ($response_data->success) {
            $imp_token = $this->getIMPToken($api, $key);

            if ($imp_token) {

                $user_res = $conn->table("user_list")
                    ->where("user_id", "=", $obj["user_id"])
                    ->whereNull("delete_date")
                    ->get();

                $cash_res = $pconn->table("cash_log")
                    ->where("idx", "=", $obj["idx"])
                    ->whereNull("delete_date")
                    ->get();

                if ($user_res->count() > 0 && $cash_res->count() > 0) {
                    $user_data = $user_res[0];
                    $cash_data = $cash_res[0];
                    $cash_types = ["현금", "계좌이체", "가상계좌", "무통장"];
                    $bank_data = [
                        "refund_bank"    => "",
                        "refund_account" => "",
                        "refund_owner"   => "",
                    ];

                    if (in_array($cash_data->method, $cash_types)) {
                        // 현금 타입 충전인 경우 (계좌정보 필요)
                        // 은행정보 검증 과정
                        if (! isset($user_data->refund_bank)) {
                            $response["success"] = false;
                            $response["msg"] = "계좌 정보가 존재하지 않습니다.";
                            $response["code"] = 300;
                            return $response;
                        } else {
                            if (! $this->getKCPBankCode($user_data->refund_bank)) {
                                $response["success"] = false;
                                $response["msg"] = "은행코드표에 없는 은행입니다.";
                                return $response;
                            } else {
                                $bank_data["refund_bank"] = $user_data->refund_bank;
                                $bank_data["refund_account"] = $user_data->refund_account;
                                $bank_data["refund_owner"] = $user_data->refund_owner;
                            }

                            if (! $this->checkBank($imp_token, $bank_data)) {
                                $response["success"] = false;
                                $response["msg"] = "계좌정보가 올바르지 않습니다.";
                                $response["code"] = 300;
                                return $response;
                            }
                        }
                    }


                    $cancel_data = [
                        "imp_uid"        => $response_data->data->imp_uid,
                        "merchant_uid"   => $response_data->data->merchant_uid,
                        "amount"         => abs($obj["amount"]),
                        "reason"         => "고객 환불요청",
                        "refund_holder"  => $bank_data["refund_owner"],
                        "refund_bank"    => $this->getKCPBankCode($bank_data["refund_bank"]),
                        "refund_account" => $bank_data["refund_account"],
                    ];

                    $cancel_result = $iamport->cancel($cancel_data);
                    if (isset($cancel_result->success) && $cancel_result->success == true) {
                        // 환불 완료 결제됨 Insert
                        $insert_data = [
                            "user_idx"       => $obj["user_id"],
                            "cash_log_idx"   => $obj["idx"],
                            "refund_imp_uid" => $obj["imp_uid"],
                            "type"           => "온라인",
                            "method"         => "마일리지",
                            "action"         => "환불",
                            "state"          => "결제됨",
                            "amount"         => $obj["amount"] * -1,
                            "vbank_num"      => $bank_data["refund_account"],
                            "vbank_name"     => $bank_data["refund_bank"],
                        ];

                        $res = $pconn->table("cash_log")
                            ->insertGetId($insert_data);

                        if (! $res) {
                            $response["success"] = false;
                            $response["msg"] = $cancel_result->error["message"];
                            $response["code"] = 300;
                            return $response;
                        }
                    } else {
                        // 환불 결제대기 Insert
                        $insert_data = [
                            "user_idx"       => $obj["user_id"],
                            "cash_log_idx"   => $obj["idx"],
                            "refund_imp_uid" => $obj["imp_uid"],
                            "type"           => "온라인",
                            "method"         => "마일리지",
                            "action"         => "환불",
                            "state"          => "결제대기",
                            "error_msg"      => $cancel_result->error["message"],
                            "amount"         => $obj["amount"] * -1,
                        ];

                        // print_r($insert_data);

                        $res = $pconn->table("cash_log")
                            ->insertGetId($insert_data);

                        if (! $res) {
                            $response["success"] = false;
                            $response["msg"] = $cancel_result->error["message"];
                            $response["code"] = 300;
                            return $response;
                        }
                    }
                } else {
                    if ($user_data->count() == 0) {
                        $response["success"] = false;
                        $response["msg"] = "해당 유저가 존재하지 않습니다.";
                        $response["code"] = 300;
                        return $response;
                    } else {
                        $response["success"] = false;
                        $response["msg"] = "해당 충전정보가 존재하지 않습니다.";
                        $response["code"] = 300;
                        return $response;
                    }
                }
            } else {
                $response["success"] = false;
                $response["msg"] = "아임포트 토큰 접근에 실패했습니다.";
                $response["code"] = 300;
                return $response;
            }

        } else {
            $response["success"] = false;
            $response["msg"] = "아임포트 정보 조회에 실패했습니다.";
            $response["code"] = 300;
        }

        return $response;
    }

    // 오프라인 환불 오브젝트 세팅
    public function setRefundObj_Offline($conn, $pconn, $obj)
    {
        $response = ["success" => true, "msg" => ""];

        $api = "";
        $key = "";
        // if(TEST_MODE == "true") {
        // $api = IMPORT_API_TEST;
        // $key = IMPORT_KEY_TEST;
        // } else {
        $api = IMPORT_API;
        $key = IMPORT_KEY;
        // }

        $imp_token = $this->getIMPToken($api, $key);
        if ($imp_token) {
            $user_res = $conn->table("user_list")
                ->where("user_id", "=", $obj["user_id"])
                ->whereNull("delete_date")
                ->get();

            $cash_res = $pconn->table("cash_log")
                ->where("idx", "=", $obj["idx"])
                ->whereNull("delete_date")
                ->get();

            if ($user_res->count() > 0 && $cash_res->count() > 0) {
                $user_data = $user_res[0];
                $cash_data = $cash_res[0];

                $bank_data = [
                    "refund_bank"    => "",
                    "refund_account" => "",
                    "refund_owner"   => "",
                ];

                if (! isset($user_data->refund_bank)) {
                    $response["success"] = false;
                    $response["code"] = 400;
                    $response["msg"] = "계좌 정보가 존재하지 않습니다.";
                    return $response;
                } else {
                    if (! $this->getKCPBankCode($user_data->refund_bank)) {
                        $response["success"] = false;
                        $response["code"] = 400;
                        $response["msg"] = "은행코드표에 없는 은행입니다.";
                        return $response;
                    } else {
                        $bank_data["refund_bank"] = $user_data->refund_bank;
                        $bank_data["refund_account"] = $user_data->refund_account;
                        $bank_data["refund_owner"] = $user_data->refund_owner;
                    }

                    if (! $this->checkBank($imp_token, $bank_data)) {
                        $response["success"] = false;
                        $response["code"] = 400;
                        $response["msg"] = "계좌정보가 올바르지 않습니다.";
                        return $response;
                    }

                    $insert_data = [
                        "user_idx"     => $obj["user_id"],
                        "cash_log_idx" => $obj["idx"],
                        "type"         => "오프라인",
                        "method"       => "마일리지",
                        "action"       => "환불",
                        "state"        => "결제대기",
                        "amount"       => $obj["amount"] * -1,
                        "vbank_num"    => $bank_data["refund_account"],
                        "vbank_name"   => $bank_data["refund_bank"],
                    ];

                    $res = $pconn->table("cash_log")
                        ->insertGetId($insert_data);

                    if (! $res) {
                        $response["success"] = false;
                        $response["msg"] = "환불 신청에 실패했습니다.";
                        $response["code"] = 400;
                        return $response;
                    } else {
                        return $response;
                    }
                }
            } else {
                if ($user_data->count() == 0) {
                    $response["success"] = false;
                    $response["msg"] = "해당 유저가 존재하지 않습니다.";
                    $response["code"] = 400;
                    return $response;
                } else {
                    $response["success"] = false;
                    $response["msg"] = "해당 충전정보가 존재하지 않습니다.";
                    $response["code"] = 400;
                    return $response;
                }
            }

        } else {
            $response["success"] = false;
            $response["msg"] = "아임포트 토큰 접근에 실패했습니다.";
            $response["code"] = 400;
            return $response;
        }
    }

    public function setRefundObj_CU($conn, $pconn, $cash_refund_objs)
    {

        $response = ["success" => true, "msg" => ""];

        $api = "";
        $key = "";
        if (TEST_MODE == "true") {
            $api = IMPORT_API_TEST;
            $key = IMPORT_KEY_TEST;
        } else {
            $api = IMPORT_API;
            $key = IMPORT_KEY;
        }

        $imp_token = $this->getIMPToken($api, $key);
        if ($imp_token) {
            $user_res;
            $user_idx = 0;
            if (isset($cash_refund_objs[0])) {
                $obj = $cash_refund_objs[0];
                $user_res = $conn->table("user_list")
                    ->where("user_id", "=", $obj["user_id"])
                    ->whereNull("delete_date")
                    ->get();
                $user_idx = $user_res[0]->user_id;
            } else {
                $response["success"] = false;
                $response["msg"] = "해당 유저가 존재하지 않습니다.";
                $response["code"] = 400;
                return $response;
            }

            if ($user_res->count() > 0) {

                $user_data = $user_res[0];

                $bank_data = [
                    "refund_bank"    => "",
                    "refund_account" => "",
                    "refund_owner"   => "",
                ];

                if (! isset($user_data->refund_bank)) {
                    $response["success"] = false;
                    $response["code"] = 400;
                    $response["msg"] = "계좌 정보가 존재하지 않습니다.";
                    return $response;
                } else {
                    if (! $this->getKCPBankCode($user_data->refund_bank)) {
                        $response["success"] = false;
                        $response["code"] = 400;
                        $response["msg"] = "은행코드표에 없는 은행입니다.";
                        return $response;
                    } else {
                        $bank_data["refund_bank"] = $user_data->refund_bank;
                        $bank_data["refund_account"] = $user_data->refund_account;
                        $bank_data["refund_owner"] = $user_data->refund_owner;
                    }

                    if (! $this->checkBank($imp_token, $bank_data)) {
                        $response["success"] = false;
                        $response["code"] = 400;
                        $response["msg"] = "계좌정보가 올바르지 않습니다.";
                        return $response;
                    }
                }

                $cash_log_idxs = "";
                $py_refund_amount = 0;
                foreach ($cash_refund_objs as $obj) {
                    $py_refund_amount += $obj["amount"];


                    $insert_cash_data = [
                        "user_idx"     => $user_idx,
                        "cash_log_idx" => $obj["idx"],
                        "type"         => "오프라인",
                        "method"       => "마일리지",
                        "action"       => "환불",
                        "state"        => "결제대기",
                        "amount"       => "-" . $obj["amount"],
                    ];

                    // print_r($insert_cash_data);
                    $res = $pconn->table("cash_log")
                        ->insertGetId($insert_cash_data);

                    if (! $res) {
                        $response["success"] = false;
                        $response["msg"] = "환불과정에 문제가 생겼습니다. 고객센터에 문의해주세요.";
                        $response["code"] = 400;
                        return $response;
                    }

                    $cash_log_idxs .= "#" . $res;

                }

                $args = [
                    "user_idx"      => $user_idx,
                    "cash_log_idxs" => $cash_log_idxs,
                    "amount"        => $py_refund_amount,
                    "bank"          => $user_data->refund_bank,
                    "bank_account"  => $user_data->refund_account,
                    "bank_owner"    => $user_data->refund_owner,
                ];

                // print_r($args);


                $request = new Request();
                $request->setMethod('POST');
                $request->request->add($args);

                $con_CU = new CURefundController();

                if ($response_data = $con_CU->apiRefundRequest($request)) {
                    $response_data = json_decode($response_data);
                    // print_r($response_data);
                    if (isset($response_data)) {
                        if ($response_data->RESULTTP == "S") {
                            $TXNO = $this->getDecrypt($response_data->RESULTDATA->TXNO);

                            $cu_refund_res = $pconn->table("cu_refund_log")
                                ->where("TXNO", "=", $TXNO)
                                ->get();

                            if (isset($cu_refund_res[0])) {
                                foreach ($cash_refund_objs as $obj) {
                                    $res = $pconn->table("cash_log")
                                        ->where("idx", "=", $obj["idx"])
                                        ->update([
                                            "appy_num" => $TXNO,
                                        ]);
                                }
                            } else {
                                $response = [];
                                $response["success"] = false;
                                $response["msg"] = "환불에 실패했습니다. 관리자에게 문의해주세요.";
                                $response["code"] = 400;


                                foreach ($cash_refund_objs as $obj) {
                                    $res = $pconn->table("cash_log")
                                        ->where("cash_log_idx", "=", $obj["idx"])
                                        ->update([
                                            "state" => "기타",
                                        ]);
                                }
                            }


                        } else if ($response_data->RESULTTP == "D" && $response_data->RESULTCD == "1103") {
                            $response = [];
                            $response["success"] = false;
                            $response["msg"] = "이미 환불요청이 완료 되었습니다.";
                            $response["code"] = 400;
                        } else {
                            $response = [];
                            $response["success"] = false;
                            $response["msg"] = "환불에 실패했습니다. 관리자에게 문의해주세요.";
                            $response["code"] = 400;

                            foreach ($cash_refund_objs as $obj) {
                                $res = $pconn->table("cash_log")
                                    ->where("cash_log_idx", "=", $obj["idx"])
                                    ->update([
                                        "state" => "기타",
                                    ]);
                            }
                        }
                    }
                } else {
                    $response = [];
                    $response["success"] = false;
                    $response["msg"] = "환불에 실패했습니다. 관리자에게 문의해주세요.(ER321)";
                    $response["code"] = 400;

                    foreach ($cash_refund_objs as $obj) {
                        $res = $pconn->table("cash_log")
                            ->where("cash_log_idx", "=", $obj["idx"])
                            ->update([
                                "state" => "기타",
                            ]);
                    }
                }

                return $response;
            } else {
                $response["success"] = false;
                $response["msg"] = "해당 유저가 존재하지 않습니다.";
                $response["code"] = 300;
                return $response;
            }

        } else {

            $response["success"] = false;
            $response["msg"] = "아임포트 토큰 접근에 실패했습니다.";
            $response["code"] = 300;
            return $response;
        }
    }


    private $key = "fleapop2playtong";
    private $iv  = "fleapop2playtong";

    private function getDecrypt($msg)
    {
        $data = base64_decode($msg);
        $endata = @openssl_decrypt($data, "aes-128-cbc", $this->key, true, $this->iv);
        return $endata;


    }


    function getIMPToken($api, $key)
    {
        $import_data = ["imp_key" => $api, "imp_secret" => $key];
        $import_get_token_url = "https://api.iamport.kr/users/getToken";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $import_get_token_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($import_data));
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);

        // This should be the default Content-type for POST requests
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-type: application/x-www-form-urlencoded"]);

        $result = curl_exec($ch);
        if (curl_errno($ch) !== 0) {
            return false;
        }

        curl_close($ch);
        $response = json_decode($result);
        // print_r($response);
        return $response->response->access_token;
    }


    function getKCPBankCode($bank)
    {
        $bank_code = [
            "경남은행"    => "BK39",
            "국민은행"    => "BK04",
            "기업은행"    => "BK03",
            "농협"      => "BK11",
            "대구은행"    => "BK31",
            "부산은행"    => "BK32",
            "산업은행"    => "BK02",
            "새마을금고"   => "BK45",
            "수협"      => "BK07",
            "신한은행"    => "BK88",
            "신협"      => "BK48",
            "외환은행"    => "BK81",
            "우리은행"    => "BK20",
            "우체국"     => "BK71",
            "전북은행"    => "BK37",
            "SC제일은행"  => "BK23",
            "카카오뱅크"   => "BK90",
            "케이뱅크"    => "BK89",
            "KEB하나은행" => "BK81",
        ];

        if (isset($bank_code[$bank])) {
            return $bank_code[$bank];
        } else {
            return false;
        }
    }

    function verificationAccount($bank_data)
    {
        $api = env('IAMPORT_REST_API_KEY', '');
        $key = env('IAMPORT_REST_API_SECRET', '');
        $token = $this->getIMPToken($api, $key);
        return $this->checkBank($token, $bank_data);
    }

    function cancelIamport($data)
    {
        $api = env('IAMPORT_REST_API_KEY', '');
        $key = env('IAMPORT_REST_API_SECRET', '');

        $cancel_data = [
            "imp_uid"        => $data["imp_uid"],
            "merchant_uid"   => $data['merchant_id'],
            "amount"         => $data['amount'],
            "reason"         => "고객 환불요청",
            "refund_holder"  => $data["bank_holder"],
            "refund_bank"    => $this->getKCPBankCode($data["bank"]),
            "refund_account" => $data["bank_account"],
        ];

        $iamport = new Iamport($api, $key);
        $cancel_result = $iamport->cancel($cancel_data);

        if (isset($cancel_result->success) && $cancel_result->success == true) {
            return [
                "success" => true,
                "msg"     => "",
            ];
        } else {
            return [
                "success" => false,
                "msg"     => $cancel_result->error["message"],
            ];
        }

    }

    function checkBank($token, $bank_data)
    {
        $imp = new FPIamport();
        $bank = [
            'bank'    => $bank_data['refund_bank'],
            'holder'  => $bank_data['refund_owner'],
            'account' => $bank_data['refund_account'],
        ];

        $res = $imp->verificationBankAccount($bank);
        if ($res->getStatusCode() != 200) {
            return false;
        }

        return true;
    }

    private function makeCuBarcode($user_idx)
    {
        $barcode = "";

        $res = $this->pconn_w->table("cu_barcode_tbl")
            ->insertGetId([
                "user_idx" => $user_idx,
            ]);

        if ($res) {
            // 12자리
            $tmp_barcode = $res;
            while (strlen($tmp_barcode) < 12) {
                $tmp_barcode .= "0";

            }

            $barcode = "4303" . $tmp_barcode;
            $update_res = $this->pconn_w->table("cu_barcode_tbl")
                ->where("idx", "=", $res)
                ->update([
                    "barcode" => $barcode,
                ]);

            if (! $update_res) {
                return false;
            } else {
                return $barcode;
            }

        } else {
            return false;
        }
    }


    // Test Refund
    public function apiGetRefundObjs_Test(Request $request)
    {
        /*
        Request Data
        user_idx : 유저 고유번호
        */

        $response = [
            "success" => true,
            "msg"     => "",
        ];

        if (! $request->filled(['user_idx'])) {
            $response["success"] = false;
            $response["msg"] = "유저값이 없습니다.";
            return $response;
        }
        $user_idx = $request->user_idx;

        // $user_idx = 1;

        $res_user = $this->conn->table("user_list")
            ->where("user_id", "=", $user_idx)
            ->whereNull("delete_date")
            ->get();

        if (! self::checkData($res_user)) {
            $response["success"] = false;
            $response["msg"] = "해당 유저가 존재하지 않습니다.";
            return $response;
        }

        // $res_cash = $this->pconn->table("cash_log")
        // ->where([["user_idx","=",$user_idx],["state","=","결제됨"]])
        // ->whereNull("delete_date")
        // ->get();

        $res_sql = "SELECT * FROM cash_log WHERE user_idx = {$user_idx} AND delete_date IS NULL AND (`state` = '결제됨' OR (`action` = '환불' AND `state` = '결제대기')) AND `action` != '취소'";

        $res_cash = $this->pconn->select($res_sql);


        $amount_types = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        $amount_objs = [[], [], [], [], [], [], [], [], [], []];

        $card_types = ["신용카드", "삼성페이", "페이코"];
        $cash_types = ["현금", "계좌이체", "가상계좌", "무통장"];
        $cu_types = ["CU현금"];

        $on_amount_objs = [];
        $off_amount_objs = [];

        // 온라인 충전, 환불금
        $on_charge_amount = 0;
        $on_refund_amount = 0;

        // 오프라인 충전, 환불금
        $off_charge_amount = 0;
        $off_refund_amount = 0;

        // CU 충전, 환불금
        $cu_charge_amount = 0;
        $cu_refund_amount = 0;

        $use_amount = 0;

        $now_date = date("Y-m-d", time());

        foreach ($res_cash as $item) {
            $data_date = "";
            if (isset($item_cash->update_date)) {
                $data_date = getDateFormatTypeOnlyDate($item->update_date);
            } else {
                $data_date = getDateFormatTypeOnlyDate($item->insert_date);
            }

            if ($item->action == "입금") {
                // 입금
                if ($now_date == $data_date) {
                    // 오늘 날짜
                    if (in_array($item->method, $card_types)) {
                        // 오늘 카드 타입 충전
                        if ($item->type == "온라인") {
                            // 오늘 카드 타입 온라인 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, false);
                            $obj = [
                                "idx"     => $amount_obj->idx,
                                "amount"  => $amount_obj->amount,
                                "type"    => "온라인",
                                "method"  => $amount_obj->method,
                                "imp_uid" => $amount_obj->imp_uid,
                            ];


                            array_push($amount_objs[$this->TOD_ON_CARD], $obj);

                            $amount_types[$this->TOD_ON_CARD] += $obj["amount"];
                            $on_charge_amount += $obj["amount"];

                            // 0 이상인 온라인 환불 가능 목록으로 빼내기
                            if ($obj["amount"] > 0) {
                                array_push($on_amount_objs, $obj);
                            }

                        } else {
                            // 오늘 카드 타입 오프라인 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, true);
                            $obj = [
                                "idx"    => $amount_obj->idx,
                                "amount" => $amount_obj->amount,
                                "type"   => "오프라인",
                            ];

                            // print_r($obj);
                            array_push($amount_objs[$this->TOD_OFF_CARD], $obj);
                            $amount_types[$this->TOD_OFF_CARD] += $obj["amount"];
                            $off_charge_amount += $obj["amount"];

                            if ($obj["amount"] > 0) {
                                array_push($off_amount_objs, $obj);
                            }
                        }
                    } else if (in_array($item->method, $cash_types)) {
                        // 오늘 현금 타입 충전
                        if ($item->type == "온라인") {
                            // 오늘 현금 타입 온라인 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, false);
                            $obj = [
                                "idx"     => $amount_obj->idx,
                                "amount"  => $amount_obj->amount,
                                "type"    => "온라인",
                                "method"  => $amount_obj->method,
                                "imp_uid" => $amount_obj->imp_uid,
                            ];

                            array_push($amount_objs[$this->TOD_ON_CASH], $obj);
                            $amount_types[$this->TOD_ON_CASH] += $obj["amount"];
                            $on_charge_amount += $obj["amount"];
                            // 0 이상인 온라인 환불 가능 목록으로 빼내기
                            if ($obj["amount"] > 0) {
                                array_push($on_amount_objs, $obj);
                            }
                        } else {
                            // 오늘 현금 타입 오프라인 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, true);
                            $obj = [
                                "idx"    => $amount_obj->idx,
                                "amount" => $amount_obj->amount,
                                "type"   => "오프라인",

                            ];
                            array_push($amount_objs[$this->TOD_OFF_CASH], $obj);

                            $amount_types[$this->TOD_OFF_CASH] += $obj["amount"];
                            $off_charge_amount += $obj["amount"];

                            if ($obj["amount"] > 0) {
                                array_push($off_amount_objs, $obj);
                            }
                        }
                    } else if (in_array($item->method, $cu_types)) {
                        // 오늘 CU 타입 충전
                        if ($item->method == "CU현금") {
                            // 오늘 CU현금 타입 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, true);
                            $obj = [
                                "idx"    => $amount_obj->idx,
                                "amount" => $amount_obj->amount,
                                "type"   => "오프라인",

                            ];
                            array_push($amount_objs[$this->TOD_CU_CASH], $obj);

                            $amount_types[$this->TOD_CU_CASH] += $obj["amount"];
                            $cu_charge_amount += $obj["amount"];

                            if ($obj["amount"] > 0) {
                                array_push($off_amount_objs, $obj);
                            }
                        }
                    }
                } else {
                    // 이전 날짜
                    if (in_array($item->method, $card_types)) {
                        // 이전 카드 타입 충전
                        if ($item->type == "온라인") {
                            // 이전 카드 타입 온라인 충전

                            $amount_obj = $this->getAmountObject($this->pconn, $item, false);

                            $obj = [
                                "idx"     => $amount_obj->idx,
                                "amount"  => $amount_obj->amount,
                                "type"    => "온라인",
                                "method"  => $amount_obj->method,
                                "imp_uid" => $amount_obj->imp_uid,
                            ];

                            array_push($amount_objs[$this->BEF_ON_CARD], $obj);
                            $amount_types[$this->BEF_ON_CARD] += $obj["amount"];
                            $on_charge_amount += $obj["amount"];

                            // 0 이상인 온라인 환불 가능 목록으로 빼내기
                            if ($obj["amount"] > 0) {
                                array_push($on_amount_objs, $obj);
                            }
                        } else {
                            // 이전 카드 타입 오프라인 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, true);
                            $obj = [
                                "idx"    => $amount_obj->idx,
                                "amount" => $amount_obj->amount,
                                "type"   => "오프라인",
                            ];
                            array_push($amount_objs[$this->BEF_OFF_CARD], $obj);

                            $amount_types[$this->BEF_OFF_CARD] += $obj["amount"];
                            $off_charge_amount += $obj["amount"];

                            if ($obj["amount"] > 0) {
                                array_push($off_amount_objs, $obj);
                            }
                        }
                    } else if (in_array($item->method, $cash_types)) {
                        // 이전 현금 타입 충전
                        if ($item->type == "온라인") {
                            // 이전 현금 타입 온라인 충전
                            // $amount_types[3] += $item->amount;
                            // $on_charge_amount += $item->amount;
                            $amount_obj = $this->getAmountObject($this->pconn, $item, false);
                            $obj = [
                                "idx"     => $amount_obj->idx,
                                "amount"  => $amount_obj->amount,
                                "type"    => "온라인",
                                "method"  => $amount_obj->method,
                                "imp_uid" => $amount_obj->imp_uid,
                            ];
                            array_push($amount_objs[$this->BEF_ON_CASH], $obj);
                            $amount_types[$this->BEF_ON_CASH] += $obj["amount"];
                            $on_charge_amount += $obj["amount"];
                            // 0 이상인 온라인 환불 가능 목록으로 빼내기
                            if ($obj["amount"] > 0) {
                                array_push($on_amount_objs, $obj);

                            }
                        } else {
                            // 이전 현금 타입 오프라인 충전 *

                            $amount_obj = $this->getAmountObject($this->pconn, $item, true);
                            $obj = [
                                "idx"    => $amount_obj->idx,
                                "amount" => $amount_obj->amount,
                                "type"   => "오프라인",
                            ];
                            // print_r($obj);
                            array_push($amount_objs[$this->BEF_OFF_CASH], $obj);
                            $amount_types[$this->BEF_OFF_CASH] += $obj["amount"];
                            $off_charge_amount += $obj["amount"];

                            if ($obj["amount"] > 0) {
                                array_push($off_amount_objs, $obj);
                            }
                        }
                    } else if (in_array($item->method, $cu_types)) {
                        // 이전 CU 타입 충전
                        if ($item->method == "CU현금") {
                            // 이전 CU카드 타입 충전
                            $amount_obj = $this->getAmountObject($this->pconn, $item, true);
                            $obj = [
                                "idx"    => $amount_obj->idx,
                                "amount" => $amount_obj->amount,
                                "type"   => "오프라인",

                            ];
                            array_push($amount_objs[$this->BEF_CU_CASH], $obj);

                            $amount_types[$this->BEF_CU_CASH] += $obj["amount"];
                            $cu_charge_amount += $obj["amount"];

                            if ($obj["amount"] > 0) {
                                array_push($off_amount_objs, $obj);
                            }
                        }
                    }
                }
            } else if ($item->action == "환불") {
                // 환불
                if ($item->amount > 0 && isset($item->market_seller_idx)) {
                    // 상품 환불
                    $use_amount += $item->amount;
                } else if ($item->amount > 0 && ! isset($item->market_seller_idx) && isset($item->order_info_idx)) {
                    // CU 상품 환불
                    $use_amount += $item->amount;
                } else {
                    // 마일리지 환불
                    if ($item->type == "온라인" && isset($item->cash_log_idx) && ! isset($item->market_seller_idx)) {
                        $on_refund_amount += $item->amount;
                    } else if ($item->type == "오프라인" && isset($item->cash_log_idx) && ! isset($item->market_seller_idx) && ! in_array($item->method, $cu_types)) {
                        $off_refund_amount += $item->amount;
                    } else if ($item->type == "오프라인" && isset($item->cash_log_idx) && ! isset($item->market_seller_idx) && in_array($item->method, $cu_types)) {
                        $cu_refund_amount += $item->amount;
                    } else if ($item->amount <= 0) {
                        $use_amount += $item->amount;
                    }
                }
            } else {
                // 결제
                $use_amount += $item->amount;
            }
        }


        $result_objs = [[], [], [], [], [], [], [], [], [], []];
        if ($use_amount < 0) {
            $use_amount = $use_amount * -1;
        }

        for ($i = 0; $i < count($amount_objs); $i++) {
            foreach ($amount_objs[$i] as $obj) {
                $use_amount = $use_amount - $obj["amount"];

                if ($use_amount < 0) {
                    $obj["amount"] = $use_amount * -1;
                    $use_amount = 0;
                } else {
                    $obj["amount"] = 0;
                }
                array_push($result_objs[$i], $obj);
            }
        }

        $response["data"] = $result_objs;

        return $response;
    }


    public function cuRefndTest($idx) {
        $refund = new RefundController();
        $bankData = [
            "bank"=>"농협",
            "holder"=>"이신우",
            "account"=>"24812397630"
        ];


        return $refund->refundCUTest($idx, false,$bankData);
    }
}
