<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;

include_once(app_path() . "/include_file/PHPExcel/func.php");
include_once(app_path() . "/includeFiles/func.php");
require_once app_path() . "/includeFiles/define.php";

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $isFinished = false;
    protected $parseInt = false;
    protected $userIdx = 0;
    protected $userInfo;
    private $rDevice = null;
    private $rUserId = null;
    private $rToken = null;

    public $request_sub = null;

    protected $conn = null;
    protected $conn_w = null;
    protected $pconn = null;
    protected $pconn_w = null;

    const WEB = 101;
    const IOS = 102;
    const ANDROID = 103;

    function __construct(Request $request)
    {
      $request_sub = $request;
      $action = app('request')->route()->getAction();
      $controller = class_basename($action['controller']);
      $controllerName = explode('@', $controller)[0];

      $this->rDevice = isset($request->device) ? $request->device : 101;
      $this->rUserId = $request->user_id;
      $this->rToken = $request->token;

      $this->init();

      $checkController = array(
        "OrderController", "CURefundController", "CUChargeController", "CUController", "CUOrderController", "PaymentController"
      );
      if(in_array($controllerName, $checkController)) {
        $this->conn = DB::connection(DB_DATABASE_R);
        $this->conn_w = DB::connection(DB_DATABASE_W);
        $this->pconn = DB::connection(DB_PAYMENT_R);
        $this->pconn_w = DB::connection(DB_PAYMENT_W);
      }
    }

    public function __destruct()
    {
    }


    public function init() {

      if($this->rDevice == self::WEB) {

//        session_start();
//        dd($_SESSION);
//        $gCV = $_SESSION["gCV"];
        if (isset($gCV)) $this->gCV = $gCV;
//        if (isset($_COOKIE['gCV'])) $this->gCV = array_decode($_COOKIE['gCV']);

        $this->userIdx = isset($this->gCV["user_id"]) ? $this->gCV["user_id"] : 0;
        $this->userInfo = isset($this->gCV) ? $this->gCV : null;
      } else {
        $this->userAuthCheck();
      }
    }

    function userAuthCheck() {

      $state = true;
      if (!isset($this->rDevice)) {
        return false;
      }
      if ($this->rDevice > 101 && !isset($this->rToken) && !isset($this->rUserId)) {
        return false;
      }
      if($state) {
        if ($this->checkToken()) {
          $this->userIdx = $this->rUserId;
          return $this->userIdx;
        } else {
          return $this->resultFail('LOGIN_FAIL', '올바른 접근이 아닙니다.');
        }
      }
    }

  public function userAuthCheckTemp($args)
  {
    @session_start();
    if (! isset($args['device'])) {
      return false;
    }

    if ($args['device'] > 101 && ! isset($args['token']) && ! isset($args['user_id'])) {
      return false;
    }

    $args = [
      'device'  => $args['device'],
      'token'   => $args['token'],
      'user_id' => $args['user_id'],
    ];

//    $request = $this->makeRequest($args);
//    $response = $this->setResponse();
    $login_data = $this->userIdx;
    if (gettype($login_data) == "array") {

      if ($login_data["code"] != 200) {
        return false;
      }
    } else {

      return $login_data;
    }
  }

    function checkToken() {
      $hash = hash("sha256", $this->rUserId . "_fleapop");

      if ($this->rToken == $hash) {
        return true;
      } else {
        return false;
      }
    }

    public function checkTokenDirect($token, $idx)
    {
      $hash = hash("sha256", $idx . "_fleapop");
      if ($token == $hash) {
        return true;
      } else {
        return false;
      }
    }

      public function outputJson($arr, $cross = false)
    {
        if ($this->parseInt) {
            $json = json_encode($arr, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);
        }
        else {
            $json = json_encode($arr, JSON_UNESCAPED_UNICODE);
        }
        $output = $json;

        return response()->json($arr);
    }



    protected function getParam($key, $def = null)
    {
      $var = isset($_POST[$key]) ? $_POST[$key] : ((isset($_GET[$key]) ? $_GET[$key] : $def));
      if ($def && strlen($var) == 0) $var = $def;
      return $var;
    }

    /**
     * POST를 우선으로한 전달된 파라메터 key=>value 배열 리턴
     */
    protected function getParameters()
    {
      return array_merge($_GET, $_POST);
    }

    protected function resultOk($data = null, $msg = null, $userInfo = null)
    {
        $result = array('result' => true, 'data' => $data, 'msg' => $msg, 'info' => $userInfo);
        return $this->outputJson($result);
    }

    protected function resultFail($fcode = 'F1', $msg = '', $extend_data = array())
    {
        return $this->outputJson(array('result' => false, 'fail' => array_merge(array('code' => $fcode, 'msg' => $msg), $extend_data)));
    }

    protected function isRequired ($required, $params) {
      $result = false;
      foreach ($required as $r){
        if(!isset($params[$r]) || $params[$r] == ''){
          return false;
        } else {
          $result = true;
        }
      }
      return $result;
    }

  public function makeRequest($args)
  {
    $request = new Request();
    $request->setMethod('POST');
    $request->request->add($args);

    return $request;
  }

  public function makeArgs()
  {
    @session_start();

    if (isset($_SESSION["user"]) && ! empty($_SESSION["user"])) {
      if (isset($_SESSION["user"]["token"]) && isset($_SESSION["user"]["user_id"])) {
        $args = [
          'device'  => self::WEB,
          'user_id' => $_SESSION['user']['user_id'],
          "token"   => $_SESSION['user']['token'],
        ];
        return $args;
      } else {
        $args = [
          'device'  => self::WEB,
          'user_id' => NULL,
          "token"   => NULL,
        ];

        return $args;
      }


    } else {

      $args = [
        'device'  => self::WEB,
        'user_id' => NULL,
        "token"   => NULL,
      ];

      return $args;

    }
  }

  public function setResponse()
  {
    $response = ["success" => true, "msg" => "", "code" => 200];

    return $response;
  }

  public function loginCheck($response, $request)
  {
    @session_start();

    if (! $request->filled(["device"])) {

      $response = $this->emptyResponse($response);
      return $response;
    }

    if ($request->device == self::WEB) {

      if (isset($_SESSION)) {

        if (isset($_SESSION['user']) && ! empty($_SESSION['user'])) {
          if (@isset($_SESSION['user']['loggedin']) && $_SESSION["user"]["loggedin"]) {
            $user_id = $_SESSION["user"]["user_id"];
            $token = $_SESSION["user"]["token"];
          } else {
            $response = $this->requiredLoginResponse($response);
            return $response;
          }
        } else {
          $response = $this->requiredLoginResponse($response);
          return $response;
        }
      } else {
        $response = $this->emptyResponse($response);
        return $response;
      }

    } else {
      if (! $request->filled(["user_id", "token"])) {
        $response = $this->emptyResponse($response);
        return $response;
      } else {
        $user_id = $request->user_id;
        $token = $request->token;
      }
    }


    if ($this->checkToken($token, $user_id)) {
      return $user_id;
    } else {
      $response = $this->errorResponse(300, "올바른 접근이 아닙니다.");
      return $response;
    }
  }

  public function requiredLoginResponse($response)
  {
    $response["success"] = false;
    $response["msg"] = "로그인이 필요한 항목입니다.";
    $response["code"] = 300;
    return $response;
  }

  public function emptyResponse($response)
  {
    $response["success"] = false;
    $response["msg"] = "필수입력 정보가 누락되었습니다.";
    $response["code"] = 500;

    return $response;
  }

  public function checkData($data, $is_db_obj = true)
  {
    if ($is_db_obj) {
      if ($data->count() > 0) {
        return true;
      } else {
        return false;
      }
    }

    return isset($data) ? true : false;
  }

  public function errorToken($response)
  {
    $response["success"] = false;
    $response["msg"] = "올바른 접근이 아닙니다.";
    $response["code"] = 600;

    return $response;
  }

  public function errorResponse($code = 300, $msg = "")
  {
    $response["success"] = false;
    $response["msg"] = $msg;
    $response["code"] = $code;

    return $response;
  }

}

