<?php


namespace App\Http\Controllers;


use App\Helper\CExhibitionHelper;
use App\Http\dao\CBaseDAO;
use App\Http\dao\CExhibitionDAO;

class ExhibitionController extends Controller
{
  public function getExhibitionToday() {
    $dao = new CExhibitionDAO();
    $res = $dao->getExhibitionToday();
    return $this->resultOk($res);
  }

  public function getExhibitionTab() {
    $dao = new CExhibitionDAO();
    $res = $dao->getExhibitionTab();
    return $this->resultOk($res);
  }

  public function getExhibitionGoodsList() {
    $page = $this->getParam("page", 1);
    $sort = $this->getParam("sort");
    $exhibition_id = $this->getParam("exhibition_id");
    $perPage = 40;
    $dao = new CExhibitionDAO();
    $res = $dao->getExhibitionGoodsList($exhibition_id, $page, $perPage, $sort);

    $lastPage = ceil($res["cnt"] / 40);
    $response = [
      "total" => $res["cnt"],
      "data" => $res["list"],
      "current_page" => (int)$page,
      "last_page" => $lastPage,
      "profile" =>  $res["profile"]
    ];
    return $this->resultOk($response);
  }

  public function getExhibitionBrand($device = 101, $user_id = NULL, $token = NULL)
  {

    $user_id = $this->userAuthCheck(['device' => $device, 'user_id' => $user_id, 'token' => $token]);
    $dao = new CBaseDAO();
    $sort = "score";
    if (isset($_GET['sort'])) $sort = $_GET['sort'];
//    $pconn = DB::connection(DB_PAYMENT);

    $exhibition_where = "and (e.start_date <= now() and e.end_date >= now())";
    if (isset($_GET['exhibition_id'])) $exhibition_where = " and e.id = ".$_GET['exhibition_id'];
    $exhibitionSql = "select atm.sort_order , atm.name as menu_name, atm.exhibition_id , e.img  ,e.img_content  , e.start_date , e.end_date
                from app_top_menu atm
                inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true' and e.type = 'exhibition'
                where atm.display = 'show'
                and e.is_hidden = 'true'
                and e.deleted_at is null
                {$exhibition_where}
				order by e.id desc
				limit 1";
    $exhibitionRes = $dao->selectQuery($exhibitionSql, "pay");

    if (!isset($exhibitionRes[0])) {
      $response = [
        "result" => false
      ];
      return $response;
    }
    $startDate = $exhibitionRes[0]->start_date;
    $start_date = date("Y-m-d", strtotime($startDate)) . " 00:00:00";
    $sDataWhere = date("Y-m-d", strtotime($startDate)) >= date("Y-m-d"). "00:00:00" ? "2020-06-01 00:00:00" : $start_date;

    $sortWhere = $sort == "score" ? "order by score desc" : "order by st.name asc";
    $sql = "select st.idx , st.name as seller_name, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image
                from fp_db.seller_tbl st
                inner join fp_db.like_tbl lt on lt.table_idx =st.idx and lt.table_name = 'seller_tbl'
                where st.display_state = 'show' and st.delete_date is null
                and lt.table_name = 'seller_tbl'
                and lt.user_idx = '$user_id'
                ";

    $sqlList = "select @rownum:=@rownum+1 as rank_num, master.* from (
                    select  st.idx,st.name as seller_name,st.introduce
                    , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image
                    , case when lt.idx is null then 0 else 1 end as is_like_state
                    , st.like_count
                from fp_db.seller_tbl as st
                inner join (
                SELECT goods.seller_id, (ifnull(orders.score,0) + sum(goods.view * 0.25) + sum(goods.like_count * 0.25)) as score
                        FROM fp_pay.goods goods
                        left join (SELECT ifnull((SUM(ifnull(od.quantity,0)) * 0.25 + SUM(ifnull(od.total_price,0)) * 0.25), 0) as score, seller_id  FROM fp_pay.order_detail AS od
                                    where created_at >= '{$sDataWhere}'
                                    group by seller_id) as orders on goods.seller_id = orders.seller_id
                        WHERE created_at >= '2020-06-01 00:00:00'
                        GROUP BY goods.seller_id ORDER BY score DESC
                ) as sub on st.idx = sub.seller_id and st.display_state = 'show' and st.delete_date is null
                inner join fp_pay.exhibition_seller as es on es.seller_id = st.idx and es.exhibition_id = {$exhibitionRes[0]->exhibition_id} and es.approval_state = 1
                left join fp_db.like_tbl lt on st.idx =lt.table_idx and table_name = 'seller_tbl' and lt.user_idx = '$user_id'
                where st.display_state = 'show' and st.delete_date is null
                {$sortWhere}
                ) as master
                ,(SELECT @rownum := 0) r";

//        $likeRes = $this->pconn->select($sql);
    $ListRes = $dao->selectQuery($sqlList, "pay");
//dd($sqlList);
    $result = [
//            "like_list" => $likeRes,
      "all_list"  => $ListRes,
      "result" => true
    ];
    return $this->resultOk($result);

  }

  public function getExhibitionRepMenu ($exhibition_id = null) {

    $exhibitionRep = new CExhibitionHelper();
    $erRes = $exhibitionRep->getExhibitionRepMenu();
    return $this->resultOk($erRes);
  }

  public function getExhibitionRepMenus () {

    $exhibitionRep = new CExhibitionHelper();
    $erRes = $exhibitionRep->getExhibitionRepMenus();
    return $this->resultOk($erRes);
  }

  public function getExhibitionList () {
    $sql = "select * from exhibition e 
              where is_hidden = 'true'
              and deleted_at is NULL  and e.type = 'exhibition'
              and (start_date <= now() and end_date >= now())
              order by view_order desc, id desc";
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "pay");
    return $this->resultOk($res);
  }
}