<?php

namespace App\Http\Controllers;

//include_once(app_path() . "/include_file/func.php");

use App\Events\LMPayChargeByBankAccount;
use App\Helper\CUserHelper;
use App\Http\dao\CBaseDAO;
use App\Http\dao\CUserDAO;
use App\Lmpay\ChargeController;
use App\Models\PaymentLog;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Image;
use Session;
use Storage;
use App\Lmpay\RefundController;
use App\Http\Controllers\PaymentController;

class ViewPaymentController extends Controller
{
//    private $conn;
//    private $conn_w;

    public $var = [
        "TAB_MENU"          => "LM",
        "TITLE"             => "",
        "MAIN_MENU"         => "",
        "HEADER_TYPE"       => "",
        "IS_HAVE_SUB_TITLE" => false,
        "SUB_HEADER_TITLE"  => [],
        "SUB_HEADER_URL"    => [],
        "SUB_HEADER_INDEX"  => 0,
    ];

    private $LMPAY_SUB_HEADER_TITLE = ["마이페이", "CU상품구매", "충전하기", "환불요청", "충전방법", "이용안내", "가맹점안내"];
    private $LMPAY_SUB_HEADER_URL   = [
        "/re/payment/home",
        "/re/payment/cu",
        "/re/payment/charge",
        "/re/payment/refund",
        "/re/how/w_pay_charge",
        "/re/how/w_pay_use",
        "/re/payment/paystore",
    ];


    public function viewPayHome()
    {
        @session_start();
        $this->var["TITLE"] = "러마페이";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = $this->LMPAY_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LMPAY_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 0;

        $args = $this->makeArgs();
        $args = $this->makeRequest($args);

        $con_payment = new PaymentController();
        $response = $con_payment->apiGetPaymentHome($args);

        $response = json_decode($response);
        $this->var["response"] = $response;


        $meta_data = [
            "meta_title"       => "러마페이 충전",
            "meta_image"       => "https://fleapop.co.kr/image/banner/pay_tm.jpg",
            "meta_description" => "러마페이 충전 및 안내",
        ];

        $this->var["meta_data"] = $meta_data;

//        dd($this->var["response"]);
        return view("remake.pay.pay_main", $this->var);
    }

    public function viewCUBarcode()
    {
        @session_start();
        $this->var["TITLE"] = "러마페이";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = $this->LMPAY_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LMPAY_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 1;

        $args = $this->makeArgs();
        $args["device"] = 101;
        $args = $this->makeRequest($args);

        $con_payment = new PaymentController();
        $response = $con_payment->apiGetCUBarcode($args);
        $response = json_decode($response);
        $this->var["response"] = $response;

        return view("remake.pay.pay_cu_barcode", $this->var);
    }

    public function viewPasswordUpdateStep1()
    {
        @session_start();
        $this->var["TITLE"] = "러마페이";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = $this->LMPAY_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LMPAY_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 0;

        $response["msg"] = "결제시 사용할 결제비밀번호를 입력해주세요.";
        $response["step"] = 1;
        $response["password"] = "";
        $this->var["response"] = $response;
        return view("remake.pay.pay_password", $this->var);
    }

    public function viewPasswordUpdateStep2(Request $request)
    {
        @session_start();

        $this->var["TITLE"] = "러마페이";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = $this->LMPAY_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LMPAY_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 0;
        $response["msg"] = "입력한 패스워드를 다시 확인합니다.";
        $response["step"] = 2;
        $response["password"] = $request->input_password;

        $this->var["response"] = $response;

        return view("remake.pay.pay_password", $this->var);
    }

    public function viewPayCharge($imp_uid = NULL)
    {
        @session_start();
        $this->var["TITLE"] = "러마페이";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = $this->LMPAY_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LMPAY_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 2;

        if (isset($imp_uid) || isset($_GET['imp_uid'])) {
            $args = $this->makeArgs();
            $args["device"] = 101;

            if (isset($imp_uid)) {
                $args["imp_uid"] = $imp_uid;
            } else if (isset($_GET['imp_uid'])) {
                $args["imp_uid"] = $_GET['imp_uid'];
            }

            $request = $this->makeRequest($args);
            $con_payment = new PaymentController();
            $response = $con_payment->apiSetChargeSubmit($request);
            $response = json_decode($response);

            if ($response->success) {
                $url = "/re/payment/history/detail/charge/" . $response->data->cash_log_idx;
                return redirect($url);
            } else {
                // TODO 결제 에러 처리
            }
        } else {
            $args = $this->makeArgs();
            $args["device"] = 101;
            $request = $this->makeRequest($args);
            $con_payment = new PaymentController();
            $response = $con_payment->apiGetBalance($request);
            $response = json_decode($response);

            if ($response->success) {
                $this->var["balance"] = number_format($response->data->balance);
            } else {
                $this->var["balance"] = 0;
            }
        }

        $args = $this->makeArgs();
        $args["device"] = 101;
        $request = $this->makeRequest($args);
        $con_payment = new PaymentController();
        $dd = new PaymentController();
        $response = $con_payment->apiGetBalance($request);

        $response = json_decode($response);

        if ($response->success) {
            $this->var["balance"] = number_format($response->data->balance);
        } else {
            $this->var["balance"] = 0;
        }

        $login_data = $this->loginCheck($response, $request);

        $user_id = 0;
        if (gettype($login_data) != "array") {
            $user_id = $login_data;
            $args = $this->makeArgs();
            $args["user_idx"] = $user_id;
            $request = $this->makeRequest($args);
            $con_user = new UserController();
            $user_response = $con_user->apiGetUserInfo($request);
            $address = $user_response["data"]->shipping_address_1 . " " . $user_response["data"]->shipping_address_2;
            $user_response["data"]->address = $address;
            $this->var["user_data"] = $user_response["data"];
        } else {
            return redirect('/re/login');
        }

        $this->var["IAMPORT_ID"] = env('IAMPORT_ID', '');
        $this->var["m_redirect_url"] = env('APP_URL', '');

        return view("remake.pay.pay_charge", $this->var);
    }


    public function viewPayRefund()
    {
        @session_start();

        $response = $this->setResponse();

        $args = $this->makeArgs();
        $args["device"] = 101;
        $request = $this->makeRequest($args);
        $login_data = $this->loginCheck($response, $request);

        $this->var["TITLE"] = "러마페이";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = $this->LMPAY_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LMPAY_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 3;

        $args = $this->makeArgs();
        $args = $this->makeRequest($args);

        $con_payment = new PaymentController();
        $response = $con_payment->apiRefund("view", $args);
        $response = json_decode($response);

        $this->var["response"] = $response;

        if ($response->data->trans_amount > 0 && ! $response->data->have_refund_account) {
            return redirect('/re/payment/refund/bank/set');
        }

        return view("remake.pay.pay_refund", $this->var);
    }

    public function viewPayHistoryList()
    {
        @session_start();
        $this->var["TITLE"] = "러마페이";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = $this->LMPAY_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LMPAY_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 4;
        $this->var["TAB"] = "lm";

        return view("remake.mypage.lm.history_main", $this->var);

    }

    public function viewPayHistoryDetail($type, $idx)
    {
        @session_start();
        $this->var["TITLE"] = "러마페이";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = $this->LMPAY_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LMPAY_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 4;

        $args = $this->makeArgs();
        $args = $this->makeRequest($args);
        $args["type"] = $type;
        $args["idx"] = $idx;

        $con_payment = new PaymentController();
        $response = $con_payment->apiGetPayHistoryDetail($args);
        $response = json_decode($response);
        $this->var["response"] = $response;

        // 러마페이 가상계좌 충전시 안내 알림톡 발송
        if ($response->success && $type === 'charge' && $response->data->method === '가상계좌') {
            event(new LMPayChargeByBankAccount($response->data));
        }

        switch ($type) {
            case "charge" :
                return view("remake/pay.pay_charge_account", $this->var);
            case "buy" :
                return view("remake.mypage.lm.history_detail", $this->var);
        }
    }

    public function viewPayDeliveryList()
    {
        @session_start();
        $this->var["TITLE"] = "주문/배송";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = ["구매내역", "주문/배송", "교환/환불"];
        $this->var["SUB_HEADER_URL"] = ["/re/user/order/history", "/re/user/ship/history", "/re/user/order/cs/list"];
        $this->var["SUB_HEADER_INDEX"] = 1;
        $this->var["TAB_MENU"] = "USER";

        return view("remake.mypage.lm.delivery", $this->var);

    }

    public function viewRefundBankSet()
    {
        @session_start();
        $this->var["TITLE"] = "러마페이";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = $this->LMPAY_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LMPAY_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 3;

        return view("remake.pay.pay_refund_account", $this->var);
    }

    public function viewRefundSuccess()
    {
        $this->var["TITLE"] = "러마페이";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = $this->LMPAY_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LMPAY_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 3;

        return view("remake.pay.pay_refund_success", $this->var);
    }


    public function viewAppPaymentCharge($pay_type, $amount, $token, $user_idx, $device)
    {

        if (!$this->checkToken($token, $user_idx)) {
            echo "<script>alert('올바른 접근이 아닙니다.');</script>";
        }

//        $con_user = new UserController();
      $dao = new CUserDAO();

        $user_data = $dao->getUserInfo($user_idx)[0];

        if(IS_TEST) {
          $appUrl = "https://lmapitest.fleapop.co.kr";
        } else {
          $appUrl = "https://lmapi.fleapop.co.kr";
        }

        $data = [
            "pg"             => "kcp" . ($pay_type == "payco") ? '.IP02U' : '',
            "pay_type"       => $pay_type,
            "amount"         => $amount,
            "email_account"  => $user_data->email_account,
            "shipping_name"  => $user_data->shipping_name,
            "address"        => $user_data->shipping_address_1 . " " . $user_data->shipping_address_2,
            "zip_code"       => $user_data->shipping_zip_code,
            "phone"          => $user_data->shipping_phone,
            "user_idx"       => $user_idx,
            "token"          => $token,
            "IAMPORT_ID"     => env("IAMPORT_ID", ''),
            "m_redirect_url" => env('APP_URL', ''),
        ];

        if ($device == 103) {
            return view("remake.pay.app_payment_charge", $data);
        } else {
            return view("remake.pay.app_ios_payment_charge", $data);
        }

    }


    public function viewAppPaymentComplete($user_id, $token)
    {
        $response = ["success" => true, "msg" => "", "code" => 200];

        $imp_uid = $_GET['imp_uid'];
        $merchant_uid = $_GET['merchant_uid'];
        $imp_success = $_GET['imp_success'];

        if (! isset($imp_uid) && ! isset($merchant_uid) && ! isset($imp_success)) {
            $response["success"] = false;
            $response["msg"] = "필수입력사항이 누락되었습니다.";
            $response["code"] = 300;
        } else {

            PaymentLog::setLog($imp_uid, "CHR");
            if ($imp_success) {
                $args = $this->makeArgs();
                $args = $this->makeRequest($args);

                $args["token"] = $token;
                $args["user_id"] = $user_id;
                $args["imp_uid"] = $imp_uid;

//                $con_payment = new PaymentController();
                $pay_response = $this->apiSetAppChargeSubmit($args);
                Log::info("#### apiSetAppChargeSubmit #### - ".$pay_response);
                $cashLogIdx = json_decode($pay_response)->data->cash_log_idx;

                $dao = new CBaseDAO();

                $cash_res = $dao->selectQuery("select * from cash_log where idx = {$cashLogIdx} and delete_date is null","pay_w");
//                  $cash_res = $this->pconn->table("cash_log")
//                    ->where("idx", "=", json_decode($pay_response)->data->cash_log_idx)
//                    ->whereNull("delete_date")
//                    ->get();

                  $cashRes = $cash_res[0];

                  if ($cashRes->action == "입금" && $cashRes->method == "가상계좌") {

                    $responseTemp = $this->setResponse();

                    $responseTemp = $this->apiGetPayHistoryDetail_Charge($cashRes, $responseTemp);
                    $responseTemp = json_decode($responseTemp);
                    // 러마페이 가상계좌 충전시 안내 알림톡 발송
                      event(new LMPayChargeByBankAccount($responseTemp->data));
                  }

                return view("remake.pay.app_payment_result", ["response" => $pay_response]);
            } else {
                $response["success"] = false;
                $response["msg"] = "거래에 실패했습니다.";
                $response["code"] = 300;
            }

        }

        return view("remake.pay.app_payment_result", ["response" => $response]);
    }
  private function apiGetPayHistoryDetail_Charge($cash_res, $response)
  {
    $data = [
      "type"           => $cash_res->type,
      "method"         => $cash_res->method,
      "state"          => $cash_res->state,
      "amount"         => number_format($cash_res->amount),
      "sender"         => $cash_res->sender,
      "vbank_num"      => $cash_res->vbank_num,
      "vbank_name"     => $cash_res->vbank_name,
      "vbank_date"     => $cash_res->vbank_date,
      "receipt_url"    => $cash_res->receipt_url,
      "result_msg"     => "",
      "result_sub_msg" => "",
      "appley_num"     => $cash_res->appy_num,
      "insert_date"    => $cash_res->insert_date,
      'user_idx'       => $cash_res->user_idx,
    ];

    if ($data["state"] == "기타") {
      $data["state"] = "결제실패";
    }

    $data["vbank_date"] = date("Y-m-d H:i:s", $data["vbank_date"]);

    switch ($data["state"]) {
      case "결제됨" :
      {
        $data["result_msg"] = "러마페이 충전이 완료 되었습니다.";
        break;
      }
      case "결제대기" :
      {
        $data["result_msg"] = "입금기한 내에 아래 계좌로 입금하시면 결제가 완료됩니다.";
        $data["result_sub_msg"] = "*입금자명과 실제 입금자가 같아야 합니다.";
        break;
      }
      default :
      {
        $data["result_msg"] = "충전에 실패 하였습니다. 다시 시도해주세요.";
        break;
      }
    }

    $response["data"] = $data;
    return json_encode($response);
  }

  private function apiSetAppChargeSubmit(Request $request)
  {
    @session_start();
    $response = $this->setResponse();

    try {

      if (! $this->checkTokenDirect($request->token, $request->user_id)) {

        $response = $this->setResponse(300, "올바른 접근이 아닙니다.");
        return json_encode($response);
      }

      $user_id = $request->user_id;
      if (! $request->filled(["imp_uid"])) {

        $response = $this->emptyResponse($response);
        return json_encode($response);
      }
      $charge = new ChargeController();
      $res = $charge->online($user_id, $request->input('imp_uid'));


      if ($res['success']) {
        $data = explode(",", $res['data']);
        $response["data"]["cash_log_idx"] = $data[0];
        $response["data"]["charge_success"] = $data[1];
      } else {
        if ($res['code'] == 423) {
          return json_encode($response);
        }
        $response = $this->errorResponse(300, "해당 거래건을 처리하는데 실패했습니다. E1");
      }

    } catch (\Illuminate\Database\QueryException $e) {
      $response = $this->errorResponse(300, "해당 거래건을 처리하는데 실패했습니다.ER3");

    } catch (PDOException $e) {
      $response = $this->errorResponse(300, "해당 거래건을 처리하는데 실패했습니다.ER3");
    }


    return json_encode($response);
  }


    public function viewPaystore()
    {
        $this->var["TITLE"] = "러마페이";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = $this->LMPAY_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LMPAY_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 6;

        return view("remake.pay.pay_store", $this->var);
    }

    public function viewHowCharge()
    {
        $this->var["TITLE"] = "러마페이";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = $this->LMPAY_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LMPAY_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 4;

        $meta_data = [
            "meta_title"       => "러마페이 충전방법 안내",
            "meta_image"       => "https://fleapop.co.kr/image/banner/pay_tm.jpg",
            "meta_description" => "러마페이 충전방법 안내",
            "meta_url"         => "https://fleapop.co.kr/re/payment/how/w_pay_charge",
        ];

        $this->var["meta_data"] = $meta_data;

        return view("remake.how.w_pay_charge", $this->var);
    }

    public function viewHowUse()
    {
        $this->var["TITLE"] = "러마페이";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = $this->LMPAY_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LMPAY_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 5;

        return view("remake.how.w_pay_use", $this->var);
    }

    public function viewDelivery($order_info_idx = 0, $type = "")
    {
        $args = $this->makeArgs();
        $args["order_info_idx"] = $order_info_idx;
        $request = $this->makeRequest($args);

        $con_payment = new PaymentController();
        $response = $con_payment->apiGetShippingData($request);
        $response = json_decode($response);

        $data = [
            "type"  => $type,
            "sdata" => $response,
        ];

        return view("remake.lm.delivery_tracking", $data);
    }
}
