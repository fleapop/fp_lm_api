<?php


namespace App\Http\Controllers;


use App\Http\dao\CBaseDAO;
use App\Http\dao\CSellerDAO;
use App\Http\dao\CUserDAO;

class SellerController extends Controller
{
  /**
   * 브랜드 상품 리스트
   * @return \Illuminate\Http\JsonResponse
   */
  public function getGoodsList() {
    $page = $this->getParam("page", 1);
    $sort = $this->getParam("sort");
    $category = $this->getParam("category");
    $seller_id = $this->getParam("seller_id");
    $lm_order = $this->getParam("lm_order");
    $exhibition_id = $this->getParam("exhibition_id", null);
    $perPage = 40;
    $dao = new CSellerDAO();
    $res = $dao->getGoodsList($this->userIdx,$seller_id,$sort, $page, $perPage, $category, $lm_order, $exhibition_id);

    $last_page = ceil($res["cnt"] / $perPage);
    $response = [
      "current_page" => (int)$page,
      "data"         => $res["list"],
      "last_page"    => $last_page,
      "total"        => $res["cnt"],
    ];
    return $this->resultOk($response);
  }
  /**
   * 스토어 추천 브랜드 랜덤 리스트
   */
  public function getSellerRandomList() {
    $dao = new CSellerDAO();
    $res = $dao->getSellerRandomList();
    return $this->resultOk($res);
  }

  /**
   * 브랜드 탭 찜한 브랜드 리스트 & 전체 브랜드
   * @return \Illuminate\Http\JsonResponse
   */
  public function getBrandInfo() {
    $dao = new CSellerDAO();
    $res = $dao->getBrandInfo($this->userIdx);
    return $this->resultOk($res);
  }

  /**
   * 찜한 브랜드 리스트
   * @return \Illuminate\Http\JsonResponse
   */
  public function getBrandLikeList() {
    $dao = new CSellerDAO();
    $res = $dao->getBrandLikeList($this->userIdx);
    return $this->resultOk($res);
  }

  public function getSnapList () {
    $page = $this->getParam("page", 1);
    $seller_id = $this->getParam("seller_id");
    $perPage = 40;
    $dao = new CSellerDAO();
    $res = $dao->getBrandSnapList($seller_id,$page,$perPage);
    return $this->resultOk($res);
  }

  public function getBrandProfile() {
    $seller_id = $this->getParam("seller_id");
    $user_idx = $this->userIdx;
    $dao = new CSellerDAO();
    $res = $dao->getBrandProfile($seller_id, $user_idx);
    return $this->resultOk(arrayToJson($res));
  }

  public function getSnapDetail () {
    $seller_id = $this->getParam("seller_id");
    $snap_idx = $this->getParam("snap_idx");

    $dao = new CSellerDAO();
    $res = $dao->getBrandSnapDetail($seller_id,$snap_idx);
    return $this->resultOk($res);
  }

  public function getLdfitList () {
    $page = $this->getParam("page", 1);
    $category = $this->getParam("category");
    $order = $this->getParam("order");
    $user_idx = $this->getParam("t_user_idx");
    $seller_id = $this->getParam("seller_id");

    $service_type = "fit";

    if(!$seller_id) return $this->resultFail("NO_REQUIRED","필수입력 정보가 누락되었습니다.");

    $dao = new CSellerDAO();
    $perPage = 40;
    $limit = $dao->getLimit($page, $perPage);
    $resTemp = $dao->fitList($category, $order, $user_idx, $limit, $seller_id);
    $res = $resTemp["data"];
    foreach ($res as $item) {

          // 러덕핏
          $sql = "select *
                from cm_ld_fit clf 
                where timeline_idx = {$item->idx}
                and deleted_at is null";
          $resFit = $dao->selectQuery($sql, "db");

          $item->ld_fit = $resFit;

          // 후기핏
          $sql = "select *
                from postscript_tbl pt
                where pt.service_idx = {$item->idx}
                and pt.deleted_at is null";
          $resPostscript = $dao->selectQuery($sql, "db");
          $item->postscript = null;
          if (isset($resPostscript[0])) {
            $item->postscript = $resPostscript;
            foreach ($resPostscript as $orderItem) {
              $orderSql = "select st.name , g2.title ,od.origin_price ,od.promotion_price ,od.total_price , GROUP_CONCAT(o2.value SEPARATOR ', ') as option_info
                          , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image
                          , g2.id as goods_id, st.idx as seller_id
                          , if(count(lt.idx) > 0, 1, 0) as is_like
                          , case when g2.sub_category = '악세사리' then 'acc'
                            when g2.sub_category = '뷰티' then 'beauty'
                            else 'etc' end as option_type
                          , g2.imgs as goods_img
                          from order_detail od
                          inner join goods g2 on od.goods_id = g2.id
                          inner join fp_db.seller_tbl st on g2.seller_id = st.idx
                          inner join option_sku os on od.sku_id = os.sku_id
                          inner join `options` o2 on os.option_id = o2.id
                          left join fp_db.like_tbl lt on lt.table_name = 'goods' and lt.table_idx = g2.id and lt.user_idx = '{$user_idx}'
                          where od.id = {$orderItem->order_detail_id}";
              $orderRes = $dao->selectQuery($orderSql, "pay");
              $orderItem->order = isset($orderRes[0]) ? $orderRes[0] : null;
            }

          }

//      }
      $item->files = $this->getCommunityFiles($item->service_type, $item->idx);
      $replySql = "select ct.idx,ct.table_name ,ct.table_idx ,ct.comment ,ct.parent_idx ,ct.to_user_idx ,ct.from_user_idx ,list_date_view(ct.insert_date) as create_date, ct.user_tags 
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as from_nic_name, ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/',ul.profile_img), '') as from_profile_img
                , if(ul2.nic_name is null or REPLACE(ul2.nic_name, ' ', '') = '', ul2.shipping_name, ul2.nic_name) as to_nic_name, ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/',ul2.profile_img), '') as to_profile_img
                from comment_tbl ct 
                left join user_list ul on ct.from_user_idx = ul.user_id 
                left join user_list ul2 on ct.to_user_idx = ul2.user_id 
                where ct.table_name = 'cm_timeline' and ct.table_idx = {$item->idx} and ct.delete_date is null order by ct.idx desc";
      $resReply = $dao->selectQuery($replySql, "db");

      if (isset($resReply)) {
        foreach ($resReply as $item2) {
          if (isset($item2->user_tags) && $item2->user_tags != "") {
            $userTagSql = "select user_id, if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name from user_list ul where user_id in ({$item2->user_tags})";
            $resUserTags = $dao->selectQuery($userTagSql, "db");
            $item2->user_tags = $resUserTags;
          } else {
            $item2->user_tags = array();
          }
        }
        $item->replyList = $resReply;
        $item->replyCnt = count($resReply);
      } else {
        $item->replyList = null;
        $item->replyCnt = 0;
      }
    }
    $dao = new CUserDAO();
    $this->userInfo = $dao->getUserInfo($user_idx);
    $last_page = ceil((int)$resTemp["cnt"] / $perPage);
    $response = [
      "current_page" => (int)$page,
      "data" => $res,
      "last_page" => $last_page,
      "total" => $resTemp["cnt"]
    ];
    $nonInfo = json_decode("{}");
    return $this->resultOk($response, null,isset($this->userInfo[0]) ? $this->userInfo[0] : $nonInfo);
  }

  private function getCommunityFiles ($service_type, $service_idx) {
    if ($service_type == "answer") {
      $sql = "select ci.*, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket',img_path,img_name) as full_path
                , ifnull(count(ads.idx), 0) as answer_cnt
                from cm_imgs ci 
                left join fp_db.answer_decided_selectd ads on ci.service_idx = ads.service_idx and ci.idx = ads.img_idx 
                where ci.service_idx = {$service_idx}
                and ci.service_type = '{$service_type}'
                and ci.deleted_at is null
                group by ci.idx ";
    } else {
      $sql = "select *, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket',img_path,img_name) as full_path
                from cm_imgs ci 
                where service_idx = {$service_idx}
                and service_type = '{$service_type}'
                and deleted_at is null";
    }
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "db");
    return $res;
  }


}