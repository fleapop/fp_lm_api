<?php

namespace App\Http\Controllers\Remake;
require_once app_path() . "/include_file/iamport.php";
include_once(app_path() . "/include_file/refund_function.php");

// include (app_path()."/include_file/func.php");

use App\Http\Controllers\Controller;
use DB;
use Image;
use Session;
use Storage;


class ViewsController extends Controller
{
    public $var = [
        "TITLE"     => "",
        "MAIN_MENU" => "",
        "TAB_MENU"  => "",
    ];

    // 인덱스 - Default Method
    public function viewMaster()
    {

        return view("remake.master", $this->var);
    }

    // 인덱스 - Default Method
    public function viewSub_Header()
    {

        return view("remake.sub_header", $this->var);
    }

    // 로그인 퍼블
    public function viewLogin()
    {

        return view("remake.login.login", $this->var);
    }

    // 로그인 퍼블
    public function viewSignUp_Step_01()
    {

        return view("remake.login.signup_step_01", $this->var);
    }

    // 로그인 퍼블
    public function viewSignUp_Step_02()
    {

        return view("remake/login.signup_step_02", $this->var);
    }

    // 로그인 퍼블
    public function viewSignUp_Step_03()
    {

        return view("remake/login.signup_step_03", $this->var);
    }

    // 로그인 퍼블
    public function viewFind()
    {

        return view("remake/login.find", $this->var);
    }

    // 로그인 퍼블
    public function viewHelp_Center()
    {

        return view("remake/login.help_center", $this->var);
    }

    // 로그인 퍼블
    public function viewSuccess()
    {

        return view("remake/login.success", $this->var);
    }

    // 로그인 퍼블
    public function viewService()
    {

        return view("remake/login.service", $this->var);
    }

    // 로그인 퍼블
    public function viewAgree()
    {

        return view("remake/login.agree", $this->var);
    }

    // 로그인 퍼블
    public function viewAgree_Info()
    {

        return view("remake/login.agree_info", $this->var);
    }

    // 페이 퍼블
    public function viewPay_Main()
    {

        return view("remake/pay.pay_main", $this->var);
    }

    // 페이 퍼블
    public function viewPay_Barcode()
    {

        return view("remake/pay.pay_barcode", $this->var);
    }

    // 페이 퍼블
    public function viewPay_Password()
    {

        return view("remake/pay.pay_password", $this->var);
    }

    // 페이 퍼블
    public function viewPay_Charge()
    {

        return view("remake/pay.pay_charge", $this->var);
    }

    // 페이 퍼블
    public function viewPay_Charge_Account()
    {

        return view("remake/pay.pay_charge_account", $this->var);
    }

    // 페이 퍼블
    public function viewCharge_Card()
    {

        return view("remake/pay.pay_charge_card", $this->var);
    }

    // 페이 퍼블
    public function viewPay_Charge_Cancel()
    {

        return view("remake/pay.pay_charge_cancel", $this->var);
    }

    // 페이 퍼블
    public function viewPay_Refund()
    {

        return view("remake/pay.pay_refund", $this->var);
    }

    // 페이 퍼블
    public function viewPay_Refund_Account()
    {

        return view("remake/pay.pay_refund_account", $this->var);
    }

    // 페이 퍼블
    public function viewPay_Refund_Success()
    {

        return view("remake/pay.pay_refund_success", $this->var);
    }

    // 페이 퍼블
    public function viewPoint()
    {

        return view("remake.pay.point", $this->var);
    }

    // 페이 퍼블
    public function viewPay_way()
    {

        return view("remake.pay.pay_way", $this->var);
    }

    // 페이 퍼블
    public function viewPay_store()
    {

        return view("remake.pay.pay_store", $this->var);
    }

    // 페이 퍼블
    public function viewPay_cu()
    {

        return view("remake.pay.pay_cu", $this->var);
    }

    // 페이 퍼블
    public function viewPay_cu_barcode()
    {

        return view("remake.pay.pay_cu_barcode", $this->var);
    }

    // 티켓 퍼블
    public function viewTicket_Main()
    {

        return view("remake/ticket.ticket_main", $this->var);
    }

    // 티켓 퍼블
    public function viewTicket_None()
    {

        return view("remake/ticket.ticket_none", $this->var);
    }

    // 티켓 퍼블
    public function viewTicket_Location()
    {

        return view("remake/ticket.ticket_location", $this->var);
    }

    // 티켓 퍼블
    public function viewTicket_Select()
    {

        return view("remake/ticket.ticket_select", $this->var);
    }

    // 티켓 퍼블
    public function viewTicket_Success()
    {

        return view("remake/ticket.ticket_success", $this->var);
    }

    // 티켓 퍼블
    public function viewTicket_List()
    {

        return view("remake/ticket.ticket_list", $this->var);
    }

    // 티켓 퍼블
    public function viewTicket_Ticket()
    {

        return view("remake/ticket.ticket_ticket", $this->var);
    }

    // 티켓 퍼블
    public function viewTicket_Noti()
    {

        return view("remake/ticket.ticket_noti", $this->var);
    }

    // 티켓 퍼블
    public function viewTicket_Feed()
    {

        return view("remake/ticket.ticket_feed", $this->var);
    }

    // 티켓 퍼블
    public function viewTicket_Edit()
    {

        return view("remake/ticket.ticket_edit", $this->var);
    }

    // 티켓 퍼블
    public function viewTicket_Comment()
    {

        return view("remake/ticket.ticket_comment", $this->var);
    }

    // 마이페이지 퍼블
    public function viewMain_Feed()
    {

        return view("remake/mypage.main_feed", $this->var);
    }

    // 마이페이지 퍼블
    public function viewMy_Follow()
    {

        return view("remake/mypage.my_follow", $this->var);
    }

    // 마이페이지 퍼블
    public function viewMy_Like_List()
    {

        return view("remake/mypage.my_like_list", $this->var);
    }

    // 마이페이지 퍼블
    public function viewMy_Menu_Noti()
    {

        return view("remake/mypage.my_menu_noti", $this->var);
    }

    // 마이페이지 퍼블
    public function viewMy_Setting()
    {

        return view("remake/mypage.my_setting", $this->var);
    }

    // 마이페이지 퍼블
    public function viewMy_Notice()
    {

        return view("remake/mypage.my_notice", $this->var);
    }

    // 마이페이지 퍼블
    public function viewMy_Event_List()
    {

        return view("remake/mypage.my_event_list", $this->var);
    }

    // 마이페이지 퍼블
    public function viewEvent_List_Open()
    {

        return view("remake/mypage.event_list_open", $this->var);
    }

    // 마이페이지 퍼블
    public function viewCall_Center()
    {

        return view("remake.mypage.call_center", $this->var);
    }

    // mypage->lm
    public function viewHistory_Main()
    {

        return view("remake.mypage.lm.history_main", $this->var);
    }

    // mypage->lm
    public function viewHistory_Detail()
    {

        return view("remake.mypage.lm.history_detail", $this->var);
    }

    // mypage->store 교환/환불
    public function viewChange_Refund()
    {

        return view("remake/mypage/store.change_refund", $this->var);
    }

    //mypage->store 배송상세
    public function viewDetail()
    {

        return view("remake/mypage/store.detail", $this->var);
    }

    //mypage->store 구매내역 러마
    public function viewHistory_Lm()
    {

        return view("remake/mypage/store.history_lm", $this->var);
    }

    //mypage->store 구매내역 online
    public function viewHistory_Online()
    {

        return view("remake/order.history_online", $this->var);
    }

    //mypage->store 구매 성공
    public function viewSuccess_Order()
    {

        return view("remake/mypage/store.success_order", $this->var);
    }

    //mypage->store 교환/환불
    public function viewChange_Refund_Order()
    {

        return view("remake/mypage/store.change_refund_order", $this->var);
    }

    //mypage->store 쿠폰
    public function viewCoupon()
    {

        return view("remake/mypage/store.coupon", $this->var);
    }

    //FIX SW  mypage->store 문의하기
    public function viewQuestion()
    {

        return view("remake/mypage/store.question", $this->var);
    }

    //FIX SW
    //주문 문의 내역
    public function orderQuestion()
    {

        return view("remake/mypage/store/order_question", $this->var);
    }

    //상품 문의 내역
    public function productQuestion()
    {

        return view("remake/mypage/store/product_question", $this->var);
    }


    //mypage->history->delivery
    public function viewDelivery()
    {

        return view("remake/mypage/history.delivery", $this->var);
    }


    // 러블리마켓 퍼블
    public function viewLm_Market_Progress()
    {

        return view("remake/lm.lm_market_progress", $this->var);
    }

    // 러블리마켓 퍼블
    public function viewLm_Market_End()
    {

        return view("remake/lm.lm_market_end", $this->var);
    }

    // 러블리마켓 퍼블
    public function viewLm_Market_Intro()
    {

        return view("remake/lm.lm_market_intro", $this->var);
    }

    // 러블리마켓 퍼블
    public function viewLm_Market_Info()
    {

        return view("remake/lm.lm_market_info", $this->var);
    }

    // 러블리마켓 퍼블
    public function viewLm_Market_Seller()
    {

        return view("remake/lm.lm_market_seller", $this->var);
    }

    // 러블리마켓 퍼블
    public function viewLm_Market_Feed()
    {

        return view("remake/lm.lm_market_feed", $this->var);
    }

    // 러블리마켓 퍼블
    public function viewLm_Review()
    {

        return view("remake/lm.lm_review", $this->var);
    }

    // 러블리마켓 퍼블
    public function viewLm_Review_Content()
    {

        return view("remake/lm.lm_review_content", $this->var);
    }

    // 러블리마켓 퍼블
    public function viewLm_Tag_Review()
    {

        return view("remake/lm.lm_tag_review", $this->var);
    }

    // 러블리마켓 퍼블
    public function viewLm_Attend_Seller()
    {

        return view("remake/lm.lm_attend_seller", $this->var);
    }

    // 러블리마켓 퍼블
    public function viewLm_Attend_Detail()
    {

        return view("remake/lm.lm_attend_detail", $this->var);
    }

    // 러블리마켓 퍼블
    public function viewLm_Channel()
    {

        return view("remake/lm.lm_channel", $this->var);
    }

    // 러블리마켓 퍼블
    public function viewDelivery_Tracking()
    {

        return view("remake.lm.delivery_tracking", $this->var);
    }

    // 러블리마켓 퍼블
    public function viewLm_Feed_Detail()
    {

        return view("remake.lm.lm_feed_detail", $this->var);
    }

    // 플리팝 커뮤니티
    public function viewFleapop_Main()
    {

        return view("remake/fleapop.fleapop_main", $this->var);
    }

    // 플리팝 커뮤니티
    public function viewFleapop_Content_Image()
    {

        return view("remake/fleapop.fleapop_content_image", $this->var);
    }

    // 플리팝 커뮤니티
    public function viewFleapop_Review()
    {

        return view("remake/fleapop.fleapop_review", $this->var);
    }

    // 플리팝 커뮤니티
    public function viewFleapop_Magazine()
    {

        return view("remake/fleapop.fleapop_magazine", $this->var);
    }

    //스토어
    public function viewStore_Main()
    {

        return view("remake/store.store_main", $this->var);
    }

    //스토어
    public function viewStore_Order_Sheet()
    {

        return view("remake/store.store_order_sheet", $this->var);
    }

    //스토어
    public function viewStore_Check_List()
    {

        return view("remake/store.store_check_list", $this->var);
    }

    //스토어
    public function viewStore_Feed()
    {

        return view("remake/store.store_feed", $this->var);
    }

    //스토어
    public function viewStore_Feed_Detail()
    {

        return view("remake/store.store_feed_detail", $this->var);
    }

    //스토어
    public function viewStore_Goods_Question()
    {

        return view("remake/store.store_goods_question", $this->var);
    }

    //스토어
    public function viewStore_Question_Write()
    {

        return view("remake/store.store_question_write", $this->var);
    }

    //스토어
    public function viewStore_Delivery_Noti()
    {

        return view("remake/store.store_delivery_noti", $this->var);
    }

    //스토어 이벤트
    public function viewEvent_Main()
    {

        return view("remake/store.event_main", $this->var);
    }

    //스토어 이벤트
    public function viewEvent_Detail()
    {

        return view("remake/store.event_detail", $this->var);
    }

    //스토어 주문서
    public function viewSuccess_Order_Sheet()
    {

        return view("remake/store.success_order_sheet", $this->var);
    }

    /**
     * 온라인 스토어 > 상품상세 정보 > 배송정보
     *
     * @return \Illuminate\View\View
     */
    public function viewDelivery_Info()
    {
        $this->var['showFooter'] = false;

        return view('remake.store.notice', $this->var);
    }

    //스토어 안내
    public function viewNotice()
    {

        return view("remake/store.notice", $this->var);
    }

    //스토어 searchStore
    public function viewSearch()
    {

        return view("remake/store.search", $this->var);
    }

    //임시
    public function viewGoods()
    {

        return view("remake/store.goods", $this->var);

    }

    //order 비회원 주문
    public function viewNon_Member_Order()
    {

        return view("remake/order.non_member_order", $this->var);
    }

    //order 비회원 주문조회
    public function viewNon_Member_List()
    {

        return view("remake/order.non_member_list", $this->var);
    }

    //업로드
    public function viewUpload_Main()
    {

        return view("remake.upload.upload_main", $this->var);
    }

    //업로드
    public function viewUpload_Edit()
    {

        return view("remake.upload.upload_edit", $this->var);
    }

    //업로드
    public function viewUpload_Tag()
    {

        return view("remake.upload.upload_tag", $this->var);
    }

    //업로드
    public function viewUpload_Tag_Location()
    {

        return view("remake.upload.upload_tag_location", $this->var);
    }

    //업로드
    public function viewUpload_Survey()
    {

        return view("remake.upload.upload_survey", $this->var);
    }

    //업로드
    public function viewUpload_Choice()
    {

        return view("remake.upload.upload_choice", $this->var);
    }

    //how
    public function viewW_Pay_Charge()
    {

        return view("remake.how.w_pay_charge", $this->var);
    }

    //how
    public function viewW_Pay_Refund()
    {

        return view("remake.how.w_pay_refund", $this->var);
    }

    //how
    public function viewW_Pay_Use()
    {

        return view("remake.how.w_pay_use", $this->var);
    }

    //how
    public function viewW_Ticket()
    {

        return view("remake.how.w_ticket", $this->var);
    }

    //image capture
    public function viewImage_Capture()
    {

        return view("remake.image_capture", $this->var);
    }

    //seller_brand
    public function viewSeller_Brand()
    {

        return view("remake.brand.seller_brand", $this->var);
    }

    //front - 오류 메세지 page
    public function viewError()
    {

        return view("remake.front.error", $this->var);
    }

    //event
    public function viewEvent()
    {
        $this->var["TAB_MENU"] = "STORE";
        return view("remake.event", $this->var);
    }

    //eventSellerViewPage
    public function viewSeller_Event()
    {

        return view("remake.seller_event", $this->var);
    }
}
