<?php


namespace App\Http\Controllers;


use App\Models\Cart;
use App\Http\dao\CBaseDAO;
use App\Http\dao\CCartDAO;
use App\Http\dao\CGoodsDAO;
use App\Models\Seller;
use App\Models\Sku;
use App\Models\Stock;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

class CartController extends Controller
{
  public function getCartCount() {
    $user_id = $this->userIdx;
    $non_user_id = $this->getParam("non_user_id");

    $dao = new CCartDAO();
    $res = $dao->getCartCount($user_id, $non_user_id);
    return $this->resultOk($res);
  }

  public function getCartList() {
    $user_id = $this->userIdx;
    $non_user_id = $this->getParam("non_user_id");
    $dao = new CCartDAO();
    if ($user_id > 0 && $non_user_id > 0) {
      $sqlNonCart = "select c.*
                      from cart c 
                      where c.non_user_id = '{$non_user_id}' and c.deleted_at is null and c.user_id is null order by c.id desc";
      $non_cart = $dao->selectQuery($sqlNonCart, "pay");

      foreach ($non_cart as $non_item) {
        $sqlUserCart = "select * from cart where user_id = {$user_id} and sku_id = {$non_item->sku_id} and c.deleted_at is null";
        $user_cart = $dao->selectQuery($sqlUserCart, "pay");
        if (isset($user_cart)) {
          $dao->updateCart($user_cart["id"], $non_item["quantity"] + $user_cart["quantity"]);
        } else {
          $data = [
            'user_id'     => $user_id,
            'goods_id'    => $non_item->goods_id,
            'sku_id'      => $non_item->sku_id,
            'quantity'    => $non_item->quantity,
            '_NQ_created_at'    => "now()",
          ];
          $dao->insertQuery("cart",$data,"pay_w");
        }
        $dao->delCart($non_item["id"]);
      }
    }

    $where = "where c.deleted_at is null";
    if ($user_id == 0) {
      $where .= " and c.non_user_id = {$non_user_id}";
    } else {
      $where .= " and c.user_id = {$user_id}";
    }
    $sql = "select c.*, g2.id as goods_id, g2.imgs as goods_imgs, g2.title as goods_title
              , concat(GROUP_CONCAT(o.value SEPARATOR ', '),case when s.price > 0 then concat(' (옵션가 : ',FORMAT(s.price,0),'원)') else '' end) as option_value
            , case when s.price > 0 then concat('(옵션가 : ',s.price,'원)') else '' end as option_price_text
            , s.price as option_price
            , g2.price as goods_price
            , if(g2.use_discount = 1, IFNULL(g2.price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g2.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g2.price) AS promotion_price
            , IF(sp.idx is null, 0, 1) as is_hotdeal
            , g2.use_discount 
            , g2.stock_state
            , g2.type as goods_type
            , g2.main_category
            , g2.sub_category
            , g2.min_buy_count
            , g2.max_buy_count
            , g2.max_buy_person
            , st.name as seller_name
            , st.idx as seller_idx
            from cart c 
            inner join goods g2 on c.goods_id = g2.id and g2.display_state = 'show' and g2.deleted_at is null
            inner join option_sku os on c.sku_id = os.sku_id 
            inner join skus s on s.id = os.sku_id 
            LEFT  join `options` o on os.option_id = o.id 
            left join fp_db.seller_tbl st on g2.seller_id = st.idx
            left join goods_promotions sp on sp.goods_id = g2.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
            {$where}
            group by c.id";
    $res = $dao->selectQuery($sql, "pay");
    return $this->resultOk($res);
  }

  public function setCartAdd() {


    $goods_ids = explode(',',$this->getParam("goods_id"));
    $sku_ids = explode(',',$this->getParam("sku_id"));
    $quantities = explode(',',$this->getParam("quantity"));
    $non_user_id = $this->getParam("non_user_id");
    if (!$goods_ids) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }
    $required = array(
      "goods_id", "quantity"
    );
    $isReq = $this->isRequired($required, $this->getParameters());

    if (!$isReq) {
      return $this->resultFail("NO_REQUIRED", "필수 파라미터가 누락되었습니다. 상품과 갯수가 일치하지 않습니다.");
    }

    $data = [
      'user_id'     => null,
      'non_user_id' => null,
      'goods_id'    => null,
      'sku_id'      => null,
      'quantity'    => null,
    ];

    for ($i = 0; $i < count($sku_ids); $i++) {
      if ($sku_ids[$i] != "") {
        if (isset($this->userIdx)) {
          $data['user_id'] = $this->userIdx;
        }

        if (isset($non_user_id)) {
          $data['non_user_id'] = $non_user_id;
        }

        $data['goods_id'] = $goods_ids[0];
        $data['quantity'] = $quantities[$i];
        $goodsDao = new CGoodsDAO();
        $goods = $goodsDao->getGoods($goods_ids[0]);

        if (!$goods["goods"][0]->use_option) {
          $data['sku_id'] = $goods["skus"][0]["id"];
        } else {
          $data["sku_id"] = $sku_ids[$i];
        }

        try {
          $cartDao = new CCartDAO();
          $where = "where deleted_at is null ";

          if (isset($data['user_id'])) {
            $where .= " and user_id = " . $data['user_id'];
          } else if (isset($data['non_user_id'])) {
            $where .= " and non_user_id = " . $data['non_user_id'];
          }
          $where .= " and goods_id = " . $data['goods_id'];
          $where .= " and sku_id = " . $data['sku_id'];
          $resCartW = $cartDao->getCartWhere($where);

          $dao = new CBaseDAO();
          if (count($resCartW) > 0) {
            $quantity = $resCartW[0]->quantity + $data['quantity'];
            $cartDao->updateCart($resCartW[0]->id, $quantity);
          } else {
            $data["_NQ_created_at"] = "now()";
            $dao->insertQuery("cart", $data, "pay_w");
          }
        } catch (\Exception $e) {
          return $this->resultFail("FAIL", "장바구니에 추가하는데 실패했습니다.");
        }
      }
    }
    return $this->resultOk(null,"장바구니에 추가했습니다");
  }

  public function setUpdateCart() {
    $cart_id = $this->getParam("cart_id");
    $quantity = $this->getParam("quantity");

    $dao = new CCartDAO();
    $res = $dao->updateCart($cart_id, $quantity);
    return $this->resultOk($res);
  }

  public function setDeleteCart() {
    $cart_ids = $this->getParam("cart_ids");

    $dao = new CCartDAO();
    $res = $dao->delCart($cart_ids);
    return $this->resultOk($res);
  }

  public function getCart($user_id = 0, $non_user_id = 0, $ids = '')
  {
    $dao = new CBaseDAO();
    $where = "";
    $user_idx = $this->userIdx;
    if ($user_idx == 0) {
      $where .= " and c.non_user_id = '{$non_user_id}'";
    } else {
      $where .= " and c.user_id = {$user_id}";
    }

    $cartIdxsWhere = $ids == 0 ? "" : "and c.id in ({$ids})";
    $sql = "select c.*, g2.id as goods_id, g2.imgs as goods_imgs, g2.title as goods_title
            , concat(GROUP_CONCAT(o.value SEPARATOR ', '),case when s2.price > 0 then concat(' (옵션가 : ',FORMAT(s2.price,0),'원)') else '' end) as option_value
            , s2.price as option_price
            , g2.price as goods_price
            , if(g2.use_discount = 1, IFNULL(g2.price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g2.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g2.price) AS promotion_price
            , IF(sp.idx is null, 0, 1) as is_hotdeal
            , g2.use_discount 
            , g2.stock_state
            , g2.type as goods_type
            , g2.main_category
            , g2.sub_category
            , g2.min_buy_count
            , g2.max_buy_count
            , g2.max_buy_person
            , st.name as seller_name
            , st.idx as seller_idx
            , g2.ship_price
            , s.stock
            from cart c 
            inner join goods g2 on c.goods_id = g2.id and g2.display_state = 'show' and g2.deleted_at is null
            inner join option_sku os on c.sku_id = os.sku_id 
            inner join skus s2 on s2.id = os.sku_id 
            LEFT  join `options` o on os.option_id = o.id 
            left join fp_db.seller_tbl st on g2.seller_id = st.idx
            left join goods_promotions sp on sp.goods_id = g2.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
            left join fp_pay.stocks s on c.sku_id = s.sku_id and g2.id = s.goods_id
            where c.deleted_at is null {$cartIdxsWhere}
            {$where}
            group by c.id";
    $res = $dao->selectQuery($sql, "pay");

    $data_pay = [
      'order_price' => 0,
      'ship_price'  => 0,
      'total_price' => 0,
    ];
    foreach ($res as $item_cart) {
      $data_pay['order_price'] += (($item_cart->promotion_price ? $item_cart->promotion_price : $item_cart->goods_price) + $item_cart->option_price) * $item_cart->quantity;
      $data_pay['ship_price'] += $item_cart->ship_price;
    }

    $data_pay['total_price'] = $data_pay['order_price'] + $data_pay['ship_price'];

    return [
      'datas' => $res,
      'prices' => $data_pay,
    ];
  }
}