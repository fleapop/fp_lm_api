<?php


namespace App\Http\Controllers;


use App\Http\dao\CBaseDAO;

class LmController extends Controller
{
  public function getMenu() {
    $res = array();
    $res['bottom'] = array(
      array('key' => 'store', 'icon' => '', 'link' => '', 'title' => '스코어'),
      array('key' => 'community', 'icon' => '', 'link' => '', 'title' => '커뮤니티'),
      array('key' => 'register', 'icon' => '', 'link' => '', 'title' => '등록'),
      array('key' => 'offline', 'icon' => '', 'link' => '', 'title' => '오프라인'),
      array('key' => 'mypage', 'icon' => '', 'link' => '', 'title' => '마이')
    );

    $res['store'] = array(
      array('key' => 'store', 'icon' => '', 'link' => '/store/home/store', 'title' => '스코어'),
      array('key' => 'brand', 'icon' => '', 'link' => '/store/home/brand', 'title' => '브랜드'),
      array('key' => 'best', 'icon' => '', 'link' => '/store/home/best', 'title' => 'BEST'),
      array('key' => 'new', 'icon' => '', 'link' => '/store/home/new', 'title' => 'NEW'),
      array('key' => 'exhibition', 'icon' => '', 'link' => '/store/home/promotion', 'title' => '기획전'),
      array('key' => 'category', 'icon' => '', 'link' => '/store/home/cate', 'title' => '카테고리')
    );

    $dao = new CBaseDAO();
    $sql = "select atm.sort_order , atm.name as menu_name, atm.exhibition_id , e.img ,e.img_pc ,e.img_content ,e.img_content_pc
                , e.title
                , CONCAT( date_format(e.start_date, '%m월 %d일'),'(',SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(e.start_date), 1),') ', date_format(e.start_date, '%H시 %i분') ,' - ', date_format(e.end_date , '%m월 %d일'),'(',SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(e.end_date), 1),') ', date_format(e.end_date, '%H시 %i분')) as sub_title
                from app_top_menu atm
                inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true' and e.type = 'exhibition'
                where atm.display = 'show'
                and e.is_hidden = 'true'
                and e.deleted_at is null
                and ((date_format(e.start_date, '%Y-%m-%d') <= now() and date_format(e.end_date, '%Y-%m-%d') >= now()))
				order by e.end_date asc
				limit 1
                ";
    $resExhibi = $dao->selectQuery($sql ,"pay");

    if (isset($resExhibi[0])) {

      $add = array('key' => 'event', 'icon' => '', 'link' => $resExhibi[0]->exhibition_id, 'title' => $resExhibi[0]->menu_name);
      $res['store'] = insert_array($res['store'], $resExhibi[0]->sort_order - 1, $add);
    }
    return $res;
  }
}