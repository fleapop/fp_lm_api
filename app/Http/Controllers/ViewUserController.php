<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\OrderDelivery;
use App\Models\OrderDetail;
use App\Models\OrderInfoTbl;
use DB;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Image;
use Session;
use Storage;

class ViewUserController extends Controller
{
    private $LOGIN_SUB_HEADER_TITLE = ["로그인", "계정/패스워드 찾기"];
    private $LOGIN_SUB_HEADER_URL   = ["/re/login", "/re/login/find"];

    // 주문,배송
    private $urlOrder          = "/re/user/order/list/lm/all/all";
    private $urlDelivery       = "/re/user/delivery/list/store/ready/all";
    private $urlExchangeRefund = "/re/user/exchange_refund/list/exchange/all";

//    private $conn;
//    private $conn_w;

    public $var = [
        "TAB_MENU"          => "USER",
        "TITLE"             => "",
        "MAIN_MENU"         => "",
        "HEADER_TYPE"       => "",
        "IS_HAVE_SUB_TITLE" => true,
        "SUB_HEADER_TITLE"  => [],
        "SUB_HEADER_URL"    => [],
        "SUB_HEADER_INDEX"  => 0,
    ];

//    function __construct()
//    {
//        $this->conn = DB::connection(DATABASE);
//        $this->conn_w = DB::connection(DATABASE_W);
//    }

    public function viewLogin($meta = "")
    {
        @session_start();
        if (@isset($_SESSION["user"])) {

            return redirect("/re/mypage/home");
        }

        $this->var["SUB_HEADER_TITLE"] = $this->LOGIN_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LOGIN_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 0;
        $this->var["meta"] = $meta;
        return view("remake.login.login", $this->var);
    }

    public function viewAppLogin($device = 101)
    {

        $this->var["SUB_HEADER_TITLE"] = $this->LOGIN_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LOGIN_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 0;
        $this->var["meta"] = "";
        $this->var["device"] = $device;
        $this->var["back_url"] = $_GET['url'];
        $this->var["hData"] = [
            "title" => "로그인",
        ];

        return view("user_service.user.login_app", $this->var);
    }


    public function viewLoginOrderNonUser($meta = "")
    {
        $this->var["SUB_HEADER_TITLE"] = $this->LOGIN_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LOGIN_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 0;
        $this->var["meta"] = $meta;


        return view("remake.order.non_member_order", $this->var);
    }

    public function viewLogin2()
    {
        @session_start();
        echo "
        <script>
            alert('viewLogin2');
        </script>
        ";
        if (@isset($_SESSION["user"])) {
            echo "
            <script>
                alert('move');
            </script>
            ";
            return redirect("/re/mypage/home");
        }
        $this->var["SUB_HEADER_TITLE"] = $this->LOGIN_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LOGIN_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 0;
        return view("remake.login.login2", $this->var);
    }

    public function viewFindAccount()
    {
        $this->var["SUB_HEADER_TITLE"] = $this->LOGIN_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LOGIN_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 1;
        return view("remake.login.find", $this->var);
    }

    public function viewHelp()
    {
        $this->var["SUB_HEADER_TITLE"] = $this->LOGIN_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LOGIN_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 2;
        return view("remake.login.help_center", $this->var);
    }

    public function viewJoinstep1()
    {
        $this->var["SUB_HEADER_TITLE"] = $this->LOGIN_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LOGIN_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 0;
        return view("remake.login.signup_step_01", $this->var);
    }

    public function viewJoinstep2(Request $request)
    {
        $this->var["SUB_HEADER_TITLE"] = $this->LOGIN_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LOGIN_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 0;
        $this->var["request"] = $request;
        return view("remake.login.signup_step_02", $this->var);
    }

    public function viewJoinstep3(Request $request)
    {
        $this->var["SUB_HEADER_TITLE"] = $this->LOGIN_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LOGIN_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 0;
        $this->var["request"] = $request;

        if ($request->hd_account_type == 1) {
            $account = $request->hd_email_account;
        } else {
            $account = $request->hd_fb_account;
        }

        $args = [
            "account_type" => $request->hd_account_type,
            "account"      => $account,
        ];

        $args = $this->makeRequest($args);
        $con_user = new UserController();
        $response = $con_user->apiCheckAccount($args);
        $response = json_decode($response);
        $this->var["response"] = $response;

        return view("remake.login.signup_step_03", $this->var);
    }

    public function viewJoinFinish(Request $request)
    {
        $this->var["SUB_HEADER_TITLE"] = $this->LOGIN_SUB_HEADER_TITLE;
        $this->var["SUB_HEADER_URL"] = $this->LOGIN_SUB_HEADER_URL;
        $this->var["SUB_HEADER_INDEX"] = 0;
        $this->var["request"] = $request;
        return view("remake.login.success", $this->var);
    }

    public function viewFeedFormImgTxt(Request $request)
    {
        @session_start();
        $images = explode("|:|", $request->hd_images);
        array_splice($images, 0, 1);

        $datas = [];
        foreach ($images as $image) {
            $image = S3_USER_CONTENT_TEMP_URL . $image;
            $data = [
                "image"  => $image,
                "string" => "",
            ];

            array_push($datas, $data);
        }

        $this->var["datas"] = $datas;

        return view("remake.upload.upload_main", $this->var);
    }

    public function viewEditFeedFormImgTxt($idx = 0)
    {
        @session_start();

        $args = [
            "idx" => $idx,
        ];
        $args = $this->makeRequest($args);

        $con_control = new ContentController();
        $response = $con_control->apiGetFeedContent($args);

        if ($response["success"]) {
            $this->var["datas"] = $response["data"];
            $this->var["idx"] = $idx;
        } else {
            $this->var["datas"] = [];
            $this->var["idx"] = 0;
        }

        return view("remake.upload.upload_main", $this->var);
    }

    /**
     * 마이페이지 홈 View
     * @ param
     * required
     */
    public function viewMypageHome()
    {
        $this->var["TAB_MENU"] = "MYPAGE";
        $this->var["MAIN_MENU"] = "MYPAGE";

        $args = $this->makeArgs();
        $args = $this->makeRequest($args);
        $args["device"] = 101;

        $con_user = new UserController();
        $response = $con_user->apiMypageHome($args);
        $response = json_decode($response);

        if ($response->code == 300) {
            echo "
            <script>
                location.href='/re/login';
            </script>
            ";
        }

        @session_start();
        $this->var["user_id"] = $this->getUserId();
        $this->var["response"] = $response;
        return view("remake.mypage.main_feed", $this->var);
    }

    public function viewMypageCStel()
    {
        $this->var["TAB_MENU"] = "MYPAGE";
        $this->var["MAIN_MENU"] = "MYPAGE";
        $this->var["IS_BACK"] = true;

        @session_start();
        $user_id = $this->getUserId();
        $this->var["user_id"] = $user_id;

        return view("remake.mypage.call_center", $this->var);
    }

    public function viewNoticeList($idx = 0)
    {
        @session_start();
        $this->var["TAB_MENU"] = "MYPAGE";
        $this->var["MAIN_MENU"] = "MYPAGE";
        $this->var["IS_BACK"] = true;

        $args = $this->makeArgs();
        $args["page"] = 1;
        $args["max"] = 100;
        $args["device"] = 101;
        $args = $this->makeRequest($args);

        $con_content = new ContentController();
        $noti_contents = $con_content->apiGetNotices($args);
        $noti_contents = json_decode($noti_contents);
        if (isset($noti_contents->data)) {
            $this->var["data"] = $noti_contents->data->contents;
        }

        $this->var["idx"] = $idx;

        return view("remake.mypage.my_notice", $this->var);
    }

    public function viewEventList()
    {
        $this->var["TAB_MENU"] = "MYPAGE";
        $this->var["MAIN_MENU"] = "MYPAGE";
        $this->var["IS_BACK"] = true;

        $args = $this->makeArgs();
        $args["page"] = 1;
        $args["max"] = 100;
        $args["device"] = 101;
        $args = $this->makeRequest($args);

        $con_content = new ContentController();
        $event_contents = $con_content->apiGetEvents($args);
        $event_contents = json_decode($event_contents);
        if (isset($event_contents->data)) {
            $this->var["data"] = $event_contents->data->contents;
        }

        return view("remake.mypage.my_event_list", $this->var);
    }

    public function viewEventData($idx = 0)
    {
        $this->var["TAB_MENU"] = "MYPAGE";
        $this->var["MAIN_MENU"] = "MYPAGE";
        $this->var["IS_BACK"] = true;
        $this->var["use_save_image"] = false;
        if ($idx > 0) {
            $args = $this->makeArgs();
            $args["idx"] = $idx;
            $args = $this->makeRequest($args);

            $con_content = new ContentController();
            $event_content = $con_content->apiGetEvent($args);
            $event_content = json_decode($event_content);
            $this->var["content"] = $event_content->data;

            $meta_data = [
                "meta_title"       => $event_content->data->title,
                "meta_image"       => $event_content->data->image,
                "meta_description" => "러블리마켓 이벤트",
            ];
            $this->var["meta_data"] = $meta_data;
        }

        return view("remake.mypage.event_list_open", $this->var);
    }

    public function viewCheckList()
    {
        $this->var["TAB_MENU"] = "MYPAGE";
        $this->var["MAIN_MENU"] = "MYPAGE";
        $this->var["IS_BACK"] = true;
        return view("remake.mypage.check_list", $this->var);
    }

    public function viewFpHelp()
    {
        $this->var["TAB_MENU"] = "MYPAGE";
        $this->var["MAIN_MENU"] = "MYPAGE";
        $this->var["IS_BACK"] = true;
        @session_start();
        $user_id = $this->getUserId();
        $this->var["user_id"] = $user_id;
        return view("remake.login.service", $this->var);
    }

    public function viewUpdateMypage()
    {
        $TAB_MENU = "MYPAGE";
        $MAIN_MENU = "MYPAGE";
        $IS_BACK = true;
        $args = $this->makeRequest($this->makeArgs());
        $args["device"] = 101;
        $con_user = new UserController();
        $user_data = $con_user->apiGetEditUserInfo($args);
        $userData = json_decode($user_data);

        if (!$userData->success) {
            throw new Exception($userData);
        } else {
            $data = $userData->data;
        }

        $identificationNumber = ['010', '016', '017', '018', '011'];

        return view(
            'remake.mypage.my_setting',
            compact('TAB_MENU', 'MAIN_MENU', 'IS_BACK', 'data', 'identificationNumber')
        );
    }

    public function viewContactTOS()
    {
        echo "Hello 이용약관";
    }

    public function viewContactInfo()
    {
        echo "Hello 개인정보";
    }

    public function viewOnOrderHistoryDetail($order_id)
    {
        $this->var["MAIN_MENU"] = "TITLE_BACK";
        $this->var["backUrl"] = "/re/user/order/history/store";
        $order_detail = OrderDetail::where("order_id", "=", $order_id)->count();
        $this->var["HEADER_TITLE"] = "상세내역 (<span>" . $order_detail . "</span>건)";
        $this->var["order_id"] = $order_id;
        $this->var["TAB_MENU"] = "MYPAGE";

        return view("remake.mypage.lm.history_detail", $this->var);
    }


    public function viewOnOrderHistoryDetail_NonUser($order_number)
    {
        $this->var["TITLE"] = "구매내역";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = ["구매정보", "배송조회"];
        $this->var["SUB_HEADER_URL"] = ["/re/nonuser/order/history/detail/" . $order_number, "/re/nonuser/ship/history/" . $order_number];
        $this->var["SUB_HEADER_INDEX"] = 0;
        $this->var["order_id"] = Order::where("order_number", "=", $order_number)->first()->id;

        return view("remake.mypage.lm.history_detail", $this->var);
    }

    public function viewOrderCSForm($form, $detail_id)
    {
        $this->var["MAIN_MENU"] = "TITLE_BACK";
        $this->var["HEADER_TITLE"] = "교환/환불 신청";
        $this->var["form_type"] = $form;
        $this->var["detail_id"] = $detail_id;

        // 인증 검사
        @session_start();
        if (isset($_SESSION["user_type"]) && $_SESSION["user_type"] == "non_user") {
            $order_id = OrderDetail::find($detail_id)->order->id;
            if ($_SESSION['order_id'] != $order_id || ! isset($_SESSION['order_id'])) {
                return redirect('/re/login');
            }
        }
        return view("remake.mypage.store.change_refund", $this->var);
    }

    /**
     * 구매내역
     *
     * @param string $tab
     * @param string $type
     * @param string $date
     */
    public function viewOrderList($tab = "lm", $type = "charge", $date = "all")
    {
        $this->var["TITLE"] = "구매내역";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = ["구매내역", "주문/배송", "교환/반품"];
        $this->var["SUB_HEADER_URL"] = [$this->urlOrder, $this->urlDelivery, $this->urlExchangeRefund];
        $this->var["SUB_HEADER_INDEX"] = 0;
        $this->var["TAB_MENU"] = "USER";
        $this->var["tab"] = $tab;
        $this->var["type"] = $type;
        $this->var["date"] = $date;

        $userController = new UserController();
        $response = $userController->getOrderList($tab, $type, $date, 101);

        if (is_array($response)) {
            $this->var["isLogin"] = true;
            $this->var['data'] = $response;
            return view("remake.mypage.order.order", $this->var);
        } else {
            $this->var["isLogin"] = false;
            return view("remake.mypage.order.order", $this->var);
         }
    }


    /**
     * 마이페이지 > 구매내역상세
     *
     * @param string $menu
     * @param null   $type
     * @param        $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\View\View
     */
    public function viewOrderDetail($menu = 'lm', $type = NULL, $id)
    {
        $this->var["MAIN_MENU"] = "TITLE_BACK";
        $this->var["TAB_MENU"] = "MYPAGE";

        $userController = new UserController();
        if ($menu == 'lm') {
            switch ($type) {
                case 'charge' :
                    $response = $userController->getOrderDetail($menu, $type, $id, 101);
                    if ($response->getStatusCode() != 200) {
                        return redirect('/re/login');
                    }
                    $this->var["HEADER_TITLE"] = "러마페이 충전결과";
                    $this->var['data'] = json_decode($response->getContent());

                    return view("remake.mypage.order.order_charge", $this->var);
                case 'refund' :
                    $response = $userController->getOrderDetail($menu, $type, $id, 101);
                    if ($response->getStatusCode() != 200) {
                        return redirect('/re/login');
                    }
                    $this->var["HEADER_TITLE"] = "러마페이 환불결과";
                    $this->var['data'] = json_decode($response->getContent());

                    return view("remake.mypage.order.order_refund", $this->var);
                case 'buy' :
                    $response = $userController->getOrderDetail($menu, $type, $id, 101);
                    if ($response->getStatusCode() != 200) {
                        return redirect('/re/login');
                    }
                    $this->var["HEADER_TITLE"] = "상세 주문내역1";
                    $response = json_decode($response->getContent());
                    $this->var['data'] = $response;
                    if ($response->type == '오프라인') {
                        return view("remake.mypage.order.detail_lm", $this->var);
                    }
                    break;
            }
        } else {
            $response = $userController->getOrderDetail($menu, $type, $id, 101);
            if ($response->getStatusCode() != 200) {
                return redirect('/re/login');
            }
            $this->var["HEADER_TITLE"] = "상세 주문내역";
            $response = json_decode($response->getContent());
            $this->var['data'] = $response;
            return view("remake.mypage.order.detail", $this->var);
        }
    }

    /**
     * 주문서 상세 - 교환 View
     * @ param
     * required
     * detailId : 주문 id
     */
    public function viewOrderCSExchange($detailId)
    {
        $this->var["MAIN_MENU"] = "TITLE_BACK";
        $this->var["HEADER_TITLE"] = "교환 신청";
        $this->var["detailId"] = $detailId;

        $userController = new UserController();
        $response = $userController->apiGetOrderDetail($detailId, 101);
        if ($response->getStatusCode() != 200) {
            // TODO : Error Page Redirect
            return;
        }
        $this->var['response'] = json_decode($response->getContent());

        return view("remake.mypage.order.form_exchange", $this->var);
    }

    /**
     * 주문서 상세 - 구매취소 View
     * @ param
     * required
     * detailId : 주문 id
     */
    public function viewOrderCSCancel($detailId)
    {
        $this->var["MAIN_MENU"] = "TITLE_BACK";
        $this->var["HEADER_TITLE"] = "취소 신청";
        $this->var["detailId"] = $detailId;

        $userController = new UserController();
        $response = $userController->apiGetOrderDetail($detailId, 101);
        if ($response->getStatusCode() != 200) {
            // TODO : Error Page Redirect
            return;
        }
        $this->var['response'] = json_decode($response->getContent());

        return view("remake.mypage.order.form_cancel", $this->var);
    }

    /**
     * 주문서 상세 - 반품 View
     * @ param
     * required
     * detailId : 주문 id
     */
    public function viewOrderCSRefund($detailId)
    {
        $this->var["MAIN_MENU"] = "TITLE_BACK";
        $this->var["HEADER_TITLE"] = "반품 신청";
        $this->var["detailId"] = $detailId;

        $userController = new UserController();
        $response = $userController->apiGetOrderDetail($detailId, 101);
        if ($response->getStatusCode() != 200) {
            // TODO : Error Page Redirect
            return;
        }
        $this->var['response'] = json_decode($response->getContent());

        return view("remake.mypage.order.form_refund", $this->var);
    }

    /**
     * 주문서 상세 - 주문문의 View
     * @ param
     * required
     * detailId : 주문 id
     */
    public function viewOrderCSQuestion($detailId)
    {
        $this->var['MAIN_MENU'] = 'TITLE_BACK';
        $this->var['HEADER_TITLE'] = '주문 문의하기';
        $this->var['detailId'] = $detailId;

        $userController = new UserController();
        $response = $userController->apiGetOrderDetail($detailId, 101);
        if ($response->getStatusCode() != 200) {
            return redirect('/re/login');
        }
        $this->var['response'] = json_decode($response->getContent());

        return view('remake.mypage.order.form_question', $this->var);
    }

    /**
     * 주문/배송 - 주문 배송 리스트
     * @ param
     * required
     * $tab : 탭 타입 ['lm','store']
     * $type : 종류 ['ready','ready_ship', 'ship', 'ship_complete', 'complete']
     */
    public function viewDeliveryList($tab = "lm", $type = "ready", $date = "all")
    {
        $this->var["TITLE"] = "주문/배송";
        $this->var["MAIN_MENU"] = "LMPAY";
        $this->var["IS_HAVE_SUB_TITLE"] = true;
        $this->var["SUB_HEADER_TITLE"] = ["구매내역", "주문/배송", "교환/반품"];
        $this->var["SUB_HEADER_URL"] = [$this->urlOrder, $this->urlDelivery, $this->urlExchangeRefund];
        $this->var["SUB_HEADER_INDEX"] = 1;
        $this->var["TAB_MENU"] = "USER";
        $this->var["USER_TYPE"] = "USER";
        $this->var['tab'] = $tab;
        $this->var['type'] = $type;
        $this->var['date'] = $date;

        $user = new UserController();
        $response = $user->getDeliveryList($tab, $type, $date, 101);
        $this->var['data'] = $response;

        return view('remake.mypage.order.delivery', $this->var);
    }

    /**
     * 주문/배송 - 운송장 조회
     * @ param
     * required
     * tab : 탭 타입 ['lm','store']
     * id : id lm - order_info_tbl id, delivery id
     * device
     */
    public function viewTracking($type, $id, $device = 101)
    {
        $tracking = [
            'code'   => NULL,
            'number' => NULL,
            'data'   => NULL,
            'device' => $device,
        ];

        if ($type == 'lm') {
            $order = OrderInfoTbl::find($id);
            $tracking['code'] = $order->courier_code;
            $tracking['number'] = $order->tracking_number;
            $tracking['data'] = $order->tracking_data;
            $tracking['courier'] =$this->convertCourierCode($order->courier_code);
        } else {
            $order = OrderDelivery::where('detail_id', $id)->first();
            if (isset($order)) {
                $tracking['code'] = $order->couirer_code;
                $tracking['number'] = $order->tracking_number;
                $tracking['data'] = $order->tracking_data;
              $tracking['courier'] =$this->convertCourierCode($order->couirer_code);
            }
        }

        if (strlen($tracking['code']) == 1) {
            $tracking['code'] = "0" . $tracking['code'];
        }

        $invoiceNumber = str_replace('-', '', $tracking['number']);
        $url = "http://api.fleapop.co.kr/api/delivery/{$tracking['code']}/{$invoiceNumber}";

        $client = new Client();
        $response = $client->request('GET', $url);
        $response = $response->getBody()->getContents();
        $tracking['data'] = json_decode($response);

        return view("remake.mypage.order.tracking", $tracking);
    }

    /**
     * 마이페이지 > 구매내역 > 교환/반품
     *
     * @param string $tab
     * @param string $date
     * @return mixed
     */
    public function viewExchangeRefundList($tab = 'exchange', $date = 'all')
    {
        @session_start();

        if (! isset($_SESSION['user'])) {
            return redirect('/re/login');
        };

        $userId = $_SESSION['user']['user_id'];

        $this->var['TITLE'] = '구매내역';
        $this->var['MAIN_MENU'] = 'LMPAY';
        $this->var['IS_HAVE_SUB_TITLE'] = true;
        $this->var['SUB_HEADER_TITLE'] = ['구매내역', '주문/배송', '교환/반품'];
        $this->var['SUB_HEADER_URL'] = [$this->urlOrder, $this->urlDelivery, $this->urlExchangeRefund];
        $this->var['SUB_HEADER_INDEX'] = 2;

        $user = new UserController();
        $response = $user->getTransRefundList($tab, $date, 101);

        $this->var['data'] = $response;
        $this->var['tab'] = $tab;
        $this->var['date'] = $date;

        return view('remake.mypage.order.exchange_refund', $this->var);
    }

  private function convertCourierCode($code)
  {
    $courierCode = [
      '01' => '우체국',
      '04'=> 'CJ대한통운',
      '05'=> '한진택배',
      '06'=> '로젠택배',
      '08'=> '롯데택배',
      '11'=> '일양로지스',
      '12'=> 'EMS',
      '13'=> 'DHL',
      '14'=> 'UPS',
      '16'=> '한의사랑택배',
      '17'=> '천일택배',
      '18'=> '건영택배',
      '19'=> '고려택배',
      '20'=> '한덱스',
      '21'=> 'Fedex',
      '22'=> '대신택배',
      '23'=>'경동택배',
      '24'=> 'CVSnet 편의점 택배',
      '25'=> 'TNT Express',
      '26'=> 'USPS',
      '27'=> 'TPL',
      '28'=> 'GSMNtoN',
      '29'=> '에어보이익스프레스',
      '30'=> 'KGL네트웍스',
      '32'=> '합동택배',
      '33'=> 'DHL Global Mail',
      '34'=> 'i-Parcel',
      '35'=> '포시즌 익스프레스',
      '37'=> '범한판토스',
      '38'=> 'ECMC Express',
      '40'=> '굿투럭',
      '41'=> 'GSI Express',
      '42'=> 'CJ대한통운 국제특송',
      '43'=> '애니트랙',
      '44'=> 'SLX',
      '45'=> '호남한서택배',
      '46'=> 'CU편의점 택배',
      '47'=> '우리한방택배',
      '48'=> 'ACI Express',
      '49'=> 'ACE Express',
      '50'=> 'GPS LOGIX',
      '51'=> '성원글로벌',
      '52'=> '세방',
      '53'=> '농협택배',
      '54'=> '홈픽택배',
      '55'=> 'Euro Parcel',
      '56'=> 'KGB 택배',
      '57'=> 'Cway Express',
      '58'=> '하이택배',
      '59'=> '지오로지스',
      '60'=> 'YJS글로벌(영국)',
      '61'=> '워팩스코리아',
      '62'=> '홈이노베이션로지스',
      '63'=> '은하쉬핑',
      '64'=> '퍼레버택배',
      '65'=> 'YJS글로별(월드)',
      '66'=> 'Giant Express',
      '67'=> '디디로지스',
      '68'=> '우리동네택배',
      '69'=> '대림통운',
      '70'=> 'LOTOS CORPORATION',
      '71'=> 'IK 물류',
      '99'=> '롯데택배 해외특송',
    ];

    return (isset($courierCode[$code])) ? $courierCode[$code] : '';
  }
}
