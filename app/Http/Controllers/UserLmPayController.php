<?php


namespace App\Http\Controllers;


use App\Http\dao\CUserDAO;
use App\Lmpay\RefundController;
use App\Models\GoodsTbl;
use App\Helper\CUserHelper;
use App\Http\dao\CBaseDAO;
use App\Models\Order;
use App\Models\OrderGoodsTbl;
use App\Models\OrderInfoTbl;
use App\Models\PaymentCancel;
use Illuminate\Http\Request;
use Exception;
use Milon\Barcode\DNS1D;

class UserLmPayController extends Controller
{

  /**
   * 러마페이 잔액
   *
   * @param $user_id
   */
  public function getBalance($user_id)
  {
    $dao = new CBaseDAO();
    $sql = "SELECT IFNULL(SUM(amount), 0) as cash FROM cash_log WHERE user_idx = {$user_id} AND delete_date IS NULL AND (`state` = '결제됨' OR (`action` = '환불' AND `state` = '결제대기') OR (action = '환불' AND state = '기타')) AND `action` != '취소'";
    $balance = $dao->selectQuery($sql, "pay");
    return $balance[0]->cash;
  }

  /**
   * 러마페이 지불
   *
   * @param $user_id
   * @param $data
   */
  public function paid($user_id, $data)
  {
    $balance = $this->getBalance($user_id);
    if ($balance < abs($data['amount'])) {
      return [
        "success" => false,
        "msg"     => "잔액이 부족합니다.",
      ];
    }
    $dao = new CBaseDAO();
    return LMPay::create($data);
  }

  public function getLmPayInfo() {
    $user_idx = $this->userIdx;

    $sql = "SELECT IFNULL(SUM(amount), 0) as amount FROM cash_log WHERE user_idx = {$user_idx} AND delete_date IS NULL AND (`state` = '결제됨' OR (`action` = '환불' AND `state` = '결제대기') OR (action = '환불' AND state = '기타')) AND `action` != '취소'";
//    $sql = "select ifnull(sum(amount), 0) as amount from cash_log cl where user_idx = {$user_idx} and delete_date is null";
    $milSql = "select ifnull(sum(amount), 0) as amount from cash_log cl where user_idx = {$user_idx} and amount > 0 and `method` in ('마일리지', 'DBIN', 'EVNET')";

    $dao = new CBaseDAO();
    $totalRes = $dao->selectQuery($sql, "pay")[0]->amount;
    $milRes = $dao->selectQuery($milSql, "pay")[0]->amount;

    $userHelper = new CUserHelper();
    $barcode = $userHelper->getBarcode($user_idx);

    $data = array(
      "total" => $totalRes,
      "mileage" => $milRes,
      "charge" => (int)$totalRes - (int)$milRes,
      "barcode" => $barcode
    );
    return $this->resultOk($data);

  }
  public function setBarcodeRefresh() {
    $user_idx = $this->userIdx;
    $userHelper = new CUserHelper();
    $res = $userHelper->setUserBarcode($user_idx);
    return $this->resultOk($res);
  }

  public function getLmPayList() {
    $user_idx = $this->userIdx;
    if(!$user_idx) return $this->resultFail("NO_LOGIN", "로그인 후 이용 가능합니다.");
    $category = $this->getParam("category");
    $page = $this->getParam("page", 0);
    $year = $this->getParam("year");
    $month = $this->getParam("month");

    $detail_idx = $this->getParam("detail_idx");
    $date = $detail_idx ? "" : "and left(cl.insert_date,7) = '".$year."-".$month."'";


    $where = $detail_idx ? "and cl.idx = {$detail_idx}" : "";
    $perPage = 40;
    $dao = new CBaseDAO();
    $limit = $dao->getLimit($page, $perPage);

    $tabWhere = "";
    switch ($category) {
      case "purchase" :
        // 구매
        $tabWhere = " and (
                          (cl.type = '온라인' and action = '지불' and cl.state = '결제됨') 
                          or (cl.type = '오프라인' and action = '지불' and cl.state ='결제됨' and cl.market_seller_idx is not null and cl.order_info_idx is not null)
                          or (cl.type = '오프라인' and action = '지불' and cl.state ='결제됨' and cl.market_seller_idx is null and cl.order_info_idx is not null)
                          )";
        break;
      case "refund" :
        // 환불
        $tabWhere = " and action = '환불' and cl.method <>'DBOUT' and cl.state = '결제됨' and cl.amount < 0";
        break;
      case "recall" :
        // 회수
        $tabWhere = " and cl.amount < 0 and cl.`method` = 'DBOUT'";
        break;
      case "charge" :
        // 충전
        $tabWhere = " and action = '입금' and cl.method not in ('REVIEW','ITEMBUY') and cl.state = '결제됨'";
        break;
      case "mileage" :
        //적립
        $tabWhere = " and ((action = '입금' and cl.method = 'ITEMBUY' and cl.state ='결제됨') or (action = '입금' and cl.method = 'REVIEW' and cl.state ='결제됨'))";
        break;
      case "cancel" :
        // 취소
        $tabWhere = "         and (detail.`state` = '취소완료' or detail.`state` = '취소접수')  and cl.method not in ('REVIEW','ITEMBUY') and cl.state = '결제됨' and cl.amount > 0 and cl.order_info_idx is not null";
        break;
      case "return" :
        // 반품
        $tabWhere = "         and (detail.`state` = '반품완료' or detail.`state` = '빈품접수') and cl.method not in ('REVIEW','ITEMBUY','DBIN') and cl.state = '결제됨' and cl.amount > 0 and cl.order_info_idx is not null";
        break;
    }

    if (isset($detail_idx)) {
      $tabWhere = "";
    }
    $newSql = "select cl.idx,
                 cl.type
                   ,
                   case
                   when cl.method in ('EVENT', 'DBIN') then '보상'
                   when action = '입금' and cl.method not in ('REVIEW','ITEMBUY') and cl.state = '결제됨' then '충전'
                       when action = '입금' and cl.state ='결제대기' and cl.method = '가상계좌' then '대기'
                       when action = '환불' and cl.amount < 0 and cl.state ='결제됨' and cl.method <>'DBOUT' then '환불'
                       when action = '환불' and cl.amount < 0 and cl.method ='DBOUT' then '회수' -- 추가
                       when action = '환불' and cl.amount < 0 and cl.state ='결제대기' then '대기'
                       when action = '입금' and cl.method = 'REVIEW' and cl.state ='결제됨' then g.imgs
                       when action = '입금' and cl.method = 'ITEMBUY' and cl.state ='결제됨' then g.imgs
                   when cl.type = '온라인' and action = '지불' and cl.state = '결제됨' then ifnull(g2.imgs,g.imgs)
                       when cl.type = '온라인' and action = '환불' and cl.state = '결제됨' and cl.amount > 0 then goods.imgs
                       when cl.type = '오프라인' and action = '지불' and cl.state ='결제됨' and cl.market_seller_idx is not null and cl.order_info_idx is not null then case when offgoods.image in ('1','2', '3', '4', '5', '6','7', '8') then concat('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/thumb/',offgoods.image,'.png') else concat('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/thumb/',offgoods.image) end
                       when cl.type = '오프라인' and action = '환불' and cl.state ='결제됨' and cl.amount > 0 and cl.market_seller_idx is not null and cl.order_info_idx is not null then case when offgoods.image in ('1','2', '3', '4', '5', '6','7', '8') then concat('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/thumb/',offgoods.image,'.png') else concat('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/thumb/',offgoods.image) end
                       when cl.type = '오프라인' and action = '지불' and cl.state ='결제됨' and cl.market_seller_idx is null and cl.order_info_idx is not null then 'CU'
                       when cl.type = '오프라인' and action = '환불' and cl.state ='결제됨' and cl.amount > 0 and cl.market_seller_idx is null and cl.order_info_idx is not null then 'CU'
                   else '기타'
                 end as circle_title
                   ,
                   case
                       when cl.method in ('EVENT', 'DBIN') then cl.memo
                       when action = '입금' and cl.method not in ('REVIEW','ITEMBUY') and cl.state = '결제됨' then concat(cl.method, ' 충전')
                       when action = '입금' and cl.state ='결제대기' and cl.method = '가상계좌' then '가상계좌 입금대기'
                       when action = '환불' and cl.amount < 0 and cl.state ='결제됨' and cl.method <>'DBOUT' then concat(cl.method, ' 환불')
                       when action = '환불' and cl.amount < 0 and cl.method ='DBOUT' then cl.memo
                       when action = '환불' and cl.amount < 0 and cl.state ='결제대기' then '환불 입금대기'
                       when action = '입금' and cl.method = 'REVIEW' and cl.state ='결제됨' then g.title -- '상품후기적립'
                       when action = '입금' and cl.method = 'ITEMBUY' and cl.state ='결제됨' then g.title -- '상품구매적립'
                       when cl.type = '온라인' and action = '지불' and cl.state = '결제됨' then ifnull(goods.title,g.title)
                       when cl.type = '온라인' and action = '환불' and cl.state = '결제됨' and cl.amount > 0 then g2.title
                       when cl.type = '오프라인' and action = '지불' and cl.state ='결제됨' and cl.market_seller_idx is not null and cl.order_info_idx is not null then offgoods.title
                       when cl.type = '오프라인' and action = '환불' and cl.state ='결제됨' and cl.amount > 0 and cl.market_seller_idx is not null and cl.order_info_idx is not null then offgoods.title
                       when cl.type = '오프라인' and action = '지불' and cl.state ='결제됨' and cl.market_seller_idx is null and cl.order_info_idx is not null then 'CU 상품구매'
                       when cl.type = '오프라인' and action = '환불' and cl.state ='결제됨' and cl.amount > 0 and cl.market_seller_idx is null and cl.order_info_idx is not null then 'CU 상품환불'
                   else '기타'
                 end as title
                   ,
                   case
                   when action = '입금' and cl.method not in ('REVIEW','ITEMBUY') and cl.state = '결제됨' then '충전'
                       when action = '입금' and cl.state ='결제대기' and cl.method = '가상계좌' then '입금대기'
                       when action = '환불' and cl.amount < 0 and cl.state ='결제됨' and cl.method <>'DBOUT' then '환불'
                       when action = '환불' and cl.amount < 0 and cl.method ='DBOUT' then '회수'
                       when action = '환불' and cl.amount < 0 and cl.state ='결제대기' then '환불대기'
                       when action = '입금' and cl.method = 'REVIEW' and cl.state ='결제됨' then '후기적립금'
                       when action = '입금' and cl.method = 'ITEMBUY' and cl.state ='결제됨' then '구매적립금'
                   when cl.type = '온라인' and action = '지불' and cl.state = '결제됨' then '온라인상품구매'
                       when cl.type = '온라인' and action = '환불' and cl.state = '결제됨' and cl.amount > 0 then '온라인상품환불'
                       when cl.type = '오프라인' and action = '지불' and cl.state ='결제됨' and cl.market_seller_idx is not null and cl.order_info_idx is not null then '오프라인상품구매'
                       when cl.type = '오프라인' and action = '환불' and cl.state ='결제됨' and cl.amount > 0 and cl.market_seller_idx is not null and cl.order_info_idx is not null then '오프라인상품환불'
                       when cl.type = '오프라인' and action = '지불' and cl.state ='결제됨' and cl.market_seller_idx is null and cl.order_info_idx is not null then 'CU상품결제'
                       when cl.type = '오프라인' and action = '환불' and cl.state ='결제됨' and cl.amount > 0 and cl.market_seller_idx is null and cl.order_info_idx is not null then 'CU상품환불'
                   else '기타'
                 end as pay_action
                 , DATE_FORMAT(cl.insert_date, '%d') as insert_day, DATE_FORMAT(cl.insert_date,'%H:%i') as insert_minute
                          ,case DAYOFWEEK(cl.insert_date)
                              when '1' then '일'
                              when '2' then '월'
                              when '3' then '화'
                              when '4' then '수'
                              when '5' then '목'
                              when '6' then '금'
                              when '7' then '토'
                              end as dayofweek
                   , action
                   , cl.method, cl.market_seller_idx, cl.order_info_idx, cl.state, cl.amount, cl.imp_uid, cl.insert_date, cl.memo, detail.goods_id
                   , cl.vbank_name, cl.vbank_num, DATE_FORMAT(FROM_UNIXTIME(cl.vbank_date),'%Y-%m-%d') as vbank_date
                   , cl.receipt_url
              from fp_pay.cash_log cl
                 left join fp_pay.orders o on cl.order_info_idx = o.id
                 left join fp_pay.order_detail detail on o.id = detail.order_id and cl.type= '온라인'
                   left join fp_pay.goods goods on detail.goods_id = goods.id
                   left join fp_pay.order_detail od on cl.order_info_idx = od.id and cl.type= '온라인'
                   left join fp_pay.goods g on od.goods_id = g.id
                   left join fp_pay.order_goods_tbl offdetail on cl.order_info_idx = offdetail.order_info_idx and cl.type= '오프라인'
                   left join fp_pay.goods_tbl offgoods on offdetail.goods_idx = offgoods.idx
                   left join fp_pay.payment_cancels pc on cl.order_info_idx = pc.order_id and cl.amount = pc.amount 
                   left join fp_pay.order_detail od2 on od2.id = pc.order_detail_id 
                   left join fp_pay.goods g2 on od2.goods_id = g2.id
              where cl.delete_date is null and cl.state <> '기타' and cl.user_idx  = {$user_idx} 
              {$date} {$where}
              {$tabWhere}
              group by cl.idx
              order by cl.idx desc";

    $sql = "select cl.idx, cl.user_idx ,cl.market_seller_idx ,cl.order_info_idx ,cl.`type` ,cl.`method`, cl.`action` ,cl.state ,cl.amount , cl.insert_date     
            , DATE_FORMAT(cl.insert_date, '%d') as insert_day, DATE_FORMAT(cl.insert_date,'%H:%i') as insert_minute
            ,case DAYOFWEEK(cl.insert_date)
                when '1' then '일'
                when '2' then '월'
                when '3' then '화'
                when '4' then '수'
                when '5' then '목'
                when '6' then '금'
                when '7' then '토'
                end as dayofweek
            ,case when cl.`action` = '지불' then '일'
                when cl.`action` = '환불' and cl.amount > 0 then '월'
                when cl.amount < 0 and cl.`method` = 'DBOUT' then '화'
                when cl.`action` = '입금' and cl.`method` in ('현금', '신용카드', '계좌이체', '가상계좌', '페이코', '삼성페이', '무통장', '마일리지', 'CU현금') then '수'
                when cl.`action` = '입금' and cl.amount > 0 and cl.`method` in ('마일리지','EVENT') then '목'
                when cl.`action` = '환불' and cl.amount < 0 then '금'
                when cl.`action` = '환불' and cl.amount > 0 and cl.`method` in ('현금', '신용카드', '계좌이체', '가상계좌', '페이코', '삼성페이', '무통장', '마일리지', 'CU현금') then '토'
                end as title                
            from cash_log cl
            where cl.user_idx = {$user_idx} and delete_date is null {$date} {$where}
            order by cl.idx desc
            ";
    # 구매
    $sql1 = "select cl.idx, cl.user_idx ,cl.market_seller_idx ,cl.order_info_idx ,cl.`type` ,cl.`method`, cl.`action` ,cl.state ,cl.amount , cl.insert_date     
            , DATE_FORMAT(cl.insert_date, '%d') as insert_day, DATE_FORMAT(cl.insert_date,'%H:%i') as insert_minute
            ,case DAYOFWEEK(cl.insert_date)
                when '1' then '일'
                when '2' then '월'
                when '3' then '화'
                when '4' then '수'
                when '5' then '목'
                when '6' then '금'
                when '7' then '토'
                end as dayofweek
            from cash_log cl
            where cl.`action` = '지불' and cl.user_idx = {$user_idx} and delete_date is null {$date} {$where}
            order by cl.idx desc
            ";

    # 환불
    $sql2 = "select cl.idx, cl.user_idx ,cl.market_seller_idx ,cl.order_info_idx ,cl.`type` , cl.`action` ,cl.state ,cl.amount , cl.insert_date 
            , DATE_FORMAT(cl.insert_date, '%d') as insert_day, DATE_FORMAT(cl.insert_date,'%H:%i') as insert_minute
            ,case DAYOFWEEK(cl.insert_date)
                when '1' then '일'
                when '2' then '월'
                when '3' then '화'
                when '4' then '수'
                when '5' then '목'
                when '6' then '금'
                when '7' then '토'
                end as dayofweek
              , cl.vbank_name, cl.vbank_num, cl2.`method` 
            from cash_log cl 		
            left join cash_log cl2 on cl.cash_log_idx = cl2.idx 			
            where cl.`action` = '환불' and cl.amount < 0 and cl.user_idx = {$user_idx} and cl.delete_date is null {$date} {$where}
            order by cl.idx desc
            ";

    # 회수
    $sql3 = "select cl.idx, cl.user_idx ,cl.market_seller_idx ,cl.order_info_idx ,cl.`type` ,cl.`method`, cl.`action` ,cl.state ,cl.amount , cl.insert_date 
            , DATE_FORMAT(cl.insert_date, '%d') as insert_day, DATE_FORMAT(cl.insert_date,'%H:%i') as insert_minute
            ,case DAYOFWEEK(cl.insert_date)
                when '1' then '일'
                when '2' then '월'
                when '3' then '화'
                when '4' then '수'
                when '5' then '목'
                when '6' then '금'
                when '7' then '토'
                end as dayofweek
            from cash_log cl 					
            where cl.amount < 0 and cl.`method` = 'DBOUT' and cl.user_idx = {$user_idx} and delete_date is null {$date} {$where}
            order by cl.idx desc
            ";

    # 충전
    $sql4 = "select cl.idx, cl.user_idx ,cl.market_seller_idx ,cl.order_info_idx ,cl.`type` ,cl.`method`, cl.`action` ,cl.state ,cl.amount , cl.insert_date, cl.receipt_url 
            , DATE_FORMAT(cl.insert_date, '%d') as insert_day, DATE_FORMAT(cl.insert_date,'%H:%i') as insert_minute
            ,case DAYOFWEEK(cl.insert_date)
                when '1' then '일'
                when '2' then '월'
                when '3' then '화'
                when '4' then '수'
                when '5' then '목'
                when '6' then '금'
                when '7' then '토'
                end as dayofweek
                , cl.vbank_name, cl.vbank_num, DATE_FORMAT(cl.vbank_date,'%Y-%m-%d') as vbank_date
            from cash_log cl 					
            where cl.`action` = '입금' 
            and cl.`method` in ('현금', '신용카드', '계좌이체', '가상계좌', '페이코', '삼성페이', '무통장', '마일리지', 'CU현금', 'EVENT') 
            and cl.user_idx = {$user_idx} {$date}
             {$where}
            and delete_date is null
            order by cl.idx desc
            ";

    # 적립
    $sql5 = "select cl.idx, cl.user_idx ,cl.market_seller_idx ,cl.order_info_idx ,cl.`type` ,cl.`method`, cl.`action` ,cl.state ,cl.amount , cl.insert_date 
            , DATE_FORMAT(cl.insert_date, '%d') as insert_day, DATE_FORMAT(cl.insert_date,'%H:%i') as insert_minute
            ,case DAYOFWEEK(cl.insert_date)
                when '1' then '일'
                when '2' then '월'
                when '3' then '화'
                when '4' then '수'
                when '5' then '목'
                when '6' then '금'
                when '7' then '토'
                end as dayofweek
            from cash_log cl 					
            where cl.amount > 0 and cl.`method` = '마일리지' and cl.`action` = '입금'  and cl.user_idx = {$user_idx} and delete_date is null {$date}
            {$where}
            order by cl.idx desc
            ";

    # 취소
    $sql6 = "select cl.idx, cl.user_idx ,cl.market_seller_idx ,cl.order_info_idx ,cl.`type` ,cl.`method`, cl.`action` ,cl.state ,cl.amount , cl.insert_date 
            , DATE_FORMAT(cl.insert_date, '%d') as insert_day, DATE_FORMAT(cl.insert_date,'%H:%i') as insert_minute
            ,case DAYOFWEEK(cl.insert_date)
                when '1' then '일'
                when '2' then '월'
                when '3' then '화'
                when '4' then '수'
                when '5' then '목'
                when '6' then '금'
                when '7' then '토'
                end as dayofweek
            from cash_log cl 					
            where cl.`action` = '환불' and cl.amount > 0 and cl.user_idx = {$user_idx} and delete_date is null {$date} {$where}
            order by cl.idx desc
            ";

    #반품
    $sql7 = "select cl.idx, cl.user_idx ,cl.market_seller_idx ,cl.order_info_idx ,cl.`type` ,cl.`method`, cl.`action` ,cl.state ,cl.amount , cl.insert_date 
            , DATE_FORMAT(cl.insert_date, '%d') as insert_day, DATE_FORMAT(cl.insert_date,'%H:%i') as insert_minute
            ,case DAYOFWEEK(cl.insert_date)
                when '1' then '일'
                when '2' then '월'
                when '3' then '화'
                when '4' then '수'
                when '5' then '목'
                when '6' then '금'
                when '7' then '토'
                end as dayofweek
            from cash_log cl 					
            where cl.`action` = '환불' and cl.amount > 0 and cl.user_idx = {$user_idx} and delete_date is null
              {$date}
             {$where}
             order by cl.idx desc
             ";

    $sqlQuery = $sql;
    switch ($category) {
      case "purchase" :
        // 구매
        $sqlQuery = $sql1;
        break;
      case "refund" :
        // 환불
        $sqlQuery = $sql2;
        break;
      case "recall" :
        // 회수
        $sqlQuery = $sql3;
        break;
      case "charge" :
        // 충전
        $sqlQuery = $sql4;
        break;
      case "mileage" :
        //적립
        $sqlQuery = $sql5;
        break;
      case "cancel" :
        // 취소
        $sqlQuery = $sql6;
        break;
      case "return" :
        // 반품
        $sqlQuery = $sql7;
        break;
    }

    $res = $dao->selectQuery($newSql.$limit, "pay");
    $resCnt = $dao->selectQuery($newSql, "pay");
    $data = null;
/*
    foreach ($res as $item) {
      if ($item->action == '지불') {
        if (isset($item->market_seller_idx) && $item->type == '오프라인') {
          $sqlDetailQuery = "select oit.idx, oit.market_seller_idx ,oit.user_idx ,oit.order_number,oit.dsno , oit.normal_price ,oit.sale_amount ,oit.refund_amount ,oit.orderer_name ,oit.orderer_phone ,oit.orderer_zipcode ,oit.orderer_address1 ,oit.orderer_address2 ,oit.delivery_state ,oit.delivery_memo ,oit.insert_date 
,ogt.goods_idx ,ogt.quantity ,ogt.price ,ogt.state as ogt_state, ogt.memo as ogt_memo, gt.title as goods_title ,gt.option_title1 ,gt.option_title2 ,gt.option_value1 ,gt.option_value2 ,gt.state as gt_state,gt.price as gt_price,gt.stock as gt_stock
, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/', gt.image, if(gt.image = '1', '.jpg',''))  as goods_img
,st.idx as seller_idx, st.name as seller_name
, count(ogt.idx) as ogt_cnt
                          from order_info_tbl oit 
                          inner join order_goods_tbl ogt on oit.idx = ogt.order_info_idx 
                          inner join goods_tbl gt on ogt.goods_idx = gt.idx 
                          where oit.idx = {$item->order_info_idx}
                          group by oit.idx";
          $orderGoods = $dao->selectQuery($sqlDetailQuery, "pay");

          $data = [
            "type"  => "러블리마켓 구매",
            "image" => $orderGoods->goodsImage,
            "title" => '',
          ];
          $title = $orderGoods->seller_name . " [{$orderGoods->goods_title}]";
          if ((int)$orderGoods->ogt_cnt > 1) {
            $count = (int)$orderGoods->ogt_cnt - 1;
            $title .= " 외 {$count}건";

          }
          $data['title'] = $title;
          $item->data = $data;

        } else if (!isset($item->market_seller_idx) && $item->type == "오프라인") {
          $data = [
            "type"  => "CU 구매",
            "image" => 'https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/thumb/cu_icon.png',
            "title" => 'CU 상품 구매',
          ];
          $item->data = $data;
        } else if (isset($item->order_info_idx) && $item->type == '온라인') {
          $order = Order::with(['detail'])->find($item->order_info_idx);

          $data = [
            "type"  => "스토어 구매",
            "image" => $order->detail[0]->goods->imgs,
            "title" => '',
          ];

          $title = $order->detail[0]->seller->name . " [{$order->detail[0]->goods->title}]";
          if ($order->detail[0]->goods_title) {
            $title = $order->detail[0]->seller->name . "[{$order->detail[0]->goods_title}]";
          }
          if (count($order->detail) > 1) {
            $count = count($order->detail) - 1;
            $title .= " 외 {$count}건";

          }

          $data['title'] = $title;
          $item->data = $data;
        }
      } else if ($item->action == '환불') {
        if ($item->amount > 0 && $item->type == '오프라인' && isset($item->market_seller_idx)) {

        } else if ($item->amount > 0 && $item->type == '온라인' && isset($item->order_info_idx)) {

        } else {

        }
      } else {

      }

    }
*/

    $catchCnt = 0;
    $cashArr = [];
    foreach ($res as $item) {
      if ($item->action == '지불') {
        if (isset($item->market_seller_idx) && $item->type == '오프라인') {
//          try {
            $order = OrderInfoTbl::with(["orderGoods"])->find($item->order_info_idx);
//            dd($item->order_info_idx);
            $goods = $order ? GoodsTbl::withTrashed()->find($order->orderGoods[0]->goods_idx) : json_decode("{\"goodsImage\":\"기타\", \"title\":\"기타\"}");
//dd($goods->title);
            $data = [
              "type"  => "러블리마켓 구매",
              "image" => $goods->goodsImage,
              "title" => '',
              "sub_data"=> $goods
            ];

            $title = $order ? $order->orderGoods[0]->marketSeller->seller->name . " [{$goods->title}]" : "기타";
            $listTitle = $goods->title;
            if ($order && count($order->orderGoods) > 1) {
              $count = count($order->orderGoods) - 1;
              $title .= " 외 {$count}건";
              $listTitle.= " 외 {$count}건";
            }
            $data['title'] = $title;
            $item->title = $listTitle;
            $item->data = $data;
//            if ($type == "all" || $type == "buy") {
              $cashArr[] = $item;
//            }
//          } catch (Exception $e) {
//            $countes[3] -= 1;
//            $countes[0] -= 1;
//          }
        } else if (! isset($item->market_seller_idx) && $item->type == "오프라인") {
//          try {
            $data = [
              "type"  => "CU 구매",
              "image" => 'https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/thumb/cu_icon.png',
              "title" => 'CU 상품 구매',
            ];
            $item->data = $data;
//            if ($type == "all" || $type == "buy") {
              $cashArr[] = $item;
//            }
//          } catch (Exception $e) {
//            $countes[3] -= 1;
//            $countes[0] -= 1;
//          }
        } else if (isset($item->order_info_idx) && $item->type == '온라인') {
//          try {
            $order = Order::with(['detail'])->find($item->order_info_idx);
            $data = [
              "type"  => "스토어 구매",
              "image" => isset($order->detail[0]) ? $order->detail[0]->goods->imgs : '',
              "title" => '',
              "sub_data"=> $order
            ];

            $title = $order["detail"][0]["seller"]["name"] . " [{$order["detail"][0]["goods"]["title"]}]";
            if ($order["detail"][0]["goods_title"]) {
              $title = $order["detail"][0]["seller"]["name"] . "[{$order["detail"][0]["goods_title"]}]";
            }
            $listTitle = $order["detail"][0]["goods"]["title"];
            if ($order["detail"][0]["goods_title"]) {
              $listTitle = $order["detail"][0]["goods_title"];
            }
//            dd($order["detail"]);
            if (isset($order["detail"]) && count($order["detail"]) > 1) {
              $count = count($order["detail"]) - 1;
              $title .= " 외 {$count}건";
              $listTitle .= " 외 {$count}건";

            }
            $item->title = $listTitle;
            $data['title'] = $title;
            $item->data = $data;
            if (isset($order->detail[0]->goods->deleted_at)) {
              $item->idx = 0;
              $item->order_info_idx = 0;
            }

//            if ($type == "all" || $type == "buy") {
              $cashArr[] = $item;
//            }
//          } catch (Exception $e) {
//            $countes[3] -= 1;
//            $countes[0] -= 1;
//          }
        }
      } else if ($item->action == '환불') {
        if ($item->amount > 0 && $item->type == '오프라인' && isset($item->market_seller_idx)) {
//          try {
            // 러블리마켓 구매환불
            $orderGoods = OrderGoodsTbl::where([['order_info_idx', $item->order_info_idx], ['state', '환불']])->first();
            $data = [
              "type"  => "러블리마켓 구매취소",
              "image" => $orderGoods->goods->goodsImage,
              "title" => '',
              "sub_data"=> $orderGoods
            ];

            $title = $orderGoods->marketSeller->seller->name . " [{$orderGoods->goods->title}]";
            $data['title'] = $title;
            $item->data = $data;
//            if ($type == "all" || $type == "cancel") {
              $cashArr[] = $item;
//            }
//
//          } catch (Exception $e) {
//            $countes[4] -= 1;
//            $countes[0] -= 1;
//          }
        } else if ($item->amount > 0 && $item->type == '온라인' && isset($item->order_info_idx)) {

          try {
            $detail = PaymentCancel::where([["order_id", $item->order_info_idx], ['method', 'lmpay']])->first()->detail;

            $data = [
              "type"  => "스토어 구매취소 및 반품",
              "image" => $detail->goods->imgs,
              "title" => '',
              "sub_data"=> $detail
            ];

            $title = $detail->seller->name . " [{$detail->goods->title}]";
            if ($detail->goods_title) {
              $title = $detail->seller->name . " [{$detail->goods_title}]";
            }
            $data['title'] = $title;
            $item->data = $data;
//            if ($type == "all" || $type == "cancel") {
              $cashArr[] = $item;
//            }
          } catch (Exception $e) {
            $catchCnt += 1;
//            $countes[4] -= 1;
//            $countes[0] -= 1;
          }
        } else if ($item->type == "오프라인" && !isset($item->market_seller_idx) && isset($item->order_info_idx)) {
          $data = [
            "type"  => "CU 구매",
            "image" => 'https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/thumb/cu_icon.png',
            "title" => 'CU 상품 구매',
          ];
          $item->data = $data;
          $cashArr[] = $item;
        } else {
//          if ($type == "all" || $type == "refund") {
            $cashArr[] = $item;
//          }
        }
      } else {
//        if ($type == "all" || $type == "charge") {
          $cashArr[] = $item;
//        }
      }
    }

    if(count($cashArr) > 0 && isset($detail_idx)) {

      if(isset($cashArr[0])) {
        $cashArr = $cashArr[0];
      } else {
        $cashArr = json_decode("{}");
      }
    } else {
      $dateRes = [];
      $dateSeq = -1;
      $dateVal = '';
      foreach ($cashArr as $item) {
        if ($item->insert_day != $dateVal) {
          $dateVal = $item->insert_day;
          $dateSeq++;
          $dateRes[$dateSeq]["day"] = $item->insert_day;
          $dateRes[$dateSeq]["week"] = $item->dayofweek;
          $dateRes[$dateSeq]["data"] = [];
        }
        array_push($dateRes[$dateSeq]["data"], $item);
      }

      $cashArr = $dateRes;
    }
    $response = array(
      $resCnt
    );
    $lastPage = ceil(count($resCnt) / $perPage);
    $response = [
      "total" => count($resCnt),
      "data" => $cashArr,
      "current_page" => (int)$page,
      "last_page" => $lastPage
    ];
    if(isset($detail_idx)) $response = $cashArr;
    return $this->resultOk($response);
  }

  public function apiRefund($type)
  {
    @session_start();
    $user_id = $this->userIdx;
    if(!$user_id) return $this->resultFail("NO_LOGIN", "로그인 후 이용 가능합니다.");

    $dao = new CUserDAO();
    $user_data = $dao->getUserInfo($user_id)[0];
//      $user_data = $this->getUser($this->conn, $user_id);
      $refund = new RefundController();

      switch ($type) {
        case "view" :
        {
          $have_refund_account = false;
          if (isset($user_data->refund_account)) {
            $have_refund_account = true;
          }

          $res = $refund->amount($user_id);

          $data = [
            "total_amount"        => number_format($res['data']['trans'] + $res['data']['card']),
            "trans_amount"        => number_format($res['data']['trans']),
            "card_amount"         => number_format($res['data']['card']),
            "have_refund_account" => $have_refund_account,
          ];
//          dd($data);
          $response["data"] = $data;
          $response["data"]["bank"] = [
            "bank"         => $user_data->refund_bank,
            "bank_account" => $user_data->refund_account,
          ];

          break;
        }
        case "submit" :
        {
          // 환불 하기 Submit
          $res = $refund->amount($user_id, NULL, true);

          if ($res['required_bank']) {
            if (! isset($user_data->refund_account)) {
              $response["success"] = false;
              $response["msg"] = "환불에 계좌가 필요합니다.";
              $response["code"] = 400;
              return $this->resultFail($response);
            } else {
              $bank = [
                'bank'    => $user_data->refund_bank,
                'account' => $user_data->refund_account,
                'holder'  => $user_data->refund_owner,
              ];
              $refundRes = $refund->refund($user_id, NULL, $bank);
              $response["success"] = true;
            }
          } else {
            $response["success"] = true;
            $refundRes = $refund->refund($user_id);
          }

          if ($refundRes['success']) {
            $response["success"] = true;
//            return json_encode($response);
          } else {
            $response["success"] = false;
            $response["msg"] = $refundRes['message'];
            $response["code"] = 400;
            return $this->resultFail($response);
          }
          break;
        }
        case "result" :
        {
          $sql = "select cl2.`method` , cl.amount from cash_log cl 
            inner join cash_log cl2 on cl.cash_log_idx = cl2.idx 
            where cl.user_idx = {$user_id} and timestampdiff(minute, cl.insert_date , now()) <= 1
            and cl.`action` = '환불' 
            and cl2.`method` in ('현금','신용카드','계좌이체','가상계좌','페이코','삼성페이','무통장','CU현금','DBIN')
            order by cl.idx desc";

          $lmPaySql = "SELECT IFNULL(SUM(amount), 0) as amount FROM fp_pay.cash_log WHERE user_idx = {$user_id} AND delete_date IS NULL AND (`state` = '결제됨' OR (`action` = '환불' AND `state` = '결제대기') OR (action = '환불' AND state = '기타')) AND `action` != '취소'";
//          $dao = new CBaseDAO();
          $refund = $dao->selectQuery($sql, "pay");
          $pay = $dao->selectQuery($lmPaySql, "pay");

          $refund_pay = 0;
          $resArray = array();
          foreach ($refund as $item) {
            $refund_pay = $refund_pay + $item->amount;
            if(isset($resArray[$item->method])) {
              $resArray[$item->method] = $resArray[$item->method] + $item->amount;
            } else {
              $resArray[$item->method] = $item->amount;
            }
          }
          $response = array(
            "refund_pay" => $refund_pay,
            "refund" => $resArray,
            "charge" => 0,
            "pay" => isset($pay[0]) ? $pay[0]->amount : 0,
          );
        }
      }



    return $this->resultOk($response);
  }

  public function setRefundAccount(Request $request) {
    $refund_bank = $this->getParam("refund_bank");
    $refund_account = $this->getParam("refund_account");
    $refund_owner = $this->getParam("refund_owner");
    $user_idx = $this->userIdx;
    if(!$user_idx) return $this->resultFail("NO_LOGIN", "로그인 후 이용 가능합니다.");
    $api = "";
    $key = "";
    if(IS_TEST) {
      $api = IMPORT_API_TEST;
      $key = IMPORT_KEY_TEST;
    } else {
      $api = IMPORT_API;
      $key = IMPORT_KEY;
    }

    $token = getToken($api, $key);
    if(!$token) {
      $response['success'] = false;
      $response['error_msg'] = "계좌 토큰 접근에 실패했습니다.";
      return $this->resultFail($response);
    }

    $bank_data = array(
      "refund_bank"=>$refund_bank,
      "refund_account"=>$refund_account,
      "refund_owner"=>$refund_owner
    );

    if(checkBank($token, $bank_data)) {
//      $update = $this->conn_w->table("user_list")
//        ->where("user_id","=",$_SESSION['user']['user_id'])
//        ->update([
//          "refund_bank"=>$refund_bank,
//          "refund_account"=>$refund_account,
//          "refund_owner"=>$refund_owner
//        ]);
      $data = array(
        "refund_bank"=>$refund_bank,
        "refund_account"=>$refund_account,
        "refund_owner"=>$refund_owner
      );
      $dao = new CBaseDAO();
      $dao->updateQuery("user_list", $data, "user_id = {$user_idx}", "db_w");

      return $this->resultOk();


    } else {
      $response['success'] = false;
      $response['error_msg'] = "계좌 실명 조회에 실패했습니다.";
      return $this->resultFail($response);
    }


  }

  public function getRefundResultInfo () {
    $user_idx = $this->userIdx;
    $sql = "select cl2.`method` , cl2.amount from cash_log cl 
            inner join cash_log cl2 on cl.cash_log_idx = cl2.idx 
            where cl.user_idx = {$user_idx} 
            and cl.`action` = '환불'
            order by cl.idx desc";

    $lmPaySql = "SELECT IFNULL(SUM(amount), 0) as amount FROM fp_pay.cash_log WHERE user_idx = {$user_idx} AND delete_date IS NULL AND (`state` = '결제됨' OR (`action` = '환불' AND `state` = '결제대기') OR (action = '환불' AND state = '기타')) AND `action` != '취소'";
    $dao = new CBaseDAO();
    $refund = $dao->selectQuery($sql, "pay");
    $pay = $dao->selectQuery($sql, "$lmPaySql");

    $res = array(
      "refund" => $refund,
      "charge" => 0,
      "pay" => $pay,
    );
    return $this->resultOk($res);
  }

  public function getCUBarcode () {
    $user_idx = $this->userIdx;
    $sql = "select * from cu_barcode_tbl where user_idx = {$user_idx} and is_used = 'N' and barcode != ''";
    $dao = new CBaseDAO();
    $barcode_res = $dao->selectQuery($sql, "pay");
    if (isset($barcode_res[0])) {
      $barcode = $barcode_res[0]->barcode;
    } else {
      $barcode = $this->makeCuBarcode($user_idx);
    }

    $DNS1D = new DNS1D();
    $barcode_img = '<img src="data:image/png;base64,' . $DNS1D->getBarcodePNG($barcode, "C128") . '" alt="barcode"   />';
    $barcode_data = "data:image/png;base64," . $DNS1D->getBarcodePNG($barcode, "C128");

    $data = [
      "barcode"      => $barcode,
      "barcode_img"  => $barcode_img,
      "barcode_data" => $barcode_data,
    ];
    return $this->resultOk($data);
  }

  private function makeCuBarcode($user_idx)
  {
    $barcode = "";

//    $res = $this->pconn_w->table("cu_barcode_tbl")
//      ->insertGetId([
//        "user_idx" => $user_idx,
//      ]);
    $data = array(
      "user_idx" => $user_idx
    );
    $dao = new CBaseDAO();
    $res = $dao->insertQuery("cu_barcode_tbl", $data, "pay_w");

    if ($res) {
      // 12자리
      $tmp_barcode = $res;
      while (strlen($tmp_barcode) < 12) {
        $tmp_barcode .= "0";

      }

      $barcode = "4303" . $tmp_barcode;
//      $update_res = $this->pconn_w->table("cu_barcode_tbl")
//        ->where("idx", "=", $res)
//        ->update([
//          "barcode" => $barcode,
//        ]);

      $data = array(
        "barcode" => $barcode
      );
      $dao = new CBaseDAO();
      $update_res = $dao->updateQuery("cu_barcode_tbl", $data,"idx = {$res}", "pay_w");

      if (! $update_res) {
        return false;
      } else {
        return $barcode;
      }

    } else {
      return false;
    }
  }


  public function setLmPasswordReset() {
    $user_idx = $this->userIdx;
    $phone = $this->getParam("phone");
    $code = $this->getParam("code");

    if(!$user_idx || $user_idx == 0) return $this->resultFail('NO_LOGIN', "로그인이 필요합니다.");

    $required = array(
      "phone", "code"
    );
    if (!$this->isRequired($required, $this->getParameters())) {
      return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    }

    $phone = preg_replace("/[^0-9]/", "", $phone);
    $dao = new CBaseDAO();
    $sql = "SELECT COUNT(*) as cnt FROM sms_auth_tbl WHERE number = '{$code}' AND phone = '{$phone}' AND time >= CURRENT_TIMESTAMP()";
    $resAuth = $dao->selectQuery($sql, "db")[0];

    if ($resAuth->cnt < 1) {
      return $this->resultFail("MISS_VALID", "올바른 인증번호가 아닙니다.");
    }


    $dao = new CBaseDAO();

    $data = array(
      "_NQ_sec_password" => "null"
    );
    $dao->updateQuery("user_list", $data, " user_id = {$user_idx}", "db_w");

    return $this->resultOk();
  }

}