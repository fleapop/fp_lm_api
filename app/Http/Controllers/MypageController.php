<?php


namespace App\Http\Controllers;


use App\Helper\CExhibitionHelper;
use App\Helper\CUserHelper;
use App\Http\dao\CBaseDAO;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MypageController extends Controller
{
  public function getUserInfo() {
    $t_user_idx = $this->getParam("t_user_idx");

    if(!$t_user_idx) return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    $m_user_idx = $this->userIdx;
    $isUser = $t_user_idx == $m_user_idx;
    $user_idx = $t_user_idx;
    $sql = "select ul.user_id, ul.account_type, ul.email_account, ul.fb_account, ul.password, ul.profile_img, ul.birth_day, ul.gender, ul.shipping_name, ul.shipping_phone, ul.shipping_zip_code, ul.shipping_address_1, ul.shipping_address_2, ul.option_contract, ul.barcode, ul.sec_password, ul.refund_bank, ul.refund_account, ul.refund_owner, ul.grade, ul.login_date, ul.insert_date, ul.update_date, ul.delete_date, ul.session_data, ul.barcode_idx, ul.description, ul.medal, ul.user_grade, ul.use_pay_type, ul.os_type, ul.os_version, ul.device_key, ul.device_model
                        , uei.introduce, uei.is_shopping_open, uei.user_height, uei.user_size, uei.user_foot_size, uei.user_skin, uei.is_body_open
                        , ul.nic_name
                        , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name_full
                     , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img_full
                     , if(ft3.idx is null, 0, 1) as is_follow
                     , (select count(ft.idx) from follow_tbl ft inner join user_list ul2 on ul2.user_id = ft.to_user_idx and ul2.delete_date is null where ft.from_user_idx = ul.user_id and ft.deleted_at is null) as from_follow_cnt
                     , (select count(ft2.idx) from follow_tbl ft2 inner join user_list ul2 on ul2.user_id = ft2.from_user_idx and ul2.delete_date is null where ft2.to_user_idx = ul.user_id and ft2.deleted_at is null) as to_follow_cnt
                     , (select count(*) as cnt from fp_pay.push_logs pl where pl.user_id = ul.user_id and `result` = 'success') as push_badge
                     , count(st.idx) as seller_cnt
                     ,uei.is_push_agree
                     ,uei.is_ad_push_agree
                 from fp_db.user_list ul 
                 left join fp_db.user_extra_info uei on ul.user_id = uei.user_id 
                 left join fp_db.like_tbl lt on ul.user_id = lt.user_idx and lt.delete_date is null and lt.table_name = 'seller_tbl'
                 left join fp_db.seller_tbl st on lt.table_idx = st.idx and st.display_state = 'show' and st.delete_date is null
                 left join fp_db.follow_tbl ft3 on ul.user_id = ft3.to_user_idx and ft3.from_user_idx = {$m_user_idx}
                 where ul.user_id = {$user_idx} and ul.delete_date is null
                 group by ul.user_id";

    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql,"db");
    if(!isset($res[0])) return $this->resultFail("NO_DATA","해당 회원이 존재하지 않습니다.");
    $response = array(
      "data" => isset($res[0]) ? $res[0] : json_decode("{}"),
      "isUser" => $isUser
    );
    return $this->resultOk($response);
  }

  public function getDiaryList($service_type) {
    $user_idx = $this->userIdx;
    $t_user_idx = $this->getParam("t_user_idx");

//    if($t_user_idx) $user_idx = $t_user_idx;

    $request = new Request();
    $communityController = new CommunityController($request);
    $List = $communityController->getListRouter($service_type, $t_user_idx,null,$user_idx);
    return $List;
  }

  public function getDiaryLikeList() {
    $user_idx = $this->userIdx;
    if(!$user_idx) return $this->resultFail("NO_LOGIN", "로그인이 필요합니다.");
    $dao = new CBaseDAO();
    $likeSql = "select count(master.idx) as cnt, master.service_type, cm_img, g_img 
                from (
select lt.idx,lt.table_name,ct.idx as cm_idx 
	                ,case when lt.table_name = 'goods' then 'goods'
	                	when ct.service_type = 'postscript' then 'fit'
	                	when lt.table_name = 'user_content_tbl' then 'magazine'
	                	else ct.service_type end as service_type
                	,case when lt.table_name = 'cm_timeline' then concat('https://fps3bucket.s3.ap-northeast-2.amazonaws.com',ci.img_path ,ci.img_name)
                		when lt.table_name = 'user_content_tbl' then CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_contents/',uct2.image)
                		end as cm_img 
	                ,g.*,g.id as g_idx, g.imgs as g_img
	                from fp_db.like_tbl lt 
	                left join fp_db.cm_timeline ct on lt.table_idx = ct.idx  and lt.table_name = 'cm_timeline' and ct.deleted_at is null and ct.claim_deleted_at is null
	                left join fp_db.cm_imgs ci on ct.idx = ci.service_idx and ct.service_type = ci.service_type and ci.deleted_at is null
	                left join fp_pay.goods g on lt.table_idx = g.id and lt.table_name = 'goods' and g.deleted_at is null and g.display_state = 'show'
	                left join fp_db.user_content_tbl uct on uct.idx = lt.table_idx and lt.table_name = 'user_content_tbl'
	                left join (select master_tmp.* from ( 
		            		select uct_tmp.*
			                from user_content_tbl uct_tmp 
			                inner join like_tbl lt_tmp on lt_tmp.table_name = 'user_content_tbl' and uct_tmp.idx = lt_tmp.table_idx and lt_tmp.user_idx = {$user_idx}
			                where uct_tmp.content_type = 'magazine' and uct_tmp.delete_date is null 
			                group by uct_tmp.idx
		                ) as master_tmp
		                inner join user_content_tbl uct2_tmp on (master_tmp.idx = uct2_tmp.idx or master_tmp.parent_idx = uct2_tmp.idx)
		                where uct2_tmp.parent_idx = 0
		                group by uct2_tmp.idx) uct2 
		                on uct2.idx = lt.table_idx and lt.table_name = 'user_content_tbl' 
	                where lt.user_idx = {$user_idx} and lt.table_name in ('cm_timeline', 'goods', 'user_content_tbl')
	                and (g.id is not null or uct2.idx is not null or ct.idx is not null)
	                #and g.deleted_at is null and ct.deleted_at is null
	                group by lt.idx
	                order by lt.idx desc
                ) as master
                group by master.service_type
                order by master.idx desc";
    $likeRes = $dao->selectQuery($likeSql);

    $data = array();
    foreach ($likeRes as $item) {
      $likeData = array(
          "category" => $item->service_type,
          "img" => isset($item->cm_img) ? $item->cm_img : $item->g_img,
          "cnt" => $item->cnt
        );
      array_push($data, $likeData);
    }
    return $this->resultOk($data);
  }

  public function getServiceLikeList($service_type) {
    $user_idx = $this->userIdx;
    $t_user_idx = $this->getParam("t_user_idx");
    $tUserIdx = $user_idx;
    if($t_user_idx) $tUserIdx = $t_user_idx;

    if ($service_type == "magazine") {
      $dao = new CBaseDAO();
      $page = $this->getParam("page", 1);
      $perPage = 40;
      $limit = $dao->getLimit($page, $perPage);
      $sql = "select uct2.idx, uct2.view_order, uct2.parent_idx, uct2.content_order,uct2.content_type,uct2.user_idx,uct2.source,uct2.content, uct2.source_bg_color
                , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_contents/', uct2.image) AS image from ( 
            		select uct.*, lt.idx as like_idx
	                from user_content_tbl uct 
	                inner join like_tbl lt on lt.table_name = 'user_content_tbl' and uct.idx = lt.table_idx and lt.user_idx = {$tUserIdx} 
	                where uct.content_type = 'magazine' and uct.delete_date is null 
	                group by uct.idx
                ) as master
                inner join user_content_tbl uct2 on (master.idx = uct2.idx or master.parent_idx = uct2.idx)
                where uct2.parent_idx = 0
                group by uct2.idx
                order by master.like_idx desc
                {$limit}";
      $res = $dao->selectQuery("$sql");
      $cntSql = "select uct2.idx, uct2.view_order, uct2.parent_idx, uct2.content_order,uct2.content_type,uct2.user_idx,uct2.source,uct2.content, uct2.source_bg_color
                , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_contents/', uct2.image) AS image from ( 
            		select uct.*
	                from user_content_tbl uct 
	                inner join like_tbl lt on lt.table_name = 'user_content_tbl' and uct.idx = lt.table_idx and lt.user_idx = {$tUserIdx} 
	                where uct.content_type = 'magazine' and uct.delete_date is null 
	                group by uct.idx
                ) as master
                inner join user_content_tbl uct2 on (master.idx = uct2.idx or master.parent_idx = uct2.idx)
                where uct2.parent_idx = 0
                group by uct2.idx";
      $cntRes = $dao->selectQuery($cntSql);
      $last_page = ceil(count($cntRes) / $perPage);
      $response = [
        "current_page" => (int)$page,
        "data" => $res,
        "last_page" => $last_page,
        "total" => count($cntRes)
      ];
      return $this->resultOk($response);
    } else {
      // Community
      $request = new Request();
      $communityController = new CommunityController($request);
      $List = $communityController->getListRouter($service_type, $user_idx, true,$tUserIdx);
      return $List;
    }
  }

  public function getShoppingbagOrderList () {
    $dao = new CBaseDAO();
    $page = $this->getParam("page", 1);
    $tempUserId = $this->getParam("t_user_idx");
    $perPage = 40;
    $limit = $dao->getLimit($page, $perPage);

    $user_id = $tempUserId ? $tempUserId : $this->userIdx;

    $erSel = "";
    $erJoin = "";
    $exhibitionRep = new CExhibitionHelper();
    $erRes = $exhibitionRep->getExhibitionRepMenu()['result'];
    if ($erRes) {
      $erSel = " , atm_tbl.exhibition_name";
      $erJoin = "left join (select sub.list_tag_name  as exhibition_name,eg.goods_id from exhibition_goods eg
  inner join (
select e.id, e.list_tag_name
from fp_pay.app_top_menu atm
inner join fp_pay.exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true'
where atm.display = 'show' and e.is_hidden = 'true' and e.deleted_at is null and (e.start_date <= now() and e.end_date >= now())
order by e.id desc limit 1
) as sub on eg.exhibition_id = sub.id and eg.deleted_at is null) as atm_tbl on g.id = atm_tbl.goods_id";
    }

    $sql = "select g.id, g.title ,g.price
                , if(g.use_discount = 1, IFNULL(price - IFNULL((SELECT amount FROM fp_pay.promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0), g.price) AS promotion_price
                , g.use_discount , g.imgs , g.stock_state , g.`view` ,st.name as seller_name, st.idx as seller_id
                , IF(TIMESTAMPDIFF(hour,g.created_at, now()) <= 48, 1, 0) as is_new
                , IF(sp.idx is null, 0, 1) as is_hotdeal
                , sp.ended_at as hotdeal_end
                , g.like_count
                {$erSel}
                from fp_pay.goods g
                inner join fp_pay.order_detail od on g.id = od.goods_id and od.state = '구매확정'
                inner join fp_pay.orders o on od.order_id = o.id and o.user_id = {$user_id} and o.state != '결제실패'
                left join fp_db.seller_tbl st on g.seller_id = st.idx
                left join fp_pay.goods_promotions sp on sp.goods_id = g.id and (sp.started_at <= now() and sp.ended_at >= now()) and sp.display_state = 'show'
                {$erJoin}
                where g.display_state = 'show' and g.deleted_at is null
                and st.display_state = 'show' and st.delete_date is null
                and g.seller_id != 431
                group by g.id
                order by g.created_at desc
                {$limit}";

    $goodsRes = $dao->selectQuery($sql, "pay");

    $sqlCnt = "select g.*
                from fp_pay.goods g
                inner join fp_pay.order_detail od on g.id = od.goods_id and od.state = '구매확정'
                inner join fp_pay.orders o on od.order_id = o.id and o.user_id = {$user_id} and o.state != '결제실패'
                left join fp_db.seller_tbl as st on g.seller_id = st.idx
                where g.display_state = 'show' and g.deleted_at is null
                and st.display_state = 'show' and st.delete_date is null
                and g.seller_id != 431
                group by g.id
                order by g.created_at desc";
    $goodsResCnt = count($dao->selectQuery($sqlCnt, "pay"));

    $optionSql = "select is_shopping_open from user_extra_info where user_id = {$user_id}";
    $optionRes = $dao->selectQuery($optionSql);
    $isShoppingOpen = isset($optionRes[0]) ? $optionRes[0]->is_shopping_open : 0;

    $last_page = ceil((int)$goodsResCnt / $perPage);
    $response = [
      "current_page" => (int)$page,
      "data" => $goodsRes,
      "last_page" => $last_page,
      "total" => $goodsResCnt,
      "isShoppingOpen" => $isShoppingOpen
    ];
    return $this->resultOk($response);
  }

  public function setMyShoppingBag() {
    $t_user_idx = $this->getParam("t_user_idx");
    $state = $this->getParam("state");

    if(!$t_user_idx) return $this->resultFail("NO_REQUIRED", "필수입력 정보가 누락되었습니다.");
    $m_user_idx = $this->userIdx;
    $isUser = $t_user_idx == $m_user_idx;
    $user_idx = $t_user_idx;
    if(!$isUser) return $this->resultFail("MISS_VALID", "본인정보만 설정 할수 있습니다.");
    $data = array(
      "is_shopping_open" => $state
    );
    $dao = new CBaseDAO();
    $dao->updateQuery("user_extra_info", $data, "user_id = $user_idx", "db_w");
    return $this->resultOk();
  }

  // type : from, to, seller
  public function getFollowList($type) {
    $search = $this->getParam("search");
    $t_user_idx = $this->getParam("t_user_idx");
    $page = $this->getParam("page",1);
    $user_idx = $t_user_idx;
    $my_user_idx = $this->userIdx == $user_idx ? $user_idx : $this->userIdx;
    $perPage = 40;

    $dao = new CBaseDAO();
    $limit = $dao->getLimit($page, $perPage);
    $sql1 = "select ul.user_id ,if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name 
,  if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img 
,uei.introduce
               , if(ft2.idx is null, 0, 1 ) as is_mine
               #,if(sum(if(ctr.idx is null, 1, 0)) > 0 , 1, 0) as is_badge
               , 0 as is_badge
               from follow_tbl ft 
               inner join user_list ul on ft.from_user_idx = ul.user_id and ul.delete_date is null
               inner join user_extra_info uei on ul.user_id = uei.user_id
               left join follow_tbl ft2 on ft2.to_user_idx = ul.user_id and ft2.from_user_idx = {$my_user_idx}
               left join cm_timeline ct on ct.user_idx = ul.user_id and ct.deleted_at is null and ct.claim_deleted_at is null
               left join cm_timeline_read ctr on ctr.service_idx = ct.idx and ctr.user_idx = {$user_idx}
               where ft.to_user_idx = {$user_idx} and if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) like '%{$search}%'
               group by ft.idx
               ";
    $sql2 = "select ul.user_id ,if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = ''
, ul.shipping_name, ul.nic_name) as nic_name 
,  if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img 
,uei.introduce
                ,if(sum(if(ctr.idx is null and ct.idx is not null, 1, 0)) > 0 , 1, 0) as is_badge
                , if(ft2.idx is null, 0, 1 ) as is_mine
               from follow_tbl ft 
               inner join user_list ul on ft.to_user_idx = ul.user_id and ul.delete_date is null
               inner join user_extra_info uei on ul.user_id = uei.user_id
               left join follow_tbl ft2 on ft2.to_user_idx = ul.user_id and ft2.from_user_idx = {$my_user_idx}
               left join cm_timeline ct on ct.user_idx = ul.user_id and ct.deleted_at is null and ct.claim_deleted_at is null and ct.service_type != 'ldlife'
               left join cm_timeline_read ctr on ctr.service_idx = ct.idx and ctr.user_idx = {$user_idx} 
               where ft.from_user_idx = {$user_idx} and if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) like '%{$search}%'
               group by ft.idx
               ";
    $sql3 = "select st.idx ,st.name , st.introduce, count(g.id) as goods_cnt, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/',st.profile_img) as profile_image
                , if(lt2.idx is null, 0, 1 ) as is_mine
               from like_tbl lt 
               inner join seller_tbl st on lt.table_idx = st.idx 
               left join fp_pay.goods g on st.idx = g.seller_id and g.display_state = 'show' and g.deleted_at is null
                      and TIMESTAMPDIFF(hour,g.created_at, now()) <= 48
               left join  like_tbl lt2 on st.idx = lt2.table_idx and lt2.table_name = 'seller_tbl' and lt2.user_idx = {$my_user_idx}
               where lt.table_name = 'seller_tbl' and lt.user_idx = {$user_idx}
               and st.display_state = 'show' and st.delete_date is null
               and st.name like '%{$search}%'
               group by st.idx
              ";
//dd($sql2);
    $res = array();
    $resCnt = 0;
    if($type == "from") {
      $res = $dao->selectQuery($sql1.$limit, "db");
      $resCnt = count($dao->selectQuery($sql1, "db"));
    } else if($type == "to") {
      $res = $dao->selectQuery($sql2.$limit, "db");
      $resCnt = count($dao->selectQuery($sql2, "db"));
    } else if($type == "seller") {
      $res = $dao->selectQuery($sql3.$limit, "db");
      $resCnt = count($dao->selectQuery($sql3, "db"));
    }
    $last_page = ceil((int)$resCnt / $perPage);
    $response = [
      "current_page" => (int)$page,
      "data" => $res,
      "last_page" => $last_page,
      "total" => $resCnt
    ];
    return $this->resultOk($response);
  }


  public function getFollowCount() {

    $t_user_idx = $this->getParam("t_user_idx");
    $user_idx = $t_user_idx;

    $dao = new CBaseDAO();
    $sql1 = "select ul.user_id ,if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name  ,uei.introduce
               , if(ft2.idx is null, 0, 1 ) as is_mine
               from follow_tbl ft 
               inner join user_list ul on ft.from_user_idx = ul.user_id  and ul.delete_date is null
               inner join user_extra_info uei on ul.user_id = uei.user_id
               left join follow_tbl ft2 on ft2.to_user_idx = ft.from_user_idx and ft2.from_user_idx = {$user_idx}
               where ft.to_user_idx = {$user_idx} 
               ";
    $sql2 = "
               select ul.user_id ,if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name  ,uei.introduce
               from follow_tbl ft 
               inner join user_list ul on ft.to_user_idx = ul.user_id  and ul.delete_date is null
               inner join user_extra_info uei on ul.user_id = uei.user_id
               where ft.from_user_idx = {$user_idx} 
               group by ft.idx
               ";
    $sql3 = "select st.idx ,st.name , st.introduce, count(g.id) as goods_cnt
               from like_tbl lt 
               inner join seller_tbl st on lt.table_idx = st.idx 
               left join fp_pay.goods g on st.idx = g.seller_id and g.display_state = 'show' and g.deleted_at is null
                      and TIMESTAMPDIFF(hour,g.created_at, now()) <= 48
               where table_name = 'seller_tbl' and user_idx = {$user_idx}
               and st.display_state = 'show' and st.delete_date is null
               group by st.idx
              ";

      $resCnt1 = count($dao->selectQuery($sql1, "db"));
      $resCnt2 = count($dao->selectQuery($sql2, "db"));
      $resCnt3 = count($dao->selectQuery($sql3, "db"));

    $response = [
      "from" => $resCnt1,
      "to" => $resCnt2,
      "seller" => $resCnt3
    ];
    return $this->resultOk($response);
  }

  public function getMyShoppingInfo () {
    $user_idx = $this->userIdx;

    $lmPaySql = "SELECT IFNULL(SUM(amount), 0) as amount FROM fp_pay.cash_log WHERE user_idx = {$user_idx} AND delete_date IS NULL AND (`state` = '결제됨' OR (`action` = '환불' AND `state` = '결제대기') OR (action = '환불' AND state = '기타')) AND `action` != '취소'";
    $sqlOrderBadge = "select *
        from orders o 
        inner join order_detail od on o.id = od.order_id and o.state = '결제완료' and od.state not in ('배송완료', '구매확정')
        where o.user_id = {$user_idx}
        order by o.id desc";
    $sqlReview = "select od.order_id ,od.seller_id , st.name , od.goods_id , g.title ,g.imgs ,DATE_FORMAT(od.confirmed_at, '%Y년 %m월 %d일') as confirmed_at, GROUP_CONCAT(o2.value) as option_name
                , case when g.sub_category = '악세사리' then 'acc'
                 		when g.sub_category = '뷰티' then 'beauty'
                 		else 'etc' end as option_type
                from fp_pay.order_detail od 
                inner join fp_pay.orders o on od.order_id = o.id  
                inner join fp_db.seller_tbl st on od.seller_id = st.idx 
                inner join fp_pay.goods g on od.goods_id = g.id and g.deleted_at is null and g.display_state = 'show'
                inner join fp_pay.option_sku os on os.sku_id = od.sku_id 
                inner join fp_pay.`options` o2 on os.option_id = o2.id 
                left join fp_db.postscript_tbl pt on od.id = pt.order_detail_id and od.state = '구매확정' and od.deleted_at is null and pt.deleted_at is null
				where od.state = '구매확정' and od.deleted_at is null and pt.deleted_at is null and o.user_id = {$user_idx}
				and pt.idx is null
				group by od.id 
				order by od.confirmed_at desc";
    $dao = new CBaseDAO();
    $res1 = $dao->selectQuery($lmPaySql, "db")[0];
    $res2 = $dao->selectQuery($sqlOrderBadge, "pay");
    $res3 = $dao->selectQuery($sqlReview, "pay");

    $userHelper = new CUserHelper();

    $data = array(
      "lmpay" => $res1->amount,
      "orders" => count($res2) > 0 ? 1 : 0,
      "review" => count($res3),
      "notice" => $userHelper->pushBacdge($user_idx)
    );
    return $this->resultOk($data);
  }

  public function getOrderList() {
    $user_idx = $this->userIdx;
//    $user_idx = 48;
    $page = $this->getParam("page", 1);
    $term = $this->getParam("term");
    $category = $this->getParam("category");
    $state = $this->getParam("state");

    $perPage = 40;
    $dao = new CBaseDAO();
    $limit = $dao->getLimit($page, $perPage);
    $cateJoin = "";
    $termWhere = "";
    $termJoinWhere = "";
//    $cateWhere = " and od.state in ('배송준비','배송중','배송완료','구매확정', '상품준비')";
    $cateWhere = "";
    $cateUnionWhere = "";

    $endDate = "";
    if ($category != "refund" && $category != "exchange") {

      switch ($state) {
        case 'ready' :
          $cateWhere = " and od.state = '상품준비'";
          $cateUnionWhere = " and oit.delivery_state = '상품준비'";
          break;

        case 'shipready' :
          $cateWhere = " and od.state = '배송준비'";
          $cateUnionWhere = " and oit.delivery_state = '배송준비'";
          break;

        case 'shipping' :
          $cateWhere = " and od.state = '배송중'";
          $cateUnionWhere = " and oit.delivery_state = '배송중'";
          break;

        case 'shipcomplete' :
          $cateWhere = " and od.state = '배송완료'";
          $cateUnionWhere = " and oit.delivery_state = '배송완료'";
          break;

        case 'complete' :
          $cateWhere = " and od.state = '구매확정'";
          $cateUnionWhere = " and oit.delivery_state = '구매확정'";
          break;
      }
    }

    if($category == "refund") {
      $cateJoin = " and od.state like '반품%'";
    } else if($category == "exchange") {
      $cateJoin = " and od.state like '교환%' ";
    }

    switch ($term) {
      case '1w':
        $endDate = Carbon::now()->subWeek(1)->toDateTimeString();
        $termWhere = " and od.created_at >= '{$endDate}'";
        $termJoinWhere = " and ogt.insert_date  >= '{$endDate}' ";
        break;

      case '1m':
        $endDate = Carbon::now()->subMonth(1)->toDateTimeString();
        $termWhere = " and od.created_at >= '{$endDate}'";
        $termJoinWhere = " and ogt.insert_date  >= '{$endDate}' ";
        break;

      case '3m':
        $endDate = Carbon::now()->subMonth(3)->toDateTimeString();
        $termWhere = " and od.created_at >= '{$endDate}'";
        $termJoinWhere = " and ogt.insert_date  >= '{$endDate}' ";
        break;

      case '6m':
        $endDate = Carbon::now()->subMonth(6)->toDateTimeString();
        $termWhere = " and od.created_at >= '{$endDate}'";
        $termJoinWhere = " and ogt.insert_date  >= '{$endDate}' ";
        break;
    }


    $union = "union all
     select 'offline' as order_type, oit.order_number,cl.idx ,ogt.goods_idx , '러블리마켓 구매' as seller_name
	, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/', gt.image, if(gt.image = '1', '.jpg',''))  as goods_img
	, gt.title 
	, ogt.price , ogt.quantity 
	, CONCAT( gt.option_value1 ,if(gt.option_value2 is null or gt.option_value2 = '', '', concat(',',gt.option_value2) )) as option_name
	, DATE_FORMAT( ogt.insert_date , '%Y.%m.%d') as created_at
	, cl.idx as cash_log_idx 
	, '구매확정' as order_state
	from fp_pay.order_goods_tbl ogt 
	inner join fp_pay.order_info_tbl oit on ogt.order_info_idx = oit.idx 		
    left join fp_pay.goods_tbl gt on ogt.goods_idx = gt.idx 
    left join fp_pay.cash_log cl on cl.order_info_idx = oit.idx and cl.user_idx = oit.user_idx 
	where ogt.state in ('배송', '현장', '직접입력') and ogt.delete_date is null  and ogt.unexposed_at is null
	{$cateUnionWhere}
	{$termJoinWhere}
	and oit.user_idx = {$user_idx}";


    $sql = "select 'online' as order_type,o.order_number,od.order_id ,g.id as goods_idx, st.name as seller_name
		, g.imgs as goods_img
		, g.title 
		, od.total_price , od.quantity 
		, (select GROUP_CONCAT(o2.value) as option_name from option_sku os inner join `options` o2 on os.option_id = o2.id where sku_id = od.sku_id) as option_name
		, DATE_FORMAT( od.created_at , '%Y.%m.%d') as created_at 
		, 0 as cash_log_idx
		, od.state as order_state
    from fp_pay.orders o 
    inner join fp_pay.payment p on o.id = p.order_id and p.deleted_at is null
    inner join fp_pay.order_detail od on o.id = od.order_id and od.deleted_at is null and o.deleted_at is null and od.unexposed_at is null
    inner join fp_pay.delivery d on d.detail_id = od.id and d.deleted_at is null
{$cateJoin}
    left join fp_db.seller_tbl st on od.seller_id = st.idx 
    left join fp_pay.goods g on od.goods_id = g.id 
    where o.deleted_at is null and o.user_id = {$user_idx} and o.user_type = 'user'
     {$termWhere}
    {$cateWhere}
    group by od.id
     {$union}
	order by created_at desc , order_number desc, seller_name desc";

    $res = $dao->selectQuery($sql.$limit,"pay");
    $resCnt = $dao->selectQuery($sql,"pay");

    $data = array();
    $orderSeq = -1;
    $sellerSeq = -1;
    $itemSeq = -1;
    $orderNumber = "";
    $sellerName = "";
    foreach ($res as $item) {
      if($item->order_number != $orderNumber) {
        $orderNumber = $item->order_number;
        $orderSeq++;
        $sellerSeq = -1;
        $data[$orderSeq]["date"] = $item->created_at;
        $data[$orderSeq]["order_number"] = $item->order_number;
        $data[$orderSeq]["order_type"] = $item->order_type;
      }
      if ($item->seller_name != $sellerName || ($sellerSeq < 0 && $item->seller_name == $sellerName)) {
        $sellerName = $item->seller_name;
        $sellerSeq++;
        $itemSeq = -1;
        $data[$orderSeq]["seller"][$sellerSeq]["seller_name"] = $item->seller_name;
      }
      $itemSeq++;
      $data[$orderSeq]["seller"][$sellerSeq]["order"][$itemSeq] = $item;
    }
    $counts = $this->orderListQuery($user_idx, $term);
    $last_page = ceil(count($resCnt) / $perPage);
    $response = [
      "current_page" => (int)$page,
      "data" => $data,
      "last_page" => $last_page,
      "total" => count($resCnt),
      "count" => $counts
    ];
    return $this->resultOk($response);
  }

  function orderListQuery($user_idx, $term) {
    $stateArray = array('all','ready', 'shipready', 'shipping', 'shipcomplete', 'complete');
    $data = array();
    $termWhere = "";
    $termJoinWhere = "";
    foreach ($stateArray as $state) {
      switch ($term) {
        case '1w':
          $endDate = Carbon::now()->subWeek(1)->toDateTimeString();
          $termWhere = " and od.created_at >= '{$endDate}'";
          $termJoinWhere = " and ogt.insert_date  >= '{$endDate}' ";
          break;

        case '1m':
          $endDate = Carbon::now()->subMonth(1)->toDateTimeString();
          $termWhere = " and od.created_at >= '{$endDate}'";
          $termJoinWhere = " and ogt.insert_date  >= '{$endDate}' ";
          break;

        case '3m':
          $endDate = Carbon::now()->subMonth(3)->toDateTimeString();
          $termWhere = " and od.created_at >= '{$endDate}'";
          $termJoinWhere = " and ogt.insert_date  >= '{$endDate}' ";
          break;

        case '6m':
          $endDate = Carbon::now()->subMonth(6)->toDateTimeString();
          $termWhere = " and od.created_at >= '{$endDate}'";
          $termJoinWhere = " and ogt.insert_date  >= '{$endDate}' ";
          break;
      }
      $cateUnionWhere = "";
      $cateWhere ="";
      switch ($state) {
        case 'ready' :
          $cateWhere = " and od.state = '상품준비'";
          $cateUnionWhere = " and oit.delivery_state = '상품준비'";
          break;

        case 'shipready' :
          $cateWhere = " and od.state = '배송준비'";
          $cateUnionWhere = " and oit.delivery_state = '배송준비'";
          break;

        case 'shipping' :
          $cateWhere = " and od.state = '배송중'";
          $cateUnionWhere = " and oit.delivery_state = '배송중'";
          break;

        case 'shipcomplete' :
          $cateWhere = " and od.state = '배송완료'";
          $cateUnionWhere = " and oit.delivery_state = '배송완료'";
          break;

        case 'complete' :
          $cateWhere = " and od.state = '구매확정'";
          $cateUnionWhere = " and oit.delivery_state = '구매확정'";
          break;
      }
      $cateJoin = "";
      if($state == "refund") {
        $cateJoin = " and od.state like '반품%'";
      } else if($state == "exchange") {
        $cateJoin = " and od.state like '교환%' ";
      }
      $dao = new CBaseDAO();
//      $union = "union all
//           select 'offline' as order_type, oit.order_number,oit.idx ,ogt.goods_idx , '러블리마켓 구매' as seller_name
//        , CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/', gt.image, if(gt.image = '1', '.jpg',''))  as goods_img
//        , gt.title
//        , oit.normal_price , ogt.quantity
//        , CONCAT( gt.option_value1 ,if(gt.option_value2 is null or gt.option_value2 = '', '', concat(',',gt.option_value2) )) as option_name
//        , DATE_FORMAT( ogt.insert_date , '%Y.%m.%d') as created_at
//        , cl.idx as cash_log_idx
//        from fp_pay.order_goods_tbl ogt
//        inner join fp_pay.order_info_tbl oit on ogt.order_info_idx = oit.idx
//          left join fp_pay.goods_tbl gt on ogt.goods_idx = gt.idx
//          left join fp_pay.cash_log cl on cl.order_info_idx = oit.idx and cl.user_idx = oit.user_idx
//        where ogt.state in ('배송', '현장', '직접입력') and ogt.delete_date is null
//        {$cateUnionWhere}
//        {$termJoinWhere}
//        and oit.user_idx = {$user_idx}";
//
//            $sql = "select 'online' as order_type,o.order_number,od.order_id ,g.id as goods_idx, st.name as seller_name
//          , g.imgs as goods_img
//          , g.title
//          , o.total_price , od.quantity
//          , (select GROUP_CONCAT(o2.value) as option_name from option_sku os inner join `options` o2 on os.option_id = o2.id where sku_id = od.sku_id) as option_name
//          , DATE_FORMAT( od.created_at , '%Y.%m.%d') as created_at
//          , 0 as cash_log_idx
//          from fp_pay.orders o
//          inner join fp_pay.payment p on o.id = p.order_id and p.deleted_at is null
//          inner join fp_pay.order_detail od on o.id = od.order_id and od.deleted_at is null and o.deleted_at is null
//          inner join fp_pay.delivery d on d.detail_id = od.id and d.deleted_at is null
//          left join fp_db.seller_tbl st on od.seller_id = st.idx
//          left join fp_pay.goods g on od.goods_id = g.id
//          where o.deleted_at is null and o.user_id = {$user_idx} and o.user_type = 'user'
//          {$termWhere}
//          {$cateWhere}
//          group by od.id
//           {$union} ";
//
//
//      $res = $dao->selectQuery($sql,"pay");
//      $data[$state] = count($res);

      $union = "union all
     select 'offline' as order_type, oit.order_number,cl.idx ,ogt.goods_idx , '러블리마켓 구매' as seller_name
	, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/', gt.image, if(gt.image = '1', '.jpg',''))  as goods_img
	, gt.title 
	, oit.normal_price , ogt.quantity 
	, CONCAT( gt.option_value1 ,if(gt.option_value2 is null or gt.option_value2 = '', '', concat(',',gt.option_value2) )) as option_name
	, DATE_FORMAT( ogt.insert_date , '%Y.%m.%d') as created_at
	, cl.idx as cash_log_idx 
	, '구매확정' as order_state
	from fp_pay.order_goods_tbl ogt 
	inner join fp_pay.order_info_tbl oit on ogt.order_info_idx = oit.idx 		
    left join fp_pay.goods_tbl gt on ogt.goods_idx = gt.idx 
    left join fp_pay.cash_log cl on cl.order_info_idx = oit.idx and cl.user_idx = oit.user_idx 
	where ogt.state in ('배송', '현장', '직접입력') and ogt.delete_date is null  and ogt.unexposed_at is null
	{$cateUnionWhere}
	{$termJoinWhere}
	and oit.user_idx = {$user_idx}";


      $sql = "select 'online' as order_type,o.order_number,od.order_id ,g.id as goods_idx, st.name as seller_name
		, g.imgs as goods_img
		, g.title 
		, o.total_price , od.quantity 
		, (select GROUP_CONCAT(o2.value) as option_name from option_sku os inner join `options` o2 on os.option_id = o2.id where sku_id = od.sku_id) as option_name
		, DATE_FORMAT( od.created_at , '%Y.%m.%d') as created_at 
		, 0 as cash_log_idx
		, od.state as order_state
    from fp_pay.orders o 
    inner join fp_pay.payment p on o.id = p.order_id and p.deleted_at is null
    inner join fp_pay.order_detail od on o.id = od.order_id and od.deleted_at is null and o.deleted_at is null and od.unexposed_at is null
    inner join fp_pay.delivery d on d.detail_id = od.id and d.deleted_at is null
{$cateJoin}
    left join fp_db.seller_tbl st on od.seller_id = st.idx 
    left join fp_pay.goods g on od.goods_id = g.id 
    where o.deleted_at is null and o.user_id = {$user_idx} and o.user_type = 'user'
     {$termWhere}
    {$cateWhere}
    group by od.id
     {$union}
	order by created_at desc , order_number desc";
      $res = $dao->selectQuery($sql,"pay");
      $data[$state] = count($res);
    }
    return $data;
  }

  public function getOrderDetail () {
    $user_idx = $this->userIdx;
    // lm, store
    $menu = $this->getParam("menu");
    // charge, refund, buy
    $type = $this->getParam("type");
    // offline : order_id
    $idx = $this->getParam("idx");
    $userHelper = new CUserHelper();
    $res = $userHelper->getOrderDetail($menu, $type, $idx, $user_idx);

    return $this->resultOk($res);
  }

  public function getNonePostscript() {
    $user_idx = $this->userIdx;
    if(!$user_idx) return $this->resultFail("NO_LOGIN", "로그인이 필요합니다.");

    $sql = "select od.id as order_detail_id ,od.seller_id , st.name , od.goods_id , g.title ,g.imgs ,DATE_FORMAT(od.confirmed_at, '%Y.%m.%d') as confirmed_at, GROUP_CONCAT(o2.value) as option_name
                , case when g.sub_category = '악세사리' then 'acc'
                 		when g.sub_category = '뷰티' then 'beauty'
                 		else 'etc' end as option_type
         		, 500 as lm_pay
         		,od.total_price
         		, od.quantity
                from fp_pay.order_detail od 
                inner join fp_pay.orders o on od.order_id = o.id  
                inner join fp_db.seller_tbl st on od.seller_id = st.idx 
                inner join fp_pay.goods g on od.goods_id = g.id and g.deleted_at is null and g.display_state = 'show'
                inner join fp_pay.option_sku os on os.sku_id = od.sku_id 
                inner join fp_pay.`options` o2 on os.option_id = o2.id 
                left join fp_db.postscript_tbl pt on od.id = pt.order_detail_id and od.state = '구매확정' and od.deleted_at is null and pt.deleted_at is null
				where od.state = '구매확정' and od.deleted_at is null and pt.deleted_at is null and o.user_id = {$user_idx}
				and pt.idx is null
				group by od.id 
				order by od.confirmed_at desc";

    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql);
    $data = array(
      "total" => count($res),
      "list" => $res,
      "total_lm" => count($res) * 500
    );
    return $this->resultOk($data);
  }

  public function getProjectMyRequest() {
    $user_idx = $this->userIdx;
    if(!$user_idx) return $this->resultFail("NO_LOGIN", "로그인이 필요합니다.");
    $sql = "select cp.idx, cp.category , cc.name as category_name,cc.img_path as category_img, title , DATE_FORMAT(cp.start_date,'%y.%m.%d') as start_date, DATE_FORMAT(cp.end_date ,'%y.%m.%d') as end_date
	                ,cp.main_img ,cp.sub_img, if(cp.start_date <= now() and cp.end_date >= now(), 1, 0) as is_state
	                ,case WEEKDAY(cp.end_date) 
					    when '0' then '월'
					    when '1' then '화'
					    when '2' then '수'
					    when '3' then '목'
					    when '4' then '금'
					    when '5' then '토'
					    when '6' then '일'
					    end as end_date_week
                 from cm_project cp 
                 inner join cm_project_request cpr on cp.idx = cpr.project_idx 
                 inner join fp_db.lm_code_tbl cc on cp.category = cc.code
                 where cpr.user_idx = {$user_idx}
                 order by cpr.idx desc";
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "db");
    return $this->resultOk($res);
  }

}