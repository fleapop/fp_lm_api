<?php

namespace App\Http\Middleware;

use Closure;

class ForceJsonResponse
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
     {

         if (! $request->isJson()) {
         // if (! $request->wantsJson()) {
//             return response('not accept headers', 406);
         }

         return $next($request);
     }
}
