<?php


namespace App\Helper;


class CPushHelper
{

  /**
   * PUSH 발송
   * @param $user_id
   * @param $title
   * @param $massage
   * @param $category
   * @param $data
   * @return bool|string
   */
  public function sendPush($user_id, $title, $massage, $category, $data) {
    if(IS_TEST) {
      $url = PUSH_API_TEST;
    } else {
      $url = PUSH_API;
    }
    $param = array(
      "title" => $title,
      "message" => $massage,
      "category" => $category,
      "data" => $data
    );

    $url = "{$url}/api/send_push_message/user/".$user_id;
    $this->setPushApi($url, $param);
  }

  /**
   * PUSH READ
   * @param $push_log_idx : 푸쉬 메세지 발송시 추가데이터(data)의 'push_log_idx'
   * @return bool|string
   */
  public function readPush($push_log_idx) {
    if(IS_TEST) {
      $url = PUSH_API_TEST;
    } else {
      $url = PUSH_API;
    }
    $url = " {$url}/api/read_push_message/:push_log_idx".$push_log_idx;
    $this->setPushApi($url, null);
//    return $res;
  }

  /**
   * PUSH API 호출
   * @param $url
   * @param $param
   * @return bool|string
   */
  private function setPushApi($url, $param) {
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_POST, 1);
    if(isset($param)) {
      curl_setopt($curl, CURLOPT_POSTFIELDS, $param);
    }
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
    $result = curl_exec($curl);
    curl_close($curl);

//    return $result;
  }

}