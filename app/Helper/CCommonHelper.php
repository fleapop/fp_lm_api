<?php


namespace App\Helper;


use App\Http\dao\CBaseDAO;

class CCommonHelper
{
  public function addShippingCodes($zipCode) {
    $dao = new CBaseDAO();
    $sql ="select * from add_shipping_codes asc2 where zip_code = {$zipCode}";
    return $dao->selectQuery($sql, "pay");
  }

  /**
   * Category Type
   * goods: 상품 카테고리,
   * @param $type
   */
  public function getCategory($type) {
    return $this->getCmCategory($type);
//    $res = null;

//    switch ($type) {
//      case "goods" :
//      {
//        $res = $this->goodsCategory();
//        break;
//      }
//      case "body" :
//      {
//        $res = $this->bodyInfo();
//        break;
//      }
//      case "ldlife" :
//      {
//        $res = $this->ldLife();
//        break;
//      }
//      case "fllife" :
//      {
//        $res = $this->flLife();
//        break;
//      }
//    }
//    return $res;
  }

  private function getCmCategory($cateType = null) {
    $where = "";
    if ($cateType) {
      $where = "where type = '{$cateType}'";
    }
    $sql = "select *
            from fp_db.lm_code_tbl cc 
            {$where}
            order by type, sort_order ";
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "db");
    $resTemp = array();
    $codeTemp = "";
    $num = 0;
    foreach ($res as $item) {
      switch ($item->type) {
        case "CM_PROJECT" : {
          $item->type_name = "프로젝트";
          break;
        }
        case "CM_SKIN" : {
          $item->type_name = "피부타입";
          break;
        }
        case "CM_LDFIT" : {
          $item->type_name = "러덕핏";
          break;
        }
        case "CM_LDLIFE" : {
          $item->type_name = "러덕생활";
          break;
        }
        case "CM_FLLIFE" : {
          $item->type_name = "플리생활";
          break;
        }
      }
      if ($codeTemp != $item->type) {
        $codeTemp = $item->type;
        $num = 0;
        $resTemp[$item->type_name][$num] = $item;
      } else {
        $num++;
        $resTemp[$item->type_name][$num] = $item;
      }
    }
    return $resTemp;
  }

  private function ldLife(){
    $data = array(
      array(
        "title" => "패션",
        "name" => "C1001"
      ),
      array(
        "title" => "뷰티",
        "name" => "C1002"
      ),
      array(
        "title" => "인생샷",
        "name" => "C1003"
      ),
      array(
        "title" => "고민상담",
        "name" => "C1004"
      ),
      array(
        "title" => "기타",
        "name" => "C1005"
      )
    );
    return $data;
  }

  private function flLife(){
    $data = array(
      array(
        "title" => "공지",
        "name" => "C2001"
      ),
      array(
        "title" => "러브레터",
        "name" => "C2002"
      )
    );
    return $data;
  }

  private function goodsCategory () {
    $response = [
      [
        'title' => '아우터',
        'name' => 'outer',
        'images' => [
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateOuter.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateOuter@2x.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateOuter@3x.png",
        ],
      ],
      [
        'title' => '상의',
        'name' => 'top',
        'images' => [
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateTop.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateTop@2x.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateTop@3x.png",
        ],
      ],
      [
        'title' => '하의',
        'name' => 'bottom',
        'images' => [
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBottom.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBottom@2x.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBottom@3x.png",
        ],
      ],
      [
        'title' => '원피스',
        'name' => 'one',
        'images' => [
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateOne.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateOne@2x.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateOne@3x.png",
        ],
      ],
      [
        'title' => '악세사리',
        'name' => 'acc',
        'images' => [
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateAcc.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateAcc@2x.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateAcc@3x.png",
        ],
      ],
      [
        'title' => '잡화',
        'name' => 'stuff',
        'images' => [
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBag.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBag@2x.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBag@3x.png",
        ],
      ],
      [
        'title' => '뷰티',
        'name' => 'beaut',
        'images' => [
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBeauty.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBeauty@2x.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateBeauty@3x.png",
        ],
      ],
      [
        'title' => '디지털',
        'name' => 'digital',
        'images' => [
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateDigital.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateDigital@2x.png",
          "https://fps3bucket.s3.ap-northeast-2.amazonaws.com/resource/store/category/imgCateDigital@3x.png",
        ],
      ]
    ];

    return $response;
  }

  private function bodyInfo () {
    $data = array(
      "height" => [140, 145, 150, 155, 160, 165, 170, 175, 180, 185, 190],
      "foot_size" => [220, 225, 230, 235, 240, 245, 250, 255, 260, 265, 270, 275, 280, 285],
      "body_size" => ["S", "M", "L"],
    );
    $data["skin"] = array(
      array(
        "title" => "건성",
        "name" => "BD001"
      ),
      array(
        "title" => "중성",
        "name" => "BD002"
      ),
      array(
        "title" => "지성",
        "name" => "BD003"
      ),
      array(
        "title" => "복합성",
        "name" => "BD004"
      )
    );
    return $data;
  }

  public function getLikeData($params) {
    if ($params["table_name"] == "user_content_tbl") {
      return [
        "is_like_state"=>count($this->getMagazineLikeState($params)) > 0,
        "like_count"=>$this->getMagazineLikeCount($params)
      ];
    } else {
      return [
        "is_like_state"=>$this->getLikeState($params),
        "like_count"=> $this->getLikeCount($params)
      ];
    }
  }


  public function toggleLike ($params) {
    $dao = new CBaseDAO();
    if ($params["table_name"] == "user_content_tbl") {
      $stateRes = $this->getMagazineLikeState($params);
      if (count($stateRes) > 0) {
        foreach ($stateRes as $item) {
          $dao->deleteQuery("fp_db.like_tbl", "table_name = '{$params["table_name"]}' and idx = {$item->idx}", "db_w");
        }
      } else {
        $dao->insertQuery("fp_db.like_tbl", $params, "db_w");
      }
      if (time_sleep_until(microtime(true) + 0.2)) {
        $res = $this->getLikeData($params);
        return $res;
      }
    } else {
      $stateRes = $this->getLikeState($params);
      if ($stateRes) {
        $dao->deleteQuery("fp_db.like_tbl", "table_name = '{$params["table_name"]}' and table_idx = '{$params["table_idx"]}' and user_idx = '{$params["user_idx"]}'", "db_w");
      } else {
        $dao->insertQuery("fp_db.like_tbl", $params, "db_w");
      }
      if (time_sleep_until(microtime(true) + 0.2)) {
        $res = $this->getLikeData($params);
        return $res;
      }
    }
  }

  public function getLikeState ($params) {
    $dao = new CBaseDAO();
    $stateSql = "select * from fp_db.like_tbl where `table_name` = '{$params["table_name"]}' and table_idx = '{$params["table_idx"]}' and user_idx = '{$params["user_idx"]}'";
    $res =$dao->selectQuery($stateSql, "db");
    return count($res) > 0;
  }

  public function getLikeCount ($params) {
    $dao = new CBaseDAO();
    $stateSql = "select * from fp_db.like_tbl where `table_name` = '{$params["table_name"]}' and table_idx = '{$params["table_idx"]}' ";
    $res = $dao->selectQuery($stateSql, "db");
    return count($res);
  }

  public function getMagazineLikeState ($params) {
    $dao = new CBaseDAO();
    $stateSql = "select lt.*
from user_content_tbl uct
inner join like_tbl lt on uct.idx = lt.table_idx and lt.table_name = '{$params["table_name"]}' and lt.delete_date is null
where (uct.idx ={$params["table_idx"]} or uct.parent_idx = {$params["table_idx"]}) and uct.delete_date is null and lt.user_idx = '{$params["user_idx"]}'";
    $res =$dao->selectQuery($stateSql, "db");
    return $res;
  }

  public function getMagazineLikeCount ($params) {
    $dao = new CBaseDAO();
    $stateSql = "select lt.*
from user_content_tbl uct
inner join like_tbl lt on uct.idx = lt.table_idx and lt.table_name = '{$params["table_name"]}' and lt.delete_date is null
where (uct.idx ={$params["table_idx"]} or uct.parent_idx = {$params["table_idx"]}) and uct.delete_date is null ";
    $res = $dao->selectQuery($stateSql, "db");
    return count($res);
  }

}