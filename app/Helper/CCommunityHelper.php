<?php


namespace App\Helper;


use App\Http\Controllers\Controller;
use App\Http\dao\CBaseDAO;

class CCommunityHelper
{
  public function setFitBest() {
    $dao = new CBaseDAO();
    $sql = "select ct.* ,list_date_view(ct.created_at) as create_date ,count(cc.idx) as claim_cnt
                , (ct.like_count * 0.7) + (ct.reply_count * 0.3) as score
                from fp_db.cm_timeline ct 
                left join fp_db.cm_claim cc on ct.service_type = cc.service_type and ct.idx = cc.service_idx and cc.deleted_at is null
                where ct.deleted_at is null ct.service_type in ('fit','postscript') and ct.claim_deleted_at is null
                group by ct.idx 
                order by score desc
                limit 10
                ";
    $res = $dao->selectQuery($sql, "db");

    $timestamp = strtotime("-1 days");
    $year = date_format($timestamp,"Y");
    $month = date_format($timestamp,"m");

    foreach ($res as $key => $item) {
      $data = array(
        "timeline_idx" => $item->idx,
        "best_year" => $year,
        "best_month" => $month,
        "best_no" => $key + 1,
      );
      $dao->insertQuery("cm_fit_best", $data, "db_w");
    }
  }

  public function getFitBest($user_idx = '') {
    $sql = "select ct.* ,list_date_view(ct.created_at) as create_date ,count(cc.idx) as claim_cnt
                , (ct.like_count * 0.7) + (ct.reply_count * 0.3) as score
                , uei.user_size , uei.user_height , uei.user_skin ,uei.user_foot_size
                , if(ul.profile_img = '' or ul.profile_img is null ,
                                ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/default_profile.png'), ''),
                                 ifnull(CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/', replace(ul.profile_img,'default_profile.jpg','default_profile.png')), '')) as profile_img
                , if(ul.nic_name is null or REPLACE(ul.nic_name, ' ', '') = '', ul.shipping_name, ul.nic_name) as nic_name
                , case when lt.idx is null then 0 else 1 end as is_like_state
                , case when ft.idx is null then 0 else 1 end as is_follow
                from fp_db.cm_timeline ct 
                inner join fp_db.cm_fit_best cfb on ct.idx = cfb.timeline_idx and cfb.best_year = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 month) ,'%Y') and cfb.best_month = DATE_FORMAT(DATE_ADD(NOW(), INTERVAL -1 month) ,'%m')
                left join fp_db.cm_claim cc on ct.service_type = cc.service_type and ct.idx = cc.service_idx and cc.deleted_at is null
                left join fp_db.user_list ul on ct.user_idx = ul.user_id
                left join fp_db.user_extra_info uei on ul.user_id = uei.user_id 
                left join fp_db.like_tbl lt on lt.table_idx = ct.idx and lt.table_name = 'cm_timeline' and lt.user_idx = '{$user_idx}'
                left join fp_db.follow_tbl ft on ft.to_user_idx = ct.user_idx and ft.from_user_idx = '{$user_idx}' 
                where ct.deleted_at is null and ct.service_type in ('fit','postscript') and ct.claim_deleted_at is null
                group by ct.idx 
                order by cfb.best_no asc";
    $dao = new CBaseDAO();
    return $dao->selectQuery($sql, "db");
  }

  public function setTimelineRead ($timelineIdx, $user_idx) {
    $sql = "select ct.service_type , ct.idx as service_idx, if(ctr.idx is null, 'N', 'Y') as is_read
               from cm_timeline ct 
               inner join user_list ul on ct.user_idx = ul.user_id 
               inner join follow_tbl ft on ul.user_id = ft.to_user_idx and ft.from_user_idx = {$user_idx}
               left join cm_timeline_read ctr on ctr.service_idx = ct.idx 
               where ct.idx = {$timelineIdx}";
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "db");
    if(isset($res[0]) && $res[0]->is_read == 'N') {
     $data = array(
       "service_type" => $res[0]->service_type,
       "service_idx" => $res[0]->service_idx,
       "user_idx" => $user_idx
     );
     $dao->insertQuery("cm_timeline_read", $data, "db_w");
    }
  }

  public function getTimelineImgType ($idx) {
    $sql = "select ci.* 
            from cm_timeline ct 
            inner join cm_imgs ci on ct.service_type = ci.service_type and ct.idx = ci.service_idx 
            where ct.idx = $idx";
    $dao = new CBaseDAO();
    $res = $dao->selectQuery($sql, "db");
    return count($res) > 0;
  }
}