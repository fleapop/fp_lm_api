<?php


namespace App\Helper;


use App\Http\dao\CBaseDAO;

class CExhibitionHelper
{
  public function getExhibitionRepMenu ($exhibition_id = null) {

    $where = $exhibition_id ? " and atm.exhibition_id = ".$exhibition_id : "";

    $dao = new CBaseDAO();
    $sql = "select atm.sort_order , atm.name as menu_name, atm.exhibition_id , e.img  ,e.img_content
                , e.title
                , CONCAT( date_format(e.start_date, '%m월 %d일'),'(',SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(e.start_date), 1),') ', date_format(e.start_date, '%H시 %i분') ,' - ', date_format(e.end_date , '%m월 %d일'),'(',SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(e.end_date), 1),') ', date_format(e.end_date, '%H시 %i분')) as sub_title
                from app_top_menu atm
                inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true' and e.type = 'exhibition'
                where atm.display = 'show'
                and e.is_hidden = 'true'
                and e.deleted_at is null
                and (e.start_date <= now() and e.end_date >= now())
                {$where}
				order by e.id desc
				limit 1
                ";
    $Res = $dao->selectQuery($sql, "pay");

    $result = array();
    if (isset($Res[0])) {
      $result["data"] = $Res[0];
      $result["result"] = true;
    } else {
      $result["result"] = false;
    }
    return $result;
  }

  public function getExhibitionRepMenus () {


    $dao = new CBaseDAO();
    $sql = "select atm.sort_order , atm.name as menu_name, atm.exhibition_id , e.img  ,e.img_content
                , e.title
                , CONCAT( date_format(e.start_date, '%m월 %d일'),'(',SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(e.start_date), 1),') ', date_format(e.start_date, '%H시 %i분') ,' - ', date_format(e.end_date , '%m월 %d일'),'(',SUBSTR(_UTF8'일월화수목금토', DAYOFWEEK(e.end_date), 1),') ', date_format(e.end_date, '%H시 %i분')) as sub_title
                from app_top_menu atm
                inner join exhibition e on atm.exhibition_id = e.id and e.is_hidden = 'true'
                where atm.display = 'show'
                and e.is_hidden = 'true'
                and e.deleted_at is null
                and (e.start_date <= now() and e.end_date >= now())
				order by e.id desc
                ";
    $Res = $dao->selectQuery($sql, "pay");

    return $Res;
  }
}