<?php


namespace App\Helper;


use Illuminate\Support\Facades\Storage;

class CFileuploadHelper
{
  /**
   * Multiple File Upload
   * @param $request
   * @param $dir
   */
  public function multipleFileUpload($request, $type) {

    $res = array(
      "result" => false
    );

    if ($request->hasFile('files')) {
      $files = $request->file('files');
      $path = "/community/".$type . '/' . date("Y") ."/". date("m")."/";
//dd($files);
      $res["cnt"] = 0;
      $imgType = array(
        "square" => 0,
        "width" => 0,
        "height" => 0
      );
      $img_type = 0;
      $imgArray = array();
      $imgSort = "";

      if(is_array($files)) {
        $imgCount = count($files);
        $res["cnt"] = $imgCount;
        foreach ($files as $key => $file) {
          $image = $this->getUnique() . time() . $file->getClientOriginalName();
//          $res["name"] = $image;
          $imgArray[$key]["name"] = $image;
          $imgArray[$key]["path"] = $path;
//          dd($file);
//          $resizeImg = $this->resize_image($file, 500, 500);
//          dd($resizeImg[0]);
          // dd(file_get_contents($file));
//          // dd(imagepng($resizeImg,$image));
          Storage::disk('s3')->put($path.$image,file_get_contents($file), 'public');

          $imgSizeType = $this->imageSizeCheck($file);
          if ($imgSizeType == 1) {
            $imgType["square"]++;
            $imgArray[$key]["tempOrder"] = 1;
            $imgSort .= "1";
          } else if ($imgSizeType == 2) {
            $imgType["width"]++;
            $imgArray[$key]["tempOrder"] = 2;
            $imgSort .= "2";
          } else if ($imgSizeType == 3) {
            $imgType["height"]++;
            $imgArray[$key]["tempOrder"] = 3;
            $imgSort .= "3";
          }
        }
//dd($imgCount."_".$imgType["height"]);
        if ($imgCount == 1) {
          $img_type = 1;
        }
        else if ($imgCount >= 3 && $imgType["height"] > 0) {
          $img_type = 4;
          $index = strpos($imgSort, "3");
          if($index) {
            $imgArrayTemp = $imgArray[0];
            $imgArray[0] = $imgArray[$index-1];
            $imgArray[$index-1] = $imgArrayTemp;
          }
        } else if ($imgCount >= 3 && $imgType["height"] <= 0) {
          $index = strpos($imgSort, "2");
          if($index) {
            $imgArrayTemp = $imgArray[0];
            $imgArray[0] = $imgArray[$index-1];
            $imgArray[$index-1] = $imgArrayTemp;
          }
          $img_type = 5;
        } else if ($imgCount == 2&& $imgType["height"] == 2) {
          $img_type = 3;
        } else if ($imgCount == 2&& $imgType["height"] < 2) {
          $img_type = 2;
        }
        $res["files"] = $imgArray;
        $res["img_type"] = $img_type;
        $res["result"] = true;

      } else {

//        $res["path"] = $path;
        $res["img_type"] = 1;
        $res["cnt"] = 1;
        $image = $this->getUnique() . time() . $files->getClientOriginalName();
        $res["name"] = $image;
        $res["files"][0]["name"] = $image;
        $res["files"][0]["path"] = $path;
        $res["result"] = true;
        Storage::disk('s3')->put($path.$image,file_get_contents($files), 'public');
      }
    }
    return $res;

  }

  public function imageSizeCheck ($img) {
      $size = getimagesize($img);
      $res = "";
      if ($size[0] == $size[1]) {
        // 가로 세로 같은 사이즈
        $res = 1;
      } else if ($size[0] > $size[1]) {
        // 가로가 더 긴 이미지
        $res = 2;
      } else if ($size[0] < $size[1]) {
        // 세로가 더 긴 이미지
        $res = 3;
      }
      return $res;
  }

  /**
   * Unique Value
   * @return string
   */
  function getUnique()
  {
    $res = preg_replace("/^0\.([0-9]+) ([0-9]+)$/", "$1-$2", microtime());
    $res = explode("-", $res);
    $res = strtoupper(dechex($res[0])) . strtoupper(dechex($res[1]));
    $res = str_pad($res, 15, '0', STR_PAD_LEFT);

    return $res;
  }

  /**
   * File Delete
   */
  public function fileDelete($path) {
    Storage::disk('s3')->delete($path);
  }

  /**
   * Multiple File Upload
   * @param $request
   * @param $dir
   */
  public function commonFileUpload($request, $path) {

    $res = array(
      "result" => false
    );

    if ($request->hasFile('files')) {
      $files = $request->file('files');
//      $path = "/community/".$type . '/' . date("Y") ."/". date("m")."/";
//dd($files);
      $res["cnt"] = 0;
      $imgType = array(
        "square" => 0,
        "width" => 0,
        "height" => 0
      );
      $imgArray = array();

      if(is_array($files)) {
        $imgCount = count($files);
        $res["cnt"] = $imgCount;
        foreach ($files as $key => $file) {
//          $image = $this->getUnique() . time() . $file->getClientOriginalName();
//          $res["name"] = $image;
//          $imgArray[$key]["name"] = $image;
          $imgArray[$key]["path"] = $path;
          Storage::disk('s3')->put($path,file_get_contents($file), 'public');
        }
//dd($imgCount."_".$imgType["height"]);
        $res["files"] = $imgArray;
        $res["result"] = true;

      } else {
//        $res["path"] = $path;
        $res["img_type"] = 1;
        $res["cnt"] = 1;
        $image = $this->getUnique() . time() . $files->getClientOriginalName();
//        $res["name"] = $image;
//        $res["files"][0]["name"] = $image;
        $res["files"][0]["path"] = $path;
        $res["result"] = true;
        Storage::disk('s3')->put($path,file_get_contents($files), 'public');
      }
    }
    return $res;

  }

  function resize_image($file, $dst_w, $dst_h, $crop=FALSE) {
     list($width, $height) = getimagesize($file);
     $ratio = $width / $height;
     if ($crop) {
             if ($width > $height) {
                 $width = ceil($width-($width*abs($ratio-$dst_w/$dst_h)));
       } else {
                 $height = ceil($height-($height*abs($ratio-$dst_w/$dst_h)));
       }
       $newwidth = $dst_w;
       $newheight = $dst_h;
     } else {
             if ($dst_w/$dst_h > $ratio) {
                 $newwidth = $dst_h*$ratio;
         $newheight = $dst_h;
       } else {
                 $newheight = $dst_w/$ratio;
         $newwidth = $dst_w;
       }
     }

     $exploding = explode(".",$file->getClientOriginalName());
     $ext = end($exploding);
//     // dd($ext);
     switch($ext){
       case "png":
             $src = imagecreatefrompng($file);
          break;
       case "jpeg":
       case "jpg":
             $src = imagecreatefromjpeg($file);
          break;
       case "gif":
             $src = imagecreatefromgif($file);
          break;
       default:
             $src = imagecreatefromjpeg($file);
          break;
     }

     $result = imagecreatetruecolor($newwidth, $newheight);
     $temp = imagecopyresampled($result, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
//     dd($src);
     return array($result, $ext);
  }

}