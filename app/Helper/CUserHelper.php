<?php

namespace App\Helper;
/**
 * Class CUserHelper
 * User: Ansc
 * Date: 20.11.05
 */

use App\Http\dao\CBaseDAO;
use App\Lmpay\LMPayController;
use DB;
use App\Http\dao;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cookie;
use Milon\Barcode\DNS1D;
use Session;
use Storage;
use Illuminate\Http\Request;

class CUserHelper
{
    public static function setLoginCookie($user) {
//      @session_start();
      $gCV = array();
      $gCV["account_type"] = $user->account_type;
      $gCV["loggedin"] = true;
      $gCV["user_id"] = $user->user_id;
      $gCV["nic_name"] = $user->nic_name;
      $gCV["token"] = makeToken($user->user_id);
      $gCV['svc'] = 'lm';
      $ck3 = crypt($user->user_id . $user->email_account . 'lm','');
      $gCV['ck3'] = $ck3;
//      $ret = setcookie('gCV', array_encode($gCV), time() + 86400 * 365 * 30, '/', '.fleapop.co.kr');
      $host = $_SERVER['REMOTE_ADDR'];
//      if ($host == "127.0.0.1") {
//
        setcookie('gCV', array_encode($gCV), 86400);
//      } else {
//        setcookie('gCV', array_encode($gCV), 0, '/', '.fleapop.co.kr');
//      }
      @session_start();
//      Cookie::queue("gCV", json_encode($gCV), 864000);
//      setcookie('auto_login_data_1', '111', time() ,0);
//      cookie('auto_login_data_2', json_encode($gCV), time() + 86400 * 30, "/");
//      Cookie::queue(cookie('name', 'value', 60));
//      setcookie('gCV', array_encode($gCV), time() + 86400 * 365 * 30, '/', '.fleapop.co.kr');
//Cookie::forever("gCV", "############### test");
//      Cookie::queue(Cookie::forever('gCV', '############### test'));
      Cookie::forever('name', json_encode($_POST), 84600);


//      @session_start();
//      @session_start();
      $_SESSION["gCV"] = "12";
//      @session_start();
//      $_SESSION["user"]["account_type"] = $user->account_type;
//      $_SESSION["user"]["loggedin"] = true;
//      $_SESSION["user"]["user_id"] = $user->user_id;
//      $_SESSION["user"]["nic_name"] = $user->nic_name;
//      $_SESSION["user"]["token"] = makeToken($user->user_id);
//      $ck3 = crypt($user->user_id . $user->email_account . 'lm','');
//      $_SESSION["user"]['ck3'] = $ck3;
    }
    public function setCookie(Request $request){
      $minutes = 60;
      $response = new Response('Set Cookie');
      $response->withCookie(cookie('name', 'MyValue', $minutes));
      return $response;
    }

    public function getOrderDetail($menu = 'lm', $type = NULL, $id, $user_id = NULL) {
      $dao = new dao\CBaseDAO();
      if ($menu == 'lm' || $menu == 'offline') {
        // LMPay, LovleyMarket

        $lmPaySql = "select st.name as seller_name, CONCAT('https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/', gt.image, if(gt.image = '1', '.jpg',''))  as goods_img
              , '결제완료' as order_state, gt.title , ogt.price , ogt.quantity , cl.`type` as order_type
              , CONCAT( gt.option_value1 ,if(gt.option_value2 is null or gt.option_value2 = '', '', concat(',',gt.option_value2) )) as option_name
              ,oit.orderer_name ,oit.order_number ,oit.orderer_zipcode ,oit.orderer_address1 ,oit.orderer_address2 ,oit.delivery_memo ,oit.delivery_state 
              , DATE_FORMAT( cl.insert_date , '%Y.%m.%d') as created_at 
              , ogt.idx as order_goods_tbl_idx
              from fp_pay.cash_log cl 
              inner join fp_pay.order_info_tbl oit on cl.order_info_idx = oit.idx 
              inner join fp_pay.order_goods_tbl ogt on  ogt.order_info_idx = oit.idx and ogt.unexposed_at is null
              left join fp_db.market_seller_tbl mst on mst.idx = ogt.market_seller_idx 
              left join fp_db.seller_tbl st on st.idx = mst.seller_idx 
              left join fp_pay.goods_tbl gt on ogt.goods_idx = gt.idx 
              where cl.idx = {$id}";

            return $dao->selectQuery($lmPaySql, "pay");

      } else {
        // Store
//        $resOrder = Order::with(['payment','delivery'])->find($id);

        $resOrderSql = "select o.id as order_idx,od.id as detail_idx,  (o.total_price - o.lmpay_amount) as total_price ,o.lmpay_amount ,sum(od.promotion_price) as promotion_price, sum(od.origin_price) as origin_price ,o.created_at as order_date, p.sender ,p.vbank_num ,p.vbank_name ,p.vbank_date 
              , if((p.`method` = '' or p.`method` is null) and p.imp_uid = 'lmpay', 'lmpay', p.`method`) as method , p.bank as refund_back, p.holder as refund_name, p.account as refund_account, sum(od.delivery_price) as delivery_price
              , od.goods_id
              , p.state 
              from fp_pay.orders o 
              inner join fp_pay.payment p on o.id = p.order_id 
              inner join fp_pay.order_detail od on o.id = od.order_id and od.unexposed_at is null
              where p.deleted_at is null and o.deleted_at is null
              and o.id = {$id}
              group by o.id ";
        $resOrder = $dao->selectQuery($resOrderSql, "pay");
        if (!isset($resOrder[0])) {
          return array("detail" => []);
        }
        $resOrder = isset($resOrder[0]) ? $resOrder[0] : json_decode('{}');
        $resDeliverySql = "select  d.name as delivery_name, d.phone as delivery_phone, d.zip_code as delivery_zip_code, d.address_1 as delivery_address_1, d.address_2 as delivery_address_2, d.msg as delivery_msg from fp_pay.delivery d where order_id = {$id} limit 1 ";
        $resDelivery = $dao->selectQuery($resDeliverySql, "pay")[0];


        $resOrder->delivery = $resDelivery;

//        $resOrder->add_shipping = $resOrder->total_shipping_price;


        $resOrder->method = getOrderMethodString($resOrder->method);

        if (isset($resOrder->vbank_date)) {
          $resOrder->vbank_date = date("Y-m-d H:i:s", $resOrder->vbank_date);
        }

        $goodsSql = "select od.id as order_detail_id ,od.total_price ,od.quantity , od.goods_title , od.state as order_state
            , (select GROUP_CONCAT(o2.value separator '_') as option_name from option_sku os inner join `options` o2 on os.option_id = o2.id where sku_id = od.sku_id) as option_name   
            , ifnull((select ct.idx 
                 from fp_db.postscript_tbl pt 
                 inner join fp_db.cm_timeline ct on pt.service_idx = ct.idx and ct.user_idx = '{$user_id}' where pt.order_detail_id = od.id and ct.deleted_at is null  order by pt.idx desc limit 1),0) as is_review  
            , g.imgs as goods_img, st.name as seller_name, 500 as lm_pay
            , if(g.deleted_at is null and g.display_state = 'show', 1, 0) as is_goods_show 
            from order_detail od 
            inner join goods g on od.goods_id = g.id and od.unexposed_at is null
            left join fp_db.seller_tbl st on od.seller_id = st.idx 
            where od.order_id = {$id}";

        $goodsRes = $dao->selectQuery($goodsSql, "pay");

        $data = array();
        $sellerSeq = -1;
        $sellerName = "";
        $goodsCnt = 0;
        foreach ($goodsRes as $item) {
          if ($sellerName != $item->seller_name) {
            $goodsCnt = 0;
            $sellerSeq++;
            $sellerName = $item->seller_name;
            $data[$sellerSeq]["seller_name"] = $sellerName;
            $data[$sellerSeq]["goods"] = [];

          }
          array_push($data[$sellerSeq]["goods"], $item);
          $goodsCnt++;
          $data[$sellerSeq]["goods_cnt"] = $goodsCnt;
        }

        $resOrder->detail = $goodsCnt <= 0 ? [] : $data;
        return $resOrder;
      }
    }

    public function getBarcode($user_idx) {
      $DNS1D = new DNS1D();

      $dao = new dao\CUserDAO();
      $user_data = $dao->getUserInfo($user_idx)[0];
      $response["barcode"] = $user_data->barcode;
      $response["barcode_img"] = "data:image/png;base64," . $DNS1D->getBarcodePNG($user_data->barcode, "C128");
//      $lmPay = new LMPayController();
//      $balance = number_format($lmPay->getBalance($user_idx));

 /*     if (isset($user_data->barcode) && isset($user_data->sec_password)) {
        $this->setUserBarcode($user_idx, true);
        $response["data"]["user_lmpay"] = $balance;
        $response["data"]["barcode_img"] = '<img src="data:image/png;base64,' . $DNS1D->getBarcodePNG($user_data->barcode, "C128") . '" alt="barcode"   />';
        $response["data"]["barcode"] = $user_data->barcode;
        $response["data"]["barcode_data"] = "data:image/png;base64," . $DNS1D->getBarcodePNG($user_data->barcode, "C128");
      } else {
        $this->setUserBarcode($user_idx, true);
        $response["data"]["user_lmpay"] = $balance;
        $response["data"]["barcode_img"] = "";
        $response["data"]["barcode"] = "";
        $response["data"]["barcode_data"] = "data:image/png;base64," . $DNS1D->getBarcodePNG("2900000000000", "C128");
      }
 */
      return $response;
    }

  public function setUserBarcode($user_id, $isForced = false)
  {
    $response = [
      "success" => true,
      "msg"     => "",
      "barcode" => "",

    ];

    $userDao = new dao\CUserDAO();
    $DNS1D = new DNS1D();
    $dao = new dao\CBaseDAO();
    $user_data = $userDao->getUserInfo($user_id)[0];
    $is_change_barcode = true;
    if (! isset($user_data->update_date)) {
      $is_change_barcode = true;
    } else {
      $sql = "SELECT COUNT(*) AS cnt FROM user_list WHERE user_id = {$user_id} AND update_date < date_add(now(), interval -1 minute) order by user_id desc";

      $res = $dao->selectQuery($sql, "db");
      if ($res[0]->cnt > 0) {
        $is_change_barcode = true;
      } else {
        $is_change_barcode = false;
        $response["barcode"] = $user_data->barcode;
      }
    }

    $barcode = "";

    if ($isForced) {
      $is_change_barcode = $isForced;
    }

    if ($is_change_barcode) {
      $barcode = $this->makeNewBarcode();
      $exists_barcode = true;

      while ($exists_barcode) {

        $exists_barcode_sql = "select * from barcode_tbl where barcode = '{$barcode}'";
        $exists_barcode_res = $dao->selectQuery($exists_barcode_sql, "pay");

        if (count($exists_barcode_res) > 0) {
          $barcode = $this->makeNewBarcode($barcode);
          $exists_barcode = true;
        } else {
          $exists_barcode = false;
        }
      }
      $barcodeInsertData = array(
        "barcode" => $barcode,
        "user_idx" => $user_data->user_id
      );
      $barcode_res = $dao->insertQuery("barcode_tbl", $barcodeInsertData, "pay_w");


      if ($barcode_res) {
        $barcodeUpdateData = array(
          "barcode" => $barcode,
          "barcode_idx" => $barcode_res
        );
        $dao->updateQuery("user_list", $barcodeUpdateData, "user_id = {$user_data->user_id}", "db_w");
      }
      if (time_sleep_until(microtime(true) + 0.2)) {
        $user_data = $userDao->getUserInfo($user_id)[0];
        $response["barcode"] = $user_data->barcode;

        $response["barcode_img"] = "data:image/png;base64," . $DNS1D->getBarcodePNG($user_data->barcode, "C128");
        $response["is_change"] = $is_change_barcode;

        return $response;
      }
    } else {
      $response["success"] = false;
      $response["barcode"] = $user_data->barcode;
      $response["msg"] = "바코드는 1분단위로 변경하실수 있습니다.";
      $response["barcode_img"] = "data:image/png;base64," . $DNS1D->getBarcodePNG($user_data->barcode, "C128");
      $response["is_change"] = $is_change_barcode;

      return $response;
    }
  }

  public function makeNewBarcode($isBarcode = "")
  {
    $dao = new dao\CBaseDAO();
    if (strlen($isBarcode) > 1) {
      $query = "SELECT min(barcode+1) AS barcode FROM barcode_tbl WHERE (barcode+1) NOT IN (SELECT barcode FROM barcode_tbl)";
      $res = $dao->selectQuery($query, "pay");
      $newBarcode = $res[0]->barcode;
      return $newBarcode;
    } else {
      $query = "SELECT min(barcode+1) AS barcode FROM barcode_tbl WHERE (barcode+1) NOT IN (SELECT barcode FROM barcode_tbl)";
      $res = $dao->selectQuery($query, "pay");
      $newBarcode = $res[0]->barcode;
      return $newBarcode;
    }
  }

  public function setUserInfoSetting($updateData, $user_idx) {
    $data = array();
    foreach ($updateData as $key => $val) {
      if ($key != "device" && $key != "user_id" && $key != "token")
      $data[$key] =  $val;
    }
    $dao = new CBaseDAO();
    $dao->updateQuery("user_extra_info", $data, "user_id = {$user_idx}","db_w");
  }

  public function pushBacdge ($userIdx) {
      $sql = "select count(*) as cnt from fp_pay.push_logs pl 
                 				where user_id = {$userIdx} and `result` = 'success'";
      $dao = new CBaseDAO();
      $res = $dao->selectQuery($sql, "db")[0]->cnt;
      return (int)$res ;
  }
}
