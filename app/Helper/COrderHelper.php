<?php


namespace App\Helper;


use App\Http\Controllers\UserAddressController;
use App\Lmpay\LMPayController;
use App\Models\Goods;
use App\Models\Sku;
use App\Models\User;
use App\Models\UserAddress;

class COrderHelper
{
  private $key = "fleapop!@#abc123";
  public function getOrder($request)
  {
//    $meta = $this->getParam("meta");
//    // dd($meta);
    @session_start();
//    $request = $this->request_sub;

    if (! $request["meta"]) {
      return $this->resultFail("FAIL",'필수 파라미터가 누락되었습니다.');
    }

    $meta = base64_decode($request["meta"]);
    $meta = $this->getDecrypt($meta);
    $meta = json_decode($meta);


    $dataOrder = [
      'user_id'      => 0,
      'user_type'    => 'non_user',
      'name'         => '',
      'phone'        => '',
      'lmpay'        => 0,
      'use_pay_type' => '',
      'addresses'    => [],
    ];

    // Set 주문자 정보
//    $authUserId = $this->userAuthCheckTemp([
//      'device'  => $request->input('device'),
//      'user_id' => $request->input('user_id'),
//      'token'   => $request->input('token'),
//    ]);
    $authUserId = $request["user_idx"];

    if (! $authUserId) {
      $dataOrder['user_type'] = 'non_user';
      $dataOrder['user_id'] = 0;
      $dataOrder['name'] = $meta->order_info->non_user_name;
      $dataOrder['phone'] = $meta->order_info->non_user_phone;
    } else {
      $dataOrder['user_type'] = 'user';
      $dataOrder['user_id'] = $authUserId;
//      $userAddress = new UserAddressController();
      $addresses = $this->getAddresses($authUserId);

      if (count($addresses) > 0) {
        $dataOrder['shipping_address'] = $addresses;
      } else {
        $address = $this->getEmptyUserAddress($dataOrder['user_id']);
        $dataOrder['shipping_address'] = $address;
      }

      $user = User::find($authUserId);
      $dataOrder['name'] = $user->shipping_name;
      $dataOrder['phone'] = $user->shipping_phone;
      $defaultPay = $user->where('user_id', $authUserId)->select('use_pay_type')->get();

      if (isset($defaultPay[0])) {
        $dataOrder['use_pay_type'] = $defaultPay[0]->use_pay_type;
      }

      $lmpay = new LMPayController();
      $dataOrder['lmpay'] = $lmpay->getBalance($authUserId);
    }

    if (! $authUserId) {
      $items = json_decode($meta->meta);
    } else {
      $items = $meta;
    }

    $goods = new Goods();
    $dataPay = [
      'origin_price'    => 0,
      'promotion_price' => 0,
      'shipping_price'  => 0,
      'shipping_count'  => 0,
      'total_price'     => 0,
    ];

    $dataGoods = [];
    $colSellers = collect([]);

    foreach ($items as $item) {
      $itemSku = Sku::find($item->id);
      $itemGoods = $goods->find($itemSku->goods_id);
      $itemGoods->sku_id = $item->id;
      $itemGoods->seller;
      $itemGoods->promotion = $itemGoods->getPromotion($itemSku->goods_id);
      $itemGoods->option = $itemSku->options;
      $itemGoods->option_value = $itemGoods->getOptionBySku($itemSku->id, true);
      $itemGoods->quantity = $item->quantity;
      $itemGoods->promotion_price = $itemGoods->getPromotionPrice();

      if ($colSellers->has($itemGoods->seller_id)) {
        if ($itemGoods->ship_price > $colSellers->get($itemGoods->seller_id)) {
          $colSellers[$itemGoods->seller_id] = $itemGoods->ship_price;
        }
      } else {
        $colSellers->put($itemGoods->seller_id, $itemGoods->ship_price);
      }

      $originPrice = ((int)$itemGoods->price * (int)$item->quantity) + ((int)$itemSku->price * (int)$item->quantity);
      $dataPay['origin_price'] += $originPrice;

      $dataPay['promotion_price'] += $originPrice - $itemSku->getBuyPrice($itemGoods, $item->quantity);

      array_push($dataGoods, $itemGoods);
    }

    foreach ($colSellers as $shipItem) {
      $dataPay['shipping_price'] += $shipItem;
      $dataPay["shipping_count"]++;
    }

    $dataPay['total_price'] = $dataPay['origin_price'] - $dataPay['promotion_price'] + $dataPay['shipping_price'];
    $data = [
      'data_goods' => $dataGoods,
      'data_pay'   => $dataPay,
      'data_order' => $dataOrder,
    ];
    return $data;
  }
  private function getEncrypt($msg)
  {
    $endata = @openssl_encrypt($msg, "aes-128-cbc", $this->key, true, $this->key);
    $endata = base64_encode($endata);
    return $endata;
  }

  private function getDecrypt($msg)
  {
    $data = base64_decode($msg);
    $endata = @openssl_decrypt($data, "aes-128-cbc", $this->key, true, $this->key);
    return $endata;
  }

  public function getAddresses($user_id) {
    return UserAddress::where('user_id', $user_id)->whereNull('deleted_at')->get();
  }

  public function getEmptyUserAddress($user_id) {
    $user = new User();
    $res_user = $user->where('user_id','=',$user_id)->select('shipping_name','shipping_phone','shipping_zip_code','shipping_address_1','shipping_address_2')->first();

    $addresses = [];

    if(isset($res_user)) {
      $data = [
        'id'=>0,
        'user_id'=>$user_id,
        'address_type'=>"신규배송지",
        'name'=>$res_user->shipping_name,
        'phone'=>$res_user->shipping_phone,
        'shipping_name'=>$res_user->shipping_name,
        'zip_code'=>$res_user->shipping_zip_code,
        'address_1'=>$res_user->shipping_address_1,
        'address_2'=>$res_user->shipping_address_2,
        'msg'=>'',
        'is_default'=>"true",
        'msg'=>'배송시 요청사항을 선택해 주세요.'
      ];
      array_push($addresses, $data);
      return $addresses;
    }

    return [];
  }
}