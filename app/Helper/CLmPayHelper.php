<?php


namespace App\Helper;


use App\Http\dao\CBaseDAO;
use App\Http\dao\CUserDAO;

class CLmPayHelper
{
  public function getLogs($user_id, $min, $max)
  {
    $dao = new CBaseDAO();

    $sql = "SELECT * FROM cash_log WHERE user_idx = {$user_id} AND delete_date IS NULL ORDER BY idx DESC LIMIT {$min}, {$max}";
    $res = $dao->selectQuery($sql,"db");

    $datas = [];
    foreach ($res as $item) {
      $data = [
        "idx"         => $item->idx,
        "type"        => $item->type,
        "method"      => $item->method,
        "action"      => $item->action,
        "amount"      => $item->amount,
        "state"       => $item->state,
        "insert_date" => $item->insert_date,
      ];
      $datas[] = $data;
    }

    $sql = "SELECT COUNT(*) AS cnt FROM cash_log WHERE user_idx = {$user_id} AND delete_date IS NULL";
    $count = $dao->selectQuery($sql,"db");
    $response = [
      "total"     => $count[0]->cnt,
      "last_page" => ceil($count[0]->cnt / $max),
      "data"      => $datas,
    ];

    return $response;
  }

  public function setCommunityLmPay ($mount, $user_idx = 0) {

  }

  public function addLmpay ($user_idx, $method, $amount,$action, $order_info_idx = null, $memo = null, $cash_log_idx = null) {
    $dao = new CUserDAO();
    $userInfo = $dao->getUserInfo($user_idx)[0];
    $data = [
      "user_idx" => $user_idx,
      "type" => "온라인",
      "method" => $method,
      "sender" => $userInfo->shipping_name,
      "action" => $action,
      "state" => "결제됨",
      "amount" => $amount,
      "memo" => $memo,
      "order_info_idx" => $order_info_idx,
      "cash_log_idx" => $cash_log_idx
    ];
    return $dao->insertQuery("cash_log",$data,"pay_w");
  }
}