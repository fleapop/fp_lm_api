<?php


namespace App\Helper;


class CGoodsHelper
{
  public function getExhibitionToday()
  {
    $sql1 = "select g.id , g.title, g.price
                    , IFNULL(price - IFNULL((SELECT amount FROM promotions WHERE goods_id = g.id and deleted_at is null and ((use_date =1 and start_date <= now() and end_date >= now()) or use_date = 0) ORDER BY id DESC LIMIT 1),0),0) AS promotion_price
                    ,g.use_discount ,g.imgs ,g.stock_state ,g.view , st.name as seller_name, st.idx as seller_id
                    , gp.ended_at as hotdeal_end
                    , g.like_count
                from fp_pay.goods g
                inner join fp_pay.goods_promotions gp
                    on g.id = gp.goods_id
                    and gp.started_at <= now()
                    and gp.ended_at >= now()
                    and date_format(gp.ended_at, '%Y-%m-%d') = date_format(now(), '%Y-%m-%d')
                    and gp.display_state = 'show'
                left join fp_db.seller_tbl st on g.seller_id = st.idx
                where g.display_state = 'show' and g.deleted_at is null and g.stock_state = 'sale' and st.display_state = 'show' and st.delete_date is null
                order by gp.display_order, gp.ended_at desc
                ";
    $this->pconn = DB::connection(DB_PAYMENT_R);
    $Res1 = $this->pconn->select($sql1);

    return $Res1;
  }
}