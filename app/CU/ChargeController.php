<?php


namespace App\CU;

use DB;
use App\CUCharge;
use App\Barcode;
use App\Lmpay\LMPayController;
use App\Lmpay\ChargeController as LMPayChargeController;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ChargeController extends CUController {
    private $isNewCode = true;
    private $CODE = [
        "SUCCESS"=>"00",
        "NOT_SUP_CARD"=>"69",
        "EMPTY_CHARGE_DATA"=>"33",
        "ALREADY_CHARGE_ORDER"=>"36",
        "ALREADY_CANCEL_ORDER"=>"51",
        "FAIL_CHARGE"=>"53",
        "FAIL_CANCEL"=>"54",
        "ERR_CHARGE_RES"=>"55",
        "ERR_CANCEL"=>"61",
        "SUCC_REFUND_REQ"=>"90",
        "ERR_REFUND_REQ"=>"91",
        "EMPTY_PARAM"=>"81",
        "REUSE_BARCODE"=>"82"
    ];

    private $CODE_MSG = [
        "00"=>"정상적으로 처리 되었습니다.",
        "69"=>"미지원카드",
        "33"=>"충전정보없음",
        "36"=>"이미완료된거래",
        "51"=>"이미충전취소된거래",
        "53"=>"충전실패",
        "54"=>"충전취소실패",
        "55"=>"충전정보응답비정상",
        "61"=>"충전망취소중 오류",
        "90"=>"정상환불요청",
        "91"=>"비정상환불요청",
        "81"=>"필수파라미터누락",
        "82"=>"중복된 바코드 사용"

    ];

    const REQ_CHARGE = 1, REQ_CHARGE_CANCEL = 2;

    /**
     * 충전 요청
     * 3.4 충전 요청
     * @param
     */

    public function viewChargeResponseBody($request) {
        $dedata = [
            "CODE"=>$request->CODE,
            "MSG"=>$request->MSG,
            "DATAINFO"=>[
                "TRANNO"=>$this->getDecrypt_($request->DATAINFO["TRANNO"])
            ]
        ];

        // dd($dedata);
        // // dd($dedata);

//       $endata = [
//         "BILLNO"=>$request->BILLNO,
//         "PININFO"=>$request->PININFO,
//         "JOBGUBUN"=>$request->JOBGUBUN,
//         "TRANNO"=>$this->getDecrypt_pf($request->TRANNO),
//         "SALEDATE"=>$request->SALEDATE,
//         "AMT"=>$request->AMT,
//         "POS_NO"=>$request->POS_NO,
//         "SALETIME"=>$request->SALETIME,
//         "STORE_NO"=>$request->STORE_NO
//       ];

//       $request = new Request();
//       $request->setMethod('POST');
//       $request->request->add($endata);

//       return $this->charge($request);
    }

     public function viewChargeBody($request) {

       $dedata = [
         "BILLNO"=>$this->getDecrypt_pf($request->BILLNO),
         "PININFO"=>$this->getDecrypt_pf($request->PININFO),
         "JOBGUBUN"=>$this->getDecrypt_pf($request->JOBGUBUN),
         "TRANNO"=>$this->getDecrypt_pf($request->TRANNO),
         "SALEDATE"=>$this->getDecrypt_pf($request->SALEDATE),
         "AMT"=>$this->getDecrypt_pf($request->AMT),
         "POS_NO"=>$this->getDecrypt_pf($request->POS_NO),
         "SALETIME"=>$this->getDecrypt_pf($request->SALETIME),
         "STORE_NO"=>$this->getDecrypt_pf($request->STORE_NO)
       ];

       // dd($dedata);
       // // dd($dedata);

//       $endata = [
//         "BILLNO"=>$request->BILLNO,
//         "PININFO"=>$request->PININFO,
//         "JOBGUBUN"=>$request->JOBGUBUN,
//         "TRANNO"=>$request->TRANNO,
//         "SALEDATE"=>$request->SALEDATE,
//         "AMT"=>$request->AMT,
//         "POS_NO"=>$request->POS_NO,
//         "SALETIME"=>$request->SALETIME,
//         "STORE_NO"=>$request->STORE_NO
//       ];

//       $request = new Request();
//       $request->setMethod('POST');
//       $request->request->add($endata);

//       return $this->charge($request);
     }

     public function makeChargeData() {
       $data = [
         "JOBGUBUN"=>$this->getEncrypt_pf('HGoZ9G91GbOMtAjt/8BYHg=='),
         "PININFO"=>$this->getEncrypt_pf('+PNHV6HOYOXnJ1zKeCKJUQ=='),
         "SALEDATE"=>$this->getEncrypt_pf('20201112'),
         "SALETIME"=>$this->getEncrypt_pf('111213'),
         "AMT"=>$this->getEncrypt_pf('1000'),
         "TRANNO"=>$this->getEncrypt_pf('123456'),
         "STORE_NO"=>$this->getEncrypt_pf('12'),
         "POS_NO"=>$this->getEncrypt_pf('1'),
         "BILLNO"=>$this->getEncrypt_pf('12345')
       ];

       $request = new Request();
       $request->setMethod('POST');
       $request->request->add($data);

       return $this->charge($request);
     }
    public function charge($request) {

        $response = $this->initResponse();
        $reqData = [
            'JOBGUBUN'=>$this->getDecrypt_pf($request->JOBGUBUN),
            'PININFO'=>$this->getDecrypt_pf($request->PININFO),
            'SALEDATE'=>$this->getDecrypt_pf($request->SALEDATE),
            'SALETIME'=>$this->getDecrypt_pf($request->SALETIME),
            'AMT'=>$this->getDecrypt_pf($request->AMT),
            'TRANNO'=>$this->getDecrypt_pf($request->TRANNO),
            'STORE_NO'=>$this->getDecrypt_pf($request->STORE_NO),
            'POS_NO'=>$this->getDecrypt_pf($request->POS_NO),
            'BILLNO'=>$this->getDecrypt_pf($request->BILLNO),
        ];


        if($this->isNewCode) {
            $reqData = [
                'JOBGUBUN'=>$request->JOBGUBUN,
                'PININFO'=>$request->PININFO,
                'SALEDATE'=>$request->SALEDATE,
                'SALETIME'=>$request->SALETIME,
                'AMT'=>$request->AMT,
                'TRANNO'=>$request->TRANNO,
                'STORE_NO'=>$request->STORE_NO,
                'POS_NO'=>$request->POS_NO,
                'BILLNO'=>$request->BILLNO,
            ];

            if(env("APP_DEBUG", true)) {
                $url = "http://testapi.fleapop.co.kr/api/request/v1/pt/charge";
                $client = new Client();
                $response = $client->request('POST', $url, [
                    'form_params' => $reqData,
                ]);
                return $response->getBody()->getContents();
            } else {
                $url = "http://api.fleapop.co.kr/api/request/v1/pt/charge";
                $client = new Client();
                $response = $client->request('POST', $url, [
                    'form_params' => $reqData,
                ]);
                return $response->getBody()->getContents();
            }
        } else {
            $logMsg = "REQUEST : ChargeController@charge\n";
            $logMsg .= "Request : \n";
            $logMsg .= json_encode($reqData);

            if(!$request->filled(["JOBGUBUN", "PININFO", "SALEDATE", "SALETIME", "AMT", "TRANNO", "STORE_NO", "POS_NO","BILLNO"])) {
                $response = $this->setError("EMPTY_PARAM", $response, self::REQ_CHARGE);
                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);
                return str_replace('\\/', '/',json_encode($response));
            }

            // 바코드 사용여부 체크
            $usedBarcode = CUCharge::where('barcode',$reqData['PININFO'])->first();
            if(isset($usedBarcode)) {
                $DATAINFO = [
                    "JOBGUBUN"=>$this->getEncrypt_pf("PFRP"),
                    "USERUNINO"=>$this->getEncrypt_pf(""),
                    "TRANNO"=>$this->getEncrypt_pf(""),
                    "ORGTRANNO"=>$this->getEncrypt_pf(""),
                    "PININFO"=>$this->getEncrypt_pf(""),
                    "OKNO"=>$this->getEncrypt_pf(""),
                    "ORGOKNO"=>$this->getEncrypt_pf(""),
                    "BALANCE"=>$this->getEncrypt_pf(""),
                    "AMT"=>$this->getEncrypt_pf("")
                ];
                $response["CODE"] = 69;
                $response["MSG"] = "타 상품 결제 후 새로고침한 고객 바코드를 충전해 주세요";
                $response["DATAINFO"] = $DATAINFO;

                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);

                return str_replace('\\/', '/',json_encode($response));
            }

            // 충전 여부 체크
            $charged = CUCharge::where([["tranno","=",$reqData['TRANNO']],["state","=","결제됨"]])->first();

            if(isset($charged)) {
                $response = $this->setError("ALREADY_CHARGE_ORDER", $response, self::REQ_CHARGE);
                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);
                return str_replace('\\/', '/',json_encode($response));
            }

            $userId = $this->getUserId($reqData['PININFO']);
            if($userId) {
                $order = $this->getOrder($userId);
            } else {
                $response = $this->setError("NOT_SUP_CARD", $response, self::REQ_CHARGE);
                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);
                return str_replace('\\/', '/',json_encode($response));
            }

            $lmpay = new LMPayChargeController();
            $lmpayRes = $lmpay->cu($userId, $order->order, (int)$reqData['AMT']);

            if($lmpayRes) {
                $balance = $lmpay->getBalance($userId);
                $DATAINFO = [
                    "JOBGUBUN"=>$this->getEncrypt_pf("PYRP"),
                    "USERUNINO"=>$this->getEncrypt_pf($userId),
                    "TRANNO"=>$this->getEncrypt_pf($reqData['TRANNO']),
                    "PININFO"=>$this->getEncrypt_pf($reqData['PININFO']),
                    "OKNO"=>$this->getEncrypt_pf($order->order),
                    "BALANCE"=>$this->getEncrypt_pf($balance),
                    "AMT"=>$this->getEncrypt_pf($reqData['AMT'])
                ];

                $response["DATAINFO"] = $DATAINFO;

                $order->type = "충전";
                $order->barcode = $reqData['PININFO'];
                $order->tranno = $reqData['TRANNO'];
                $order->okno = $order->order;
                $order->amount = (int)$reqData['AMT'];
                $order->state = "결제대기";
                $order->cash_log_idx = $lmpayRes->idx;
                $order->req_data = json_encode($reqData);
                $order->req_timestamp = $reqData['SALEDATE'].$reqData['SALETIME'];
                $order->res_data = json_encode($response);
                $order->res_timestamp = date("YmdHis",time());

                $order->save();

                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);

                return str_replace('\\/', '/',json_encode($response));

            } else {
                $response = $this->setError("FAIL_CHARGE", $response, self::REQ_CHARGE);

                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);

                return str_replace('\\/', '/',json_encode($response));
            }

        }






    }

    /**
     * 충전 취소 요청
     * 3.5 충전 취소 요청
     * @param
     */
    public function cancel($request) {
        $response = $this->initResponse();
        $reqData = [
            'JOBGUBUN'=>$this->getDecrypt_pf($request->JOBGUBUN),
            'PININFO'=>$this->getDecrypt_pf($request->PININFO),
            'SALEDATE'=>$this->getDecrypt_pf($request->SALEDATE),
            'SALETIME'=>$this->getDecrypt_pf($request->SALETIME),
            'AMT'=>$this->getDecrypt_pf($request->AMT),
            'TRANNO'=>$this->getDecrypt_pf($request->TRANNO),
            'ORGTRANNO'=>$this->getDecrypt_pf($request->ORGTRANNO),
            'STORE_NO'=>$this->getDecrypt_pf($request->STORE_NO),
            'POS_NO'=>$this->getDecrypt_pf($request->POS_NO),
            'BILLNO'=>$this->getDecrypt_pf($request->BILLNO),
        ];

        if($this->isNewCode) {
            $reqData = [
                'JOBGUBUN'=>$request->JOBGUBUN,
                'PININFO'=>$request->PININFO,
                'SALEDATE'=>$request->SALEDATE,
                'SALETIME'=>$request->SALETIME,
                'AMT'=>$request->AMT,
                'TRANNO'=>$request->TRANNO,
                'ORGTRANNO'=>$request->ORGTRANNO,
                'STORE_NO'=>$request->STORE_NO,
                'POS_NO'=>$request->POS_NO,
                'BILLNO'=>$request->BILLNO,
            ];

            if(env("APP_DEBUG", true)) {
                $url = "http://testapi.fleapop.co.kr/api/request/v1/pt/charge/cancel";
                $client = new Client();
                $response = $client->request('POST', $url, [
                    'form_params' => $reqData,
                ]);
                return $response->getBody()->getContents();
            } else {
                $url = "http://api.fleapop.co.kr/api/request/v1/pt/charge/cancel";
                $client = new Client();
                $response = $client->request('POST', $url, [
                    'form_params' => $reqData,
                ]);
                return $response->getBody()->getContents();
            }
        } else {
            $logMsg = "REQUEST : ChargeController@cancel\n";
            $logMsg .= "Request : \n";
            $logMsg .= json_encode($reqData);

            // 필수 파라미터 검사
            if(!$request->filled(["JOBGUBUN", "PININFO", "SALEDATE", "SALETIME", "AMT", "TRANNO", "ORGTRANNO", "STORE_NO", "POS_NO","BILLNO"])) {
                $response = $this->setError("EMPTY_PARAM", $response, self::REQ_CHARGE_CANCEL);
                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);
                return str_replace('\\/', '/',json_encode($response));
            }

            // 기존 취소건 검사
            $canceled = CUCharge::where([['tranno',$reqData['TRANNO']],['state','결제됨']])->first();
            if(isset($canceled)) {
                $response = $this->setError("ALREADY_CANCEL_ORDER", $response, self::REQ_CHARGE_CANCEL);
                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);
                return str_replace('\\/', '/',json_encode($response));
            }

            // 바코드 유저 조회 검사
            $userId = $this->getUserId($reqData['PININFO']);
            if(!$userId) {
                $response = $this->setError("NOT_SUP_CARD", $response, self::REQ_CHARGE_CANCEL);
                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);
                return str_replace('\\/', '/',json_encode($response));
            }

            // 기존 요청번호 충전 금액, 충전취소 금액 비교
            $charged = CUCharge::where([['tranno', $reqData['ORGTRANNO']],['state','결제됨']])->first();
            if(!isset($charged)) {
                $response = $this->setError("EMPTY_CHARGE_DATA", $response, self::REQ_CHARGE_CANCEL);
                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);
                return str_replace('\\/', '/',json_encode($response));
            } else if(isset($charged) && $charged->amount != (int)$reqData['AMT']) {
                $response = $this->setError("FAIL_CANCEL", $response, self::REQ_CHARGE_CANCEL);
                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);
                return str_replace('\\/', '/',json_encode($response));
            }

            // 현재 잔액과 취소 요청 금액 비교
            $pay = new LMPayChargeController();
            $balance = $pay->getBalance($userId);
            if($balance < (int)$reqData['AMT']) {
                $response = $this->setError("FAIL_CANCEL", $response, self::REQ_CHARGE_CANCEL);

                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);

                return str_replace('\\/', '/',json_encode($response));
            }

            // 기존 러마페이 충전로그 확인
            $cashLog = $pay->getLog($charged->cash_log_idx);
            if(!isset($cashLog)) {
                $response = $this->setError("FAIL_CANCEL", $response, self::REQ_CHARGE_CANCEL);

                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);

                return str_replace('\\/', '/',json_encode($response));
            }

            $order = $this->getOrder($userId);
            $pay->cancel($cashLog->idx, "CU 충전취소를 진행하였습니다.");

            $balance = $pay->getBalance($userId);
            $DATAINFO = [
                'JOBGUBUN'=>$this->getEncrypt_pf('PFRP'),
                'USERUNINO'=>$this->getEncrypt_pf($userId),
                'TRANNO'=>$this->getEncrypt_pf($reqData['TRANNO']),
                'ORGTRANNO'=>$this->getEncrypt_pf($reqData['ORGTRANNO']),
                'PININFO'=>$this->getEncrypt_pf($reqData['PININFO']),
                'OKNO'=>$this->getEncrypt_pf($order->order),
                'ORGOKNO'=>$this->getEncrypt_pf($charged->okno),
                'BALANCE'=>$this->getEncrypt_pf($balance),
            ];

            $response["DATAINFO"] = $DATAINFO;

            $order->type = "충전취소";
            $order->log_idx = $charged->idx;
            $order->barcode = $reqData['PININFO'];
            $order->tranno = $reqData['TRANNO'];
            $order->okno = $order->order;
            $order->orgtranno = $charged->tranno;
            $order->amount = (int)$reqData['AMT'] * -1;
            $order->state = "결제됨";
            $order->cash_log_idx = $cashLog->idx;
            $order->req_data = json_encode($reqData);
            $order->req_timestamp = $reqData['SALEDATE'].$reqData['SALETIME'];
            $order->res_data = json_encode($response);
            $order->res_timestamp = date("YmdHis",time());
            $order->save();

            $logMsg .= "\nResponse : \n";
            $logMsg .= json_encode($response);
            Log::channel("cu")->info($logMsg);

            return str_replace('\\/', '/',json_encode($response));
        }
    }

    /**
     * 충전 망취소 요청
     * 3.7 충전 망취소 요청
     * @param
     */
    public function requestCancel($request) {
        $response = [
            'CODE'=>'',
            'MSG'=>'',
            'TRANNO'=>'',
            'TMSTAMT'=>date("YmdHis",time())
        ];

        $reqData = [
            'JOBGUBUN'=>$this->getDecrypt_pf($request->JOBGUBUN),
            'TRANNO'=>$this->getDecrypt_pf($request->TRANNO),
        ];

        if($this->isNewCode) {
            $reqData["JOBGUBUN"] = $request->JOBGUBUN;
            $reqData["TRANNO"] = $request->TRANNO;

            $url = "http://api.fleapop.co.kr/api/request/v1/pt/request/cancel";
            $client = new Client();
            $response = $client->request('POST', $url, [
                'form_params' => $reqData,
            ]);
            return $response->getBody()->getContents();
        } else {
            $logMsg = "REQUEST : ChargeController@requestCancel\n";
            $logMsg .= "Request : \n";
            $logMsg .= json_encode($reqData);

            // 필수 파라미터 검사
            if(!$request->filled(["JOBGUBUN","TRANNO"])) {
                $response['CODE'] = $this->CODE['EMPTY_PARAM'];
                $response['MSG'] = $this->CODE_MSG[$response['CODE']];

                $logMsg .= "\nResponse : \n";
                $logMsg .= json_encode($response);
                Log::channel("cu")->info($logMsg);

                return str_replace('\\/', '/',json_encode($response));
            }



            $response['TRANNO'] = $reqData['TRANNO'];

            $pay = new LMPayChargeController();

            if($reqData['JOBGUBUN'] == "PYRQ") {
                // 충전요청 망취소
                $charged = CUCharge::where([['tranno', $reqData['TRANNO']],['type','충전']])->first();
                $pay->cancel($charged->cash_log_idx, "[망취소] CU 충전취소를 진행하였습니다.");
                $charged->state = "기타";
                $charged->save();

            } else {
                // 충전취소요청 망취소

                $canceled = CUCharge::where([['tranno', $reqData['TRANNO']],['type','충전취소']])->first();
                $canceled->state = "기타";
                $canceled->save();

                $charged = CUCharge::find($canceled->log_idx);

                if(isset($charged)) {
                    $cashLog = $pay->getLog($canceled->cash_log_idx);
                    $cashLog->state = "결제됨";
                    $cashLog->save();
                }
            }

            $response['CODE'] = $this->CODE['SUCCESS'];
            $response['MSG'] = $this->CODE_MSG[$response['CODE']];

            $logMsg .= "\nResponse : \n";
            $logMsg .= json_encode($response);
            Log::channel("cu")->info($logMsg);

            return str_replace('\\/', '/',json_encode($response));

        }


    }



    private function getOrder($user_id = 0) {
        $data = [
            'user_idx'=>$user_id
        ];

        $order = CUCharge::create($data);
        $tmpOrder = $order->idx;

        while(strlen($tmpOrder) < 7) {
            $tmpOrder = "0".$tmpOrder;
        }
        $tmpOrder = "1".$tmpOrder;

        $order->order = $tmpOrder;
        $order->save();
        return $order;
    }


    private function setError($ERROR_CODE, $response, $type = 0) {
        $response["CODE"] = $this->CODE[$ERROR_CODE];
        $response["MSG"] = $this->CODE_MSG[$this->CODE[$ERROR_CODE]];
        switch($type) {
            case self::REQ_CHARGE : {
                $DATAINFO = [
                    "JOBGUBUN"=>$this->getEncrypt_pf("PFRP"),
                    "USERUNINO"=>$this->getEncrypt_pf(""),
                    "TRANNO"=>$this->getEncrypt_pf(""),
                    "ORGTRANNO"=>$this->getEncrypt_pf(""),
                    "PININFO"=>$this->getEncrypt_pf(""),
                    "OKNO"=>$this->getEncrypt_pf(""),
                    "ORGOKNO"=>$this->getEncrypt_pf(""),
                    "BALANCE"=>$this->getEncrypt_pf(""),
                    "AMT"=>$this->getEncrypt_pf("")
                ];
                $response["DATAINFO"] = $DATAINFO;
                break;
            }
            case self::REQ_CHARGE_CANCEL : {
                $DATAINFO = [
                    "JOBGUBUN"=>$this->getEncrypt_pf("PFRP"),
                    "USERUNINO"=>$this->getEncrypt_pf(""),
                    "TRANNO"=>$this->getEncrypt_pf(""),
                    "ORGTRANNO"=>$this->getEncrypt_pf(""),
                    "PININFO"=>$this->getEncrypt_pf(""),
                    "OKNO"=>$this->getEncrypt_pf(""),
                    "ORGOKNO"=>$this->getEncrypt_pf(""),
                    "BALANCE"=>$this->getEncrypt_pf("")
                ];
                $response["DATAINFO"] = $DATAINFO;
                break;
            }
        }

        return $response;
    }


    private function initResponse() {
        $response = array(
            "CODE"=>$this->CODE["SUCCESS"],
            "MSG"=>$this->CODE_MSG[$this->CODE["SUCCESS"]],
            "TMSTAMT"=>date("YmdHis",time())
        );
        return $response;
    }


    private function getUserId($barcode) {
        $user = Barcode::where("barcode",$barcode)->whereNotNull("user_idx")->first();
        if(isset($user)) {
            return $user->user_idx;
        } else {
            return false;
        }
    }

}
