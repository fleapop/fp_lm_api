<?php


namespace App\CU;

use App\CURefund;
use App\LMPay;
use DB;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class RefundController extends CUController {

    private $MBRNO = "";
    private $CHNCD = "FLP";
    private $MCTCD = "fleapop";

    private $url_refund = "http://112.175.103.79:4436/fleapop/v1/refund";
    private $url_result = "http://112.175.103.79:4436/fleapop/v1/refund/result";
    private $url_cancel = "http://112.175.103.79:4436/fleapop/v1/refund/cancel";

//     private $url_refund = "http://127.0.0.1:8000/api/dev/cu/refund";
//     private $url_result = "http://127.0.0.1:8000/api/dev/cu/refund";
    // private $url_cancel = "http://127.0.0.1:8000/api/dev/cu/refund";

    private $bank_code = [
        "경남은행"=>"039",
        "국민은행"=>"004",
        "기업은행"=>"003",
        "농협"=>"011",
        "대구은행"=>"031",
        "부산은행"=>"032",
        "산업은행"=>"002",
        "새마을금고"=>"045",
        "수협"=>"007",
        "신한은행"=>"088",
        "신협"=>"048",
        "외환은행"=>"081",
        "우리은행"=>"020",
        "우체국"=>"071",
        "전북은행"=>"037",
        "SC제일은행"=>"023",
        "카카오뱅크"=>"090",
        "케이뱅크"=>"089",
        "KEB하나은행"=>"081"
    ];

    function __construct() {
        if(env('APP_DEBUG') == "true") {
            // $this->MBRNO = "1000000033";
            $this->MBRNO = "1000000001";
        } else {
            $this->MBRNO = "1000000001";
        }
    }

    /**
     * 송금요청
     * 3.3 송금요청
     * @param
     */
    public function refund($request) {



        try {
            $data = $this->getTRANNO($request['user_idx']);
            $reqData = [
                'MBRNO'=>$this->getEncrypt_fp($this->MBRNO),
                'TXREQNO'=>$this->getEncrypt_fp($data->TRANNO),
                'SNDUSRNM'=>$this->getEncrypt_fp('러블리마켓'),
                'CHNCD'=>$this->getEncrypt_fp($this->CHNCD),
                'MCTCD'=>$this->getEncrypt_fp($this->MCTCD),
                'AMT'=>$this->getEncrypt_fp($request['amount']),
                'RCVBNKCD'=>$this->getEncrypt_fp($this->bank_code[$request['bank']]),
                'RCVACCNO'=>$this->getEncrypt_fp($request['account']),
                'USERUNINO'=>$this->getEncrypt_fp($request['user_idx']),
                'RCVDPSNM'=>$this->getEncrypt_fp($request['holder']),
            ];

            $logReqData = [
                'MBRNO'=>$this->MBRNO,
                'TXREQNO'=>$data->TRANNO,
                'SNDUSRNM'=>'러블리마켓',
                'CHNCD'=>$this->CHNCD,
                'MCTCD'=>$this->MCTCD,
                'AMT'=>$request['amount'],
                'RCVBNKCD'=>$this->bank_code[$request['bank']],
                'RCVACCNO'=>$request['account'],
                'USERUNINO'=>$request['user_idx'],
                'RCVDPSNM'=>$request['holder'],
            ];

            // dd($logReqData);

            $log = "REQUEST : RefundController@refund\n";
            $log .= json_encode($logReqData,JSON_UNESCAPED_UNICODE);
            $log .= "\n".now()."\n";
            Log::channel("cu")->info($log);

            $data->cash_log_idxs = $request['cash_log_idx'];
            $data->amount = $request['amount'];
            $data->req_data .= "|:|".json_encode($reqData);
            $data->req_time .= "|:|".date("YmdHis",time());

            $client = new Client();
            $response = $client->request('POST', $this->url_refund, [
                'form_params' => $reqData,
            ]);


            $result = json_decode($response->getBody()->getContents());

            print_r(json_encode($result, JSON_UNESCAPED_UNICODE));

            if($response->getStatusCode() != 200) {
                $log = "Response ({$response->getStatusCode()}) : \n";
                $log .= json_encode($result);
                $log .= "\n".now()."\n";
                Log::channel("cu")->info($log);
                return false;
            }

            $result->RESULTDATA->TXNO = $this->getDecrypt_fp($result->RESULTDATA->TXNO);
            $result->RESULTDATA->TXDTTM = $this->getDecrypt_fp($result->RESULTDATA->TXDTTM);

            $log = "Response ({$response->getStatusCode()}) : \n";
            $log .= json_encode($result);
            $log .= "\n".now()."\n";
            Log::channel("cu")->info($log);

            $data->RESULTTP = $result->RESULTTP;
            $data->RESULTCD = $result->RESULTCD;
            $data->RESULTMSG = $result->RESULTMSG;

            $data->TXNO = $result->RESULTDATA->TXNO;
            $data->TXDTTM = $result->RESULTDATA->TXDTTM;
            $data->res_data .= "|:|".json_encode($result);
            $data->res_time .= "|:|".date("YmdHis",time());
            $data->update_date = date("Y-m-d H:i:s", time());
            $data->save();



            return $data->TRANNO;
        } catch(Exception $e) {
            $log = "Response Error : \n";
            $log .= $e->getMessage();
            $log .= "\n".now()."\n";
            Log::channel("cu")->info($log);
            return false;
        }



    }

    /**
     * 송금 결과 조회
     * 3.4 송금 결과 조회
     * @param $idx cu_refund_log idx
     */
    public function check($idx) {
        $refund = CURefund::find($idx);
        if(isset($refund)) {
            $reqData = [
                'TXREQNO'=>$this->getEncrypt_fp($refund->TRANNO),
                'MBRNO'=>$this->getEncrypt_fp($this->MBRNO),
                'CHNCD'=>$this->getEncrypt_fp($this->CHNCD),
                'MCTCD'=>$this->getEncrypt_fp($this->MCTCD),
            ];

            $log = "Request RefundController@check : \n";
            $log .= json_encode([
                'TXREQNO'=>$refund->TRANNO,
                'MBRNO'=>$this->MBRNO,
                'CHNCD'=>$this->CHNCD,
                'MCTCD'=>$this->MCTCD
            ]);
            $log .= "\n".now()."\n";
            Log::channel("cu")->info($log);

            $refund->req_data .= '|:|'.json_encode($reqData);
            $refund->req_time .= '|:|'.date('YmdHis', time());

            $client = new Client();
            $response = $client->request('POST', $this->url_result, [
                'form_params' => $reqData,
            ]);

            $result = json_decode($response->getBody()->getContents());
            if($response->getStatusCode() != 200) {
                $log = "Response {$response->getStatusCode()} : \n";
                $log .= json_encode($result);
                $log .= "\n".now()."\n";
                Log::channel("cu")->info($log);
            }

            $refund->res_data .= '|:|'.json_encode($result);
            $refund->res_time .= '|:|'.date("YmdHis", time());

            $waitCode = ["01","10","20","23","90"];
            $resData = [
                'RESULTTP'=>$result->RESULTTP,
                'RESULTCD'=>$result->RESULTCD,
                'RESULTMSG'=>$result->RESULTMSG,
                'RESULTMSG'=>$result->RESULTMSG,
                'RESULTDATA'=>[
                    'AMT'=>$this->getDecrypt_fp($result->RESULTDATA->AMT),
                    'TXSTSCD'=>$this->getDecrypt_fp($result->RESULTDATA->TXSTSCD),
                    'ORGRESCD'=>$this->getDecrypt_fp($result->RESULTDATA->ORGRESCD),
                    'BNKRESCD'=>$this->getDecrypt_fp($result->RESULTDATA->BNKRESCD),
                ]
            ];

            $log = "Response {$response->getStatusCode()} : \n";
            $log .= json_encode($resData);
            $log .= "\n".now()."\n";
            Log::channel("cu")->info($log);

            $refund->TXSTSCD = $resData['RESULTDATA']['TXSTSCD'];
            $refund->ORGRESCD = $resData['RESULTDATA']['ORGRESCD'];
            $refund->BNKRESCD = $resData['RESULTDATA']['BNKRESCD'];
            $cashLog = LMPay::where([['type','오프라인'],['action','환불'],['appy_num',$refund->TRANNO]])->first();

            if($resData['RESULTDATA']['TXSTSCD'] == "21") {
                $refund->result_check = '결제됨';
                $cashLog->state = '결제됨';
                $cashLog->save();
            } else if(in_array($resData['RESULTDATA']['TXSTSCD'], $waitCode)) {
                $refund->result_check = '결제대기';
                $cashLog->state = '결제대기';
                $cashLog->save();
            } else {
                $refund->result_check = '실패';
                $cashLog->state = '기타';
                $cashLog->save();
            }

            $refund->save();
            return ['success'=>true];
        } else {
            return ['success'=>false];
        }
    }


    /**
     * 송금 요청 확인
     * 3.4 송금 요청 확인
     * @param $id cu_refund_log idx
     */
     public function checkRefund($request) {
         $response = [
             "CODE"=>"",
             "MSG"=>"",
             "TMSTAMT"=>""
         ];


         $reqData = [
             'TXREQNO'=>$request->TXREQNO,
             'AMT'=>$request->AMT,
             'VFMSG'=>$request->VFMSG,
         ];

         $pconn = DB::connection(DB_PAYMENT_W);
         $endTime = microtime(true);
         $filename = 'cu_refund_logger_' . date('d-m-y') . '.log';
         $res = $pconn->table("cu_refund_check_log")
         ->where("title","=",$filename)
         ->get();
         if(isset($res[0])) {
             $res = $pconn->table("cu_refund_check_log")
             ->where("idx","=",$res[0]->idx)
             ->update([
                 "log"=>$res[0]->log."|:|\n".json_encode($reqData)
             ]);
         } else {
             $data = [
                 "title"=>$filename,
                 "log"=>json_encode($reqData)
             ];

             $res = $pconn->table("cu_refund_check_log")
             ->insertGetId($data);
         }

         $reqData = [
             'TXREQNO'=>$this->getDecrypt_pf($request->TXREQNO),
             'AMT'=>$this->getDecrypt_pf($request->AMT),
             'VFMSG'=>$this->getDecrypt_pf($request->VFMSG),
         ];

         $log = "REQUEST : RefundController@checkRefund\n";
         $log .= "Request : \n";
         $log .= json_encode($reqData,JSON_UNESCAPED_UNICODE);
         $log .= "\n".now()."\n";
         Log::channel("cu")->info($log);




         $refund = CURefund::where('TRANNO', $reqData['TXREQNO'])->first();

         $refund->request_check = "true";
         $refund->request_check_time = date("Y-m-d H:i:s",time());
         $refund->request_check_code = 0;
         $refund->req_data .= "|:|".json_encode($reqData, true);
         $refund->req_time .= "|:|".date("Y-m-d H:i:s", time());

         if(!$request->filled(["TXREQNO", "AMT", "VFMSG"])) {
             $response["CODE"] = "91";
             $response["MSG"] = "필수입력값 누락";
             $response["TMSTAMT"] = date("YmdHis",time());

             $refund->request_check_code = 91;
             $refund->res_time .= "|:|".date("YmdHis",time());
             $refund->res_data .= "|:|".json_encode($response);
             $refund->save();

             $log = "REQUEST : RefundController@checkRefund\n";
             $log .= "Response : \n";
             $log .= json_encode($response,JSON_UNESCAPED_UNICODE);
             $log .= "\n".now()."\n";
             Log::channel("cu")->info($log);
             return str_replace('\\/', '/',json_encode($response));
         }

         $VFMSG = hash("sha256",$refund->TRANNO.$this->MBRNO);

         if(!strcmp($VFMSG, $reqData['VFMSG']) && $refund->amount == $reqData['AMT']) {
             $refund->request_check_code = 90;
             $refund->res_time .= "|:|".date("YmdHis",time());

             $response["CODE"] = "90";
             $response["MSG"] = "정상환불요청";
             $response["TMSTAMT"] = date("YmdHis",time());

             $refund->res_data .= "|:|".json_encode($response);
             $refund->save();

             $log = "REQUEST : RefundController@checkRefund\n";
             $log .= "Response : \n";
             $log .= json_encode($response,JSON_UNESCAPED_UNICODE);
             $log .= "\n".now()."\n";
             Log::channel("cu")->info($log);

             return str_replace('\\/', '/',json_encode($response));
         } else {
             $refund->request_check_code = 91;
             $refund->res_time .= "|:|".date("YmdHis",time());

             $response["CODE"] = "91";
             $response["MSG"] = "검증값 불일치";
             $response["TMSTAMT"] = date("YmdHis",time());

             $refund->res_data .= "|:|".json_encode($response);
             $refund->save();

             $log = "REQUEST : RefundController@checkRefund\n";
             $log .= "Response : \n";
             $log .= json_encode($response,JSON_UNESCAPED_UNICODE);
             $log .= "\n".now()."\n";
             Log::channel("cu")->info($log);

             return str_replace('\\/', '/',json_encode($response));
         }
     }

    /**
     * 송금 요청 취소
     * 3.5 송금 요청 취소
     * @param $idx cu_refund_log idx
     */
    public function cancel($idx) {
        $refund = CURefund::find($idx);

        if(isset($refund)) {

            $reqData = [
                'TXREQNO'=>$this->getEncrypt_fp($refund->TRANNO),
                'MBRNO'=>$this->getEncrypt_fp($this->MBRNO),
                'CHNCD'=>$this->getEncrypt_fp($this->CHNCD),
                'MCTCD'=>$this->getEncrypt_fp($this->MCTCD),
            ];


            $client = new Client();
            $response = $client->request('POST', $this->url_cancel, [
                'form_params' => $reqData,
            ]);

            $result = json_decode($response->getBody());
            if($response->getStatusCode() != 200) {
                return [
                  'success'=>false,
                  'code'=>-99,
                  'msg'=>"응답이 없습니다."
                ];
            }

            if($result->RESULTTP == 'S') {
                $refund->delete();
                $cashLog = LMPay::where([['type','오프라인'],['action','환불'],['appy_num',$refund->TRANNO]])->first();
                if(isset($cashLog)) {
                    $cashLog->delete();
                }
            } else {
                return [
                    'success'=>false,
                    'code'=>$result->RESULTCD,
                    'msg'=>$result->RESULTMSG
                ];
            }
            return ['success'=>true];
        } else {

            return [
              'success'=>false,
              'code'=>-99,
              'msg'=>"해당 환불건이 없습니다."
            ];
        }
    }


    private function getTRANNO($user_idx = 0) {
        $refund = CURefund::create([
            'user_idx'=>$user_idx
        ]);


        $tran_no = $refund->idx;
        while(strlen($tran_no) < 9) {
            $tran_no = "0".$tran_no;
        }

        $tran_no = "1".$tran_no;

        $refund->TRANNO = $tran_no;
        $refund->save();

        return $refund;
    }

}
