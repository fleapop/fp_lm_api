<?php


namespace App\CU;

use App\Models\CUBarcode;
use App\Lmpay\LMPayController;
use DB;
use GuzzleHttp\Client;


class CUController
{
    private $isNewCode = true;
    private $conn;
    private $conn_w;
    private $pconn;
    private $pconn_w;
    private $host     = 'http://112.175.103.79:4436';
    private $pfkey    = "playtong2fleapop";
    private $fpkey    = "fleapop2playtong";
    private $is_enc   = true;
    private $url_dash = "/fleapop/v1/statistics";

    public function __construct()
    {
        $this->conn = DB::connection('fleapop_w');
        $this->pconn = DB::connection('fp_payment_w');
    }

    /**
     * 잔액 조회 요청
     * 3.8 잔액 조회 요청
     * @param
     */
    public function balance($request)
    {
        $response = [
            "CODE"     => "0000",
            "MSG"      => "정상적으로 처리 되었습니다.",
            "TMSTAMT"  => date("YmdHis", time()),
            "DATAINFO" => [
                "JOBGUBUN"  => $this->getEncrypt_pf("1010"),
                "TRANAPPNO" => $this->getEncrypt_pf(""),
                "REMAINAMT" => $this->getEncrypt_pf(""),
            ],
        ];

        // 필수값 검사
        if (! $request->filled(["JOBGUBUN", "TRANREQNO", "SALEDATE", "MEMBERNO", "STORENO", "BARCODE"])) {
            $response["CODE"] = "2021";
            $response["MSG"] = "결제 중 오류";
            return str_replace('\\/', '/', json_encode($response));
        }

        $reqData = [
            'JOBGUBUN'  => $this->getDecrypt_pf($request->JOBGUBUN),
            'TRANREQNO' => $this->getDecrypt_pf($request->TRANREQNO),
            'SALEDATE'  => $this->getDecrypt_pf($request->SALEDATE),
            'MEMBERNO'  => $this->getDecrypt_pf($request->MEMBERNO),
            'STORENO'   => $this->getDecrypt_pf($request->STORENO),
            'BARCODE'   => $this->getDecrypt_pf($request->BARCODE),
        ];

        if($this->isNewCode) {
            $reqData["JOBGUBUN"] = $request->JOBGUBUN;
            $reqData["TRANREQNO"] = $request->TRANREQNO;
            $reqData["SALEDATE"] = $request->SALEDATE;
            $reqData["MEMBERNO"] = $request->MEMBERNO;
            $reqData["STORENO"] = $request->STORENO;
            $reqData["BARCODE"] = $request->BARCODE;

            $url = "http://api.fleapop.co.kr/api/request/v1/pt/get/balance";
            $client = new Client();
            $response = $client->request('POST', $url, [
                'form_params' => $reqData,
            ]);
            return $response->getBody()->getContents();
        } else {
            $cuBarcode = CUBarcode::where('barcode', $reqData['BARCODE'])->first();
            // 바코드 존재 검사
            if (! isset($cuBarcode)) {
                $response["CODE"] = "2021";
                $response["MSG"] = "바코드를 새로고침 해주세요.";
                return str_replace('\\/', '/', json_encode($response));
            }

            $lmpay = new LMPayController();
            $balance = $lmpay->getBalance($cuBarcode->user_idx);
            $response["DATAINFO"]["REMAINAMT"] = $this->getEncrypt_pf($balance);
            return str_replace('\\/', '/', json_encode($response));
        }


    }

    /**
     * 기간별 데이터 조회
     * 3.6 기간별 데이터 조회
     * @param $start_date (YYYYMMDD), $end_date(YYYYMMDD)
     */
    public function dash($start_date, $end_date)
    {
        if (! isset($start_date) || ! isset($end_date)) {
            return [
                'success' => false,
                'msg'     => '필수입력값이 누락되었습니다.',
            ];
        }

        $reqData = [
            'SDATE' => $start_date,
            'EDATE' => $end_date,
        ];
        $url = $this->host . $this->url_dash;


        $client = new Client();
        $response = $client->request('POST', $url, [
            'form_params' => $reqData,
        ]);

        if ($response->getStatusCode() != 200) {
            return [
                'success' => false,
                'msg'     => '올바른 요청값이 아닙니다.',
            ];
        }

        $result = json_decode($response->getBody());

        return [
            'success' => true,
            'msg'     => '',
            'data'    => [
                'CHARGE_AMT'         => $result->RESULTDATA->CHARGE_AMT,
                'CHARGE_CNT'         => $result->RESULTDATA->CHARGE_CNT,
                'CHARGE_CANCEL_AMT'  => $result->RESULTDATA->CHARGE_CANCEL_AMT,
                'CHARGE_CANCEL_CNT'  => $result->RESULTDATA->CHARGE_CANCEL_CNT,
                'PAYMENT_AMT'        => $result->RESULTDATA->PAYMENT_AMT,
                'PAYMENT_CNT'        => $result->RESULTDATA->PAYMENT_CNT,
                'PAYMENT_CANCEL_AMT' => $result->RESULTDATA->PAYMENT_CANCEL_AMT,
                'PAYMENT_CANCEL_CNT' => $result->RESULTDATA->PAYMENT_CANCEL_CNT,
                'REFUND_AMT'         => $result->RESULTDATA->REFUND_AMT,
                'REFUND_CNT'         => $result->RESULTDATA->REFUND_CNT,
            ],
        ];
    }

    public function getEncrypt_pf($msg)
    {
        if (! $this->is_enc) {
            return $msg;
        }

        $endata = @openssl_encrypt($msg, "aes-128-cbc", $this->pfkey, true, $this->pfkey);
        $endata = base64_encode($endata);
        return $endata;
    }

    public function getDecrypt_pf($msg)
    {
        if (! $this->is_enc) {
            return $msg;
        }

        $data = base64_decode($msg);
        $endata = @openssl_decrypt($data, "aes-128-cbc", $this->pfkey, true, $this->pfkey);
        return $endata;
    }

    public function getEncrypt_fp($msg)
    {
        if (! $this->is_enc) {
            return $msg;
        }

        $endata = @openssl_encrypt($msg, "aes-128-cbc", $this->fpkey, true, $this->fpkey);
        $endata = base64_encode($endata);
        return $endata;
    }

    public function getDecrypt_fp($msg)
    {
        if (! $this->is_enc) {
            return $msg;
        }

        $data = base64_decode($msg);
        $endata = @openssl_decrypt($data, "aes-128-cbc", $this->fpkey, true, $this->fpkey);
        return $endata;
    }
}
