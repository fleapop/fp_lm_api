<?php


namespace App\CU;

use DB;
use App\Models\CUBuy;
use App\Models\CUBarcode;
use App\Lmpay\LMPayController;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class BuyController extends CUController {

    private $isNewCode = true;
    /**
     * 결제요청
     * 3.9 결제요청
     * @param
     */
    public function buy($request) {
        $response = [
            "CODE"=>"0000",
            "MSG"=>"정상적으로 처리 되었습니다.",
            "TMSTAMT"=>date("YmdHis",time()),
            "DATAINFO"=>[
                "JOBGUBUN"=>$this->getEncrypt_pf("2010"),
                "TRANAPPNO"=>$this->getEncrypt_pf(""),
                "USERUNINO"=>$this->getEncrypt_pf(""),
                "AMT"=>$this->getEncrypt_pf(""),
                "REMAINAMT"=>$this->getEncrypt_pf(""),
            ]
        ];

        if(!$request->filled(["JOBGUBUN","TRANREQNO","SALEDATE","MEMBERNO","STORENO","BARCODE","AMT"])) {

            $response["CODE"] = "2021";
            $response["MSG"] = "결제 중 오류";
            return str_replace('\\/', '/',json_encode($response));
        }

        $reqData = [
            "JOBGUBUN"=>$this->getDecrypt_pf($request->JOBGUBUN),
            "TRANREQNO"=>$this->getDecrypt_pf($request->TRANREQNO),
            "SALEDATE"=>$this->getDecrypt_pf($request->SALEDATE),
            "MEMBERNO"=>$this->getDecrypt_pf($request->MEMBERNO),
            "STORENO"=>$this->getDecrypt_pf($request->STORENO),
            "BARCODE"=>$this->getDecrypt_pf($request->BARCODE),
            "AMT"=>$this->getDecrypt_pf($request->AMT)
        ];

        if($this->isNewCode) {
            $reqData = [
                "JOBGUBUN"=>$request->JOBGUBUN,
                "TRANREQNO"=>$request->TRANREQNO,
                "SALEDATE"=>$request->SALEDATE,
                "MEMBERNO"=>$request->MEMBERNO,
                "STORENO"=>$request->STORENO,
                "BARCODE"=>$request->BARCODE,
                "AMT"=>$request->AMT
            ];

            if(env("APP_DEBUG", true)) {
                $url = "http://testapi.fleapop.co.kr/api/request/v1/pt/payment";
                $client = new Client();
                $response = $client->request('POST', $url, [
                    'form_params' => $reqData,
                ]);
                return $response->getBody()->getContents();
            } else {
                $url = "http://api.fleapop.co.kr/api/request/v1/pt/payment";
                $client = new Client();
                $response = $client->request('POST', $url, [
                    'form_params' => $reqData,
                ]);
                return $response->getBody()->getContents();
            }
        } else {
            $cuBarcode = CUBarcode::where('barcode',$reqData['BARCODE'])->first();
            // 바코드 존재 검사

            if(!isset($cuBarcode) || $cuBarcode->is_used == "Y") {

                $response["CODE"] = "2002";
                $response["MSG"] = "존재하지 않은 카드번호이거나 사용 가능 카드가 아닙니다.";
                return str_replace('\\/', '/',json_encode($response));
            }

            // 요청번호 존재 유무 검사
            $buyLog = CUBuy::where("TRANREQNO", $reqData['TRANREQNO'])->first();
            if(isset($buyLog)) {


                $response["CODE"] = "2021";
                $response["MSG"] = "결제실패 중복된 요청번호가 존재합니다.";
                return str_replace('\\/', '/',json_encode($response));
            }

            $lmpay = new LMPayController();
            $balance = $lmpay->getBalance($cuBarcode->user_idx);

            // 잔액검증
            if((int)$balance < (int)$reqData["AMT"]) {
                $response["CODE"] = "2021";
                $response["MSG"] = "결제실패 (잔액부족)";
                return str_replace('\\/', '/',json_encode($response));
            }

            $data = [
                'user_idx'=>$cuBarcode->user_idx,
                'barcode'=>$reqData['BARCODE'],
                'TRANREQNO'=>$reqData['TRANREQNO'],
                'SALEDATE'=>$reqData['SALEDATE'],
                'state'=>'결제됨',
                'MEMBERNO'=>$reqData['MEMBERNO'],
                'STORENO'=>$reqData['STORENO'],
                'AMT'=>$reqData['AMT']
            ];

            $buy = CUBuy::create($data);
            if($buy) {
                $cashData = [
                    'user_idx'=>$cuBarcode->user_idx,
                    'order_info_idx'=>$buy->idx,
                    'type'=>'오프라인',
                    'method'=>'마일리지',
                    'action'=>'지불',
                    'state'=>'결제됨',
                    'amount'=>(int)$reqData['AMT'] * -1
                ];

                $payRes = $lmpay->paid($cuBarcode->user_idx, $cashData);

                if(isset($payRes['success']) && !$payRes['success']) {
                    $response["CODE"] = "0006";
                    $response["MSG"] = "결제 중 오류";
                    return str_replace('\\/', '/',json_encode($response));
                }


                $TRANAPPNO = $this->getOrder($cuBarcode->user_idx, $buy->idx);

                $buy->cash_log_idx = $payRes->idx;
                $buy->TRANAPPNO = $TRANAPPNO;
                $buy->save();

                $payRes->appy_num = $TRANAPPNO;
                $payRes->save();

                $cuBarcode->is_used = "Y";
                $cuBarcode->save();

                $response['DATAINFO']['TRANAPPNO'] = $this->getEncrypt_pf($TRANAPPNO);
                $response['DATAINFO']['USERUNINO'] = $this->getEncrypt_pf($cuBarcode->user_idx);
                $response['DATAINFO']['AMT'] = $this->getEncrypt_pf($reqData['AMT']);
                $response['DATAINFO']['REMAINAMT'] = $this->getEncrypt_pf($lmpay->getBalance($cuBarcode->user_idx));
                return str_replace('\\/', '/',json_encode($response));
            } else {
                $response["CODE"] = "0006";
                $response["MSG"] = "결제 중 오류";
                return str_replace('\\/', '/',json_encode($response));
            }
        }
    }

    /**
     * 결제 취소 요청
     * 3.10 결제 취소 요청
     * @param
     */
    public function cancel($request) {
        $response = [
            "CODE"=>"0000",
            "MSG"=>"정상적으로 처리 되었습니다.",
            "TMSTAMT"=>date("YmdHis",time()),
            "DATAINFO"=>[
                "JOBGUBUN"=>$this->getEncrypt_pf("3010"),
                "TRANAPPNO"=>$this->getEncrypt_pf(""),
                "USERUNINO"=>$this->getEncrypt_pf(""),
                "REMAINAMT"=>$this->getEncrypt_pf("")
            ]
        ];

        if(!$request->filled(["JOBGUBUN","TRANREQNO","SALEDATE","MEMBERNO","STORENO","ORGAPPNO","ORGSALEDATE","ORGAMT"])) {

            $response["CODE"] = "2021";
            $response["MSG"] = "결제 중 오류";
            return str_replace('\\/', '/',json_encode($response));
        }

        $reqData= [
            "JOBGUBUN"=>$this->getDecrypt_pf($request->JOBGUBUN),
            "TRANREQNO"=>$this->getDecrypt_pf($request->TRANREQNO),
            "SALEDATE"=>$this->getDecrypt_pf($request->SALEDATE),
            "MEMBERNO"=>$this->getDecrypt_pf($request->MEMBERNO),
            "STORENO"=>$this->getDecrypt_pf($request->STORENO),
            "ORGAPPNO"=>$this->getDecrypt_pf($request->ORGAPPNO),
            "ORGSALEDATE"=>$this->getDecrypt_pf($request->ORGSALEDATE),
            "ORGAMT"=>$this->getDecrypt_pf($request->ORGAMT)
        ];

        $log = "Request CU Buy Cancel Decryp\n";
        $log .= json_encode($reqData,JSON_UNESCAPED_UNICODE);
        $log .= "\n";

        if($this->isNewCode) {

            $reqData["JOBGUBUN"] = $request->JOBGUBUN;
            $reqData["TRANREQNO"] = $request->TRANREQNO;
            $reqData["SALEDATE"] = $request->SALEDATE;
            $reqData["MEMBERNO"] = $request->MEMBERNO;
            $reqData["STORENO"] = $request->STORENO;
            $reqData["ORGAPPNO"] = $request->ORGAPPNO;
            $reqData["ORGSALEDATE"] = $request->ORGSALEDATE;
            $reqData["ORGAMT"] = $request->ORGAMT;



            $log .= "Request CU Buy Cancel Encryp\n";
            $log .= json_encode($reqData,JSON_UNESCAPED_UNICODE);
            Log::channel("cu")->info($log);

            if(env("APP_DEBUG", true)) {
                $url = "http://testapi.fleapop.co.kr/api/request/v1/pt/payment/cancel";
                $client = new Client();
                $response = $client->request('POST', $url, [
                    'form_params' => $reqData,
                ]);
                return $response->getBody()->getContents();
            } else {
                $url = "http://api.fleapop.co.kr/api/request/v1/pt/payment/cancel";
                $client = new Client();
                $response = $client->request('POST', $url, [
                    'form_params' => $reqData,
                ]);
                return $response->getBody()->getContents();
            }
        } else {
            $order = CUBuy::where('TRANAPPNO', $reqData['ORGAPPNO'])->first();
            // 기존 구매내역 검사
            if(!isset($order)) {
                $response["CODE"] = "2016";
                $response["MSG"] = "존재하지 않은 승인번호 입니다.";
                return str_replace('\\/', '/',json_encode($response));
            }

            $canceled = CUBuy::where('log_idx',$order->idx)->first();
            // 기존 취소내역 검사
            if(isset($canceled)) {
                $response["CODE"] = "3004";
                $response["MSG"] = "이미 취소된 거래";
                return str_replace('\\/', '/',json_encode($response));
            }

            // 금액 비교
            if($order->AMT != $reqData["ORGAMT"]) {
                $response["CODE"] = "2022";
                $response["MSG"] = "결제취소 실패";
                return str_replace('\\/', '/',json_encode($response));
            }

            $data = [
                'user_idx'=>$order->user_idx,
                'log_idx'=>$order->idx,
                'TRANREQNO'=>$reqData['TRANREQNO'],
                'CANCELDATE'=>$reqData['SALEDATE'],
                'state'=>'결제됨',
                'MEMBERNO'=>$reqData['MEMBERNO'],
                'STORENO'=>$reqData['STORENO'],
                'AMT'=>(int)$reqData['ORGAMT'] * -1
            ];

            $cancel = CUBuy::create($data);
            if(isset($cancel)) {
                $lmpay = new LMPayController();
                $cancelRes = $lmpay->cancel($order->cash_log_idx);
                if($cancelRes['success']) {

                    $TRANAPPNO = $this->getOrder($order->user_idx, $cancel->idx);
                    $cancel->cash_log_idx = $order->cash_log_idx;
                    $cancel->TRANAPPNO = $TRANAPPNO;
                    $cancel->save();

                    $response["DATAINFO"]["TRANAPPNO"] = $this->getEncrypt_pf($TRANAPPNO);
                    $response["DATAINFO"]["USERUNINO"] = $this->getEncrypt_pf($order->user_idx);
                    $response["DATAINFO"]["REMAINAMT"] = $this->getEncrypt_pf($lmpay->getBalance($order->user_idx));

                    return str_replace('\\/', '/',json_encode($response));
                } else {
                    $response["CODE"] = "0006";
                    $response["MSG"] = "결제 중 오류";
                    return str_replace('\\/', '/',json_encode($response));
                }
            } else {
                $response["CODE"] = "0006";
                $response["MSG"] = "결제 중 오류";
                return str_replace('\\/', '/',json_encode($response));
            }
        }

    }

    /**
     * 결제 망취소
     * 3.11 결제 망취소
     * @param
     */
    public function requestCancel($request) {
        $response = [
            "CODE"=>"0000",
            "MSG"=>"정상적으로 처리 되었습니다.",
            "TMSTAMT"=>date("YmdHis",time())
        ];

        if(!$request->filled(["JOBGUBUN","TRANREQNO"])) {
            $response["CODE"] = "2021";
            $response["MSG"] = "결제 중 오류";

            return str_replace('\\/', '/',json_encode($response));
        }

        $reqData = [
            "JOBGUBUN"=>$this->getDecrypt_pf($request->JOBGUBUN),
            "TRANREQNO"=>$this->getDecrypt_pf($request->TRANREQNO)
        ];

        if($this->isNewCode) {
            $reqData["JOBGUBUN"] = $request->JOBGUBUN;
            $reqData["TRANREQNO"] = $request->TRANREQNO;

            if(env("APP_DEBUG", true)) {
                $url = "http://testapi.fleapop.co.kr/api/request/v1/pt/payment/request/cancel";
                $client = new Client();
                $response = $client->request('POST', $url, [
                    'form_params' => $reqData,
                ]);
                return $response->getBody()->getContents();
            } else {
                $url = "http://api.fleapop.co.kr/api/request/v1/pt/payment/request/cancel";
                $client = new Client();
                $response = $client->request('POST', $url, [
                    'form_params' => $reqData,
                ]);
                return $response->getBody()->getContents();
            }
        } else {
            $order = CUBuy::where('TRANREQNO', $reqData['TRANREQNO'])->first();
            if(!isset($order)) {
                $response["CODE"] = "2013";
                $response["MSG"] = "결제 정보 응답 망취소 업데이트 오류";
                return str_replace('\\/', '/',json_encode($response));
            }


            if($reqData['JOBGUBUN'] == "2000") {
                // 결제요청 망취소

                $order->state = "기타";
                $order->save();

                $lmpay = new LMPayController();
                $log = $lmpay->cancel($order->cash_log_idx);
            } else {
                // 결제취소요청 망취소
                $order->state = "기타";
                $order->save();
                $lmpay = LMPay::find($order->cash_log_idx);
                $lmpay->state = "결제됨";
                $lmpay->save();

            }


            return str_replace('\\/', '/',json_encode($response));
        }


    }

    private function getOrder($user_idx = 0, $idx = 0) {
        if($idx == 0) {
            $order_idx = CUBuy::create(['user_idx'=>$user_idx])->idx;
        } else {
            $order_idx = $idx;
        }

        $tmp_order = $order_idx;
        while(strlen($tmp_order) < 9) {
            $tmp_order = "0".$tmp_order;
        }

        $tmp_order = "1".$tmp_order;
        $order = CUBuy::find($order_idx);
        $order->TRANAPPNO = $tmp_order;
        $order->save();

        return $tmp_order;
    }
}
