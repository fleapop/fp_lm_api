<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
//    protected $listen = [
//        Registered::class => [
//            SendEmailVerificationNotification::class,
//        ],
//    ];
  protected $listen = [
    'App\Events\MemberJoin'                            => [
      'App\Listeners\SendWelcomeMessage',
    ],
    'App\Events\OrderVbankCompleted'                   => [
      'App\Listeners\SendVbankMessage',
    ],
    'App\Events\OrderCompleted'                        => [
      'App\Listeners\SendOrderedMessage',
    ],
    'App\Events\OrderCanceled'                         => [
      'App\Listeners\SendOrderCanceledMessage',
    ],
    'App\Events\OrderCancelRequest'                    => [
      'App\Listeners\SendOrderCancelRequestMessage',
    ],
    'App\Events\OrderExchangeRequest'                  => [
      'App\Listeners\SendOrderExchangeRequestMessage',
    ],
    'App\Events\OrderRefundRequest'                    => [
      'App\Listeners\SendOrderRefundRequestMessage',
    ],
    'App\Events\StockSoldOut'                          => [
      'App\Listeners\SendStockSoldOutMessage',
    ],
    'App\Events\AnsweredCustomerServices'              => [
      'App\Listeners\SendAnsweredCustomerServicesMessage',
    ],
    'App\Events\AnsweredOrderCustomerServices'         => [
      'App\Listeners\SendAnsweredCustomerServicesMessage',
    ],
    'App\Events\ApprovedExchangeRequestBeforeDelivery' => [
      'App\Listeners\SendAnsweredExchangeApprovedMessage',
    ],
    'App\Events\DeclineExchangeRequestBeforeDelivery'  => [
      'App\Listeners\SendAnsweredExchangeDeclineMessage',
    ],
    'App\Events\CancelOrderBySeller'                   => [
      'App\Listeners\SendCancelOrderMessage',
    ],
    'App\Events\StartDelivery'                         => [
      'App\Listeners\SendStartDeliveryMessage',
    ],
    'App\Events\UpdateInvoiceInformation'              => [
      'App\Listeners\SendUpdatedInvoiceMessage',
    ],
    'App\Events\RequestRefundBySeller'       => [
      'App\Listeners\SendRequestRefundMessage',
    ],
    'App\Events\PaymentCancelBySeller'       => [
      'App\Listeners\SendPaymentCancelBySellerMessage',
    ],
    'App\Events\DeclineRequestCancelOrder'   => [
      'App\Listeners\SendDeclineRequestCancelOrderMessage',
    ],
    'App\Events\ApprovedExchangeOrder'       => [
      'App\Listeners\SendApprovedExchangeOrderMessage',
    ],
    'App\Events\DeclineExchangeOrder'        => [
      'App\Listeners\SendDeclineExchangeOrderMessage',
    ],
    'App\Events\ApprovedRefundOrder'         => [
      'App\Listeners\SendApprovedRefundOrderMessage',
    ],
    'App\Events\DeclineRefundOrder'          => [
      'App\Listeners\SendDeclineRefundOrderMessage',
    ],
    'App\Events\LMPayChargeByBankAccount'    => [
      'App\Listeners\SendLMPayVbankMessage',
    ],
    'App\Events\NoticeRegistered'            => [
      'App\Listeners\SendNoticeRegisteredMessage',
    ],
    'App\Events\ApplicationNoticeRegistered' => [
      'App\Listeners\SendApplicationNoticeRegisteredMessage',
    ],
  ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
