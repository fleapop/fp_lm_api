<?php

namespace App\Imports;

use App\Seller\Order;
use Exception;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class DeliveryImport implements ToCollection
{
    public function collection(Collection $rows)
    {
        $message = [];

        foreach ($rows as $index => $row) {
            // 열제목 무시
            if ($index === 0) {
                continue;
            }

            // 택배사코드, 송장번호 누락 무시
            if (! isset($row[4]) || ! isset($row[5]) || empty($row[4]) || empty($row[5])) {
                continue;
            }

            // 송장번호 문자열 포함 무시
            if (preg_match("/([a-zA-Z+])+/", $row[5], $matches)) {
                continue;
            }

            $orderDetail = Order::where('order_goods_number', $row[3])->first();

            if ($orderDetail) {
                $message[] = $orderDetail->send($row[4], $row[5]);
            }
        }

        if (array_search('구매취소', $message) > 0) {
            throw new Exception('구매취소');
        } else if (array_search('CS관리', $message) > 0) {
            throw new Exception('CS관리');
        }
    }
}
