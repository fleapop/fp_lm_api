<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

if (! function_exists('getAWSS3Url')) {
    function getAWSS3Url($filename)
    {
        $s3 = Storage::disk('s3')->getAdapter()->getClient();

        return $s3->getObjectUrl(Config::get('filesystems.disks.s3.bucket'), $filename);
    }
}

if (! function_exists('getUnique')) {
    function getUnique()
    {
        $res = preg_replace("/^0\.([0-9]+) ([0-9]+)$/", "$1-$2", microtime());
        $res = explode("-", $res);
        $res = strtoupper(dechex($res[0])) . strtoupper(dechex($res[1]));
        $res = str_pad($res, 15, '0', STR_PAD_LEFT);

        return $res;
    }
}

if (! function_exists('generateRandomString')) {
    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

if (! function_exists('getCourierCodeToName')) {
    function getCourierCodeToName($code = null)
    {
        $courierCode = [
            '01' => '우체국',
            '04' => 'CJ대한통운',
            '05' => '한진택배',
            '06' => '로젠택베',
            '08' => '롯데택배',
            '11' => '일양로지스',
            '12' => 'EMS',
            '13' => 'DHL',
            '14' => 'UPS',
            '16' => '한의사랑택배',
            '17' => '천일택배',
            '18' => '건영택배',
            '19' => '고려택배',
            '20' => '한덱스',
            '21' => 'Fedex',
            '22' => '대신택배',
            '23' => '경동택배',
            '24' => 'CVSnet 편의점 택배',
            '25' => 'TNT Express',
            '26' => 'USPS',
            '27' => 'TPL',
            '28' => 'GSMNtoN',
            '29' => '에어보이익스프레스',
            '30' => 'KGL네트웍스',
            '32' => '합동택배',
            '33' => 'DHL Global Mail',
            '34' => 'i-Parcel',
            '35' => '포시즌 익스프레스',
            '37' => '범한판토스',
            '38' => 'ECMC Express',
            '40' => '굿투럭',
            '41' => 'GSI Express',
            '42' => 'CJ대한통운 국제특송',
            '43' => '애니트랙',
            '44' => 'SLX',
            '45' => '호남한서택배',
            '46' => 'CU편의점 택배',
            '47' => '우리한방택배',
            '48' => 'ACI Express',
            '49' => 'ACE Express',
            '50' => 'GPS LOGIX',
            '51' => '성원글로벌',
            '52' => '세방',
            '53' => '농협택배',
            '54' => '홈픽택배',
            '55' => 'Euro Parcel',
            '56' => 'KGB 택배',
            '57' => 'Cway Express',
            '58' => '하이택배',
            '59' => '지오로지스',
            '60' => 'YJS글로벌(영국)',
            '61' => '워팩스코리아',
            '62' => '홈이노베이션로지스',
            '63' => '은하쉬핑',
            '64' => '퍼레버택배',
            '65' => 'YJS글로별(월드)',
            '66' => 'Giant Express',
            '67' => '디디로지스',
            '68' => '우리동네택배',
            '69' => '대림통운',
            '70' => 'LOTOS CORPORATION',
            '71' => 'IK 물류',
            '99' => '롯데택배 해외특송',
        ];

        if (! $code) {
            return $courierCode;
        } else {
            return $courierCode[$code];
        }
    }
}

if (! function_exists('removeEmoji')) {
    function removeEmoji($text)
    {
        // Match Emoticons
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        $cleanText = preg_replace($regexEmoticons, '', $text);

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        $cleanText = preg_replace($regexSymbols, '', $cleanText);

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        $cleanText = preg_replace($regexTransport, '', $cleanText);

        // Match Miscellaneous Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        $cleanText = preg_replace($regexMisc, '', $cleanText);

        // Match Dingbats
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        $cleanText = preg_replace($regexDingbats, '', $cleanText);

        // Match Flags
        $regexDingbats = '/[\x{1F1E6}-\x{1F1FF}]/u';
        $cleanText = preg_replace($regexDingbats, '', $cleanText);

        // Others
        $regexDingbats = '/[\x{1F910}-\x{1F95E}]/u';
        $cleanText = preg_replace($regexDingbats, '', $cleanText);
        $regexDingbats = '/[\x{1F980}-\x{1F991}]/u';
        $cleanText = preg_replace($regexDingbats, '', $cleanText);
        $regexDingbats = '/[\x{1F9C0}]/u';
        $cleanText = preg_replace($regexDingbats, '', $cleanText);
        $regexDingbats = '/[\x{1F9F9}]/u';
        $cleanText = preg_replace($regexDingbats, '', $cleanText);

        return $cleanText;
    }
}
