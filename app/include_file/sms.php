<?php
class SMS_Cls {

	public function SEND($dest_phone, $text) {
		$data = array(
				'send_name'=>'플리팝',
				'send_phone'=>'07088120820',
				'dest_name'=>'플리팝고객',
				'dest_phone'=>$dest_phone,
				'msg_body'=>$text
		);
		$data = http_build_query($data);

		if(in_array($dest_phone, array('01025623012','01083366825'))) {
			return false;
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://api.apistore.co.kr/ppurio/1/message/sms/fleapopinc");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$headers = [
				"Content-Type: application/x-www-form-urlencoded; charset=UTF-8",
				"x-waple-authorization: Nzk2MS0xNTIyMTMxMTQxMjUyLTc2MzZjZDE0LWQ2YzctNDJlZS1iNmNkLTE0ZDZjN2QyZWU3Mg=="
		];
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		$result = curl_exec($ch);
		curl_close($ch);

		return $result;
	}
}


?>
