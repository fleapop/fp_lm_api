<?php
function getDateFormatTypeOnlyDate($date_str) {
    return date("Y-m-d", strtotime($date_str));
}
function getDateFormatTypeDote($date_str)
{
    return date("Y.m.d", strtotime($date_str));
}


function getTimeFormatTypeAMPM($date_str) {
    $result = "";
    $hour = date("H", strtotime($date_str));
    $minute = date("i", strtotime($date_str));

    if($hour > 12) {
        $hour = $hour - 12;
        $result = "PM ".$hour.":".$minute;
    } else {
        $result = "AM ".$hour.":".$minute;
    }

    return $result;
}

function passing_time($date_str) {
    $time_lag = time() - strtotime($date_str);

    if($time_lag < 60) {
        $posting_time = "방금";
    } elseif($time_lag >= 60 and $time_lag < 3600) {
        $posting_time = floor($time_lag/60)."분 전";
    } elseif($time_lag >= 3600 and $time_lag < 86400) {
        $posting_time = floor($time_lag/3600)."시간 전";
    } else {
        $posting_time = date("y년 m월 d일", strtotime($date_str));
    }

    return $posting_time;

}
?>
