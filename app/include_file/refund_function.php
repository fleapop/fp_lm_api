<?php

function refundOfflineFleapop($conn, $obj) {
    $response = array("success"=>true, "error_code"=>200);
    $api = "";
    $key = "";
    if(IS_TEST) {
        $api = IMPORT_API_TEST;
        $key = IMPORT_KEY_TEST;
    } else {
        $api = IMPORT_API;
        $key = IMPORT_KEY;
    }

    $imp_token = getToken($api, $key);
    if($imp_token) {

        $user_data = $conn->table("user_list")
        ->where("user_id","=",$obj["user_id"])
        ->whereNull("delete_date")
        ->get();

        $cash_data = $conn->table("cash_log")
        ->where("idx","=",$obj["idx"])
        ->whereNull("delete_date")
        ->get();



        if($user_data->count() > 0 && $cash_data->count() > 0) {
            $user_data = $user_data[0];
            $cash_data = $cash_data[0];

            $bank_data = array(
                "refund_bank"=>"",
                "refund_account"=>"",
                "refund_owner"=>""
            );


            if(!isset($user_data->refund_bank)) {
                $response["success"] = false;
                $response["error_code"] = 400;
                $response["error_msg"] = "계좌 정보가 존재하지 않습니다.";
                return $response;
            } else {
                if(!getKCPBankCode($user_data->refund_bank)) {
                    $response["success"] = false;
                    $response["error_code"] = 400;
                    $response["error_msg"] = "은행코드표에 없는 은행입니다.";
                    return $response;
                } else {
                    $bank_data["refund_bank"] = $user_data->refund_bank;
                    $bank_data["refund_account"] = $user_data->refund_account;
                    $bank_data["refund_owner"] = $user_data->refund_owner;
                }

                if(!checkBank($imp_token, $bank_data)) {
                    $response["success"] = false;
                    $response["error_code"] = 400;
                    $response["error_msg"] = "계좌정보가 올바르지 않습니다.";
                    return $response;
                }

                $insert_data = [
                    "user_idx"=>$obj["user_id"],
                    "cash_log_idx"=>$obj["idx"],
                    "type"=>"오프라인",
                    "method"=>"마일리지",
                    "action"=>"환불",
                    "state"=>"결제대기",
                    "amount"=>$obj["amount"] * -1,
                    "vbank_num"=>$bank_data["refund_account"],
                    "vbank_name"=>$bank_data["refund_bank"]
                ];

                $res = $conn->table("cash_log")
                ->insertGetId($insert_data);

                if(!$res) {
                    $response["success"] = false;
                    $response["error_code"] = 400;
                    $response["error_msg"] = "환불 신청에 실패했습니다.";
                    return $response;
                } else {
                    return $response;
                }
            }

        } else {
            if($user_data->count() == 0) {
                $response["success"] = false;
                $response["error_code"] = 500;
                $response["error_msg"] = "해당 유저가 존재하지 않습니다.";
                return $response;
            } else {
                $response["success"] = false;
                $response["error_code"] = 500;
                $response["error_msg"] = "해당 충전정보가 존재하지 않습니다.";
                return $response;
            }
        }

    } else {
        $response["success"] = false;
        $response["error_code"] = 500;
        $response["error_msg"] = "아임포트 토큰 접근에 실패했습니다.";
        return $response;
    }

    return $response;
}

function refundOnline($conn, $obj) {
    $response = array("success"=>true, "error_code"=>200);
    $api = "";
    $key = "";
    if(IS_TEST) {
        $api = IMPORT_API_TEST;
        $key = IMPORT_KEY_TEST;
    } else {
        $api = IMPORT_API;
        $key = IMPORT_KEY;
    }

    $iamport = new Iamport($api, $key);
    $response_data = Iamport::getPayment($obj["imp_uid"]);

    if($response_data->success) {
        $imp_token = getToken($api, $key);

        if($imp_token) {

            $user_data = $conn->table("user_list")
            ->where("user_id","=",$obj["user_id"])
            ->whereNull("delete_date")
            ->get();

            $cash_data = $conn->table("cash_log")
            ->where("idx","=",$obj["idx"])
            ->whereNull("delete_date")
            ->get();

            if($user_data->count() > 0 && $cash_data->count() > 0) {
                $user_data = $user_data[0];
                $cash_data = $cash_data[0];
                $cash_types = array("현금","계좌이체","가상계좌","무통장");
                $bank_data = array(
                    "refund_bank"=>"",
                    "refund_account"=>"",
                    "refund_owner"=>""
                );

                if(in_array($cash_data->method, $cash_types)) {
                    // 현금 타입 충전인 경우 (계좌정보 필요)
                    // 은행정보 검증 과정
                    if(!isset($user_data->refund_bank)) {
                        $response["success"] = false;
                        $response["error_code"] = 400;
                        $response["error_msg"] = "계좌 정보가 존재하지 않습니다.";
                        return $response;
                    } else {
                        if(!getKCPBankCode($user_data->refund_bank)) {
                            $response["success"] = false;
                            $response["error_code"] = 400;
                            $response["error_msg"] = "은행코드표에 없는 은행입니다.";
                            return $response;
                        } else {
                            $bank_data["refund_bank"] = $user_data->refund_bank;
                            $bank_data["refund_account"] = $user_data->refund_account;
                            $bank_data["refund_owner"] = $user_data->refund_owner;
                        }

                        if(!checkBank($imp_token, $bank_data)) {
                            $response["success"] = false;
                            $response["error_code"] = 400;
                            $response["error_msg"] = "계좌정보가 올바르지 않습니다.";
                            return $response;
                        }
                    }
                }


                $cancel_data = array(
                    "imp_uid"=>$response_data->data->imp_uid,
                    "merchant_uid"=>$response_data->data->merchant_uid,
                    "amount"=>$obj["amount"],
                    "reason"=>"고객 환불요청",
                    "refund_holder"=>$bank_data["refund_owner"],
                    "refund_bank"=>getKCPBankCode($bank_data["refund_bank"]),
                    "refund_account"=>$bank_data["refund_account"]
                );



                // $cancel_result = $iamport->cancel($cancel_data);
                $iamport = new Iamport([
                    'apiKey'    => config('iamport.apiKey'),
                    'apiSecret' => config('iamport.apiSecret'),
                ]);
                $cancel_result = $iamport->cancelPayment($cancel_data["imp_uid"], $cancel_data["amount"], $cancel_data["reason"]);
                if(isset($cancel_result->success) && $cancel_result->success == true) {
                    // 환불 완료 결제됨 Insert
                    $insert_data = [
                        "user_idx"=>$obj["user_id"],
                        "cash_log_idx"=>$obj["idx"],
                        "refund_imp_uid"=>$obj["imp_uid"],
                        "type"=>"온라인",
                        "method"=>"마일리지",
                        "action"=>"환불",
                        "state"=>"결제됨",
                        "amount"=>$obj["amount"] * -1,
                        "vbank_num"=>$bank_data["refund_account"],
                        "vbank_name"=>$bank_data["refund_bank"]
                    ];

                    $res = $conn->table("cash_log")
                    ->insertGetId($insert_data);

                    if(!$res) {
                        $response["success"] = false;
                        $response["error_code"] = 200;
                        $response["error_msg"] = $cancel_result->error["message"];
                        return $response;
                    }
                } else {
                    // 환불 결제대기 Insert
                    $insert_data = [
                        "user_idx"=>$obj["user_id"],
                        "cash_log_idx"=>$obj["idx"],
                        "refund_imp_uid"=>$obj["imp_uid"],
                        "type"=>"온라인",
                        "method"=>"마일리지",
                        "action"=>"환불",
                        "state"=>"결제대기",
                        "error_msg"=>$cancel_result->error["message"],
                        "amount"=>$obj["amount"] * -1
                    ];

                    $res = $conn->table("cash_log")
                    ->insertGetId($insert_data);

                    if(!$res) {
                        $response["success"] = false;
                        $response["error_code"] = 200;
                        $response["error_msg"] = $cancel_result->error["message"];
                        return $response;
                    }
                }
            } else {
                if($user_data->count() == 0) {
                    $response["success"] = false;
                    $response["error_code"] = 500;
                    $response["error_msg"] = "해당 유저가 존재하지 않습니다.";
                    return $response;
                } else {
                    $response["success"] = false;
                    $response["error_code"] = 500;
                    $response["error_msg"] = "해당 충전정보가 존재하지 않습니다.";
                    return $response;
                }
            }
        } else {
            $response["success"] = false;
            $response["error_code"] = 500;
            $response["error_msg"] = "아임포트 토큰 접근에 실패했습니다.";
            return $response;
        }

    } else {
        $response["success"] = false;
        $response["error_code"] = 500;
        $response["error_msg"] = "아임포트 정보 조회에 실패했습니다.";
    }

    return $response;


}

?>
