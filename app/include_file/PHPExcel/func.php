<?php
/*
유저 잔액
function getUserLmPlayBalance($conn, $user_idm $format) -> int balance
*/
function getLMPayBalance($conn, $user_id = 0, $format = true)
{
    if ($user_id == 0) {
        return 0;
    }

    $sql = "SELECT IFNULL(SUM(amount), 0) as cash FROM cash_log WHERE user_idx = {$user_id} AND delete_date IS NULL AND (`state` = '결제됨' OR (`action` = '환불' AND `state` = '결제대기') OR (action = '환불' AND state = '기타')) AND `action` != '취소'";

    $res = $conn->select($sql);
    if ($res) {
        if ($format) {
            return number_format($res[0]->cash);
        } else {
            return $res[0]->cash;
        }
    } else {
        return 0;
    }
}

function getUserLmPlayBalance($conn, $user_id = 0, $format = true)
{
    if ($user_id == 0) {

        return 0;
    }
    $sql = "SELECT IFNULL(SUM(amount), 0) as cash FROM cash_log WHERE user_idx = {$user_id} AND delete_date IS NULL AND (`state` = '결제됨' OR (`action` = '환불' AND `state` = '결제대기') OR (action = '환불' AND state = '기타')) AND `action` != '취소'";
    $res = $conn->select($sql);
    if ($res) {
        if ($format) {
            return number_format($res[0]->cash);
        } else {
            return $res[0]->cash;
        }

    } else {
        return 0;
    }
}


function sendSMS($sms_data)
{
    $dest_phone = str_replace("", " ", $sms_data["dest_phone"]);
    $dest_phone = str_replace("", ".", $dest_phone);
    $dest_phone = str_replace("", "-", $dest_phone);

    $data = [
        "send_name"  => '플리팝',
        "send_phone" => '07088120820',
        "dest_name"  => $sms_data["dest_name"],
        "dest_phone" => $dest_phone,
        "msg_body"   => $sms_data["msg_body"],
    ];

    $data = http_build_query($data);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://api.apistore.co.kr/ppurio/1/message/sms/fleapopinc");
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $headers = [
        "Content-Type: application/x-www-form-urlencoded; charset=UTF-8",
        "x-waple-authorization: Nzk2MS0xNTIyMTMxMTQxMjUyLTc2MzZjZDE0LWQ2YzctNDJlZS1iNmNkLTE0ZDZjN2QyZWU3Mg==",
    ];

    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_exec($ch);
    curl_close($ch);
}

function getKCPBankCode($bank)
{
    $bank_code = [
        "경남은행"    => "BK39",
        "국민은행"    => "BK04",
        "기업은행"    => "BK03",
        "농협"      => "BK11",
        "대구은행"    => "BK31",
        "부산은행"    => "BK32",
        "산업은행"    => "BK02",
        "새마을금고"   => "BK45",
        "수협"      => "BK07",
        "신한은행"    => "BK88",
        "신협"      => "BK48",
        "외환은행"    => "BK81",
        "우리은행"    => "BK20",
        "우체국"     => "BK71",
        "전북은행"    => "BK37",
        "SC제일은행"  => "BK23",
        "카카오뱅크"   => "BK90",
        "케이뱅크"    => "BK89",
        "KEB하나은행" => "BK81",
    ];

    if (isset($bank_code[$bank])) {
        return $bank_code[$bank];
    } else {
        return false;
    }
}


function checkBank($token, $bank_data)
{
    // bank_data - refund_bank, refund_account, refund_owner
    $bank_code = [
        "경남은행"    => "039",
        "국민은행"    => "004",
        "기업은행"    => "003",
        "농협"      => "011",
        "대구은행"    => "031",
        "부산은행"    => "032",
        "산업은행"    => "002",
        "새마을금고"   => "045",
        "수협"      => "007",
        "신한은행"    => "026",
        "신협"      => "048",
        "외환은행"    => "081",
        "우리은행"    => "020",
        "우체국"     => "071",
        "전북은행"    => "037",
        "SC제일은행"  => "023",
        "카카오뱅크"   => "090",
        "케이뱅크"    => "089",
        "KEB하나은행" => "081",
    ];

    $bank_data["refund_bank"] = $bank_code[$bank_data["refund_bank"]];

    $check_url = "https://api.iamport.kr/vbanks/holder?bank_code={$bank_data["refund_bank"]}&bank_num={$bank_data["refund_account"]}&_token={$token}";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $check_url);
    curl_setopt($ch, CURLOPT_POST, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    // curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($import_data));
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);

// This should be the default Content-type for POST requests
    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-type: application/x-www-form-urlencoded"]);

    $result = curl_exec($ch);
    if (curl_errno($ch) !== 0) {
        return false;
    }

    curl_close($ch);
    $response = json_decode($result);
    // print_r($response);

    if ($response->code < 0) {
        return false;
    }

    if (strpos($response->response->bank_holder, $bank_data["refund_owner"]) !== false) {
        return true;
    } else {
        return false;
    }


}

function getToken($api, $key)
{
    $import_data = ["imp_key" => $api, "imp_secret" => $key];
    $import_get_token_url = "https://api.iamport.kr/users/getToken";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $import_get_token_url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($import_data));
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
    curl_setopt($ch, CURLOPT_TIMEOUT, 60);
    curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-type: application/x-www-form-urlencoded"]);

    $result = curl_exec($ch);
    if (curl_errno($ch) !== 0) {
        return false;
    }

    curl_close($ch);
    $response = json_decode($result);
    // print_r($response);
    return $response->response->access_token;


}

function getAmount($conn, $obj, $is_offline = true)
{
    // print_r("Asdfasdf");
    if ($is_offline) {
        // 오프라인
        $res = $conn->table("cash_log")
            ->where("cash_log_idx", "=", $obj->idx)
            ->whereNull("delete_date")
            ->whereNull("order_info_idx")
            ->get();

        $resultAmount = $obj->amount;
        foreach ($res as $item) {
            $waiting_refund_amount = 0;
            $resultAmount -= abs($item->amount);
            if ($item->state == "결제대기" && $item->action == "환불" && ! isset($market_seller_idx)) {
                $waiting_refund_amount = $item->amount;
            }
        }

        $obj->amount = $resultAmount;
        return $obj;

    } else {
        // 온라인
        if (IS_TEST) {
            $api = IMPORT_API_TEST;
            $key = IMPORT_KEY_TEST;
        } else {
            $api = IMPORT_API;
            $key = IMPORT_KEY;
        }

        $iamport = new Iamport($api, $key);

        if (isset($obj->imp_uid)) {

            $result = Iamport::getPayment($obj->imp_uid);

            if ($result->success) {
                $obj->amount = $result->data->amount - $result->data->cancel_amount;
            } else {
                $obj->amount = 0;
            }


            $resultAmount = $obj->amount;

            $res = $conn->table("cash_log")
                ->where("cash_log_idx", "=", $obj->idx)
                ->whereNull("delete_date")
                ->whereNull("order_info_idx")
                ->get();


            $log_refund_amount = 0;
            foreach ($res as $item) {

                $waiting_refund_amount = 0;

                if ($item->state == "결제대기" && $item->action == "환불" && ! isset($market_seller_idx)) {
                    $waiting_refund_amount = $item->amount;
                }

                if ($item->state == "결제됨" && $item->action == "환불" && ! isset($market_seller_idx)) {
                    $log_refund_amount += $item->amount;
                }

                $result = Iamport::getPayment($item->refund_imp_uid);

                if ($result->success) {
                    $item->amount = $result->data->amount - $result->data->cancel_amount;
                } else {
                    $item->amount = 0;
                }
                $resultAmount = $item->amount + $waiting_refund_amount;
            }


            if (isset($result->data->cancel_amount) && $result->data->cancel_amount != $log_refund_amount) {
                $resultAmount = $log_refund_amount;
            }


            $obj->amount = $resultAmount;
        } else {
            $obj->amount = 0;
        }

        // echo "<br /><br />";

        return $obj;
    }

}

function getDeliveryExcelRow($data)
{
    // print_r($data);
    $phone = preg_replace("/[^0-9]/", "", $data['orderer_phone']);
    $length = strlen($phone);
    $orderer_phone = "";

    switch ($length) {
        case 11 :
            $orderer_phone = preg_replace("/([0-9]{3})([0-9]{4})([0-9]{4})/", "$1-$2-$3", $phone);
            break;
        case 10:
            $orderer_phone = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1-$2-$3", $phone);
            break;
        default :
            $orderer_phone = $data['orderer_phone'];
            break;
    }

    $row = "";
    $goods_data = "";

    if (isset($data['order_goods'][0])) {
        foreach ($data['order_goods'][0] as $gData) {

            $goods_data = $gData->title;

            $option = "";
            if (strlen($gData->option_title1) > 0) {
                $option .= " [ " . $gData->option_title1 . ":" . $gData->option_value1;
            }

            if (strlen($gData->option_title2) > 0) {
                $option .= " / " . $gData->option_title2 . ":" . $gData->option_value2 . " ] ";
            } else {
                $option .= " ]";
            }

            if (strlen($gData->memo) > 0) {
                $goods_data = $gData->title . " - " . $gData->memo;
            }

            $goods_data .= $option;

            $goods_data .= ",<br/>";

            $row .= "
            <tr>
            <td style='border:1px solid gray; text-align:center;'>{$data['orderer_idx']}</td>
            <td style='border:1px solid gray; text-align:center;'>{$data['order_dates']}</td>
            <td style='border:1px solid gray; text-align:center;'>{$data['orderer_name']}</td>
            <td style='border:1px solid gray; text-align:center;'>{$orderer_phone}</td>
            <td style='border:1px solid gray; text-align:left;'> {$data['orderer_zipcode']}</td>
            <td style='border:1px solid gray; text-align:left;'> {$data['orderer_address1']} {$data['orderer_address2']}</td>
            <td style='border:1px solid gray; text-align:left;'>{$goods_data}</td>
            </tr>";

        }
    }
    return $row;
}

function isAdminLogin($conn)
{
    @session_start();

    if (isset($_SESSION['admin']) && ! empty($_SESSION['admin'])) {
        if ($_SESSION['admin']['loggined'] && $_SESSION['admin']['grade'] > 0) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }

}

function getDateFormat_type2($date_str)
{
    return date("Y.m.d", strtotime($date_str));
}

function getDateFormat_type3($date_str)
{
    return date("Y-m-d", strtotime($date_str));
}

function getDateFormat_type4($date_str)
{
    return date("Y년 m월 d일", strtotime($date_str));
}


function getTimeFormat_type1($date_str)
{
    $hour = date("H", strtotime($date_str));
    $minute = date("i", strtotime($date_str));

    if ($hour > 12) {
        $hour = $hour - 12;
        $result = "PM " . $hour . ":" . $minute;
    } else {
        $result = "AM " . $hour . ":" . $minute;
    }
    return $result;
}

function getTimeFormat_type4($date_str)
{
    $hour = date("H", strtotime($date_str));
    $minute = date("i", strtotime($date_str));

    if ($hour > 12) {
        $hour = $hour - 12;
        $result = "오후 " . $hour . ":" . $minute;
    } else {
        $result = "오전 " . $hour . ":" . $minute;
    }
    return $result;
}

function getTimeFormat_type2($date_str)
{
    $hour = date("H", strtotime($date_str));
    $minute = date("i", strtotime($date_str));
    $result = $hour . ":" . $minute;

    return $result;
}

function getTimeFormat_type3($date_str)
{
    $hour = date("H", strtotime($date_str));
    $minute = date("i", strtotime($date_str));
    $second = date("s", strtotime($date_str));
    $result = $hour . ":" . $minute . ":" . $second;

    return $result;
}

function encryptPassword($plainText)
{
    $encryptText = base64_encode(hash('sha256', $plainText, true));
    return $encryptText;
}

function isLogin($conn)
{
    @session_start();

    if (isset($_SESSION['user']) && ! empty($_SESSION['user'])) {
        if ($_SESSION['user']['loggedin']) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function getNewBarcode()
{
    $micro_time = microtime();

    $micro_time = explode(' ', $micro_time);
    $micro_time = ($micro_time[1] + 60 * 60 * 24 * 3) . str_pad($micro_time[0] * 1000000, 6, 0, STR_PAD_LEFT);

    $split = str_split($micro_time);
    $ret = [];
    while (null !== ($n = (count($split) % 2 ? array_shift($split) : array_pop($split)))) {
        array_push($ret, $n);
    }
    $ret = implode($ret);
    return $ret;
}

function checkBarcode($BARCODE)
{
    $split = str_split($BARCODE);
    $origin = [];
    while (null !== ($n = array_pop($split))) {
        if (count($split) % 2) {
            array_unshift($origin, $n);
        } else {
            array_push($origin, $n);
        }
    }
    $origin = (int)substr(implode($origin), 0, 10);
    return (time() < $origin && (time() + 60 * 60 * 24 * 3) > $origin);
}


function getUserRefundDash($conn, $user_id)
{
    $user_data = $conn->table("user_list")
                     ->where("user_id", "=", $user_id)
                     ->get()[0];


    // 1. 현재 고객의 결제됨 데이터 조회
    $now_date = date("Y-m-d", time());

    $res = $conn->table("cash_log")
        ->where([["user_idx", "=", $user_id], ["state", "=", "결제됨"]])
        ->whereNull("delete_date")
        ->get();

    $amount_types = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    $amount_objs = [[], [], [], [], [], [], [], [], [], []];
    $on_amount_objs = [];
    $off_amount_objs = [];
    $card_types = ["신용카드", "삼성페이", "페이코"];
    $cash_types = ["현금", "계좌이체", "가상계좌", "무통장"];
    $cu_types = ["CU현금"];

    // 지불금
    $use_amount = 0;
    // 온라인 충전, 환불금
    $on_charge_amount = 0;
    $on_refund_amount = 0;
    // 오프라인 충전, 환불금
    $off_charge_amount = 0;
    $off_refund_amount = 0;
    // CU 충전, 환불금
    $cu_charge_amount = 0;
    $cu_refund_amount = 0;

    foreach ($res as $item) {

        $data_date = "";
        if (isset($item->update_date)) {
            $data_date = getDateFormat_type3($item->update_date);
        } else {
            $data_date = getDateFormat_type3($item->insert_date);
        }

        if ($item->action == "입금") {
            // 입금
            if ($now_date == $data_date) {
                // 오늘 날짜
                if (in_array($item->method, $card_types)) {
                    // 오늘 카드 타입 충전
                    if ($item->type == "온라인") {
                        // 오늘 카드 타입 온라인 충전
                        $amount_obj = getAmount($conn, $item, false);
                        $obj = [
                            "idx"     => $amount_obj->idx,
                            "amount"  => $amount_obj->amount,
                            "type"    => "온라인",
                            "method"  => $amount_obj->method,
                            "imp_uid" => $amount_obj->imp_uid,
                        ];

                        array_push($amount_objs[6], $obj);

                        $amount_types[6] += $obj["amount"];
                        $on_charge_amount += $obj["amount"];

                        // 0 이상인 온라인 환불 가능 목록으로 빼내기
                        if ($obj["amount"] > 0) {
                            array_push($on_amount_objs, $obj);
                        }

                    } else {
                        // 오늘 카드 타입 오프라인 충전
                        $amount_obj = getAmount($conn, $item, true);
                        $obj = [
                            "idx"    => $amount_obj->idx,
                            "amount" => $amount_obj->amount,
                            "type"   => "오프라인",
                        ];
                        array_push($amount_objs[2], $obj);
                        $amount_types[2] += $obj["amount"];
                        $off_charge_amount += $obj["amount"];

                        if ($obj["amount"] > 0) {
                            array_push($off_amount_objs, $obj);
                        }
                    }
                } else if (in_array($item->method, $cash_types)) {
                    // 오늘 현금 타입 충전
                    if ($item->type == "온라인") {
                        // 오늘 현금 타입 온라인 충전
                        $amount_obj = getAmount($conn, $item, false);
                        $obj = [
                            "idx"     => $amount_obj->idx,
                            "amount"  => $amount_obj->amount,
                            "type"    => "온라인",
                            "method"  => $amount_obj->method,
                            "imp_uid" => $amount_obj->imp_uid,
                        ];
                        array_push($amount_objs[7], $obj);
                        $amount_types[7] += $obj["amount"];
                        $on_charge_amount += $obj["amount"];
                        // 0 이상인 온라인 환불 가능 목록으로 빼내기
                        if ($obj["amount"] > 0) {
                            array_push($on_amount_objs, $obj);
                        }
                    } else {
                        // 오늘 현금 타입 오프라인 충전
                        $amount_obj = getAmount($conn, $item, true);
                        $obj = [
                            "idx"    => $amount_obj->idx,
                            "amount" => $amount_obj->amount,
                            "type"   => "오프라인",

                        ];
                        array_push($amount_objs[3], $obj);

                        $amount_types[3] += $obj["amount"];
                        $off_charge_amount += $obj["amount"];

                        if ($obj["amount"] > 0) {
                            array_push($off_amount_objs, $obj);
                        }
                    }
                } else if (in_array($item->method, $cu_types)) {
                    // 오늘 CU 타입 충전
                    if ($item->method == "CU현금") {
                        // 오늘 CU현금 타입 충전
                        $amount_obj = getAmount($conn, $item, true);
                        $obj = [
                            "idx"    => $amount_obj->idx,
                            "amount" => $amount_obj->amount,
                            "type"   => "오프라인",

                        ];
                        array_push($amount_objs[9], $obj);

                        $amount_types[9] += $obj["amount"];
                        $cu_charge_amount += $obj["amount"];

                        if ($obj["amount"] > 0) {
                            array_push($off_amount_objs, $obj);
                        }
                    }
                }
            } else {
                // 이전 날짜
                if (in_array($item->method, $card_types)) {
                    // 이전 카드 타입 충전
                    if ($item->type == "온라인") {
                        // 이전 카드 타입 온라인 충전
                        $amount_obj = getAmount($conn, $item, false);

                        $obj = [
                            "idx"     => $amount_obj->idx,
                            "amount"  => $amount_obj->amount,
                            "type"    => "온라인",
                            "method"  => $amount_obj->method,
                            "imp_uid" => $amount_obj->imp_uid,
                        ];

                        array_push($amount_objs[4], $obj);
                        $amount_types[4] += $obj["amount"];
                        $on_charge_amount += $obj["amount"];

                        // 0 이상인 온라인 환불 가능 목록으로 빼내기
                        if ($obj["amount"] > 0) {
                            array_push($on_amount_objs, $obj);
                        }
                    } else {
                        // 이전 카드 타입 오프라인 충전
                        $amount_obj = getAmount($conn, $item, true);
                        $obj = [
                            "idx"    => $amount_obj->idx,
                            "amount" => $amount_obj->amount,
                            "type"   => "오프라인",
                        ];
                        array_push($amount_objs[0], $obj);

                        $amount_types[0] += $obj["amount"];
                        $off_charge_amount += $obj["amount"];

                        if ($obj["amount"] > 0) {
                            array_push($off_amount_objs, $obj);
                        }
                    }
                } else if (in_array($item->method, $cash_types)) {
                    // 이전 현금 타입 충전
                    if ($item->type == "온라인") {
                        // 이전 현금 타입 온라인 충전
                        $amount_obj = getAmount($conn, $item, false);
                        $obj = [
                            "idx"     => $amount_obj->idx,
                            "amount"  => $amount_obj->amount,
                            "type"    => "온라인",
                            "method"  => $amount_obj->method,
                            "imp_uid" => $amount_obj->imp_uid,
                        ];
                        array_push($amount_objs[5], $obj);
                        $amount_types[5] += $obj["amount"];
                        $on_charge_amount += $obj["amount"];
                        // 0 이상인 온라인 환불 가능 목록으로 빼내기
                        if ($obj["amount"] > 0) {
                            array_push($on_amount_objs, $obj);

                        }
                    } else {
                        // 이전 현금 타입 오프라인 충전 *

                        $amount_obj = getAmount($conn, $item, true);
                        $obj = [
                            "idx"    => $amount_obj->idx,
                            "amount" => $amount_obj->amount,
                            "type"   => "오프라인",
                        ];
                        // print_r($obj);
                        array_push($amount_objs[1], $obj);
                        $amount_types[1] += $obj["amount"];
                        $off_charge_amount += $obj["amount"];

                        if ($obj["amount"] > 0) {
                            array_push($off_amount_objs, $obj);
                        }
                    }
                } else if (in_array($item->method, $cu_types)) {
                    // 이전 CU 타입 충전
                    if ($item->method == "CU현금") {
                        // 이전 CU카드 타입 충전
                        $amount_obj = getAmount($conn, $item, true);
                        $obj = [
                            "idx"    => $amount_obj->idx,
                            "amount" => $amount_obj->amount,
                            "type"   => "오프라인",

                        ];
                        array_push($amount_objs[8], $obj);

                        $amount_types[8] += $obj["amount"];
                        $cu_charge_amount += $obj["amount"];

                        if ($obj["amount"] > 0) {
                            array_push($off_amount_objs, $obj);
                        }
                    }
                }
            }
        } else if ($item->action == "환불") {
            // 환불
            if ($item->amount > 0 && isset($item->market_seller_idx)) {
                // 상품 환불
                $use_amount += $item->amount;
            } else if ($item->amount > 0 && ! isset($item->market_seller_idx) && isset($item->order_info_idx)) {
                // CU 상품 환불
                $use_amount += $item->amount;
            } else {
                // 마일리지 환불
                if ($item->type == "온라인" && isset($item->cash_log_idx) && ! isset($item->market_seller_idx)) {
                    $on_refund_amount += $item->amount;
                } else if ($item->type == "오프라인" && isset($item->cash_log_idx) && ! isset($item->market_seller_idx) && ! in_array($item->method, $cu_types)) {
                    $off_refund_amount += $item->amount;
                } else if ($item->type == "오프라인" && isset($item->cash_log_idx) && ! isset($item->market_seller_idx) && in_array($item->method, $cu_types)) {
                    $cu_refund_amount += $item->amount;

                } else if ($item->amount <= 0) {
                    $use_amount += $item->amount;
                }
            }
        } else {
            // 결제
            $use_amount += $item->amount;
        }

    }

    $result_objs = [[], [], [], [], [], [], [], [], [], []];
    if ($use_amount < 0) {
        $use_amount = $use_amount * -1;
    }

    for ($i = 0; $i < count($amount_objs); $i++) {
        foreach ($amount_objs[$i] as $obj) {
            $use_amount = $use_amount - $obj["amount"];
            if ($use_amount < 0) {
                $obj["amount"] = $use_amount * -1;
                $use_amount = 0;
            } else {
                $obj["amount"] = 0;
            }
            array_push($result_objs[$i], $obj);
        }
    }

    return $result_objs;
}
