<?php

define("DB_TICKET","fp_ticket");
define("DB_PAYMENT_W","fp_payment_w");
define("DB_PAYMENT","fp_payment_r");

define("SWEET_KEY", "vGMQyTVMBZ8JznewwbfVtw");
define("S3_PATH_LMCHANNEL", "/contents/");
define("S3_PATH_USER_CONTENT", "/user_contents/");
define("S3_PATH_USER_PROFILE", "/user_profile/");
define("S3_PATH_USER_CONTENT_TEMP", "/temp/");
define("S3_PATH_LM_VIDEOS", "/lm_videos/");
define("S3_PATH_STORE_CONTENT", "/store_content/");
define("S3_PATH_CS_CONTENT", "/cs_contents/");
define("PATH_ON_GOODS_IMAGE", "/seller_on_goods/");
define("S3_OPEN_CONTENT_URL","https://s3.ap-northeast-2.amazonaws.com/fps3bucket/open_contents/");
define("S3_CLOSE_CONTENT_URL","https://s3.ap-northeast-2.amazonaws.com/fps3bucket/close_contents/");
define("S3_IMG_CONTENT_URL","https://s3.ap-northeast-2.amazonaws.com/fps3bucket/image_contents/");
define("S3_SELLER_CONTENT_URL","https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/");
define("S3_GOODS_CONTENT_URL","https://s3.ap-northeast-2.amazonaws.com/fps3bucket/goods_contents/");
define("S3_USER_CONTENT_URL","https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_contents/");
define("S3_USER_CONTENT_TEMP_URL","https://s3.ap-northeast-2.amazonaws.com/fps3bucket/temp/");
define("S3_USER_PROFILE_URL","https://s3.ap-northeast-2.amazonaws.com/fps3bucket/user_profile/");
define("S3_LMCHANNER","https://s3.ap-northeast-2.amazonaws.com/fps3bucket/contents/");
define("S3_LM_VIDEOS","https://s3.ap-northeast-2.amazonaws.com/fps3bucket/lm_videos/");
define("S3_ON_GOODS_CONTENT","https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_on_goods/");
define("S3_ON_STORE_CONTENT","https://s3.ap-northeast-2.amazonaws.com/fps3bucket/store_content/");
?>
