<?php

namespace App\Seller;

use App\Events\StartDelivery;
use App\Models\Goods;
use App\Models\Order as CustomerOrder;
use App\Models\OrderCustomerService;
use App\Models\OrderDelivery;
use App\Models\PaymentCancel;
use App\Scope\SellerOrderScope;
use App\Models\Sku;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    protected $connection = 'fp_payment_w';
    protected $table      = 'order_detail';
    protected $guarded    = [];

    protected static function boot()
    {
        static::addGlobalScope(new SellerOrderScope);
    }

    public function order()
    {
        return $this->belongsTo(CustomerOrder::class);
    }

    public function goods()
    {
        return $this->belongsTo(Goods::class, 'goods_id')->withTrashed();
    }

    public function sku()
    {
        return $this->hasOne(Sku::class, 'id', 'sku_id')->withTrashed();
    }

    public function delivery()
    {
        return $this->hasOne(OrderDelivery::class, 'detail_id');
    }

    public function paymentCancel()
    {
        return $this->hasMany(PaymentCancel::class, 'order_detail_id');
    }

    public function orderCustomerServices()
    {
        return $this->hasMany(OrderCustomerService::class, 'detail_id')->latest();
    }

    public function scopeSearch($query, $request)
    {
        if ($request->has('order_state')) {
            $query = self::whereState($request->order_state);
        }

        if ($request->has('field')) {
            switch ($request->field) {
                case 'shipping_name':
                    if ($request->filled('keyword')) {
                        $userIds = User::searchIdByColumn($request->keyword, 'shipping_name');
                        $query->whereHas('order', function (Builder $builder) use ($userIds) {
                            $builder->whereIn('user_id', $userIds);
                        });
                    }
                    break;

                case 'delivery_name':
                    $query->whereHas('delivery', function (Builder $builder) use ($request) {
                        $builder->where('name', 'LIKE', "%$request->keyword%");
                    });
                    break;

                case 'phone':
                    $query->whereHas('delivery', function (Builder $builder) use ($request) {
                        $builder->where('phone', 'LIKE', "%$request->keyword%");
                    });
                    break;

                case 'order_goods_number':
                    $query->where('order_goods_number', 'LIKE', "%$request->keyword%");
                    break;

                case 'order_number':
                    $query->whereHas('order', function (Builder $builder) use ($request) {
                        $builder->where('order_number', 'LIKE', "%$request->keyword%");
                    });
                    break;

                case 'goods_title':
                    $keywords = "%$request->keyword%";
                    $query->where('goods_title', 'like', $keywords)
                        ->orWhereHas('goods', function (Builder $builder) use ($keywords) {
                            $builder->where('title', 'like', $keywords);
                        });
                    break;

                case 'tracking_number':
                    $query->whereHas('delivery', function (Builder $builder) use ($request) {
                        $builder->where('tracking_number', 'LIKE', "%$request->keyword%");
                    });
                    break;
            }
        }

        if ($request->filled('date_type') && $request->filled('start_date') && $request->filled('end_date')) {
            switch ($request->date_type) {
                case 'ordered':
                    $query->whereHas('order', function (Builder $builder) use ($request) {
                        $builder->whereBetween('created_at', self::getPeriod($request));
                    });
                    break;

                case 'ready':
                    $query->whereHas('order.payment', function (Builder $builder) use ($request) {
                        $builder->whereState('결제완료')->whereBetween('updated_at', self::getPeriod($request));
                    });
                    break;

                case 'send':
                    $query->whereHas('delivery', function (Builder $builder) use ($request) {
                        $builder->whereState('배송중')->whereBetween('sent_at', self::getPeriod($request));
                    });
                    break;

                case 'complete':
                    $query->whereHas('delivery', function (Builder $builder) use ($request) {
                        $builder->whereState('배송완료')->whereBetween('delivered_at', self::getPeriod($request));
                    });
                    break;

                case 'cancel_request':
                    $query->whereHas('orderCustomerServices', function (Builder $builder) use ($request) {
                        $builder->whereBetween('created_at', self::getPeriod($request));
                    });
                    break;

                case 'confirmed':
                    $query->whereBetween('confirmed_at', self::getPeriod($request));
                    break;
            }
        }

        if (in_array($request->order_state, ['취소접수', '반품접수', '교환접수'])) {
            $query->with(['order', 'goods', 'sku', 'order.user']);
        } else if (in_array($request->order_state, ['취소완료', '반품완료', '교환완료'])) {
            $query->with(['order', 'goods', 'sku', 'order.user', 'orderCustomerServices', 'paymentCancel']);
        } else {
            $query->with(['order', 'goods', 'sku', 'order.user', 'delivery', 'order.payment']);
        }

        return $query;
    }

    public function scopeDeliveryState($query, $state)
    {
        if (! is_array($state)) {
            $state = [$state];
        }

        $query->whereIn('order_detail.state', $state)->whereHas('delivery', function (Builder $builder) use ($state) {
            $builder->whereNull('deleted_at');
        })->whereHas('order.payment', function (Builder $builder) {
            $builder->whereState('결제완료');
        });
    }

    /**
     * 배송 처리
     *
     * @param $courierCode
     * @param $invoiceNo
     * @throws Exception
     */
    public function send($courierCode, $invoiceNo)
    {
        $delayedCs = OrderCustomerService::where([
            ['detail_id', $this->id],
            ['state', '답변대기'],
        ])->whereIn('type', ['배송 전 변경', '구매취소', '배송전 취소/변경 문의']);

        if ($delayedCs->count() > 0) {
            if (($delayedCs->first())->type == '구매취소') {
                $message = '구매취소';
            } else {
                $message = 'CS관리';
            }

            return $message;
        } else if ($this->state !== '취소완료') {
            $delivery = OrderDelivery::where('detail_id', $this->id)->first();

            if ($delivery && strlen($invoiceNo) > 5 && $delivery->state == '배송준비') {
                $delivery->update([
                    'couirer_code'    => $courierCode,
                    'tracking_number' => trim(str_replace('-', '', $invoiceNo)),
                    'state'           => '배송중',
                    'sent_at'         => now(),
                ]);

                $this->update(['state' => '배송중',]);

                event(new StartDelivery($delivery->orderDetail));
            }
        }
    }

    public static function getTotalPurchased($request)
    {
        $query = self::where('state', '구매확정');

        if ($request->filled('start_date', 'end_date')) {
            $query->whereRaw("left(confirmed_at, 7) between '{$request->start_date}' and '{$request->end_date}'");
        } else {
            $query->whereNotNull('confirmed_at');
        }

        return $query;
    }

    public static function getPurchasedGoods($request)
    {
        return self::getTotalPurchased($request)
            ->selectRaw('*, sum(quantity) as total_quantity, sum(total_price) as total_sales_amount, @rank := @rank + 1 as rank')
            ->whereHas('goods', function (Builder $query) use ($request) {
                $query->where('title', 'LIKE', '%' . "$request->keyword%");
            });
    }

    public static function purchaseAmountByOptions($request, $goodsId)
    {
        return self::getTotalPurchased($request)
            ->where('goods_id', $goodsId)
            ->groupBy('sku_id')
            ->selectRaw('*, sum(quantity) as sum')
            ->with('sku')
            ->get();
    }

    public function isLastOrder()
    {
        $count = self::where([
            ['order_id', $this->order_id],
            ['seller_id', $this->seller_id],
        ])->whereNotIn('state', ['취소완료', '반품완료'])->count();

        return $count == 1;
    }

    private static function getPeriod($request)
    {
        return [
            ($request->has('start_date')) ? $request->start_date . ' 00:00:00' : now()->subYear(),
            ($request->has('end_date')) ? $request->end_date . ' 23:59:59' : now(),
        ];
    }

}
