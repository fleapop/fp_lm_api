<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class StockSoldOut
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $seller;
    public $sku;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($seller, $sku)
    {
        $this->seller = $seller;
        $this->sku = $sku;
    }
}
