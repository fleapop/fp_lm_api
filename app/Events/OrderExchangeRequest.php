<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderExchangeRequest
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $phoneNumber;
    public $sellerName;
    public $userName;
    public $orderNo;
    public $goodsTitle;
    public $reason;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($phoneNumber, $sellerName, $userName, $orderNo, $goodsTitle, $reason)
    {
        $this->phoneNumber = $phoneNumber;
        $this->sellerName = $sellerName;
        $this->userName = $userName;
        $this->orderNo = $orderNo;
        $this->goodsTitle = $goodsTitle;
        $this->reason = $reason;
    }
}
