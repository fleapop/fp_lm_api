<?php

namespace App\Events;

use App\Models\Order;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderCompleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $phoneNumber;
    public $username;
    public $orderNo;
    public $itemName;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->phoneNumber = (strlen($order->user->shipping_phone) < 2) ? $order->delivery[0]->phone : $order->user->shipping_phone;
        $this->username = (strlen($order->user->shipping_name) < 2) ? $order->delivery[0]->name : $order->user->shipping_name;
        $this->orderNo = $order->order_number;
        $this->itemName = $order->getItemName();
    }
}
