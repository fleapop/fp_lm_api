<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class OrderCancelRequest
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $phoneNumber;
    public $sellerName;
    public $userName;
    public $orderNo;
    public $goodsTitle;
    public $reason;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($phoneNumber, $sellerName, $userName, $orderNo, $goodsTitle, $reason)
    {
        $this->phoneNumber = $phoneNumber;
        $this->sellerName = $sellerName;
        $this->userName = $userName;
        $this->orderNo = $orderNo;
        $this->goodsTitle = $goodsTitle;
        $this->reason = $reason;
    }
}
