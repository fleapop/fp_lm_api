<?php

namespace App\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StartDelivery
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $phoneNumber;
    public $sellerName;
    public $userName;
    public $orderNumber;
    public $goodsTitle;
    public $courierName;
    public $invoiceNumber;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($orderDetail)
    {
        $this->phoneNumber = (strlen($orderDetail->order->user->shipping_phone) < 2) ? $orderDetail->delivery->phone : $orderDetail->order->user->shipping_phone;
        $this->userName = (strlen($orderDetail->order->user->shipping_name) < 2) ? $orderDetail->delivery->name : $orderDetail->order->user->shipping_name;
        $this->sellerName = $orderDetail->goods->seller->name;
        $this->orderNumber = $orderDetail->order->order_number;
        $this->goodsTitle = $orderDetail->goods->title;
        $this->courierName = getCourierCodeToName($orderDetail->delivery->couirer_code);
        $this->invoiceNumber = $orderDetail->delivery->tracking_number;
    }
}
