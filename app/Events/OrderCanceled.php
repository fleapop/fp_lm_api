<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OrderCanceled
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $phoneNumber;
    public $userName;
    public $orderNo;
    public $goodsTitle;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($phoneNumber, $userName, $orderNo, $goodsTitle)
    {
        $this->phoneNumber = $phoneNumber;
        $this->userName = $userName;
        $this->orderNo = $orderNo;
        $this->goodsTitle = $goodsTitle;
    }
}
