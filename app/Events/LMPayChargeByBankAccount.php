<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

/**
 * 러마페이 가상계좌 충전 이벤트
 *
 * Class LMPayChargeByBankAccount
 * @package App\Events
 */
class LMPayChargeByBankAccount
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $phoneNumber;
    public $userName;
    public $bank;
    public $accountNumber;
    public $amount;
    public $endDate;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $user = User::find($data->user_idx);
        $this->phoneNumber = $user->shipping_phone;
        $this->userName = $data->sender;
        $this->bank = $data->vbank_name;
        $this->accountNumber = $data->vbank_num;
        $this->amount = $data->amount;
        $this->endDate = $data->vbank_date;
    }
}
