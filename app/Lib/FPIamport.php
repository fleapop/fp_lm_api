<?php

namespace App\Lib;

use Alliv\Iamport\ApiClient;
use Alliv\Iamport\Iamport;
use App\Http\Controllers\OrderController;
use App\Models\LMPay;
use App\Models\OrderPayment;
use DB;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class FPIamport extends Iamport
{
    /*
    거래건 조회
    public function getImpObject($imp_uid)

    가상계좌 callback
    callbackReceiveVbank

    환불처리
    -가상계좌 환불인 경우
    $bankData['bank','account','holder']
    cancelPayment($impUid, $amount, $reason, $bankData = NULL)

    계좌검증
    $bankData['bank','account','holder']
    verificationBankAccount($bankData)
    */

    private $apiKey;
    private $apiSecret;
    private $token  = NULL;
    private $client;
    private $header = NULL;

    function __construct()
    {
        $this->apiKey = env('IAMPORT_REST_API_KEY', '');
        $this->apiSecret = env('IAMPORT_REST_API_SECRET', '');
        $this->token = $this->getToken();
        $this->client = new ApiClient([
            "apiKey"    => $this->apiKey,
            "apiSecret" => $this->apiSecret,
        ]);

        $this->header = [
            'Content-Type' => 'application/json',
            'Accept'       => 'application/json',
        ];
    }

    public function home()
    {
        $fpImport = new FPIamport();
        return $fpImport->cancelPayment('imp_612764923692', 19900, '고객환불요청', ["bank" => "농협", "holder" => "이신우", "account" => "24812397630"]);
    }

    public function getImpObject($imp_uid)
    {
        $iamport = new Iamport([
            'apiKey'    => $this->apiKey,
            'apiSecret' => $this->apiSecret,
        ]);

        $impData = $iamport->getPayment($imp_uid);
        return $impData;
    }

    public function callbackReceiveVbank(Request $request)
    {
        if (! $request->filled(['imp_uid', 'merchant_uid', 'status'])) {
            return response()->json("Required Params", 422);
        }

        if ($request->input('status') === 'paid') {
            $iamport = new Iamport([
                'apiKey'    => $this->apiKey,
                'apiSecret' => $this->apiSecret,
            ]);

            $impData = $iamport->getPayment($request->input('imp_uid'));
            if ($impData->success) {

                $lmPay = LMPay::where('imp_uid', $request->input('imp_uid'))->first();
                $orderPay = OrderPayment::where('imp_uid', $request->input('imp_uid'))->first();

                if (isset($lmPay) && ! isset($orderPay)) {
                    // lmpay
                    if ($impData->data->amount === $lmPay->amount) {
                        $lmPay->state = "결제됨";
                        $lmPay->save();
                    }
                } else {
                    // orderpay
                    // TODO: 테스트 필요
                    if ($impData->data->amount === $orderPay->amount) {
                        $params = new Request();
                        $params->setMethod('POST');
                        $params->request->add([
                            'order_id' => $orderPay->order_id,
                            'imp_uid'  => $orderPay->imp_uid,
                        ]);

                        $orderCon = new OrderController();
                        $orderCon->getOrderPaymentResultSubmit($params);
                    }

                }
            }

        }

        return response()->json("Receive", 200);
    }

    public function cancelPayment($impUid, $amount, $reason, $bankData = NULL)
    {
        try {
            if (isset($bankData)) {
                $this->client->authRequest('POST', '/payments/cancel', [
                    'imp_uid'        => $impUid,
                    'amount'         => $amount,
                    'reason'         => $reason,
                    'refund_holder'  => $bankData['holder'],
                    'refund_bank'    => $this->getKCPBankCode($bankData['bank']),
                    'refund_account' => $bankData['account'],
                ]);
            } else {
                $data = [
                    'imp_uid' => $impUid,
                    'amount'  => $amount,
                    'reason'  => $reason,
                ];
                $this->client->authRequest('POST', '/payments/cancel', $data);
            }
            Log::info('[환불완료]' . $impUid);
            return response()->json("", 200);
        } catch (Exception $e) {
            $message = $e->getMessage();
            Log::alert('[환불실패]' . $message);
            return response()->json($message, 422);
        }
    }

    public function verificationBankAccount($bankData)
    {
        try {
            if (! isset($bankData['bank']) || ! isset($bankData['account']) || ! isset($bankData['holder'])) {
                return response()->json("필수값이 누락되었습니다.", 401);
            } else {
                // TODO : url encoding 문제 해결하면 GuzzleHttp로 변경할것.
                $bankData['bank'] = $this->getBankCode($bankData['bank']);
                $url = "https://api.iamport.kr/vbanks/holder?bank_code={$bankData['bank']}&bank_num={$bankData['account']}&_token={$this->token}";

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
                curl_setopt($ch, CURLOPT_TIMEOUT, 60);

                curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-type: application/x-www-form-urlencoded"]);

                $result = curl_exec($ch);
                if (curl_errno($ch) !== 0) {
                    $hour = (int)date("H");

                    if (($hour >= 00 && $hour <= 02) || ($hour >= 23 && $hour <= 24)) {
                        $msg = "23:00 ~ 02:00 사이에는\n은행 시스템 점검시간으로 계좌 검증에 실패하였습니다.\n은행점검시간 이후에 다시 결제해주세요.\n감사합니다.";
                        return response()->json($msg, 499);
                    } else {
                        return response()->json("계좌검증에 실패했습니다.", 499);
                    }

                }

                curl_close($ch);
                $response = json_decode($result);

                if ($response->code < 0) {

                    $hour = (int)date("H");

                    if (($hour >= 00 && $hour <= 02) || ($hour >= 23 && $hour <= 24)) {
                        $msg = "23:00 ~ 02:00 사이에는\n은행 시스템 점검시간으로 계좌 검증에 실패하였습니다.\n은행점검시간 이후에 다시 결제해주세요.\n감사합니다.";
                        return response()->json($msg, 499);
                    } else {
                        return response()->json("계좌검증에 실패했습니다.", 499);
                    }
                } else {
                    if (! strcmp($response->response->bank_holder, $bankData["holder"]) !== false) {
                        return response()->json("", 200);
                    } else {
                        $hour = (int)date("H");

                        if (($hour >= 00 && $hour <= 02) || ($hour >= 23 && $hour <= 24)) {
                            $msg = "23:00 ~ 02:00 사이에는\n은행 시스템 점검시간으로 계좌 검증에 실패하였습니다.\n은행점검시간 이후에 다시 결제해주세요.\n감사합니다.";
                            return response()->json($msg, 499);
                        } else {
                            return response()->json("계좌검증에 실패했습니다.", 499);
                        }
                    }
                }
            }
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $hour = (int)date("H");

            if (($hour >= 00 && $hour <= 02) || ($hour >= 23 && $hour <= 24)) {
                $msg = "23:00 ~ 02:00 사이에는\n은행 시스템 점검시간으로 계좌 검증에 실패하였습니다.\n은행점검시간 이후에 다시 결제해주세요.\n감사합니다.";
                return response()->json($msg, 499);
            } else {
                return response()->json("계좌검증에 실패했습니다.", 499);
            }
        }
    }

    private function getToken()
    {

        $url = "https://api.iamport.kr/users/getToken";
        $client = new Client();
        $response = $client->request('POST', $url, [
            'form_params' => [
                "imp_key"    => $this->apiKey,
                "imp_secret" => $this->apiSecret,
            ],
        ]);

        $response = $response->getBody()->getContents();
        $response = json_decode($response);
        if ($response->code == 0) {
            return $response->response->access_token;
        } else {
            return false;
        }
    }

    private function getKCPBankCode($bank)
    {
        $bank_code = [
            "경남은행"    => "BK39",
            "국민은행"    => "BK04",
            "기업은행"    => "BK03",
            "농협"      => "BK11",
            "대구은행"    => "BK31",
            "부산은행"    => "BK32",
            "산업은행"    => "BK02",
            "새마을금고"   => "BK45",
            "수협"      => "BK07",
            "신한은행"    => "BK88",
            "신협"      => "BK48",
            "외환은행"    => "BK81",
            "우리은행"    => "BK20",
            "우체국"     => "BK71",
            "전북은행"    => "BK37",
            "SC제일은행"  => "BK23",
            "카카오뱅크"   => "BK90",
            "케이뱅크"    => "BK89",
            "KEB하나은행" => "BK81",
        ];

        if (isset($bank_code[$bank])) {
            return $bank_code[$bank];
        } else {
            return false;
        }
    }

    private function getBankCode($bank)
    {
        $bank_code = [
            "경남은행"    => "039",
            "국민은행"    => "004",
            "기업은행"    => "003",
            "농협"      => "011",
            "대구은행"    => "031",
            "부산은행"    => "032",
            "산업은행"    => "002",
            "새마을금고"   => "045",
            "수협"      => "007",
            "신한은행"    => "088",
            "신협"      => "048",
            "외환은행"    => "081",
            "우리은행"    => "020",
            "우체국"     => "071",
            "전북은행"    => "037",
            "SC제일은행"  => "023",
            "카카오뱅크"   => "090",
            "케이뱅크"    => "089",
            "KEB하나은행" => "081",
        ];

        if (isset($bank_code[$bank])) {
            return $bank_code[$bank];
        } else {
            return false;
        }
    }
}
