<?php


namespace App\Lmpay;

use App\Models\LMPay;
use DB;

class LMPayController
{
    protected $method = [
        'card'    => '신용카드',
        'trans'   => '계좌이체',
        'vbank'   => '가상계좌',
        'payco'   => '페이코',
        'samsung' => '삼성페이',
    ];

    public function getLogs($user_id, $min, $max)
    {
        $pconn = DB::connection("fp_payment_r");

        $sql = "SELECT * FROM cash_log WHERE user_idx = {$user_id} AND delete_date IS NULL ORDER BY idx DESC LIMIT {$min}, {$max}";
        $res = $pconn->select($sql);

        $datas = [];
        foreach ($res as $item) {
            $data = [
                "idx"         => $item->idx,
                "type"        => $item->type,
                "method"      => $item->method,
                "action"      => $item->action,
                "amount"      => $item->amount,
                "state"       => $item->state,
                "insert_date" => $item->insert_date,
            ];
            $datas[] = $data;
        }

        $sql = "SELECT COUNT(*) AS cnt FROM cash_log WHERE user_idx = {$user_id} AND delete_date IS NULL";
        $count = $pconn->select($sql);
        $response = [
            "total"     => $count[0]->cnt,
            "last_page" => ceil($count[0]->cnt / $max),
            "data"      => $datas,
        ];

        return $response;
    }

    /**
     * 러마페이 잔액
     *
     * @param $user_id
     */
    public function getBalance($user_id)
    {
        $pconn = DB::connection('fp_payment_w');
        $sql = "SELECT IFNULL(SUM(amount), 0) as cash FROM cash_log WHERE user_idx = {$user_id} AND delete_date IS NULL AND (`state` = '결제됨' OR (`action` = '환불' AND `state` = '결제대기') OR (action = '환불' AND state = '기타')) AND `action` != '취소'";
        $balance = $pconn->select($sql);
        return $balance[0]->cash;
    }

    /**
     * 러마페이 지불
     *
     * @param $user_id
     * @param $data
     */
    public function paid($user_id, $data)
    {
        $balance = $this->getBalance($user_id);
        if ($balance < abs($data['amount'])) {
            return [
                "success" => false,
                "msg"     => "잔액이 부족합니다.",
            ];
        }
        return LMPay::create($data);
    }

    /**
     * 러마페이 취소
     *
     * @param $cash_log_id
     */
    public function cancel($cash_log_id)
    {
        $log = LMPay::find($cash_log_id);
        if (! isset($log)) {
            return [
                "success" => false,
                "msg"     => "해당 결제 정보가 없습니다.",
            ];
        } else {
            $log->state = "기타";
            $log->error_msg = "취소 요청";
            $log->save();
            return [
                "success" => true,
                "msg"     => "",
            ];
        }
    }

    protected function success($data)
    {
        return [
            'success' => true,
            'code'    => 200,
            'message' => '',
            'data'    => $data,
        ];
    }

    protected function fail($code, $message)
    {
        return [
            'success' => false,
            'code'    => $code,
            'message' => $message,
        ];
    }
}
