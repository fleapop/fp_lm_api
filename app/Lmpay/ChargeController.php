<?php

namespace App\Lmpay;

use App\Models\CashierApp;
use App\Lib\FPIamport;
use App\Models\LMPay;
use DB;
use Illuminate\Support\Facades\Log;

class ChargeController extends LMPayController
{
    /**
     * 온라인 충전
     *
     * @param $imp_uid
     */
    public function online($user_id, $imp_uid)
    {
        if (isset($imp_uid) && isset($user_id)) {
            $imp = new FPIamport();
            $impData = $imp->getImpObject($imp_uid);

            if ($impData->success) {
                $data = [
                    'user_idx'     => $user_id,
                    'type'         => '온라인',
                    'sender'       => $impData->data->buyer_name,
                    'method'       => $this->method[$impData->data->pay_method],
                    'action'       => '입금',
                    'state'        => '결제대기',
                    'amount'       => $impData->data->amount,
                    'imp_uid'      => $imp_uid,
                    'merchant_uid' => $impData->data->merchant_uid,
                    'paid_amount'  => $impData->data->amount,
                    'appy_num'     => $impData->data->apply_num,
                    'vbank_num'    => $impData->data->vbank_num,
                    'vbank_name'   => $impData->data->vbank_name,
                    'vbank_date'   => $impData->data->vbank_date,
                    'receipt_url'  => $impData->data->receipt_url,
                ];

                switch ($impData->data->status) {
                    case 'paid' :
                        $data['state'] = '결제됨';
                        break;
                    case 'ready' :
                        $data['state'] = '결제대기';
                        break;
                    default :
                        $data['state'] = '기타';
                        break;
                }

                $payLog = LMPay::where('imp_uid', $imp_uid)->first();
                if (isset($payLog)) {
                    if ($payLog->state == '결제대기' && $impData->data->status == 'paid') {
                        $payLog->state = '결제됨';
                        $payLog->appy_num = $data['appy_num'];
                        $payLog->save();
                    }

                    $data = "{$payLog->idx},{$payLog->state}";
                    return $this->success($data);
                } else {
                    $res = LMPay::create($data);
                    $data = "{$res->idx},{$res->state}";
                    return $this->success($data);
                }
            } else {
                return $this->fail(422, '올바른 결제값이 아닙니다.');
            }
        } else {
            return $this->fail(422, '필수값 누락되었습니다.');
        }
    }

    /**
     * 오프라인 충전
     *
     * @param $user_id , $casher_id, $amount
     */
    public function offline($user_id, $cashier_id, $amount)
    {
        if (! isset($user_id) || ! isset($cashier_id) || ! isset($amount)) {
            return $this->fail(422, '필수값 누락되었습니다.');
        }

        $cashier = CashierApp::find($cashier_id);
        if ($cashier->enable == 'N') {
            return $this->fail(422, '비활성화 된 캐셔입니다.');
        }


        $data = [
            'user_idx' => $user_id,
            'type'     => '오프라인',
            'casher'   => $cashier->idx,
            'method'   => $cashier->type,
            'amount'   => $amount,
            'state'    => '결제됨',
        ];

        LMPay::create($data);
        return $this->success();

    }

    /**
     * CU 충전
     *
     * @param $user_id , $order(요청번호), $amount
     */
    public function cu($user_id, $order, $amount)
    {
        if (! isset($user_id) || ! isset($order) || ! isset($amount)) {
            return $this->fail(422, '필수값 누락되었습니다.');
        }

        $payLog = LMPay::where([['method', 'CU현금'], ['appy_num', $order]])->first();
        if (isset($payLog)) {
            return $this->fail(422, '이미 존재하는 충전건 입니다.');
        }

        $data = [
            'user_idx' => $user_id,
            'type'     => '오프라인',
            'method'   => 'CU현금',
            'amount'   => $amount,
            'state'    => '결제대기',
            'appy_num' => $order,
        ];


        return LMPay::create($data);
    }


    /**
     *  충전 객체 가져오기
     *
     * @param $order (요청번호)
     */
    public function getLog($id)
    {
        if (! isset($id)) {
            return NULL;
        }

        $payLog = LMPay::find($id);
        if (! isset($payLog)) {
            return NULL;
        }

        return $payLog;
    }

    /**
     * 충전 취소
     *
     * @param $cash_log_id , $msg
     */
    public function cancel($cash_log_idx, $msg = "")
    {
        if (! isset($cash_log_idx)) {
            return $this->fail(422, '필수값 누락되었습니다.');
        }
        $payLog = LMPay::find($cash_log_idx);

        if ($payLog->state != '결제됨') {
            return $this->fail(422, '취소는 결제가 완료된 결제건에 해당합니다.');
        }

        $balance = $this->getBalance($payLog->user_idx);
        $afterBalance = $balance - $payLog->amount;

        if ($afterBalance < 0) {
            return $this->fail(422, '취소후 금액이 0원 이하가 될 수 없습니다.');
        }

        $payLog->state = '기타';
        $payLog->error_msg = $msg;
        $payLog->save();
        return $this->success(NULL);
    }

}
