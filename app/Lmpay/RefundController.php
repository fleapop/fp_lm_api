<?php


namespace App\Lmpay;

use App\Models\CashierApp;
use App\CU\RefundController as CURefundController;
use App\Models\CUCharge;
use App\Models\CURefund;
use App\Lib\FPIamport;
use App\Models\LMPay;
use App\Models\PaymentLog;
use Carbon\Carbon;
use GuzzleHttp\Client;


class RefundController extends LMPayController
{
    protected $bank = NULL;
    protected $requiredBank = false;


  /**
   * 환불금액 submit 조회
   *
   * @param $user_id , $only_cashier(캐셔), $return_object
   */
  public function amountTemp($user_id, $only_cashier = NULL, $return_object = false)
  {
    $amounts = [
      'balance' => 0,
      'amount' => 0,
      'card' => 0,
    ];

    if (!isset($user_id)) {
      return $this->fail(422, '필수값 누락되었습니다.');
    }

    $refundDatas = [];
    $balance = $this->getBalance($user_id);

    $bal = $balance;

    $amounts['balance'] = (int)$balance;
    if($return_object) {
      $charges = LMPay::where([['user_idx', $user_id], ['action', '입금'], ['state', '결제됨']])->whereNull('delete_date')->whereIn('method', ['현금','신용카드','계좌이체','가상계좌','페이코','삼성페이','무통장','CU현금', "DBIN"])->orderBy('idx', 'DESC')->get();
    } else {
      $charges = LMPay::where([['user_idx', $user_id], ['action', '입금'], ['state', '결제됨']])->whereNull('delete_date')->orderBy('idx', 'DESC')->get();
    }
//      dd($balance);

//      dd($charges);

    $refundDatas = [];
    $requiredBank = false;
    $cashier = NULL;
    if(isset($only_cashier)) {

      $cashier = CashierApp::find($only_cashier);
    }

    foreach ($charges as $charge) {

      $availableAmount = $charge->leaveAmount();

      if ($balance > 0) {
        $bal = $balance - $availableAmount;

        if ($bal > 0) {
          if(isset($only_cashier)) {
            $now = Carbon::now()->toDateString();
            $chargeDate = Carbon::createFromFormat("Y-m-d H:i:s", $charge->insert_date)->toDateString();

            if($now == $chargeDate && $cashier->type == $charge->method) {
              array_push($refundDatas, $charge);
            }
          } else {
            array_push($refundDatas, $charge);
          }
          $charge->amount = $charge->leaveAmount();


          $balance = $bal;
        } else {

          $charge->amount = $balance;
          if(isset($only_cashier)) {
            $now = Carbon::now()->toDateString();
            $chargeDate = Carbon::createFromFormat("Y-m-d H:i:s", $charge->insert_date)->toDateString();

            if($now == $chargeDate && $cashier->type == $charge->method) {
              array_push($refundDatas, $charge);
            }
          } else {
            array_push($refundDatas, $charge);
          }
          $balance = 0;
        }
      }
    }




    foreach ($refundDatas as $charge) {

      if (isset($only_cashier)) {
        if ($charge->type == '오프라인' && $charge->method == "신용카드") {
          $amounts['amount'] += $charge->amount;
        } else if ($charge->type == '오프라인' && $charge->method == "현금") {
          $amounts['amount'] += $charge->amount;
        }
      } else {
        if ($charge->type == '온라인' && in_array($charge->method, ['가상계좌', '계좌이체'])) {
          $amounts['amount'] += $charge->amount;
          $requiredBank = true;
        } else if ($charge->type == "오프라인" && in_array($charge->method, ["현금", "신용카드", "CU현금", "DBIN"])) {
          $requiredBank = true;
          $amounts['amount'] += $charge->amount;
        } else if ($charge->type == '온라인' && in_array($charge->method, ['신용카드', '페이코', '삼성페이'])) {
          $amounts['amount'] += $charge->amount;
        }
      }
    }


    if ($return_object) {
      return [
        'refunds' => $refundDatas,
        'required_bank' => $requiredBank,
      ];


    } else {

      return $this->success($amounts);
    }
  }

    /**
     * 환불금액 조회
     *
     * @param $user_id , $only_cashier(캐셔), $return_object
     */
    public function amount($user_id, $only_cashier = NULL, $return_object = false)
    {
        $amounts = [
            'balance' => 0,
            'trans' => 0,
            'card' => 0,
        ];

        if (!isset($user_id)) {
            return $this->fail(422, '필수값 누락되었습니다.');
        }

        $refundDatas = [];
//        if ($return_object) {
//          $balance = $this->amountTemp($user_id, $only_cashier, false)["data"]["amount"];
//        } else {
//          $balance = $this->getBalance($user_id);
//        }
      $balance = $this->getBalance($user_id);


        $bal = $balance;

        $amounts['balance'] = (int)$balance;
//        if($return_object) {
//          $charges = LMPay::where([['user_idx', $user_id], ['action', '입금'], ['state', '결제됨']])->whereNull('delete_date')->whereIn('method', ['현금','신용카드','계좌이체','가상계좌','페이코','삼성페이','무통장','CU현금', "DBIN"])->orderBy('idx', 'DESC')->get();
//        } else {
//          $charges = LMPay::where([['user_idx', $user_id], ['action', '입금'], ['state', '결제됨'], ['idx','<','336536']])->whereNull('delete_date')->orderBy('idx', 'DESC')->get();
//        }
      $charges = LMPay::where([['user_idx', $user_id], ['action', '입금'], ['state', '결제됨']])->whereNull('delete_date')->orderBy('idx', 'DESC')->get();
//      dd($balance);

//      dd($charges);

        $refundDatas = [];
        $requiredBank = false;
        $cashier = NULL;
        if(isset($only_cashier)) {

            $cashier = CashierApp::find($only_cashier);
        }

        foreach ($charges as $charge) {

            $availableAmount = $charge->leaveAmount();

            if ($balance > 0) {
                $bal = $balance - $availableAmount;

                if ($bal > 0) {
                    if(isset($only_cashier)) {
                        $now = Carbon::now()->toDateString();
                        $chargeDate = Carbon::createFromFormat("Y-m-d H:i:s", $charge->insert_date)->toDateString();

                        if($now == $chargeDate && $cashier->type == $charge->method) {
                            array_push($refundDatas, $charge);
                        }
                    } else {
                        array_push($refundDatas, $charge);
                    }
                    $charge->amount = $charge->leaveAmount();


                    $balance = $bal;
                } else {

                    $charge->amount = $balance;
                    if(isset($only_cashier)) {
                        $now = Carbon::now()->toDateString();
                        $chargeDate = Carbon::createFromFormat("Y-m-d H:i:s", $charge->insert_date)->toDateString();

                        if($now == $chargeDate && $cashier->type == $charge->method) {
                            array_push($refundDatas, $charge);
                        }
                    } else {
                        array_push($refundDatas, $charge);
                    }
                    $balance = 0;
                }
            }
        }




        foreach ($refundDatas as $charge) {

            if (isset($only_cashier)) {
                if ($charge->type == '오프라인' && $charge->method == "신용카드") {
                    $amounts['card'] += $charge->amount;
                } else if ($charge->type == '오프라인' && $charge->method == "현금") {
                    $amounts['trans'] += $charge->amount;
                }
            } else {
                if ($charge->type == '온라인' && in_array($charge->method, ['가상계좌', '계좌이체'])) {
                    $amounts['trans'] += $charge->amount;
                    $requiredBank = true;
                } else if ($charge->type == "오프라인" && in_array($charge->method, ["현금", "신용카드", "CU현금", "DBIN"])) {
                    $requiredBank = true;
                    $amounts['trans'] += $charge->amount;
                } else if ($charge->type == '온라인' && in_array($charge->method, ['신용카드', '페이코', '삼성페이'])) {
                    $amounts['card'] += $charge->amount;
                }
            }
        }


        if ($return_object) {
            return [
                'refunds' => $refundDatas,
                'required_bank' => $requiredBank,
            ];


        } else {

            return $this->success($amounts);
        }
    }

    /**
     * 환불
     *
     * @param $user_id , $only_cashier(캐셔), $bank_data ['bank','holder','account']
     */
    public function refund($user_id, $only_cashier = NULL, $bank_data = NULL)
    {

        if (!isset($user_id)) {
            return $this->fail(422, '필수값 누락되었습니다.');
        }
        $refund = $this->amount($user_id, $only_cashier, true);




        if ($refund['required_bank'] && !isset($bank_data)) {
            return $this->fail(422, '환불 진행을 위해 계좌정보가 필요합니다.');
        } else if ($refund['required_bank'] && isset($bank_data)) {
            $this->required_bank = $refund['required_bank'];
            $this->bank = $bank_data;

            // 은행 검증
            $imp = new FPIamport();
            $bankResponse = $imp->verificationBankAccount($this->bank);
            if ($bankResponse->getStatusCode() != 200) {
                $message = json_decode($bankResponse->getContent());
                return $this->fail(422, $message);
            }
        }

        $refunds = $refund['refunds'];

//      dd($refunds);
        foreach ($refunds as $refund) {

            if (!isset($only_cashier)) {
                if ($refund->type == '온라인' && !in_array($refund->method, ["ITEMBUY", "REVIEW"])) {
                    $this->online($refund);
                } else {
                    if ($refund->method == 'CU현금') {
                        $this->cu($refund);
                    } else if (in_array($refund->method, ["현금", "신용카드", "DBIN"])) {
                        $this->offline($refund, $only_cashier);
                    }
                }
            } else {
                $cashier = CashierApp::find($only_cashier);
                if ($cashier->enable == 'N') {
                    return $this->fail(422, '비활성화 된 캐셔입니다.');
                }

                if ($cashier->type == $refund->method) {
                    $this->offline($refund, $only_cashier);
                } else if ($cashier->type == $refund->method) {
                    $this->offline($refund, $only_cashier);
                }
            }
        }

        return $this->success(NULL);
    }

    /**
     * 온라인 환불
     *
     * @param $obj (cash_log)
     */
    private function online($obj)
    {
        if ($obj->amount <= 0) {
            return;
        }

        $imp = new FPIamport();
        if (in_array($obj->method, ['가상계좌', '계좌이체'])) {
            $response = $imp->cancelPayment($obj->imp_uid, abs($obj->amount), '고객 취소요청', $this->bank);
        } else {
            $response = $imp->cancelPayment($obj->imp_uid, abs($obj->amount), '고객 취소요청');
        }
        $impUid = isset($obj->imp_uid) ? $obj->imp_uid : '';
        PaymentLog::setLog($impUid, 'REF');


        $data = [
            'user_idx' => $obj->user_idx,
            'cash_log_idx' => $obj->idx,
            'type' => '온라인',
            'method' => '마일리지',
            'action' => '환불',
            'state' => '결제됨',
            'amount' => $obj->amount * -1,
            'refund_imp_uid' => $obj->imp_uid,
        ];

        if ($response->getStatusCode() == 200) {
            LMPay::create($data);
        } else {
            $response = $imp->cancelPayment($obj->imp_uid, abs($obj->amount), '고객 취소요청');
            $data['state'] = '기타';
            $data['error_msg'] = json_decode($response->getContent());
            LMPay::create($data);
        }
    }

    /**
     * 오프라인 환불
     *
     * @param $obj (cash_log), $only_cashier (캐셔)
     */
    private function offline($obj, $only_cashier = NULL, $is_forced_cu = false)
    {
        if ($obj->amount <= 0) {
            return;
        }

        if (isset($only_cashier)) {
            // 캐셔로 환불하는 경우.
            $cashier = CashierApp::find($only_cashier);
            $data = [
                "user_idx" => $obj->user_idx,
                "cash_log_idx" => $obj->idx,
                "type" => "오프라인",
                "casher" => $cashier->idx,
                "method" => "마일리지",
                "action" => "환불",
                "amount" => $obj->amount * -1,
                "state" => "결제됨",
            ];

            LMPay::create($data);
        } else {
            // 온라인으로 환불신청하는 경우.
            $data = [
                "user_idx" => $obj->user_idx,
                "cash_log_idx" => $obj->idx,
                "type" => "오프라인",
                "method" => "마일리지",
                "action" => "환불",
                "amount" => $obj->amount * -1,
                "state" => "결제대기",
                "vbank_num" => $this->bank['account'],
                'vbank_name' => $this->bank['bank'],
            ];

            if ($is_forced_cu) {
                $data['cu_com_refund'] = 'Y';
            }

            LMPay::create($data);
        }
    }

    /**
     * CU 환불
     *
     * @param $obj (cash_log), $is_forced
     */
    public function cu($obj)
    {
        if ($obj->amount <= 0) {
            return;
        }

        $chargeCnt = CUCharge::where([['user_idx', $obj->user_idx], ['type', '충전'], ['state', '결제됨']])->count();
        $cancelCnt = CUCharge::where([['user_idx', $obj->user_idx], ['type', '충전취소'], ['state', '결제됨']])->count();
        $refundCnt = CURefund::where('user_idx', $obj->user_idx)->whereIn('result_check', ['결제됨', '결제대기'])->count();

        if (($chargeCnt - $cancelCnt) > $refundCnt) {
            // CU 환불 진행
            // TODO : CU 환불 진행후 CREATE

            if (isset($this->bank)) {

                $cu = new CURefundController();
                $cuData = [
                    'user_idx' => $obj->user_idx,
                    'amount' => abs($obj->amount),
                    'cash_log_idx' => $obj->idx,
                    'bank' => $this->bank['bank'],
                    'holder' => $this->bank['holder'],
                    'account' => $this->bank['account'],
                ];

                $data = [
                    "user_idx"     => $obj->user_idx,
                    "cash_log_idx" => $obj->idx,
                    "type"         => "오프라인",
                    "method"       => "마일리지",
                    "action"       => "환불",
                    "amount"       => $obj->amount * -1,
                    "state"        => "결제대기",
                    "vbank_num"    => $this->bank['account'],
                    'vbank_name'   => $this->bank['bank'],

                ];

                $refundCash = LMPay::create($data);
                $cuData["cash_log_idx"] = $refundCash->idx;

                $params = [
                    "cash_log_idx" => $cuData["cash_log_idx"],
                    "bank"         => $cuData["bank"],
                    "holder"       => $cuData["holder"],
                    "account"      => $cuData["account"],
                ];

                $url = "http://api.fleapop.co.kr/api/response/cu/refund";
                $client = new Client();
                $response = $client->request('POST', $url, [
                    'form_params' => $params,
                ]);

                if ($response->getStatusCode() != 200) {
                    $refundCash->state = "기타";
                    $refundCash->save();
                }

                $resRequest = json_decode($response->getBody()->getContents());

                if (! $resRequest->success) {
                    $refundCash->state = "기타";
                    $refundCash->save();
                }
                return $response->getBody()->getContents();
            }
        } else {

            // 강제 오프라인 환불건으로 전환
            $this->offline($obj, NULL, true);
        }
    }

    /**
     * 강제 환불
     *
     * @param $id (cash_log), $is_online
     */
    public function refundCompulsion($id, $is_online)
    {
        $payLog = LMPay::find($id);
        if ($payLog->state == '결제됨') {
            return $this->fail(422, '환불이 완료된 진행건입니다.');
        }

        if ($is_online) {
            // 온라인 강제환불
            $payLog->compulsion_refund = 'Y';
            $payLog->save();
        } else {
            // CU 강제환불
            $payLog->appy_num = NULL;
            $payLog->cu_com_refund = 'Y';
            $payLog->save();
        }

        return $this->success(NULL);
    }


    /**
     * 환불
     *
     * @param $user_id , $only_cashier(캐셔), $bank_data ['bank','holder','account']
     */
    public function refundCUTest($user_id, $only_cashier = NULL, $bank_data = NULL)
    {

        if (!isset($user_id)) {
            return $this->fail(422, '필수값 누락되었습니다.');
        }
        $refund = $this->amount($user_id, false, true);
        if ($refund['required_bank'] && !isset($bank_data)) {
            return $this->fail(422, '환불 진행을 위해 계좌정보가 필요합니다.');
        } else if ($refund['required_bank'] && isset($bank_data)) {
            $this->required_bank = $refund['required_bank'];
            $this->bank = $bank_data;

            // 은행 검증
            $imp = new FPIamport();
            $bankResponse = $imp->verificationBankAccount($this->bank);
            if ($bankResponse->getStatusCode() != 200) {
                $message = json_decode($bankResponse->getContent());
                return $this->fail(422, $message);
            }
        }

        $refunds = $refund['refunds'];

        foreach ($refunds as $refund) {

            if (!isset($only_cashier) || !$only_cashier) {

                if ($refund->type == '온라인') {

                    $this->online($refund);
                } else {
                    if ($refund->method == 'CU현금') {
                        $this->cuTest($refund);
                    } else {
                        $this->offline($refund, $only_cashier);
                    }
                }
            } else {
                $cashier = CashierApp::find($only_cashier);
                if ($cashier->enable == 'N') {
                    return $this->fail(422, '비활성화 된 캐셔입니다.');
                }

                if ($cashier->type == $refund->method) {
                    $this->offline($refund, $only_cashier);
                } else if ($cashier->type == $refund->method) {
                    $this->offline($refund, $only_cashier);
                }
            }
        }

        return $this->success(NULL);
    }


    public function cuTest($obj)
    {
        if ($obj->amount <= 0) {
            return;
        }

        $chargeCnt = CUCharge::where([['user_idx', $obj->user_idx], ['type', '충전'], ['state', '결제됨']])->count();
        $cancelCnt = CUCharge::where([['user_idx', $obj->user_idx], ['type', '충전취소'], ['state', '결제됨']])->count();
        $refundCnt = CURefund::where('user_idx', $obj->user_idx)->whereIn('result_check', ['결제됨', '결제대기'])->count();

        if (($chargeCnt - $cancelCnt) > $refundCnt) {
            // CU 환불 진행
            // TODO : CU 환불 진행후 CREATE

            if (isset($this->bank)) {

                $cu = new CURefundController();
                $cuData = [
                    'user_idx' => $obj->user_idx,
                    'amount' => abs($obj->amount),
                    'cash_log_idx' => $obj->idx,
                    'bank' => $this->bank['bank'],
                    'holder' => $this->bank['holder'],
                    'account' => $this->bank['account'],
                ];

                $data = [
                    "user_idx" => $obj->user_idx,
                    "cash_log_idx" => $obj->idx,
                    "type" => "오프라인",
                    "method" => "마일리지",
                    "action" => "환불",
                    "amount" => $obj->amount * -1,
                    "state" => "결제대기",
                    "vbank_num" => $this->bank['account'],
                    'vbank_name' => $this->bank['bank'],

                ];

//            $this->offline($obj, NULL, true);
                $log = LMPay::create($data);
                if($tranno = $cu->refund($cuData)) {
                    $log->appy_num = $tranno;
                    $log->update();
                }



                // $tranno = $cu->refund($cuData);
                // if ($tranno) {
                //     $log->appy_num = $tranno;
                //     $log->save();
                // } else {
                //     $log->state = '기타';
                //     $log->save();
                // }
            }
        } else {

            // 강제 오프라인 환불건으로 전환
            $this->offline($obj, NULL, true);
        }
    }
}
