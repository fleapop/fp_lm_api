<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\OrderCompleted;

class SendOrderedMessage
{
    use KakaoMessage;

    private $templateCode;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param OrderCompleted $event
     * @return void
     */
    public function handle(OrderCompleted $event)
    {
        $this->sendWithTemplateReplace($event->phoneNumber, 'UP011', '주문완료', [
            ['#{username}', $event->username],
            ['#{orderno}', $event->orderNo],
            ['#{itemname}', $event->itemName],
        ]);
    }
}
