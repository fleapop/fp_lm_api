<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\ApprovedRefundOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendApprovedRefundOrderMessage
{
    use KakaoMessage;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApprovedRefundOrder  $event
     * @return void
     */
    public function handle(ApprovedRefundOrder $event)
    {
        $this->sendWithTemplateReplace($event->phoneNumber, 'SR006', '판매자반품상품수거', [
            ['#{seller}', $event->sellerName],
            ['#{username}', $event->userName],
            ['#{orderno}', $event->orderNumber],
            ['#{itemname}', $event->goodsTitle],
        ]);
    }
}
