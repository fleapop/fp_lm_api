<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\DeclineExchangeOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class SendDeclineExchangeOrderMessage
{
    use KakaoMessage;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DeclineExchangeOrder  $event
     * @return void
     */
    public function handle(DeclineExchangeOrder $event)
    {
        $this->sendWithTemplateReplace($event->phoneNumber, 'SC005', '판매자주문교환접수철회', [
            ['#{seller}', $event->sellerName],
            ['#{username}', $event->userName],
            ['#{orderno}', $event->orderNumber],
            ['#{itemname}', $event->goodsTitle],
            ['#{reason}', $event->reason],
        ]);
    }
}
