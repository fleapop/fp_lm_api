<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\OrderCancelRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrderCancelRequestMessage
{
    use KakaoMessage;

    private $templateCode;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->templateCode = 'UP013';
    }

    /**
     * Handle the event.
     *
     * @param  OrderCancelRequest  $event
     * @return void
     */
    public function handle(OrderCancelRequest $event)
    {
        $template = $this->getTemplate($this->templateCode);
        $template = str_replace('#{seller}', $event->sellerName, $template);
        $template = str_replace('#{username}', $event->userName, $template);
        $template = str_replace('#{orderno}', $event->orderNo, $template);
        $template = str_replace('#{itemname}', $event->goodsTitle, $template);
        $template = str_replace('#{reason}', $event->reason, $template);

        $this->send($event->phoneNumber, $template, $this->templateCode, '취소접수완료', $template);
    }
}
