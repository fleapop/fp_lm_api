<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\CancelOrderBySeller;

class SendCancelOrderMessage
{
    use KakaoMessage;

    private $templateCode;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param CancelOrderBySeller $event
     * @return void
     */
    public function handle(CancelOrderBySeller $event)
    {
        $this->sendWithTemplateReplace($event->phoneNumber, 'SO006', '판매자주문취소', [
            ['#{seller}', $event->sellerName],
            ['#{username}', $event->userName],
            ['#{orderno}', $event->orderNumber],
            ['#{itemname}', $event->goodsTitle],
            ['#{reason}', $event->reason],
        ]);
    }
}
