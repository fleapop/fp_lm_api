<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\UpdateInvoiceInformation;

class SendUpdatedInvoiceMessage
{
    use KakaoMessage;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UpdateInvoiceInformation $event
     * @return void
     */
    public function handle(UpdateInvoiceInformation $event)
    {
        $this->sendWithTemplateReplace($event->phoneNumber, 'SD004', '판매자송장수정', [
            ['#{seller}', $event->sellerName],
            ['#{username}', $event->userName],
            ['#{orderno}', $event->orderNumber],
            ['#{itemname}', $event->goodsTitle],
            ['#{delivery}', $event->courierName],
            ['#{deliveryno}', $event->invoiceNumber],
        ]);
    }
}
