<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\ApprovedExchangeOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendApprovedExchangeOrderMessage
{
    use KakaoMessage;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ApprovedExchangeOrder  $event
     * @return void
     */
    public function handle(ApprovedExchangeOrder $event)
    {
        $this->sendWithTemplateReplace($event->phoneNumber, 'SC004', '판매자주문교환접수처리', [
            ['#{seller}', $event->sellerName],
            ['#{username}', $event->userName],
            ['#{orderno}', $event->orderNumber],
            ['#{itemname}', $event->goodsTitle]
        ]);
    }
}
