<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\DeclineRefundOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendDeclineRefundOrderMessage
{
    use KakaoMessage;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DeclineRefundOrder  $event
     * @return void
     */
    public function handle(DeclineRefundOrder $event)
    {
        $this->sendWithTemplateReplace($event->phoneNumber, 'SR008', '판매자반품접수철회', [
            ['#{seller}', $event->sellerName],
            ['#{username}', $event->userName],
            ['#{orderno}', $event->orderNumber],
            ['#{itemname}', $event->goodsTitle],
            ['#{reason}', $event->reason],
        ]);
    }
}
