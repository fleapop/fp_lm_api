<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\DeclineRequestCancelOrder;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendDeclineRequestCancelOrderMessage
{
    use KakaoMessage;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DeclineRequestCancelOrder  $event
     * @return void
     */
    public function handle(DeclineRequestCancelOrder $event)
    {
        $this->sendWithTemplateReplace($event->phoneNumber, 'SO008', '판매자주문취소접수철회', [
            ['#{seller}', $event->sellerName],
            ['#{username}', $event->userName],
            ['#{orderno}', $event->orderNumber],
            ['#{itemname}', $event->goodsTitle],
            ['#{reason}', $event->reason]
        ]);
    }
}
