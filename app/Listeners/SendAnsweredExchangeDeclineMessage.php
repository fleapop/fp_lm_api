<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\DeclineExchangeRequestBeforeDelivery;

class SendAnsweredExchangeDeclineMessage
{
    use KakaoMessage;

    private $templateCode;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->templateCode = 'SQ003';
    }

    /**
     * Handle the event.
     *
     * @param  DeclineExchangeRequestBeforeDelivery  $event
     * @return void
     */
    public function handle(DeclineExchangeRequestBeforeDelivery $event)
    {
        $template = $this->getTemplate($this->templateCode);
        $replace = [
            ['#{seller}', $event->sellerName],
            ['#{username}', $event->userName],
            ['#{orderno}', $event->orderNumber],
            ['#{itemname}', $event->goodsTitle],
        ];

        $template = $this->replaceTemplate($template, $replace);

        $this->send($event->phoneNumber, $template, $this->templateCode, '판매자배송전문의거절', $template);
    }
}

