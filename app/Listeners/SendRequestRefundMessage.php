<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\RequestRefundBySeller;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRequestRefundMessage
{
    use KakaoMessage;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RequestRefundBySeller  $event
     * @return void
     */
    public function handle(RequestRefundBySeller $event)
    {
        if (in_array($event->reason, ['오배송', '상품불량', '기타(판매자)'])) {
            $templateCode = 'SR007';
        }

        if (in_array($event->reason, ['단순변심', '주문실수', '기타'])) {
            $templateCode = 'SR009';
        }

        $this->sendWithTemplateReplace($event->phoneNumber, $templateCode, '판매자반품접수완료', [
            ['#{seller}', $event->sellerName],
            ['#{username}', $event->userName],
            ['#{orderno}', $event->orderNumber],
            ['#{itemname}', $event->goodsTitle],
            ['#{reason}', $event->reason]
        ]);
    }
}
