<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\NoticeRegistered;
use App\Models\Seller;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNoticeRegisteredMessage implements ShouldQueue
{
    use KakaoMessage;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param NoticeRegistered $event
     * @return void
     */
    public function handle(NoticeRegistered $event)
    {
        $sellers = Seller::getCanReceiveMessage()->get();
        $sellers->map(function ($seller) use ($event) {
            $this->sendWithTemplateReplace($seller->manager_hp, 'LN002', '일반공지사항', [
                ['#{seller_name}', $seller->name],
                ['#{title}', $event->title],
            ]);
        });
    }
}
