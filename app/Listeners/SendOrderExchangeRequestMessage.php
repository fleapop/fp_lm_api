<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\OrderExchangeRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use function GuzzleHttp\Psr7\str;

class SendOrderExchangeRequestMessage
{
    use KakaoMessage;

    private $templateCode;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->templateCode = 'UE002';
    }

    /**
     * Handle the event.
     *
     * @param  OrderExchangeRequest  $event
     * @return void
     */
    public function handle(OrderExchangeRequest $event)
    {
        $template = $this->getTemplate($this->templateCode);
        $template = str_replace('#{seller}', $event->sellerName, $template);
        $template = str_replace('#{username}', $event->userName, $template);
        $template = str_replace('#{orderno}', $event->orderNo, $template);
        $template = str_replace('#{itemname}', $event->goodsTitle, $template);
        $template = str_replace('#{reason}', $event->reason, $template);

        $this->send($event->phoneNumber, $template, $this->templateCode, '교환접수완료', $template);
    }
}
