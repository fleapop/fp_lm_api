<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\PaymentCancelBySeller;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPaymentCancelBySellerMessage
{
    use KakaoMessage;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PaymentCancelBySeller  $event
     * @return void
     */
    public function handle(PaymentCancelBySeller $event)
    {
        $this->sendWithTemplateReplace($event->phoneNumber, 'SO007', '판매자주문취소접수처리', [
            ['#{seller}', $event->sellerName],
            ['#{username}', $event->userName],
            ['#{orderno}', $event->orderNumber],
            ['#{itemname}', $event->goodsTitle],
        ]);
    }
}
