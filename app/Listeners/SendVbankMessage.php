<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\OrderVbankCompleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendVbankMessage
{
    use KakaoMessage;

    private $templateCode;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderVbankCompleted  $event
     * @return void
     */
    public function handle(OrderVbankCompleted $event)
    {
        $this->sendWithTemplateReplace($event->phoneNumber, 'UP010', '입금안내', [
            ['#{username}', $event->username],
            ['#{orderno}', $event->orderNo],
            ['#{accountno}', $event->accountNo],
            ['#{enddate}', $event->enddate],
            ['#{amount}', $event->amount],
            ['#{bank}', $event->bank],
        ]);
    }
}
