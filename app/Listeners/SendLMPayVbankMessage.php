<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\LMPayChargeByBankAccount;

class SendLMPayVbankMessage
{
    use KakaoMessage;

    /*
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param object $event
     * @return void
     */
    public function handle(LMPayChargeByBankAccount $event)
    {
        $this->sendWithTemplateReplace($event->phoneNumber, 'LM001', '러마페이 가상계좌 입금안내', [
            ['#{username}', $event->userName],
            ['#{bank}', $event->bank],
            ['#{accountno}', $event->accountNumber],
            ['#{amount}', $event->amount],
            ['#{enddate}', $event->endDate],
        ]);
    }
}
