<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\OrderCanceled;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrderCanceledMessage
{
    use KakaoMessage;

    private $templateCode;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->templateCode = 'UP003';
    }

    /**
     * Handle the event.
     *
     * @param  OrderCanceled  $event
     * @return void
     */
    public function handle(OrderCanceled $event)
    {
        $this->sendWithTemplateReplace($event->phoneNumber, 'UP012', '주문취소', [
            ['#{username}', $event->userName],
            ['#{orderno}', $event->orderNo],
            ['#{itemname}', $event->goodsTitle],
        ]);
    }
}

