<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\ApprovedExchangeRequestBeforeDelivery;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAnsweredExchangeApprovedMessage
{
    use KakaoMessage;

    private $templateCode;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->templateCode = 'SQ006';
    }

    /**
     * Handle the event.
     *
     * @param  ApprovedExchangeRequestBeforeDelivery  $event
     * @return void
     */
    public function handle(ApprovedExchangeRequestBeforeDelivery $event)
    {
        $template = $this->getTemplate($this->templateCode);
        $replace = [
            ['#{seller}', $event->sellerName],
            ['#{username}', $event->userName],
            ['#{orderno}', $event->orderNumber],
            ['#{itemname}', $event->goodsTitle]
        ];
        $template = $this->replaceTemplate($template, $replace);

        $this->send($event->phoneNumber, $template, $this->templateCode, '판매자배송전답변완료', $template);
    }
}
