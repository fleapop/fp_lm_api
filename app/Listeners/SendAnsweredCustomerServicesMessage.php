<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;

class   SendAnsweredCustomerServicesMessage
{
    use KakaoMessage;

    private $templateCode;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->templateCode = 'SQ005';
    }

    /**
     * Handle the event.
     *
     * @param  $event
     * @return void
     */
    public function handle($event)
    {
        $template = $this->getTemplate($this->templateCode);
        $replace = [
            ['#{seller}', $event->sellerName],
            ['#{username}', $event->userName],
        ];
        $template = $this->replaceTemplate($template, $replace);

        $this->send($event->phoneNumber, $template, $this->templateCode, '판매자일반문의답변', $template);
    }
}
