<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\ApplicationNoticeRegistered;
use App\Models\Seller;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendApplicationNoticeRegisteredMessage implements ShouldQueue
{
    use KakaoMessage;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param ApplicationNoticeRegistered $event
     * @return void
     */
    public function handle(ApplicationNoticeRegistered $event)
    {
        $sellers = $sellers = Seller::getCanReceiveMessage()->get();

        $sellers->map(function ($seller) use ($event) {
            $this->sendWithTemplateReplace($seller->manager_hp, 'LN003', '신청서공지사항', [
                ['#{title}', $event->title],
                ['#{greetings}', $this->contentsReplace($event->greetings)],
                ['#{target}', $event->target],
                ['#{deadline}', substr($event->deadline, 0, 10) . ' ' . $event->deadlineHour . '시'],
                ['#{start}', substr($event->start, 0, 10)],
                ['#{end}', substr($event->end, 0, 10)],
                ['#{link}', $event->link],
                ['#{how}', $this->contentsReplace($event->how)],
            ]);
        });
    }

    private function contentsReplace($contents)
    {
        $temp = str_replace("<p><br></p>", "\n\r", $contents);
        $temp = str_replace("&nbsp;", " ", $temp);
        $temp = str_replace("</p>", "\n", $temp);
        $temp = strip_tags($temp);
        $temp = htmlspecialchars_decode($temp);

        return $temp;
    }
}
