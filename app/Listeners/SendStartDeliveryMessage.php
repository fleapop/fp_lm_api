<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\StartDelivery;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendStartDeliveryMessage implements ShouldQueue
{
    use KakaoMessage;

    private $templateCode;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->templateCode = 'SD003';
    }

    /**
     * Handle the event.
     *
     * @param StartDelivery $event
     * @return void
     */
    public function handle(StartDelivery $event)
    {
        $template = $this->getTemplate($this->templateCode);
        $replace = [
            ['#{seller}', $event->sellerName],
            ['#{username}', $event->userName],
            ['#{orderno}', $event->orderNumber],
            ['#{itemname}', $event->goodsTitle],
            ['#{delivery}', $event->courierName],
            ['#{deliveryno}', $event->invoiceNumber],
        ];
        $template = $this->replaceTemplate($template, $replace);

        $this->send($event->phoneNumber, $template, $this->templateCode, '판매자상품발송', $template);
    }
}
