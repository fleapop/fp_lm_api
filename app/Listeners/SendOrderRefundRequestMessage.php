<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\OrderRefundRequest;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendOrderRefundRequestMessage
{
    use KakaoMessage;

    private $templateCode;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->templateCode = 'UR002';
    }

    /**
     * Handle the event.
     *
     * @param  OrderRefundRequest  $event
     * @return void
     */
    public function handle(OrderRefundRequest $event)
    {
        $template = $this->getTemplate($this->templateCode);
        $template = str_replace('#{seller}', $event->sellerName, $template);
        $template = str_replace('#{selller}', $event->sellerName, $template);
        $template = str_replace('#{username}', $event->userName, $template);
        $template = str_replace('#{orderno}', $event->orderNo, $template);
        $template = str_replace('#{itemname}', $event->goodsTitle, $template);
        $template = str_replace('#{reason}', $event->reason, $template);

        $this->send($event->phoneNumber, $template, $this->templateCode, '반품접수완료', $template);
    }
}
