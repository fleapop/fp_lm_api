<?php

namespace App\Listeners;

use App\Events\MemberJoin;
use App\ApiStore\KakaoMessage;

class SendWelcomeMessage
{
    use KakaoMessage;

    const EMAIL_ACCOUNT    = 1;
    const FACEBOOK_ACCOUNT = 2;
    const APPLE_ACCOUNT    = 4;

    private $templateCode;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->templateCode = 'UJ001';
    }

    /**
     * Handle the event.
     *
     * @param MemberJoin $event
     * @return void
     */
    public function handle(MemberJoin $event)
    {
        $template = $this->getTemplate($this->templateCode);
        $template = str_replace('#{username}', $event->username, $template);
        $template = str_replace('#{jointype}', $this->convertJoinType($event->joinType), $template);

        $this->send($event->phoneNumber, $template, $this->templateCode, '회원가입', $template);
    }

    private function convertJoinType($joinType) {
        switch ($joinType) {
            case self::EMAIL_ACCOUNT:
                return '이메일';
            case self::FACEBOOK_ACCOUNT:
                return '페이스북';
            case self::APPLE_ACCOUNT:
                return '애플';
        }
    }
}
