<?php

namespace App\Listeners;

use App\ApiStore\KakaoMessage;
use App\Events\StockSoldOut;

class SendStockSoldOutMessage
{
    use KakaoMessage;

    private $templateCode;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        $this->templateCode = 'LI001';
    }

    /**
     * Handle the event.
     *
     * @param StockSoldOut $event
     * @return void
     */
    public function handle(StockSoldOut $event)
    {
        $template = $this->getTemplate($this->templateCode);
        $template = str_replace('#{seller}', $event->seller->name, $template);
        $template = str_replace('#{itemname}', $event->sku->goods->title, $template);
        $template = str_replace('#{option}', implode(',', $event->sku->option_values->keys()->toArray()), $template);
        $template = str_replace('#{optionname}', implode(',', $event->sku->option_values->values()->toArray()), $template);

        $this->send($event->seller->manager_hp, $template, $this->templateCode, '품절안내', $template);
    }
}
