<!doctype html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, target-densityDpi=device-dpi, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>다음 주소 검색</title>
</head>
<body>
    <div id="wrap" style="display:none;border:0px solid;width:100%;height:100vh;margin:5px 0;position:relative">
    </div>
    <script src="//ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
    <script>

        var element_layer = document.getElementById('wrap');

        sample2_execDaumPostcode();
        function getAddress(zonecode, roadAddress, buildingName) {
            console.log("getAddress");
            var data = {
                "zonecode":zonecode,
                "roadAddress":roadAddress,
                "buildingName":buildingName
            };

            webkit.messageHandlers.callbackHandler.postMessage(data);
        }
        function sample2_execDaumPostcode() {
            new daum.Postcode({
                oncomplete: function(data) {
                    var str = data.zonecode+", "+data.roadAddress+", "+data.buildingName;
                    console.log(str)
                    window.TestApp.setAddress(data.zonecode, data.roadAddress, data.buildingName);
                },
                width : '100%',
                height : '100%',
                maxSuggestItems : 5
            }).embed(element_layer);

            element_layer.style.display = 'block';
            initLayerPosition();
        }

        function initLayerPosition(){
        var width = 300; //우편번호서비스가 들어갈 element의 width
        var height = 400; //우편번호서비스가 들어갈 element의 height
        var borderWidth = 5; //샘플에서 사용하는 border의 두께

        // 위에서 선언한 값들을 실제 element에 넣는다.
        element_layer.style.width = document.documentElement.clientWidth + 'px';
        element_layer.style.height = document.documentElement.clientHeight + 'px';
        // element_layer.style.border = borderWidth + 'px solid';
        // 실행되는 순간의 화면 너비와 높이 값을 가져와서 중앙에 뜰 수 있도록 위치를 계산한다.
        element_layer.style.left = '0px';
        element_layer.style.top = '0px';
    }
    </script>
</body>
</html>
