<!DOCTYPE html>
<html lang="ko">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width" />
        <meta property="og:type" name="og_type" content="website" />
        @if(isset($meta_url))
        <meta property="og:url" name="og_url" content="{{$meta_url}}" />
        @endif
        @if(isset($meta_title))
        <meta property="og:title" name="og_title" content="{{$meta_title}}" />
        @else
        <meta property="og:title" name="og_title" content="플리팝 - 러블리마켓" />
        @endif
        @if(isset($meta_description))
        <meta property="og:description" name="og_description" content="{{$meta_description}}" />
        @else
        <meta property="og:description" name="og_description" content="10~20대 여성들을 위한 마켓플레이스 러블리마켓" />
        @endif
        @if(isset($meta_image))
        <meta property="og:image" name="og_image" content="{{$meta_image}}" />
        @else
        <meta property="og:image" content="/image/1.jpg"/>
        @endif
        <meta property="al:ios:url" content="gps1215.ShareGuide:///eventpage?event_seq=15">
        <script src="/js/jquery.min.3.2.1.js"></script>

        <style>
            @font-face {
                font-family: "NanumSquareRoundR";
                src: url("/fonts/NanumSquare/NanumSquareRoundR.eot");
                src:
                    url("/fonts/NanumSquare/NanumSquareRoundR.woff") format("woff"),
                    url("/fonts/NanumSquare/NanumSquareRoundR.otf") format("opentype");
                src: url('/fonts/NanumSquare/NanumSquareRoundR.ttf');
            }

            html, body {
                width: 100%;
                height: 100%;
                min-height: 100%;
                position: relative;

                margin: auto;
            }

            .load_wrap {
                display: block;
                /*margin: auto;*/
                text-align: center;


                position: absolute;
                top: 20%;
                left: 50%;
                transform: translate(-50%, 0%);

            }

            .load_wrap p {
                color: #f7ccdc;
                font-size: 16px;
                font-family: NanumSquareRoundR, sans-serif;
            }
        </style>
    </head>
    <body>

    <div class="load_wrap">
        <img src="/image/login_logo.png" />
        @if(isset($title))
            <p>{{$title}}</p>
        @else
            <p>마켓으로 이동중입니다.</p>
        @endif

    </div>


        @if(isset($url_str))
        <input type="hidden" id="hd_url" value="{{$url_str}}" />
        <input type="hidden" id="hd_type" value="{{$type}}" />
        <input type="hidden" id="hd_idx" value="{{$idx}}" />
        <input type="hidden" id="hd_position" value="{{$position}}" />
        <input type="hidden" id="hd_host" value="{{$host}}" />
        @else
        <input type="hidden" id="hd_url" value="" />
        @endif
        <input type="hidden" id="hd_once" value="true" />


    </body>
    <script type="text/javascript">
        $("document").ready(function() {
          appCheck();
            // if($("#hd_type").val() == "goods") {
            //     location.href = host+$("#hd_url").val();
            // } else {
            //     appCheck();
            // }

        });

        function moveUrl() {
            @isset($url_str)
            location.href = "{{$url_str}}";
            @endisset
        }

        function appCheck() {
            // alert("appCheck");
            var userAgent = navigator.userAgent;

            var visitedAt = (new Date()).getTime(); // 방문 시간
            if (userAgent.match(/iPhone|iPad|iPod|Macintosh/)) {
               setTimeout(
                  function() {

                      if ((new Date()).getTime() - visitedAt < 2000) {
               setTimeout(function() {
                   if ((new Date()).getTime() - visitedAt < 2000) {
                       // MoveTo AppStore Code

                           // $("#msg").html("앱스토어로 이동");
                           var host = $("#hd_host").val();
                           var url = host+$("#hd_url").val();
                           if(typeof host != "undefined") {

                               location.replace(url);
                           }

                      }

                   }, 500);
                       var host = $("#hd_host").val();
                       var url = host+$("#hd_url").val();
                       if(typeof host != "undefined") {
                           location.replace(url);
                       }
                   }
               }, 500);



               setTimeout(function() {
                   var url_str = $("#hd_url").val();
                   if(url_str.length > 1) {
                       var type = $("#hd_type").val();
                       var idx = $("#hd_idx").val();
                       var position = $("#hd_position").val();
                       var host = type;
                       // if(host != "magazine") {
                           host = "view";
                       // }
                       var app_url = 'fp.FleapopProject:///fleapop/{"host":"'+host+'","type":"'+type+'","idx":'+idx+',"position":'+position+'}';
                    }
                       location.href = app_url;



                }, 0);

            } else if (userAgent.match(/Android/)) {
                setTimeout(function() {
                    if ((new Date()).getTime() - visitedAt < 2000) {
                        var host = $("#hd_host").val();
                        if(typeof host != "undefined") {
                            location.href = host+$("#hd_url").val();
                        }

                    }
                }, 500);

               setTimeout(function() {
                   var url_str = $("#hd_url").val();
                   if(url_str.length > 1) {
                       var type = $("#hd_type").val();
                       var idx = $("#hd_idx").val();
                       var position = $("#hd_position").val();
                       var host = type;
                       // if(host != "magazine") {
                           host = "view";
                       // }
                       var app_url = "fleapop://"+host+"/{type:'"+type+"',idx:"+idx+",position:"+position+"}";

                       location.href = app_url;
                   }

                }, 0);

            } else {
               var host = $("#hd_host").val();
               var url = host+$("#hd_url").val();
               if(typeof host != "undefined") {
                   location.replace(url);
               }
            }
        }
    </script>
</html>
