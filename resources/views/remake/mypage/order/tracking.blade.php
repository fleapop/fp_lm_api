<!DOCTYPE html>
<html lang="ko" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="format-detection" content="telephone=no" />
        <meta name="viewport" content="initial-scale=1.0,user-scalable=no,maximum-scale=1,width=device-width" />
        <title></title>



    </head>
    <style media="screen">
        .delivery_tracking_body {
            width: 100%;
            max-width: 800px;
            margin: 0 auto;
        }
        h3{
            font-size: 1.4rem;
        }
        .tracking_info{
            width: 100%;
            border-bottom:2px solid #111;
            margin-top: 1.5rem;
        }
        .tracking_table_top tr th{
            font-size: 1.2rem;
            font-weight: bold;
            color: #111;
            text-align: left;
            padding:20px;
        }
        .tracking_table_top tr td{
            font-size: 1.2rem;
            color: #666;
        }
        .tracking_table_bottom{
            width: 100%;
            text-align: center;
        }
        .tracking_table_bottom tr td{
            width: 33.3333%;
            padding: 20px 0;
            font-size: 1.2rem;
            color: #666;
            border-bottom: 1px solid #ddd;
        }
        #td_head td{
            color: #111;
            font-weight: bold;
            font-size: 1.4rem;
        }
        .tracking_header{
            width: 100%;

        }
        .tracking_header img{
            width: 30px;
            height: 30px;
            opacity: 0.3;
        }
        @media screen and (max-width:500px) {
            .delivery_tracking_body {
                width: 100%;
                max-width: 800px;
                margin: 0 auto;
            }
            h3{
                font-size: 1.1rem;
            }
            .tracking_info{
                width: 100%;
                border-bottom:2px solid #111;
                margin-top: 1.5rem;
            }
            .tracking_table_top tr th{
                font-size: 1rem;
                font-weight: bold;
                color: #111;
                text-align: left;
                padding:20px 0;
            }
            .tracking_table_top tr td{
                font-size: 0.9rem;
                color: #666;
                padding-left: 1rem;
            }
            .tracking_table_bottom{
                width: 100%;
                text-align: center;
            }
            .tracking_table_bottom tr td{
                width: 33.3333%;
                padding: 20px 0;
                font-size: 0.9rem;
                color: #666;
                border-bottom: 1px solid #ddd;
            }
            #td_head td{
                color: #111;
                font-weight: bold;
                font-size: 1rem;
            }
        }
    </style>
    <body>
        <div class="delivery_tracking_body">
            @if($device == "101")
            <div class="tracking_header">
                <a href="javascript:void(0);" onclick="window.history.back();">
                    <img src="/image/remake/mypage_img/fp_b_line_left.png" alt="">
                </a>
            </div>
            @endif


            <div class="tracking_info">
                <h3>기본정보</h3>
            </div>
            <table class="tracking_table_top">
                @if($courier)
                <tr>
                    <th>택배사</th> <td>{{ $courier ?? ''}}</td>
                </tr>
                @endif
                <tr>
                    <th>운송장번호</th> <td>{{ $data->invoiceNo ?? ''}}</td>
                </tr>
            </table>
            <div class="tracking_info">
                <h3>배송진행 현황</h3>
            </div>
            <table class="tracking_table_bottom">
                <tr id="td_head">
                    <td>시간</td> <td>현재위치</td> <td>배송상태</td>
                </tr>
                @if(!empty($data->trackingDetails))
                    @foreach($data->trackingDetails as $row)
                        <tr>
                            <td>{{$row->timeString}}</td> <td>{{$row->where}}</td><td>{{$row->kind}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr><td colspan="3">내역이 없습니다.</td></tr>
                @endif
            </table>

        </div>
    </body>
</html>
