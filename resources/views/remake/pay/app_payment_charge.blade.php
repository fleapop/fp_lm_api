<!-- 아임포트 자바스크립트는 jQuery 기반으로 개발되었습니다 -->
<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js" ></script>
<script type="text/javascript" src="https://cdn.iamport.kr/js/iamport.payment-1.1.5.js" ></script>
<script src="/js/remake/comm.js"></script>

<script type="text/javascript">
var IMP = window.IMP; // 생략가능
IMP.init('{{$IAMPORT_ID}}');


//onclick, onload 등 원하는 이벤트에 호출합니다
IMP.request_pay({
    pg : '{{$pg}}', // version 1.1.0부터 지원.
    pay_method : '{{$pay_type}}',
    merchant_uid : 'merchant_'+"{{$user_idx}}"+ new Date().getTime(),
    name : '러블리마켓',
    amount : {{$amount}},
    buyer_email : '{{$email_account}}',
    buyer_name : '{{$shipping_name}}',
    buyer_tel : '{{$phone}}',
    buyer_addr : '{{$address}}',
    buyer_postcode : '{{$zip_code}}',
    m_redirect_url : '{{$m_redirect_url}}/re/app/payments/complete/{{$user_idx}}/{{$token}}',
    app_scheme : 'iamportapp'
}, function(rsp) {
    if ( rsp.success ) {
      setPaymentLog("\'"+rsp.imp_uid+"\'", 'CHR');
        var msg = '결제가 완료되었습니다.';
        msg += '고유ID : ' + rsp.imp_uid;
        msg += '상점 거래ID : ' + rsp.merchant_uid;
        msg += '결제 금액 : ' + rsp.paid_amount;
        msg += '카드 승인번호 : ' + rsp.apply_num;
    } else {
      setPaymentLog("\'"+rsp.imp_uid+"\'", 'CHR');
        var msg = '결제에 실패하였습니다.';
        msg += '에러내용 : ' + rsp.error_msg;
    }

    alert(msg);
});

</script>
