<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Pay document</title>
	<script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js" ></script>
	<script type="text/javascript" src="https://service.iamport.kr/js/iamport.payment-1.1.5.js"></script>
	<script src="/js/remake/comm.js"></script>
</head>
<body>

	<script type="text/javascript">
		var IMP = window.IMP; // 생략가능
		IMP.init('{{$IAMPORT_ID}}'); //아임포트 회원가입 후 발급된 가맹점 고유 코드를 사용해주세요. 예시는 KCP공식 아임포트 데모 계정입니다.

		IMP.request_pay({
			pg : '{{$pg}}', // version 1.1.0부터 지원.
			pay_method : '{{$pay_type}}',
			merchant_uid : 'merchant_'+"{{$user_idx}}"+ new Date().getTime(),
			name : '러블리마켓',
			amount : {{$amount}},
			buyer_email : '{{$email_account}}',
			buyer_name : '{{$shipping_name}}',
			buyer_tel : '{{$phone}}',
			buyer_addr : '{{$address}}',
			buyer_postcode : '{{$zip_code}}',
			m_redirect_url : '{{$m_redirect_url}}/re/app/payments/complete/{{$user_idx}}/{{$token}}',
			app_scheme : 'Fleapop'
		}, function(rsp) {
			setPaymentLog(rsp.imp_uid, 'CHR');
			if(rsp.success) {
				console.log("success");
			} else {
				console.log("fail");
			}
		});
	</script>
</body>
</html>
