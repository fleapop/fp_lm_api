<!DOCTYPE html>
<html lang="ko">
<head>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="/js/jquery.min.3.2.1.js"></script>
</head>

<body>
  <p></p>
  <script>

  $(document).ready(function() {
    var varUA = navigator.userAgent.toLowerCase();
    if ( varUA.indexOf('android') > -1) {
        //안드로이드
        callAndroid('{{$response}}');
    } else if ( varUA.indexOf("iphone") > -1||varUA.indexOf("ipad") > -1||varUA.indexOf("ipod") > -1 ) {
        //IOS
        callIOS('{{$response}}');
    } else {

    }
  });

  function callAndroid(response){
      window.FP_PAYMENT.chargeLmPayment(response);
  }

  function callIOS(response) {
    webkit.messageHandlers.callbackHandler.postMessage(response);
  }






  </script>

</body>
</html>
