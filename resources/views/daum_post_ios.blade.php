<!doctype html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, target-densityDpi=device-dpi, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>다음 주소 검색</title>
</head>
<body>
    <div id="wrap" style="display:none;border:0px solid;width:100%;height:100vh;margin:5px 0;position:relative">
    </div>
    <script src="https://ssl.daumcdn.net/dmaps/map_js_init/postcode.v2.js"></script>
    <script>
    var element_layer = document.getElementById('wrap');

    sample2_execDaumPostcode();
    function getAddress(zonecode, roadAddress, buildingName) {
        console.log("getAddress");
        var data = {
            "zonecode":zonecode,
            "roadAddress":roadAddress,
            "buildingName":buildingName
        };

        webkit.messageHandlers.callbackHandler.postMessage(data);
    }
    function sample2_execDaumPostcode() {
        new daum.Postcode({
            oncomplete: function(data) {
                var str = data.zonecode+", "+data.roadAddress+", "+data.buildingName;
                console.log(str)
                getAddress(data.zonecode, data.roadAddress,data.buildingName);

            },
            width : '100%',
            height : '100%',
            maxSuggestItems : 5
        }).embed(element_layer);

        element_layer.style.display = 'block';
    }
    </script>
</body>
</html>
