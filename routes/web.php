<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');

});
Route::get("/re/app/payment/charge/{pay_type}/{amount}/{token}/{user_id}/{device}", 'ViewPaymentController@viewAppPaymentCharge');
Route::get("/re/app/payments/complete/{user_idx}/{token}", "ViewPaymentController@viewAppPaymentComplete");

Route::get("/remake/login/agree", 'Remake\ViewsController@viewAgree');
Route::get("/remake/login/agree_info", 'Remake\ViewsController@viewAgree_Info');

/* App */
Route::get("/daum/address/and", function () {
  return view("daum_post_and");
});

Route::get("/daum/address/ios", function () {
  return view("daum_post_ios");
});

// 주문/배송 - 운송장 조회
Route::get("/re/user/delivery/view/{type}/{id}/{device}", "ViewUserController@viewTracking");

/* Share */
Route::get("/re/share/{type}/{idx}/{position?}", 'AppController@viewShare');
Route::get("/re/img/down/{table_name}/{table_idx}", 'AppController@viewImgDown');
Route::get("/re/fleapop/magazine/view/{idx}", 'ViewFleapopController@viewMagazineData');