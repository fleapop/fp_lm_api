<?php

//use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Route::group(['domain' => env('API_DOMAIN'), 'middleware' => 'cors'], function() {
//    Route::get("/store/store", "StoreController@getStoreList");
//    Route::get("/store/goods/detail/{goodsId}", "StoreController@getGoodsDetail");
//    Route::get("/store/test", "StoreController@getGoodsTest");
//
//    Route::post("/store/store", "StoreController@getStoreList");
//    Route::post("/store/goods/detail/{goodsId}", "StoreController@getGoodsDetail");
//});


//
//Route::middleware(['cors'])->group(function(){
//    Route::get("/store/store", "StoreController@getStoreList");
//    Route::get("/store/goods/detail/{goodsId}", "StoreController@getGoodsDetail");
//
//    Route::post("/store/store", "StoreController@getStoreList");
//    Route::post("/store/goods/detail/{goodsId}", "StoreController@getGoodsDetail");
//});

Route::get("/lm/menu", "LmController@getMenu");

/**
 * Common
 */
Route::get("/common/category/{category?}", "CommonController@getCategory");

Route::post("/common/lmpay/add", "CommonController@addLmpay");

Route::post("/user/setDevice", "CommonController@setDeviceInfo");

Route::post("/app/version/get", "CommonController@apiGetAppVersion");
Route::get('/service/state/{device}/{user_id?}/{token?}', 'CommonController@getServiceState');

// 배너 정보 - 2차 개편
Route::get("/service/bannerv2/{device?}/{user_id?}/{token?}", "CommonController@getBanner");

Route::get("/footer", "CommonController@getFooter");

/**
 * Store
 */
Route::get("/store/store/list", "StoreController@getStoreList");
Route::get("/store/new/list/{device?}/{user_id?}/{token?}", "StoreController@getNewList");
Route::get("/store/best/list", "StoreController@getBestList");
Route::get("/store/brand/list/{device?}/{user_id?}/{token?}", "StoreController@getBrandList");
Route::get("/store/category/list", "StoreController@getCategoryList");
Route::get("/store/category/goods", "StoreController@getCategoryDetail");
Route::get("/store/goods/detail/{device?}/{user_id?}/{token?}", "StoreController@getGoodsDetail");
Route::get("/store/new/like/{device?}/{user_id?}/{token?}", "StoreController@newLike");
Route::get("/store/like/list/{device?}/{user_id?}/{token?}", "StoreController@getMyLikeList");

Route::get("/store/search/{device?}/{user_id?}/{token?}", "StoreController@goodsSearch");

Route::get("/store/goods/options/{goodsId?}", "StoreController@getByGoods");

Route::get("/store/order/sheet/{meta}", "StoreController@OrderSheet");

Route::get("/store/goods/roas/{device?}/{user_id?}/{token?}", "StoreController@setRoas");
// 기획전 참여 브랜드
Route::get("/exhibition/brand/{device?}/{user_id?}/{token?}", "ExhibitionController@getExhibitionBrand");

Route::get("/store/exhibition/rep/{exhibition_id?}", "ExhibitionController@getExhibitionRepMenu");
Route::get("/store/exhibition/reps", "ExhibitionController@getExhibitionRepMenus");

/**
 * Exhibition
 */
Route::get("/store/exhibition/list", "ExhibitionController@getExhibitionList");
Route::get("/store/exhibition", "ExhibitionController@getExhibitionTab");
Route::get("/store/exhibition/goods", "ExhibitionController@getExhibitionGoodsList");
Route::get("/store/exhibition/today", "ExhibitionController@getExhibitionToday");

/**
 * Seller
 */
Route::get("/seller/store/goods/{device?}/{user_id?}/{token?}", "SellerController@getGoodsList");
Route::get("/seller/recommendation/list", "SellerController@getSellerRandomList");
Route::get("/seller/brand/list/{device?}/{user_id?}/{token?}", "SellerController@getBrandInfo");
Route::get("/seller/brand/like/list/{device?}/{user_id?}/{token?}", "SellerController@getBrandLikeList");

Route::get("/seller/snap/list", "SellerController@getSnapList");
Route::get("/seller/snap/detail", "SellerController@getSnapDetail");
Route::get("/seller/profile/{device?}/{user_id?}/{token?}", "SellerController@getBrandProfile");
Route::get("/seller/fit/list", "SellerController@getLdfitList");



/**
 * Cart
 */
Route::get("/cart/count/{device?}/{user_id?}/{token?}", "CartController@getCartCount");
Route::get("/cart/list/{device?}/{user_id?}/{token?}", "CartController@getCartList");
Route::post("/cart/add/{device?}/{user_id?}/{token?}", "CartController@setCartAdd");
Route::post("/cart/update/{device?}/{user_id?}/{token?}", "CartController@setUpdateCart");
Route::post("/cart/delete/{device?}/{user_id?}/{token?}", "CartController@setDeleteCart");

Route::get("/cart/{user_id?}/{non_user_id?}/{ids}/{device?}/{token?}", "CartController@getCart");

/**
 * User
 */
Route::post("/login", "UserController@Login");
Route::post("/user/native/login", "UserController@nativeLogin")->middleware("force_json");
Route::get("/user/account/check", "UserController@checkAccount");
Route::post("/user/cert/sms/send", "UserController@sendJoinSms");
Route::get("/user/cert/sms/confirm", "UserController@checkJoinSMS");
Route::post("/join/register", "UserController@Join");
Route::get("/user/info/{device?}/{user_id?}/{token?}", "UserController@getUserInfo");

Route::get("/user/find/account", "UserController@getFindAccount");
Route::get("/user/find/password", "UserController@getFindPassword");
Route::post("/user/find/password/change", "UserController@setPasswordChange");

Route::post("/user/profile/edit/{device?}/{user_id?}/{token?}", "UserController@setUserProfileImg");
Route::post("/user/profile/info/{device?}/{user_id?}/{token?}", "UserController@setUserInfo");
Route::post("/user/password/change/{device?}/{user_id?}/{token?}", "UserController@setAccountPasswordChange");
Route::post("/user/phone/change/{device?}/{user_id?}/{token?}", "UserController@setPhoneNumber");

Route::get("/user/withdrawal/{device?}/{user_id?}/{token?}", "UserController@getUserWithdrawal");
Route::post("/user/withdrawal/edit/{device?}/{user_id?}/{token?}", "UserController@setUserWithdrawal");

Route::get("/user/address/list/{device?}/{user_id?}/{token?}", "UserController@getUserAddress");
Route::post("/user/address/edit/{device?}/{user_id?}/{token?}", "UserController@setUserAddress");
Route::post("/user/address/del/{device?}/{user_id?}/{token?}", "UserController@setUserAddressDel");


Route::get('/user/question/{type}/{device?}/{user_id?}/{token?}', 'UserController@getQuestion');

Route::post('/user/question/set/{device?}/{user_id?}/{token?}', 'UserController@setQuestion');

Route::post('/user/info/setting/{device?}/{user_id?}/{token?}', 'UserController@setUserSetting');

Route::get('/user/push/list/{device?}/{user_id?}/{token?}', 'UserController@getPushList');
Route::get('/user/push/category/badge/{device?}/{user_id?}/{token?}', 'UserController@getPushCategoryBadge');

Route::post('/user/lmpay/password/update/{device?}/{user_id?}/{token?}', 'UserController@lmpayPasswordUpdate');
Route::get('/user/lmpay/password/check/{device?}/{user_id?}/{token?}', 'UserController@lmpayPasswordCheck');
Route::get('/user/lmpay/password/isset/{device?}/{user_id?}/{token?}', 'UserController@lmpayPasswordSetCheck');

Route::get('/user/push/badge/{device?}/{user_id?}/{token?}', 'UserController@getUserPushBadge');

Route::post('/user/block/{device?}/{user_id?}/{token?}', 'UserController@setUserBlock');



/********************************************************
 **                Community API                       **
 *********************************************************/
#region Community
/**
 * 커뮤니티
 */
Route::get("/community/{service_type}/list/{device?}/{user_id?}/{token?}", "CommunityController@getList");
Route::get("/community/{service_type}/detail/{device?}/{user_id?}/{token?}", "CommunityController@getList");
Route::post("/community/{service_type}/edit/{device?}/{user_id?}/{token?}", "CommunityController@setCommunityEdit");
Route::post("/community/{service_type}/del/{device?}/{user_id?}/{token?}", "CommunityController@delTimeline");

Route::get("/community/{service_type}/goods/list/{device?}/{user_id?}/{token?}", "CommunityController@getGoodsFitList");

// 상품태그 검색
Route::get("/community/goods/tag/{keyword}", "CommunityController@getTagSearch");
// 상품 태그 검색 결과
Route::get("/community/goods/tag/{keyword}/result", "CommunityController@getTagSearchResult");

// 신고하기
Route::post("/community/{service_type}/claim/{device?}/{user_id?}/{token?}", "CommunityController@setClaim");
// 게시물 콕
Route::post("/common/like/set/{device?}/{user_id?}/{token?}", "CommonController@setlike");

// 팔로
Route::post("/user/follow/{device?}/{user_id?}/{token?}", "UserController@setFollow");

Route::get("/community/answerfit/none/{device?}/{user_id?}/{token?}", "CommunityController@getAnswerFitNone");

Route::get("/reply/list", "CommonController@getReplyList");
Route::post("/reply/edit/{device?}/{user_id?}/{token?}", "CommonController@setReplyEdit");
Route::post("/reply/del/{device?}/{user_id?}/{token?}", "CommonController@setReplyDel");

Route::post("/community/answer/submit/{device?}/{user_id?}/{token?}", "CommunityController@setAnswerSelected");

// 프로젝트
Route::get("/community/project/{project_idx?}", "CommunityController@getProjectList");
Route::post("/community/project/{project_idx}/request", "CommunityController@setProjectRequest");
Route::get("/community/project/{project_idx}/request/check", "CommunityController@getProjectRequestCheck");


Route::post("/community/fit/best", "CommunityController@setFitBest");
Route::get("/community/fit/best/{device?}/{user_id?}/{token?}", "CommunityController@getFitBest");

Route::get("/community/magazine", "CommunityController@apiGetContentList");
Route::get("/community/magazine/{idx}/{device?}/{user_id?}/{token?}", "CommunityController@apiGetContentData");

Route::get("/community/fit/follow/badge/{device?}/{user_id?}/{token?}", "CommunityController@getFitBadge");
#endregion Community

/********************************************************
 **                MyPage API                       **
 *********************************************************/
#region Mypage API
// 다이어리 탭 상단 정보
Route::get("/mypage/user/info/{device?}/{user_id?}/{token?}", "MypageController@getUserInfo");
Route::get("/mypage/diary/{service_type}/list/{device?}/{user_id?}/{token?}", "MypageController@getDiaryList");
Route::get("/mypage/diary/like/group/list/{device?}/{user_id?}/{token?}", "MypageController@getDiaryLikeList");
Route::get("/mypage/diary/{service_type}/like/list/{device?}/{user_id?}/{token?}", "MypageController@getServiceLikeList");

// My쇼핑백 리스트
Route::get("/mypage/order/list/{device?}/{user_id?}/{token?}", "MypageController@getShoppingbagOrderList");
Route::post("/mypage/order/shppingbag/{device?}/{user_id?}/{token?}", "MypageController@setMyShoppingBag");

// Follow List
Route::get("/mypage/follow/{type}/list", "MypageController@getFollowList");
Route::get("/mypage/follow/count", "MypageController@getFollowCount");

//////////// 마이쇼핑 ///////////////
Route::get("/mypage/myshopping/info/{device?}/{user_id?}/{token?}", "MypageController@getMyShoppingInfo");
Route::get("/mypage/myshopping/order/list/{device?}/{user_id?}/{token?}", "MypageController@getOrderList");
Route::get("/mypage/myshopping/order/detail/{device?}/{user_id?}/{token?}", "MypageController@getOrderDetail");

Route::get("/mypage/myshopping/none/postscript/{device?}/{user_id?}/{token?}", "MypageController@getNonePostscript");
Route::get("/mypage/project/request/{device?}/{user_id?}/{token?}", "MypageController@getProjectMyRequest");


//////// MyShopping ///////////
Route::get("/lmpay/info/{device?}/{user_id?}/{token?}", "UserLmPayController@getLmPayInfo");
Route::get("/lmpay/cu/barcode/{device?}/{user_id?}/{token?}", "UserLmPayController@getCUBarcode");

Route::post("/lmpay/barcode/refresh/{device?}/{user_id?}/{token?}", "UserLmPayController@setBarcodeRefresh");
Route::get("/lmpay/list/{device?}/{user_id?}/{token?}", "UserLmPayController@getLmPayList");
Route::get("/lmpay/detail/{device?}/{user_id?}/{token?}", "UserLmPayController@getLmPayList");
//Route::get("/lmpay/detail/{device?}/{user_id?}/{token?}", "UserLmPayController@getLmPayList");
Route::get("/lmpay/refund/{type}/{device?}/{user_id?}/{token?}", "UserLmPayController@apiRefund");
Route::post("/lmpay/refund/account/{device?}/{user_id?}/{token?}", "UserLmPayController@setRefundAccount");

Route::post("/lmpay/password/reset/{device?}/{user_id?}/{token?}", "UserLmPayController@setLmPasswordReset");


Route::get("/lm/search/total/{device?}/{user_id?}/{token?}", "CommonController@getSearchTotal");
Route::get("/lm/search/list/{service_type}/{device?}/{user_id?}/{token?}", "CommonController@getSearch");

// Payment
Route::post("/user/payment/history/detail", "PaymentController@apiGetPayHistoryDetail");



#endregion Mypage API

/**
 * 결제 관련 API
 */
#region ### OrderController / CURefundController / CUOrderController / CUChargeController ###

Route::get('/payment/log/set/{imp_uid}/{action}', "PaymentController@setLog");
Route::get('/payment/log/get/{imp_uid}', "PaymentController@getLog");

Route::post("/order/checkout/meta", "OrderController@getOrderMetaData");
Route::post("/order/checkout/meta/non_user", "OrderController@getOrderMetaDataNonUser");
Route::post("/order/checkout", "OrderController@getOrder");
Route::post("/app/order/checkout_test", "OrderController@setAppPGData");
Route::get("/app/order/checkout/{meta}", "OrderController@getAppPGData");
Route::get('/order/post/search/{zip_code}', 'OrderController@getPostAddPrice');
Route::post("/order/checkout/submit", "OrderController@setOrderSubmit")->middleware("force_json");
Route::get("/order/checkout/submit/vbank/{imp_uid}", "OrderController@receiveVbankResult")->middleware("force_json");
Route::put("/order/checkout/cancel/{order_id}", "OrderController@cancelOrderSubmit");
Route::get("/order/payment/result/{order_id}","OrderController@getOrderPaymentResult");

Route::get("/order/paymentv3/result/{order_id}","OrderController@getOrderPaymentResultRenewal");
Route::post("/order/paymentv3/mobile/result","OrderController@OrderResultMobileSubmit");


Route::get('/user/address/{id}', 'UserAddressController@getAddress');
// #User Order
// 주문 상세 내역 Api
Route::get('/user/order/detail/{menu}/{orderId}/{device?}/{userId?}/{token?}', 'UserController@apiGetOrderDetails');
// // 주문 상세 내역 - 문의하기
Route::get('/user/order/cs/{detailId}/{device?}/{userId?}/{token?}', 'OrderController@apiGetOrderDetail');
// 주문 상세 내역 - 문의하기 - 주문문의
Route::post('/user/order/cs/question', 'OrderController@setCSQuestion');
Route::post('/user/order/cs/exchange', 'OrderController@setCSExchange');
Route::post('/user/order/cancel', 'OrderController@cancel');
// 주문 상세 내역 - 문의하기 - 구매취소(신청)
Route::post('/user/order/cs/cancel', 'OrderController@setCSCancel');
Route::post('/user/order/cs/refund', 'OrderController@setCSRefund');
// 주문 상세 내역 - 문의하기 - 반품(실행)
Route::post('/order/refund', 'OrderController@refund');

Route::post('/order/detail/delete/{menu}/{detail_id}', 'OrderController@setOrderDetailUnexposed');

// #Iamport
Route::post('/imp/vbank/callback', 'Lib\FPIamport@callbackReceiveVbank');

// 주문서
Route::get("/re/store/order/checkout/{meta}/{cookie?}", "StoreController@viewOrderCheckForm");
Route::get("/re/store/order/sheet/{meta}", "ViewStoreController@viewOrderSheet")->middleware('service_state');
Route::get("/re/store/order/error/{reason}", "ViewStoreController@viewOrderResultError");
Route::get("/re/store/order/result/{order_id}", "ViewStoreController@viewOrderResult");
Route::get("/re/mobile/store/order/result/{data}", "OrderController@viewOrderResultMobile");
Route::get("/remake/store/order/agree/{type}", "ViewStoreController@viewStoreOrderAgree");

Route::post('/user/order/confirm/{detail_id}', 'OrderController@setOrderConfirm');



// #CU
// #CU - REFUND
// 송금 연동 전문 3.3 송금요청
Route::post('/cu/request/refund', 'CU\CURefundController@refund');

// #Order

//주문반품
Route::post('/user/order/refund', 'OrderController@orderRefund');

// 송금 연동 전문 3.4 송금요청 확인
Route::post('/cu/request/refund/check', 'CU\CURefundController@refundCheck');
// 송금 연동 전문 3.5 송금요청 취소
Route::post('/cu/request/refund/cancel', 'CU\CURefundController@refundCancel');
// 송금내역
Route::get('/cu/request/refund/log/{cash_idx}', 'CU\CURefundController@refundLog');
// 플리팝 연동 전문 3.6 환불 확인 요청
Route::post("/request/{version}/pt/check/refund/refact", "CU\CURefundController@refundCallCheck");

// #CU - CHARGE
Route::post("/request/pt/charge/view", "CUChargeController@viewChargeBody");
Route::post("/request/pt/charge/view/response", "CUChargeController@viewChargeResponseBody");
// 플리팝 연동 전문 3.4 충전요청
Route::post("/request/{version}/pt/charge/refact/", "CU\CUChargeController@charge");
// 플리팝 연동 전문 3.5 충전 취소 요청
Route::post("/request/{version}/pt/charge/cancel/refact", "CU\CUChargeController@chargeCancel");
// 플리팝 연동 전문 3.7 충전 망취소 요청
Route::post("/request/{version}/pt/request/cancel/refact", "CU\CUChargeController@requestCancel");
// 플리팝 연동 전문 3.8 잔액 조회 요청
Route::post("/request/{version}/pt/get/balance/refact", "CU\CUController@getBalance");

// #CU - ORDER
// 플리팝 연동 전문 3.9 결제 요청
Route::post("/request/{version}/pt/payment/refact", "CU\CUOrderController@payment");
// 플리팝 연동 전문 3.10 결제 취소 요청
Route::post("/request/{version}/pt/payment/cancel/refact", "CU\CUOrderController@paymentCancel");
// 플리팝 연동 전문 3.11 결제 망취소
Route::post("/request/{version}/pt/payment/request/cancel/refact", "CU\CUOrderController@requestCancel");

// ROAS DATA INSERT
Route::post("/roas/set", "CU\CUOrderController@payment");

#endregion
