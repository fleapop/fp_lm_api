/*
#Api
function setPaymentLog(imp_uid, action)
function getImgUrl(type, image)
function apiCall(url, data, callback, http_method)
function toggleLike(table_name, table_idx, user_id)
*/

$('.barket_success').hide();
$('.barket_page').hide();
$('.purchase_success').hide();



function requestApiGET(url, callback) {
    $.ajax({
        type: "GET",
        url: url,
        success: function(result) {
          console.log(result);
            callback(result, 200, "");
        },
        error: function(xhr, status, error) {
            console.log(xhr);
            callback(false, xhr.status, xhr.responseJSON);
        }
    });
}





function loadingActivity(isShow) {
    if(isShow) {
        HoldOn.open({
            theme:"sk-rect",
            message:"<h4>잠시만 기다려주세요.</h4>"
        });
    } else {
        HoldOn.close();
    }
}

$(document).ready(function() {
    $('.drop').hide();

    // 콤마찍는 플러그인
      $.fn.digits = function(){
        return this.each(function(){
            $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") );
        })
      };
});

function dropTapMenu(){
    var drop_header = $("#drop_header");
    $('.drop').on('scroll touchmove mousewheel', function(event) {
        event.preventDefault();
        event.stopPropagation();
        return false;
    });
    $('.drop').slideToggle();
     $('.header_arrow').toggleClass('active');
     $('.header').toggleClass('active');
     $('.info').toggleClass('active');
}

function action(){
    var success = $(".call_search_btn");
}

function addComma(num) {
    var num = 1234;
    var regexp = /\B(?=(\d{3})+(?!\d))/g;
    return num.toString().replace(regexp, ',');
}

/////////////fleapop_main_header
$(function() {
    var lastScrollTop = 0,
        delta = 15;
    $(window).scroll(function(event) {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        if ((st > lastScrollTop) && (lastScrollTop > 0)) {
            $(".lm_header").css("top", "-170px");
        } else {
            $(".lm_header").css("top", "0");
        }
        lastScrollTop = st;
    });
});
/////////////market_black_header
$(function() {
    var lastScrollTop = 0,
        delta = 15;
    $(window).scroll(function(event) {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        if ((st > lastScrollTop) && (lastScrollTop > 0)) {
            $(".fleapop_main_header").css("top", "-170px");
        } else {
            $(".fleapop_main_header").css("top", "0");
        }
        lastScrollTop = st;
    });
});

/////////pc_header

$(function() {
    var lastScrollTop = 0,
        delta = 15;

    $(window).scroll(function(event) {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        if ((st < lastScrollTop) && (lastScrollTop < 0)) {
            $("#pc_header").css("top", "-130px");
        } else {
            $("#pc_header").css("top", "0px");
        }
        lastScrollTop = st;
    });
});


///////////mobile_header
$(function() {
    var lastScrollTop = 0,
        delta = 15;
    $(window).scroll(function(event) {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        if ((st > lastScrollTop) && (lastScrollTop > 0)) {
            $("#navbar").css("top", "-120px");
        } else {
            $("#navbar").css("top", "0px");
        }
        lastScrollTop = st;
    });
});


/////////////mobile_footer
$(function() {
    var lastScrollTop = 0,
        delta = 15;

    $(window).scroll(function(event) {
        var st = $(this).scrollTop();
        var body = $('body').width(); //pc버전일 때는 footer 하단 고정
        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        if ((st > lastScrollTop) && (lastScrollTop > 0) && (body < 500) ) {
            $(".footer").css("bottom", "-60px");

        } else {
            $(".footer").css("bottom", "0");

        }
        lastScrollTop = st;
    });
});


/////////////mobile_footer
$(function() {
    var lastScrollTop = 0,
        delta = 15;
    $(window).scroll(function(event) {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        if ((st > lastScrollTop) && (lastScrollTop > 0)) {
            $(".point_footer").css("bottom", "-100px");
        } else {
            $(".point_footer").css("bottom", "60px");
        }
        lastScrollTop = st;
    });
});
/////////////market_footer
$(function() {
    var lastScrollTop = 0,
        delta = 15;
    $(window).scroll(function(event) {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        if ((st > lastScrollTop) && (lastScrollTop > 0)) {
            $(".market_scroll_top").css("bottom", "-200px");
        } else {
            $(".market_scroll_top").css("bottom", "80px");
        }
        lastScrollTop = st;
    });
});


/////////////ticket_feed_footer
$(function() {
    var lastScrollTop = 0,
        delta = 15;
    $(window).scroll(function(event) {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        if ((st > lastScrollTop) && (lastScrollTop > 0)) {
            $(".edit_footer").css("bottom", "-170px");
        } else {
            $(".edit_footer").css("bottom", "80px");
        }
        lastScrollTop = st;
    });
});
/////////////ticket_feed_header
$(function() {
    var lastScrollTop = 0,
        delta = 15;
    $(window).scroll(function(event) {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        if ((st > lastScrollTop) && (lastScrollTop > 0)) {
            $(".point_header").css("top", "-170px");
        } else {
            $(".point_header").css("top", "100px");
        }
        lastScrollTop = st;
    });
});

///////////배너 들어가있는 푸터 숨기기
$(function() {
    var lastScrollTop = 0,
        delta = 15;
    $(window).scroll(function(event) {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        if ((st > lastScrollTop) && (lastScrollTop > 0)) {
            $(".app_header_banner").css("bottom", "-80px");
        } else {
            $(".app_header_banner").css("bottom", "40px");
        }
        lastScrollTop = st;
    });
});

//스토어/기획전 헤더
///////////배너 들어가있는 푸터 숨기기
$(function() {
    var lastScrollTop = 0,
        delta = 15;
    $(window).scroll(function(event) {
        var st = $(this).scrollTop();
        var body = $('body').width();
        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        if ((st > lastScrollTop) && (lastScrollTop > 0) && body <500) {
            $(".event_main_header").css("top", "-170px");
        } else {
            $(".event_main_header").css("top", "0");
        }
        lastScrollTop = st;
    });
});

//스토어/상품 상세 헤더
///////////배너 들어가있는 푸터 숨기기
$(function() {
    var lastScrollTop = 0,
        delta = 15;
    $(window).scroll(function(event) {
        var st = $(this).scrollTop();

        if (Math.abs(lastScrollTop - st) <= delta)
            return;
        if ((st > lastScrollTop) && (lastScrollTop > 0)) {
            $(".my_feed_backheader feed").css("top", "-170px");
        } else {
            $(".my_feed_backheader feed").css("top", "0");
        }
        lastScrollTop = st;
    });
});


// 드롭박스
var drop = $(".drop>ul>li").children();

drop.click(function(e) {
    $(this).addClass("active");
    $(this).siblings().removeClass("active");
});


var footer = $(".footer>.icon>a").children();

footer.click(function(e) {
    $(this).addClass("active");
    $(this).siblings().removeClass("active");
});


function setPaymentLog(imp_uid, action) {
  var url = "/api/payment/log/set/"+imp_uid+"/"+action;
  console.log("setPaymentLog:"+url);
  var callback = function (result) {

  };
  apiCall(url, null, callback, 'GET');
}


// #Api
function apiCall(url, data, callback, http_method) {
    if(data == null) {
        console.log("apiCall");
        $.ajax({
            type: http_method,
            url: url,
            success: function(result) {
                callback(result);
            },
            error: function(xhr, status, error) {
                console.log(xhr);
            }
        });

    } else {
        $.ajax({
            type: http_method,
            url: url,
            data: data,
            success: function(result) {
                callback(result);
            },
            error: function(xhr, status, error) {
                console.log(xhr);
            }
        });
    }
}

function toggleLike(table_name, table_idx, user_id) {
    var url = "/api/like/"+table_name+"/"+table_idx+"/"+user_id;
    var data = "device=101";
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        dataType: "json",
        success: function(result) {
            switch(table_name) {
                case 'seller_tbl' : {
                    if(result.is_like_state) {
                        $("#seller_heart_btn_"+table_idx).addClass("active");
                    } else {
                        $("#seller_heart_btn_"+table_idx).removeClass("active");
                    }
                    $("#seller_like_count_"+table_idx).text(result.like_count);
                }
                case 'goods' : {

                    if(result.is_like_state) {
                        $("#goods_heart_btn_"+table_idx).addClass("active");
                    } else {
                        $("#goods_heart_btn_"+table_idx).removeClass("active");
                    }
                    $("#goods_like_count_"+table_idx).text(result.like_count);
                }
            }
        },
        error: function(xhr, status, error) {
            alert('로그인이 필요한 항목입니다.');
        }
    });

}

function getImgUrl(type, image) {
    switch(type) {
        case 'SELLER_CONTENT_URL' : {
            return 'https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_contents/'+image;
        }
        case 'ON_GOODS_CONTENT' : {
            return 'https://s3.ap-northeast-2.amazonaws.com/fps3bucket/seller_on_goods/'+image;
        }
        case 'STORE_CONTENT' : {
            return 'https://fps3bucket.s3.ap-northeast-2.amazonaws.com/store_content/'+image;
        }
    }
}

//팔로우 언팔로우 액션
function followAction(followIdx) {
    var follow = $("#follow_btn_" + followIdx);
    var active = document.getElementById("followAction_" + followIdx);

    if (active.innerHTML === "팔로우") {
        follow.addClass("active");
        active.innerHTML = "언팔로우";
    } else {
        follow.removeClass("active");
        active.innerHTML = "팔로우";
    }
};
//좋아요 안좋아요 액션
function likeBtnAction(idx) {
    var like = $("#like_action_" + idx);
    var active = document.getElementById("like_btn_" + idx);

    if (active.innerHTML === "좋아요") {
        like.addClass("active");
        active.innerHTML = "좋아하기";
    } else {
        like.removeClass("active");
        active.innerHTML = "좋아요";
    }
};

//callback 함수
function firstShowAction(){
    var callback = function(result){
        $('.popup.hide').hide();
    }
    var result = firstPopup(callback);
}
function secondShowAction(){
    var callback = function(result){

    }
    var result = secondPopup(callback, cancel);
}
function thirdShowAction(){
    var callback = function(result){
        $('.popup_3.hide').hide();
    }
    var result = thirdPopup(callback);
}

function ajaxCallJson(url, data, callback) {
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: "json",
        success: function(result) {
            callback(result);
        },
        error: function(xhr, status, error) {
            console.log(xhr);
            callback(false);
        }
    });
}

function ajaxGetJson(url, data, callback) {
    $.ajax({
        type: "GET",
        url: url,
        data: data,
        dataType: "json",
        success: function(result) {
            callback(result);
        },
        error: function(xhr, status, error) {
            console.log(xhr);
            callback(false);
        }
    });
}

function ajaxCallJsonAsync(url, data, callback) {
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: "json",
        async:false,
        success: function(result) {
            callback(result);
        },
        error: function(xhr, status, error) {
            console.log(xhr);
            callback(false);
        }
    });
}

function likeCall(data, callback) {
    var url = "/api/like/set";
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function(result) {
            callback(result);
        },
        error: function(xhr, status, error) {
            console.log(xhr);
            callback(false);
        }
    });
}

function ajaxCall(url, data, callback) {
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        success: function(result) {
            callback(result);
        },
        error: function(xhr, status, error) {
            console.log(xhr);
            callback(false);
        }
    });
}

function makeFormData(data, is_null_chk) {
    if(!is_null_chk) {
        is_null_chk = false;
    }
    var form = new FormData();
    for(i in data) {
        if(is_null_chk) {
            if(data[i] == null) {
                return false;
            } else {
                if(typeof data[i] == 'object') {
                    form.append(i, JSON.stringify(data[i]));
                } else {
                    form.append(i, data[i]);
                }
            }
        } else {
            if(typeof data[i] == 'object') {
                form.append(i, JSON.stringify(data[i]));
            } else {
                form.append(i, data[i]);
            }
        }
    }

    return form;
}

function showFooter() {
    ajaxGetJson('/api/footer', null, function (result) {
        if (result) {
            $(".company_footer").append(Mustache.render($("#footers").html(), result));
        }
    });
}

function ajaxCallMulti(url, data, callback) {
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: url,
        processData: false,
        contentType: false,
        data: data,
        type: 'POST',
        success: function(result) {
            result = jQuery.parseJSON(result);
            if(result.success) {
                callback(result);
            } else {
                callback(result);
            }
        },
        error: function(xhr, status, error) {
            console.log(xhr);
            callback(false);
        }
    });
}

function historyBackAction() {
    if ( document.referrer && document.referrer.indexOf("fleapop") != -1 ) {
        console.log(document.referrer);
        history.back();
    } else {
        var serviceHost = $(location).attr('protocol')+"//"+$(location).attr('hostname')+"/store/home/store";
        location.href = serviceHost;
    }
}
